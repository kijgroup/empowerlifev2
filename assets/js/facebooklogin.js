function statusChangeCallback(response) {
	if (response.status === 'connected') {
		getFacebookInfo();
	} else if (response.status === 'not_authorized') {
		console.log('not authorized');
	} else {
		login();
	}
}
function checkLoginState() {
	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
		document.getElementById("formFacebook").submit();
	});
}
function Logout(){
	FB.logout(function(){document.location.reload();});
}
window.fbAsyncInit = function() {
	FB.init({
		appId      : '927868787290974',
		cookie     : true,  // enable cookies to allow the server to access
		xfbml      : true,  // parse social plugins on this page
		version    : 'v2.5'
	});
	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	});
};
function getFacebookInfo() {
	FB.api('/me?fields=id,name,email', function(response) {
		document.getElementById("txt_fb_id").value = response.id;
		document.getElementById("txt_fb_name").value = response.name;
		document.getElementById("txt_fb_email").value = response.email;
	});
}
function login(){
	FB.login(function(response) {
		if (response.authResponse) {
			getFacebookInfo();
		}
	}, {scope: 'email,public_profile'});
}
(function(d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
