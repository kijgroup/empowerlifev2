"use strict";

//Plaeholder handler
$(function() {

var $errorArea = $('.form-error');
$errorArea.hide(0);

var $errorHolder = $('.form-error__holder');

$('#contact-form, #contact-form-4').submit(function(e) {

		e.preventDefault();
		var error = 0;
		var self = $(this);

	    var $number = self.find('[name=number]');
      var $fullname = self.find('[name=fullname]');
      var $age = self.find('[name=age]');
      var $telephone = self.find('[name=telephone]');
      //var $province = self.find('[name=province]');
      //var $branch = self.find('[name=branch]');


		if( $number.val().length>1 &&  $number.val()!= $number.attr('placeholder')  ) {
			$number.removeClass('invalid_field');
		}
		else {
			document.getElementById("check1").checked = false;
			document.getElementById('submit').disabled=true;
			createErrTult('<strong>รหัสชิงรางวัล 9 หลัก</strong>', $number)
			error++;
		}

    if( $fullname.val().length>1 &&  $fullname.val()!= $fullname.attr('placeholder')  ) {
      $fullname.removeClass('invalid_field');
    }
    else {
			document.getElementById("check1").checked = false;
			document.getElementById('submit').disabled=true;
      createErrTult('<strong>กรุณากรอกข้อมูลให้ครบถ้วน</strong>', $fullname)
      error++;
    }

    if( $age.val().length>1 &&  $age.val()!= $age.attr('placeholder')  ) {
      $age.removeClass('invalid_field');
    }
    else {
			document.getElementById("check1").checked = false;
			document.getElementById('submit').disabled=true;
      createErrTult('<strong>กรุณากรอกข้อมูลให้ครบถ้วน</strong>', $age)
      error++;
    }

    if( $telephone.val().length>1 &&  $telephone.val()!= $telephone.attr('placeholder')  ) {
      $telephone.removeClass('invalid_field');
    }
    else {
			document.getElementById("check1").checked = false;
			document.getElementById('submit').disabled=true;
      createErrTult('<strong>กรุณากรอกข้อมูลให้ครบถ้วน</strong>', $telephone)
      error++;
    }
/*
    if( $province.val().length>1 &&  $province.val()!= $province.attr('placeholder')  ) {
      $province.removeClass('invalid_field');
    }
    else {
      createErrTult('<strong>กรุณากรอกข้อมูลให้ครบถ้วน</strong>', $province)
      error++;
    }

    if( $branch.val().length>1 &&  $branch.val()!= $branch.attr('placeholder')  ) {
      $branch.removeClass('invalid_field');
    }
    else {
      createErrTult('<strong>กรุณากรอกข้อมูลให้ครบถ้วน</strong>', $branch)
      error++;
    }
*/

		$errorArea.delay(4000).slideUp(300);

		if (error!=0)return;
		self.find('[type=submit]').attr('disabled', 'disabled');


		var formInput = self.serialize();
		$.post(self.attr('action'),formInput, function(data,status){

      if(data == "3"){
				document.getElementById("check1").checked = false;
        alert("ไม่มีรหัส 9 หลักในฐานข้อมูล");
				number.focus();
        return;

      } else if(data == "2"){
        alert("บันทึกข้อมูลเรียบร้อย");
				window.location.href='thankyou.php';

      } else if(data == "1") {
				document.getElementById("check1").checked = false;
        alert("มีข้อมูลอยู่แล้วในระบบ");
				number.focus();
        return
      }

    }); // end post
}); // end submit


// Init subscribe form here
// $('.subscribe').submit(function(e) {

// }); // end submit

$('#question-form').submit(function(e) {

		e.preventDefault();
		var error = 0;
		var self = $(this);

      var $number = self.find('[name=number]');
      var $fullname = self.find('[name=fullname]');
      var $age = self.find('[name=age]');
      var $telephone = self.find('[name=telephone]');
      //var $province = self.find('[name=province]');
      //var $branch = self.find('[name=branch]');

      if( $number.val().length>1 &&  $number.val()!= $number.attr('placeholder')  ) {
  			$number.removeClass('invalid_field');
  		}
  		else {
				document.getElementById("check1").checked = false;
				document.getElementById('submit').disabled=true;
  			createErrTult('<strong>รหัสชิงรางวัล 9 หลัก</strong>', $number)
  			error++;
  		}

      if( $fullname.val().length>1 &&  $fullname.val()!= $fullname.attr('placeholder')  ) {
        $fullname.removeClass('invalid_field');
      }
      else {
				document.getElementById("check1").checked = false;
				document.getElementById('submit').disabled=true;
        createErrTult('<strong>กรุณากรอกข้อมูลให้ครบถ้วน</strong>', $fullname)
        error++;
      }

      if( $age.val().length>1 &&  $age.val()!= $age.attr('placeholder')  ) {
        $age.removeClass('invalid_field');
      }
      else {
				document.getElementById("check1").checked = false;
				document.getElementById('submit').disabled=true;
        createErrTult('<strong>กรุณากรอกข้อมูลให้ครบถ้วน</strong>', $age)
        error++;
      }

      if( $telephone.val().length>1 &&  $telephone.val()!= $telephone.attr('placeholder')  ) {
        $telephone.removeClass('invalid_field');
      }
      else {
				document.getElementById("check1").checked = false;
				document.getElementById('submit').disabled=true;
        createErrTult('<strong>กรุณากรอกข้อมูลให้ครบถ้วน</strong>', $telephone)
        error++;
      }
/*
      if( $province.val().length>1 &&  $province.val()!= $province.attr('placeholder')  ) {
        $province.removeClass('invalid_field');
      }
      else {
        createErrTult('<strong>กรุณากรอกข้อมูลให้ครบถ้วน</strong>', $province)
        error++;
      }

      if( $branch.val().length>1 &&  $branch.val()!= $branch.attr('placeholder')  ) {
        $branch.removeClass('invalid_field');
      }
      else {
        createErrTult('<strong>กรุณากรอกข้อมูลให้ครบถ้วน</strong>', $branch)
        error++;
      }
*/
		$errorArea.delay(4000).slideUp(300);


		if (error!=0)return;
		self.find('[type=submit]').attr('disabled', 'disabled');
		window.location.href='thankyou.php';


		var formInput = self.serialize();
		$.post(self.attr('action'),formInput, function(data){}); // end post
}); // end submit


function createErrTult(text, $elem){
			$errorArea.show(0)

			$elem.focus();
			$('<p />', {
				'class':'inv-em',
				'html': text,
			})
			.appendTo($elem.addClass('invalid_field').parent())
			.insertBefore($errorHolder)
			.delay(4000).animate({'opacity':0},300, function(){ $(this).slideUp(400,function(){ $(this).remove() }) });
	}
});
