var $allVideos = false;
$(document).ready(function(){
  init_subscribe();
  init_youtube();
});
function init_subscribe(){
  //$('#modal-subscribe').modal('show');
  var subscribe_form = $('#subscribe_form');
  var success_msg = subscribe_form.data('successmsg');
  subscribe_form.on("submit", function (ev) {
    $.ajax({
      type: subscribe_form.attr('method'),
      url: subscribe_form.attr('action'),
      data: subscribe_form.serialize(),
      success: function (data) {
        var n = noty({
          text: success_msg,
          theme: 'metroui', // or relax
          type: 'success',
          layout: 'center',
          timeout: 4000, // [integer|boolean] delay for closing event in milliseconds. Set false for sticky notifications
          progressBar: true, // [boolean] - displays a progress bar
        });
        $('.subscribe_email ').val('');
      }
    });
    ev.preventDefault();
  });
}
function init_youtube(){
  $allVideos = $("iframe");
  $allVideos.each(function() {
    $(this)
      .data('aspectRatio', this.height / this.width)
      .data('defaultWidth', this.width)
      // and remove the hard coded width/height
      .removeAttr('height')
      .removeAttr('width');
  });
  // When the window is resized
  $(window).resize(function() {
    // Resize all videos according to their own aspect ratio
    $allVideos.each(function() {
      var $el = $(this);
      $el.width('100%');
      var newWidth = $el.width();
      var maxWidth = $el.data('defaultWidth');
      newWidth = (newWidth<maxWidth)?newWidth:maxWidth;
      $el
        .width(newWidth)
        .height(newWidth * $el.data('aspectRatio'));
    });
  // Kick off one resize to fix all videos on page load
  }).resize();
}
