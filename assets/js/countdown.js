$(document).ready(function(){
console.log('call before interval');
  setInterval(function(){
    var now = new Date().getTime();
    var lang = $('#lang').val();
    $('.promotion_countdown').each(function(index){
      var countDownDate = new Date($(this).data('expire')).getTime();
      // Find the distance between now an the count down date
      var distance = countDownDate - now;
      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Display the result in the element with id="demo"
      str_date = '';
      if(days > 0){
        str_date += days;
        str_date += (lang == "th")?" วัน ":"days ";
      }else{
        if(hours > 0){
          str_date += hours;
          str_date += (lang == "th")?" ชม. ":"h ";
        }else{
          if(minutes > 0){
            str_date += minutes;
            str_date += (lang == "th")?" นาที ":"m ";
          }
          if(seconds > 0){
            str_date += seconds;
            str_date += (lang == "th")?" วินาที ":"s ";
          }
        }
      }
      $(this).html(str_date);
      if (distance < 0) {
        location.reload();
      }
    });
  }, 1000);
});
