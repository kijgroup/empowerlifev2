$(document).ready(function(){
  checkNext();
  $('#is_tax').on('click', check_display_tax);
  check_display_tax();
  $('#same_address').on('click', check_show_bill_addres);
  check_show_bill_addres();
  $('input[name=payment_type]').on('change', check_display_bank_detail);
  check_display_bank_detail();
  $('.discount_point').on('change paste keyup', on_change_discount_point);
  on_change_discount_point();
  prevent_enter_submit();
});
function prevent_enter_submit(){
  $(function(){
    var keyStop = {
      8: ":not(input:text, textarea, input:file, input:password)", // stop backspace = back
      13: "input:text, input:password", // stop enter = submit
      end: null
    };
    $(document).bind("keydown", function(event){
     var selector = keyStop[event.which];
   
     if(selector !== undefined && $(event.target).is(selector)) {
         event.preventDefault(); //stop event
     }
     return true;
    });
   });
}
function check_display_tax(){
  if(document.getElementById('is_tax').checked){
    $('#display-tax').show();
  }else{
    $('#display-tax').hide();
  }
}
function check_show_bill_addres(){
  if(document.getElementById('same_address').checked){
    $('#display-tax-address').hide();
  }else{
    $('#display-tax-address').show();
  }
}
function check_display_bank_detail(){
  var payment_type = $('input[name=payment_type]:checked').val();
  if(payment_type == 'bank'){
    $('#bank-info').slideDown();
  }else{
    $('#bank-info').slideUp();
  }
  if(payment_type == 'post'){
    $('#post-info').slideDown();
  }else{
    $('#post-info').slideUp();
  }
}
function on_change_discount_point(){
  $total_point_use = 0;
  $total_discount_get = 0;
  $('.discount_point').each(function(){
    $discount_amount = parseInt($(this).val());
    $(this).val($discount_amount);
    if($discount_amount < 0){
      $(this).val(0);
      $discount_amount = 0;
    }
    $total_point_use += $(this).data('use_point') * $discount_amount;
    $total_discount_get += $(this).data('discount_amount') * $discount_amount;
  });
  $('#discount_point_amount').val(parseFloat($total_point_use));
  $('#discount_by_point_amount').val(parseFloat($total_discount_get));
  $('.discount_point_amount').html(parseFloat($total_point_use));
  $('.discount_by_point_amount').html(parseFloat($total_discount_get));
  
  $item_price = parseFloat($('#item_price').val());
  $discount_rate = parseFloat($('#discount_rate').val());
  $remain_price = $item_price - $total_discount_get;
  $member_discount = parseFloat(0);
  if($remain_price > 0){
    $member_discount = parseFloat($remain_price * $discount_rate / 100);
  }
  $shipping_price = parseFloat($('#shipping_price').val());
  $paid_amount = $remain_price - $member_discount + $shipping_price;
  $('.member_discount').html(numberWithCommas($member_discount));
  $('.paid_amount').html(numberWithCommas($paid_amount));
}
function numberWithCommas(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return parts.join(".");
}
