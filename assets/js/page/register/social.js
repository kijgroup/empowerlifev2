$(document).ready(function() {
  $("#register-form").submit(function(event) {
    event.preventDefault();
    var action = $(this).attr("action");
    send_form(action);
  });
});
function send_form(action){
  var posting = $.post(action, $("#register-form").serialize());
  posting.done(function(data) {
    if(!data.resp_status){
      $('#alert_register').show();
      $('#alert_register_msg').html(data.resp_msg);
    }else{
      $('#alert_register').hide();
      window.location.replace(data.resp_msg);
    }
  });
  posting.fail(function(data){
    setTimeout(function() { send_form(action); }, 5000);
  });
}
