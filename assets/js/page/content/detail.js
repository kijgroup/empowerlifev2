$(document).ready(function(){
  $('.bxslider').bxSlider({
    mode: 'fade',
    adaptiveHeight: true,
    pagerCustom: '#bx-pager',
    captions: false
  });
  $('#bx-pager').owlCarousel({
    loop:true,
    margin:10,
    nav:true,
    responsive:{
      0:{
        items:2
      },
      600:{
        items:4
      },
      1000:{
        items:6
      }
    }
  });
});
