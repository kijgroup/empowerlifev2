$(document).ready(function(){
  selectBox();
  $('#content_group').on('change', function(){
    var page_url = $('#site_url').val()+'/content/index/';
    var category_group_slug = $(this).val();
    window.location.href = page_url+category_group_slug;
  });
});
