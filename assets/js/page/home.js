$(document).ready(function(){
  if (sessionStorage.getItem('whenToShowDialog') == null) {
    sessionStorage.setItem('whenToShowDialog', 'yes');
    showPopup();
  }
  $('body').show();
  revAlternative();
  //productSlider();
  scrollSlider();
  initCheckHashSubscribe();
  setTimeout(function(){initCheckHashSubscribe();}, 100);
  setTimeout(function(){initCheckHashSubscribe();}, 200);
  setTimeout(function(){initCheckHashSubscribe();}, 300);
});

function initCheckHashSubscribe(){
  if(window.location.hash == '#subscribeus' || window.location.hash == '#subscribe'){
    $('html body').scrollTop($("#subscribeus").offset().top);
    $('#email_newsletter').focus();
  }
}
function showPopup(){
  var dt = new Date();
  var site_url = $('#site_url').val();
  var uniqueStr = "?v="+dt.getUTCFullYear()+dt.getMonth()+dt.getDate()+dt.getHours()+dt.getMinutes()+dt.getSeconds();
	$.getJSON(site_url+"/home/popup/"+uniqueStr, function(json) {
	    console.log(json);
	    if(json.popup_status == 'show'){
	    	var popupStr = '<div style="max-width:800px;"><img src="'+json.upload_file+'" alt="popup" style="max-width:100%;"></div>';
	    	if(json.redirect_url != ''){
	    		popupStr = '<a href="'+json.redirect_url+'" target="blank">'+popupStr+'</a>';
	    	}
	    	var loadImage = new Image();
        	loadImage.src = json.upload_file;
        	loadImage.onload=function(){
            $.fancybox.open(
              popupStr,
              {
                'padding' : 10,
              }
            );
	        }
	    }
	});
}
