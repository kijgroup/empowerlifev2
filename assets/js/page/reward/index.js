$(document).ready(function(){
  qNumber();
  $('.qty-reward').on('input change paste keyup', update_display_point);
  $('.quantity__elem').on('click', update_display_point);
  add_event_submit_form();
});
function calc_point(){
  var member_point = parseInt($('#member_point').val());
  var use_point = 0;
  var reward_qty = 0;
  $('#reward_item_list').html('');
  $('.qty-reward').each(function(){
    var item_qty = parseInt(this.value);
    var item_point = parseInt($(this).data('point')) * item_qty;
    reward_qty += item_qty;
    use_point += item_point;
    if(item_qty > 0){
      $('#reward_item_list').append('<tr><td style="padding-left:0">'+$(this).data('reward_name')+'</td><td style="padding:0 10px;" class="text-center">'+item_qty+'</td><td style="padding-left:0" class="checkout__result text-right">'+item_point+' P</td></tr>');
    }
  });
  var remain_point = member_point - use_point;
  return [use_point, remain_point, reward_qty];
}
function update_display_point(){
  var calc_result = calc_point();
  var use_point = calc_result[0];
  var remain_point = calc_result[1];
  var reward_qty = calc_result[2];
  $('#use_point').html(use_point);
  $('#remain_point').html(remain_point);
  $('#reward_qty').html(reward_qty);
  if(remain_point < 0){
    $('#remain_point').css("color", "red");
  }else{
    $('#remain_point').css("color", "");
  }
}
function add_event_submit_form(){
  $("#reward-form").off('submit').on('submit', function(event) {
    var calc_result = calc_point();
    var use_point = calc_result[0];
    var remain_point = calc_result[1];
    var reward_qty = calc_result[2];
    if(use_point == 0){
      display_error('โปรดระบุของรางวัลที่คุณต้องการแลกแต้ม', '');
      return false;
    }
    if(remain_point < 0){
      display_error('คุณใช้แต้มแลกของรางวัลมากกว่าที่คุณมี', '');
      return false;
    }
    if($('#shipping_name').val() == ''){
      display_error('โปรดระบุชื่อผู้รับ', '#shipping_name');
      return false;
    }
    if($('#shipping_address').val() == ''){
      display_error('โปรดระบุที่อยู่ผู้รับ', '#shipping_address');
      return false;
    }
    if($('#shipping_district').val() == ''){
      display_error('โปรดระบุเขต/อำเภอ', '#shipping_district');
      return false;
    }
    if($('#shipping_province').val() == ''){
      display_error('โปรดระบุจังหวัด', '#shipping_province');
      return false;
    }
    if($('#shipping_post_code').val() == ''){
      display_error('โปรดระบุรหัสไปรษณีย์', '#shipping_post_code');
      return false;
    }
    return true;
  });
}
function display_error(error_message, field_id){
  console.log('display error: ' + error_message);
  $('#form-error-msg').html(error_message);
  $('#form-error').show();
  if(field_id == ''){
    $('html, body').animate({
      scrollTop: $("#form-error").offset().top - 150
    }, 300);
  }else{
    $(field_id).focus();
  }
  event.preventDefault();
}