$(document).ready(function(){
  add_event_toggle_slide();
});
function add_event_toggle_slide(){
  $('.toggle_anchor').on('click', function(){
    var target = $(this).data('target');
    $(target).slideToggle(300);
  });
}