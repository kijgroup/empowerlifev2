$(document).ready(function() {
  $("#login-form").submit(function(event) {
    event.preventDefault();
    var action = $(this).attr("action");
    var member_email = $('#member_email').val();
    var member_password = $('#member_password').val();
    var check = $('#check').val();
    send_form(action, member_email, member_password, check);
  });
});
function send_form(action, member_email, member_password, check){
  var posting = $.post(action, {
    email: member_email,
    login_password: member_password,
    check: check
  });
  posting.done(function(data) {
    if(!data.resp_status){
      $('#alert_login').show();
      $('#alert_login_msg').html(data.resp_msg);
    }else{
      $('#alert_login').hide();
      window.location.replace(data.resp_msg);
    }
  });
  posting.fail(function(data){
    setTimeout(function() { send_form(action, member_email, member_password, check); }, 5000);
  });
}
