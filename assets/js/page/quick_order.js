$(document).ready(function(){
  $('.product_qty').on('change keyup paste mouseup', function(){
    var product_id = $(this).data('product_id');
    var total_price = $(this).val() * $(this).data('price');
    $('#total_'+product_id).html(total_price);
  });
});