<?php
class Import_member extends CI_Controller{
  public function import(){
    set_time_limit(300);
    $this->load->model(array('Excel_reader_service'));
    $obj_excel = $this->Excel_reader_service->get_xlsx_obj('20170901_member_list_import.xlsx');
    $obj_excel->setActiveSheetIndex(0);
    $sheet_data = $obj_excel->getActiveSheet()->toArray(null,true,true,true);
    $this->insert_member_list($sheet_data, 'normal');
    $obj_excel->setActiveSheetIndex(1);
    $sheet_data = $obj_excel->getActiveSheet()->toArray(null,true,true,true);
    $this->insert_member_list($sheet_data, 'silver');
    echo 'complete!';
  }
  private function insert_member_list($sheet_data, $member_type){
    $member_list = array();
    $qty = count($sheet_data) - 1;
    for($i = 2; $i<$qty + 2; $i++){
      if($sheet_data[$i]['A'] != ''){
        if($this->check_dup_member_code($sheet_data[$i]['A'])){
          continue;
        }
        list($firstname, $lastname) = $this->split_name(trim($sheet_data[$i]['B']));
        $birthdate = $this->format_date($sheet_data[$i]['F']);
        $buy_total = intval(str_replace(",","",$sheet_data[$i]['S'])) + intval(str_replace(",","",$sheet_data[$i]['T']));
        $member_list[] = array(
          'member_code' => $sheet_data[$i]['A'],
          'member_type' => $member_type,
          'expire_date' => '0000-00-00',
          'buy_total' => $buy_total,
          'member_point' => str_replace(",","",$sheet_data[$i]['V']),
          'member_firstname' => $firstname,
          'member_lastname' => $lastname,
          'member_address' => $sheet_data[$i]['C'],
          'member_postcode' => $sheet_data[$i]['D'],
          'member_mobile' => $sheet_data[$i]['E'],
          'member_birthdate' => $birthdate,
          'enable_status' => 'active',
          'create_date' => date('Y-m-d h:i:s'),
          'create_by' => $this->session->userdata('admin_id')
        );
      }
    }
    if(count($member_list) > 0){
      $this->db->insert_batch('tbl_member', $member_list);
    }
  }
  private function check_dup_member_code($member_code){
    $this->db->where('member_code', trim($member_code));
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_member');
    return ($query->num_rows() > 0);
  }
  private function split_name($name){
    if(trim($name) == ''){
      return array('','');
    }
    $name = str_replace('  ',' ', $name);
    $arr_name = explode(" ", $name);
    $lastname = '';
    if(count($arr_name) == 1){
      $firstname = trim($name);
    }else{
      $lastname = array_pop($arr_name);
      $firstname = implode(" ",$arr_name);
    }
    return array($firstname,$lastname);
  }
  private function format_date($date){
    if($date == ''){
      return '0000-00-00';
    }
    list($day, $month, $year_input) = explode("/", $date);
    $year_input = intval($year_input);
    if($year_input > 2000){
      $year = $year_input-543;
    }else{
      $year = $year_input;
    }
    return $year.'-'.$month.'-'.$day;
  }
}
