<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Changepassword extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('admin/Admin_model'));
    }
    public function index() {
        $this->Login_service->must_login();
        $data_content = array();
        $this->session->userdata('admin_id');
        $data_content['admin'] = $this->Admin_model->get_data($this->session->userdata('admin_id'));
        $title = $data_content['title'] = 'เปลี่ยนรหัสผ่าน';
        $content = $this->load->view('changepassword/index', $data_content, TRUE);
        $this->Masterpage_service->display($content, $title, 'changepassword');
    }

    public function form_post() {
        $this->Login_service->must_login();
        $password = $this->input->post('txtPassword');
        $this->Admin_model->update_password($this->session->userdata('admin_id'),$password);
        $this->Login_service->update_session();
        redirect('changepassword/success');
    }

    public function success() {
        $this->Login_service->must_login();
        $data_content = array();
        $title = $data_content['title'] = 'เปลี่ยนรหัสผ่าน';
        $content = $this->load->view('changepassword/success', $data_content, TRUE);
        $this->Masterpage_service->display($content, $title, 'changepassword');
    }
}
