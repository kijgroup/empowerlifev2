<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forgotpassword extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('forgotpassword/ForgotpasswordModel'));
    }
    public function index() {
        $this->LoginService->mustNotLogin();
        $data = array();
        $data['title'] = 'ลืมรหัสผ่าน';
        $this->load->view('forgotpassword/index', $data);
    }
    public function form_post() {
        $this->LoginService->mustNotLogin();
        $loginName = $this->input->post('login_name');
        $this->ForgotpasswordModel->forgotpassword($loginName);
        redirect('forgotpassword/success');
    }
    public function success(){
        $this->LoginService->mustNotLogin();
        $data = array();
        $data['title'] = 'ส่งอีเมลในการเปลี่ยนรหัสผ่านแล้ว';
        $this->load->view('forgotpassword/success', $data);
    }
    public function changepassword(){
        $this->LoginService->mustNotLogin();
        $data['title'] = 'เปลี่ยนรหัสผ่าน';
        $this->load->view('forgotpassword/changepassword', $data);
    }
    public function changepassword_post(){
        //
    }
    public function changepassword_success(){
        $this->LoginService->mustNotLogin();
        $data = array();
        $data['title'] = 'เปลี่ยนรหัสผ่านเสร็จสิ้น';
        $this->load->view('forgotpassword/changepassword_success', $data);
    }
}