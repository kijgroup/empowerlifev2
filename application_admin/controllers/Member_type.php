<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Member_type extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('member/Member_type_model'));
  }
  public function index() {
    $this->Login_service->must_login();
    $content = $this->load->view('member_type/list', array(
      'member_type_list' => $this->Member_type_model->get_list()
    ), TRUE);
    $this->Masterpage_service->display($content, 'ประเภทสมาชิก', 'member_type');
  }
  public function form($member_type_code){
    $this->Login_service->must_login();
    $title = 'แก้ไขประเภทสมาชิก';
    $content = $this->load->view('member_type/form', array(
      'member_type_code' => $member_type_code,
      'member_type' => $this->Member_type_model->get_data($member_type_code),
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->display($content, $title, 'member_type');
  }

  public function form_post($member_type_code){
    $this->Login_service->must_login();
    $columns = array('member_type_name', 'require_amount', 'convert_point', 'discount', 'min_amount');
    $data = array();
    foreach($columns as $column){
      $data[$column] = $this->input->post($column);
    }
    $data['member_type_image'] = '';
    $this->load->model(array('Upload_image_model'));
    $image_data = $this->Upload_image_model->upload_single_image('member_type','member_type_image');
    if($image_data['success']){
      $data['member_type_image'] = $image_data['image_file'];
    }
    $this->Member_type_model->update($member_type_code, $data);
    redirect('member_type');
  }
}
