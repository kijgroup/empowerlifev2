<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content_contact extends CI_Controller {

  function __construct() {
    parent::__construct();
    $this->load->model(array(
      'content_contact/Content_contact_model',
      'content/Content_model'
    ));
  }

  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('content_contact/list', array(), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/content_contact/list.js');
    $this->Masterpage_service->display($content, 'เบอร์ติดต่อกลับ', 'content_contact');
  }
  
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('searchVal');
    $content_id = $this->input->post('content_id');
    $page = $this->input->post('page');
    $perPage = $this->input->post('per_page');
    $this->load->model('content_contact/Content_contact_filter_service');
    $data = $this->Content_contact_filter_service->get_data_content(
      $search_val,
      $content_id,
      $page,
      $perPage
    );
    $this->load->view('content_contact/list/content', $data);
  }

  public function form($content_contact_id = 0){
    $this->Login_service->must_login();
    $title = (($content_contact_id == 0)?'เพิ่ม':'แก้ไข').'เบอร์ติดต่อกลับ';
    $content_contact = $this->Content_contact_model->get_data($content_contact_id);
    $content = $this->load->view('content_contact/form', array(
      'content_contact_id' => $content_contact_id,
      'content_contact' => $content_contact,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/content_contact/form.js?v=2');
    $this->Masterpage_service->display($content, $title, 'content_contact');
  }

  public function form_post($content_contact_id){
    $this->Login_service->must_login();
    $data = array(
      'name' => $this->input->post('name'),
      'mobile' => $this->input->post('mobile'),
      'remark' => $this->input->post('remark'),
    );
    if($content_contact_id == 0){
      $content_contact_id = $this->Content_contact_model->insert($data);
    }else{
      $this->Content_contact_model->update($content_contact_id, $data);
    }
    redirect('content_contact');
  }

  public function detail($content_contact_id){
    $this->Login_service->must_login();
    $content_contact = $this->Content_contact_model->get_data($content_contact_id);
    $data_content = array();
    $data_content['content_contact_id'] = $content_contact_id;
    $data_content['content_contact'] = $content_contact;
    $title = 'ข้อมูลเบอร์ติดต่อกลับ';
    $data_content['title'] = $title;
    $content = $this->load->view('content_contact/detail', $data_content, TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/content_contact/detail.js');
    $this->Masterpage_service->display($content, $title, 'content_contact');
  }

  public function delete($content_contact_id){
    $this->Login_service->must_login();
    $this->Content_contact_model->delete($content_contact_id);
    redirect('content_contact/');
  }
}
