<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Reward_transaction extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model(array(
      'reward/Reward_model',
      'reward_transaction/Reward_transaction_model',
      'reward_transaction/Reward_transaction_item_model',
      'reward_transaction/Reward_transaction_status_model',
      'member/Member_model',
    ));
  }
  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('reward_transaction/list/index', array(), true);
    $this->Masterpage_service->add_js('/assets_admin/js/reward_transaction/list.js');
    $this->Masterpage_service->display($content, 'Reward Transaction', 'reward_transaction');
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('search_val');
    $reward_transaction_status = $this->input->post('reward_transaction_status');
    $page = $this->input->post('page');
    $per_page = $this->input->post('per_page');
    $this->load->model('reward_transaction/Reward_transaction_filter_service');
    $data = $this->Reward_transaction_filter_service->get_data_content(
      $search_val,
      $reward_transaction_status,
      $page,
      $per_page);
    $this->load->view('reward_transaction/list/content', $data);
  }
  public function detail($reward_transaction_id){
    $this->Login_service->must_login();
    $reward_transaction = $this->Reward_transaction_model->get_data($reward_transaction_id);
    $reward_transaction_item_list = $this->Reward_transaction_item_model->get_list($reward_transaction_id);
    $title = 'รายละเอียดการแลกของรางวัล';
    $content = $this->load->view('reward_transaction/detail/index', array(
      'reward_transaction_id' => $reward_transaction_id,
      'reward_transaction' => $reward_transaction,
      'reward_transaction_item_list' => $reward_transaction_item_list,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/reward_transaction/detail.js');
    $this->Masterpage_service->display($content, $title, 'reward_transaction');
  }
  public function print_pdf($reward_transaction_id){
    $this->Login_service->must_login();
    $reward_transaction = $this->Reward_transaction_model->get_data($reward_transaction_id);
    $reward_transaction_item_list = $this->Reward_transaction_item_model->get_list($reward_transaction_id);
    $html = $this->load->view('reward_transaction/pdf/index', array(
      'reward_transaction_id' => $reward_transaction_id,
      'reward_transaction' => $reward_transaction,
      'reward_transaction_item_list' => $reward_transaction_item_list
    ), true);
    $stylesheet = file_get_contents(FCPATH.'assets_admin/css/pdf.css');
    $file_name = 'reward_transaction'.$reward_transaction_id;
    $this->load->model(array('Pdf_service'));
    //echo $html;
    $this->Pdf_service->view_pdf_file($html, $stylesheet, '', '', $file_name);
  }
  public function pdf_list(){
    $this->Login_service->must_login();
    $id_list = $this->input->post('reward_transaction_id');
    if($id_list == NULL){
      echo '<h1>ไม่มีรายการที่ถูกเลือก</h1>';
      exit();
    }
    $html = '';
    foreach($id_list as $index => $reward_transaction_id){
      $reward_transaction = $this->Reward_transaction_model->get_data($reward_transaction_id);
      $reward_transaction_item_list = $this->Reward_transaction_item_model->get_list($reward_transaction_id);
      if($index > 0){
        $html .= '<pagebreak>';
      }
      $html .= $this->load->view('reward_transaction/pdf/index', array(
        'reward_transaction_id' => $reward_transaction_id,
        'reward_transaction' => $reward_transaction,
        'reward_transaction_item_list' => $reward_transaction_item_list
      ), true);
    }
    $stylesheet = file_get_contents(FCPATH.'assets_admin/css/pdf.css');
    $footer = '';
    $file_name = 'reward_transaction.pdf';
    $this->load->model(array('Pdf_service'));
    $this->Pdf_service->view_pdf_file($html, $stylesheet, '', $footer, $file_name);
  }
  public function form($reward_transaction_id = 0){
    $this->Login_service->must_login();
    $title = (($reward_transaction_id == 0)?'เพิ่ม':'แก้ไข').'รายการแลกของรางวัล';
    $reward_transaction = $this->Reward_transaction_model->get_data($reward_transaction_id);
    $reward_transaction_item_list = $this->Reward_transaction_item_model->get_list($reward_transaction_id);
    $content = $this->load->view('reward_transaction/form/index', array(
      'reward_transaction_id' => $reward_transaction_id,
      'reward_transaction' => $reward_transaction,
      'reward_transaction_item_list' => $reward_transaction_item_list,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_css('assets_admin/vendor/select2/css/select2.css');
    $this->Masterpage_service->add_css('assets_admin/vendor/select2-bootstrap-theme/select2-bootstrap.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/select2/js/select2.js');
    $this->Masterpage_service->add_js('assets_admin/js/address/popup.js');
    $this->Masterpage_service->add_js('assets_admin/js/reward_transaction/form.js');
    $this->Masterpage_service->display($content, $title, 'reward_transaction');
  }
  public function form_post($reward_transaction_id){
    $this->Login_service->must_login();
    $field_list = array('member_id', 'reward_transaction_status', 'shipping_name', 'shipping_mobile', 'shipping_address', 'shipping_district', 'shipping_amphur', 'shipping_province', 'shipping_post_code','note');
    $data = array();
    foreach($field_list as $field_name){
      $data[$field_name] = $this->input->post($field_name);
    }
    $reward_transaction = false;
    if($reward_transaction_id == 0){
      $reward_transaction_id = $this->Reward_transaction_model->insert($data);
    }else{
      $reward_transaction = $this->Reward_transaction_model->get_data($reward_transaction_id);
      $this->Reward_transaction_model->update($reward_transaction_id, $data);
      $this->Reward_transaction_item_model->delete($reward_transaction_id);
    }
    $reward_id_list = $this->input->post('reward_id');
    $qty_list = $this->input->post('qty');
    $reward_point_list = $this->input->post('reward_point');
    $reward_total_item_point = 0;
    foreach($reward_id_list as $index => $reward_id){
      if($qty_list[$index] > 0){
        $total_reward_point = $reward_point_list[$index] * $qty_list[$index];
        $reward_total_item_point += $total_reward_point;
        $reward = $this->Reward_model->get_data($reward_id);
        $this->Reward_transaction_item_model->insert(array(
          'reward_transaction_id' => $reward_transaction_id,
          'reward_id' => $reward_id,
          'reward_name_en' => $reward->name_en,
          'reward_name_th' => $reward->name_th,
          'reward_point' => $reward_point_list[$index],
          'qty' => $qty_list[$index],
          'total_reward_point' => $total_reward_point
        ));
      }
    }
    $this->Reward_transaction_model->update_point($reward_transaction_id, $reward_total_item_point);
    if($reward_transaction){
      $this->Member_model->add_point($reward_transaction->member_id, $reward_transaction->total_reward_point);
    }
    $this->Member_model->add_point($data['member_id'], -$reward_total_item_point);
    redirect('reward_transaction/detail/'.$reward_transaction_id);
  }
  public function delete($reward_transaction_id){
    $this->Login_service->must_login();
    $reward_transaction = $this->Reward_transaction_model->get_data($reward_transaction_id);
    if($reward_transaction){
      $this->Member_model->add_point($reward_transaction->member_id, $reward_transaction->total_reward_point);
      $this->Reward_transaction_model->delete($reward_transaction_id);
      $this->Reward_transaction_item_model->delete($reward_transaction_id);
    }
    redirect('reward_transaction');
  }
}
