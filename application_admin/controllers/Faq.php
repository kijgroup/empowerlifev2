<?php
class Faq extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model(array('faq/Faq_model', 'faq/Faq_group_model'));
  }
  public function index(){
    $this->Login_service->must_login();
    $faq_list = $this->Faq_model->get_list(false);
    $content = $this->load->view('faq/list', array(
      'faq_list' => $faq_list
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/faq/list.js');
    $this->Masterpage_service->display($content, 'FAQ', 'faq');
  }
  public function form($faq_id = 0){
    $this->Login_service->must_login();
    $title = (($faq_id == 0)?'เพิ่ม':'แก้ไข').'ถาม-ตอบ';
    $faq = $this->Faq_model->get_data($faq_id);
    $content = $this->load->view('faq/form', array(
      'faq_id' => $faq_id,
      'faq' => $faq,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_css('assets_admin/vendor/summernote/summernote.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/summernote/summernote.min.js');
    $this->Masterpage_service->add_js('assets_admin/js/faq/form.js');
    $this->Masterpage_service->display($content, $title, 'faq');
  }
  public function form_post($faq_id){
    $this->Login_service->must_login();
    $data = array(
      'faq_group_id' => $this->input->post('faq_group_id'),
      'faq_question_th' => $this->input->post('faq_question_th'),
      'faq_question_en' => $this->input->post('faq_question_en'),
      'faq_answer_th' => $this->input->post('faq_answer_th'),
      'faq_answer_en' => $this->input->post('faq_answer_en'),
      'enable_status' => $this->input->post('enable_status'),
      'sort_priority' => $this->input->post('sort_priority')
    );
    if($faq_id == 0){
      $faq_id = $this->Faq_model->insert($data);
    }else{
      $this->Faq_model->update($faq_id, $data);
    }
    redirect('faq');
  }

  public function upload_image(){
    $content_image = '';
    $this->load->model('Upload_image_model');
    $image_data = $this->Upload_image_model->upload_single_image('faq_detail', 'file');
    if($image_data['success']){
      $content_image = $image_data['image_file'];
    }
    echo base_url('uploads/'.$content_image);
  }

  public function detail($faq_id){
    $this->Login_service->must_login();
    $faq = $this->Faq_model->get_data($faq_id);
    $title = $faq->faq_question_th;
    $content = $this->load->view('faq/detail', array(
      'faq_id' => $faq_id,
      'faq' => $faq,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/faq/detail.js');
    $this->Masterpage_service->display($content, $title, 'faq');
  }

  public function delete($faq_id){
    $this->Login_service->must_login();
    $this->Faq_model->delete($faq_id);
    redirect('faq/');
  }

}
