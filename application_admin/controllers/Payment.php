<?php
class Payment extends CI_Controller{
  function __construct() {
    parent::__construct();
    $this->load->model(array('bank/Bank_model', 'payment/Payment_model'));
  }
  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('payment/list', array(), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/payment/list.js');
    $this->Masterpage_service->display($content, 'รายการแจ้งชำระเงิน', 'payment');
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('searchVal');
    $start_date = $this->input->post('startDate');
    $end_date = $this->input->post('endDate');
    $page = $this->input->post('page');
    $per_page = $this->input->post('per_page');
    $this->load->model('payment/Payment_filter_service');
    $data = $this->Payment_filter_service->get_data_content(
      $search_val,
      $start_date,
      $end_date,
      $page,
      $per_page);
    $this->load->view('payment/listcontent', $data);
  }
  public function form(){
    //
  }
  public function form_post(){
    //
  }
  public function detail($bank_payment_id){
    $this->Login_service->must_login();
    $payment = $this->Payment_model->get_data($bank_payment_id);
    $dataContent = array();
    $dataContent['bank_payment_id'] = $bank_payment_id;
    $dataContent['payment'] = $payment;
    $title = 'รายละเอียดการแจ้งชำระ';
    $dataContent['title'] = $title;
    $content = $this->load->view('payment/detail', $dataContent, TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/payment/detail.js');
    $this->Masterpage_service->display($content, $title, 'payment');
  }
  public function delete($bank_payment_id){
    $this->Login_service->must_login();
    $this->Payment_model->delete($bank_payment_id);
    redirect('payment/');
  }
  
}
