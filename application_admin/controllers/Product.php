<?php
class Product extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model(array(
      'product/Product_model',
      'category/Category_model',
      'product/Product_image_model',
      'product/Product_relate_model',
      'product/Enable_status_model',
    ));
  }
  public function index(){
    $this->Login_service->must_login();
    $category_list = $this->Category_model->get_list(false);
    $content = $this->load->view('product/list', array(
      'category_list' => $category_list
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/product/list.js');
    $this->Masterpage_service->display($content, 'สินค้า', 'product');
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('search_val');
    $category_id = $this->input->post('category_id');
    $enable_status = $this->input->post('enable_status');
    $page = $this->input->post('page');
    $per_page = $this->input->post('per_page');
    $this->load->model('product/Product_filter_service');
    $data = $this->Product_filter_service->get_data_content(
      $search_val,
      $category_id,
      $enable_status,
      $page,
      $per_page);
    $this->load->view('product/list/content', $data);
  }
  public function form($product_id = 0){
    $this->Login_service->must_login();
    $title = (($product_id == 0)?'เพิ่ม':'แก้ไข').'สินค้า';
    $product = $this->Product_model->get_data($product_id);
    $content = $this->load->view('product/form', array(
      'product_id' => $product_id,
      'product' => $product,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_css('assets_admin/vendor/summernote/summernote.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/summernote/summernote.min.js');
    $this->Masterpage_service->add_js('assets_admin/vendor/summernote-image-attributes/summernote-image-attributes.js');
    $this->Masterpage_service->add_css('assets_admin/vendor/jquery.multiple-select/css/multi-select.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/jquery.multiple-select/js/jquery.multi-select.js');
    $this->Masterpage_service->add_js('assets_admin/vendor/jquery.multiple-select/js/jquery.quicksearch.js');
    $this->Masterpage_service->add_js('assets_admin/js/product/form.js?v=1');
    $this->Masterpage_service->display($content, $title, 'product');
  }
  public function form_post($product_id){
    $this->Login_service->must_login();
    $data = array(
      'product_type' => 'product',
      'category_id' => $this->input->post('category_id'),
      'is_bundle' => 'false',
      'is_expire' => 'false',
      'expire_date' => '',
      'name_th' => $this->input->post('name_th'),
      'name_en' => $this->input->post('name_en'),
      'slug' => $this->input->post('slug'),
      'price' => $this->input->post('price'),
      'price_before_discount' => $this->input->post('price_before_discount'),
      'price_discount_rate' => $this->input->post('price_discount_rate'),
      'main_image' => '',
      'thumb_image' => '',
      'detail_th' => $this->input->post('detail_th'),
      'detail_en' => $this->input->post('detail_en'),
      'enable_status' => $this->input->post('enable_status'),
      'sort_priority' => $this->input->post('sort_priority')
    );
    $this->load->model(array('Upload_image_model'));
    $this->Upload_image_model->set_thumb(400, 400);
    $image_data = $this->Upload_image_model->upload_image('product','thumb_image');
    if($image_data['success']){
      $data['main_image'] = $image_data['image_medium'];
      $data['thumb_image'] = $image_data['image_small'];
    }
    if($product_id == 0){
      $product_id = $this->Product_model->insert($data);
    }else{
      $this->Product_model->update($product_id, $data);
    }
    $product_relate = $this->input->post('product_relate');
    $this->Product_relate_model->add_list($product_id, $product_relate);
    redirect('product');
  }
  public function detail($product_id){
    $this->Login_service->must_login();
    $product = $this->Product_model->get_data($product_id);
    $product_image_list = $this->Product_image_model->get_list($product_id);
    $title = $product->name_th;
    $content = $this->load->view('product/detail', array(
      'product_id' => $product_id,
      'product' => $product,
      'product_image_list' => $product_image_list,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_css('assets_admin/vendor/dropzone/basic.css');
    $this->Masterpage_service->add_css('assets_admin/vendor/dropzone/dropzone.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/dropzone/dropzone.js');
    $this->Masterpage_service->add_css('assets_admin/vendor/jquery-ui/jquery-ui.min.css');
    $this->Masterpage_service->add_css('assets_admin/vendor/jquery-ui/jquery-ui.theme.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/jquery-ui/jquery-ui.min.js');
    $this->Masterpage_service->add_js('assets_admin/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js');
    $this->Masterpage_service->add_js('assets_admin/js/product/detail.js?v=1');
    $this->Masterpage_service->display($content, $title, 'product');
  }
  public function delete($product_id){
    $this->Login_service->must_login();
    $this->Product_model->delete($product_id);
    redirect('product/');
  }
  public function upload_image(){
    $content_image = '';
    $this->load->model('Upload_image_model');
    $image_data = $this->Upload_image_model->upload_single_image('product_detail', 'file');
    if($image_data['success']){
      $content_image = $image_data['image_file'];
    }
    echo base_url('uploads/'.$content_image);
  }
  public function image_add($product_id){
    $data = array(
      'product_id' => $product_id,
      'main_image' => '',
      'thumb_image' => ''
    );
    $this->load->model(array('Upload_image_model'));
    $this->Upload_image_model->set_thumb(400, 400);
    $image_data = $this->Upload_image_model->upload_image('product_image','file');
    if($image_data['success']){
      $data['main_image'] = $image_data['image_medium'];
      $data['thumb_image'] = $image_data['image_small'];
    }
    $this->Product_image_model->insert($data);
    redirect('product/detail/'.$product_id);
  }
  public function update_image_priority(){
    $this->Login_service->must_login();
    $product_image_id = $this->input->post('image_id');
    $sort_priority = $this->input->post('sort_priority');
    $this->Product_image_model->sort_priority($product_image_id, $sort_priority);
  }
  public function image_delete($product_id, $product_image_id){
    $this->Product_image_model->delete($product_image_id);
    redirect('product/detail/'.$product_id);
  }
}
