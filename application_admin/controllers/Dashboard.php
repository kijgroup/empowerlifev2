<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('dashboard/index', array(), true);
    $this->Masterpage_service->display($content, 'Dashboard', 'dashboard');
  }
}
