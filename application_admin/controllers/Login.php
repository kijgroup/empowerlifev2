<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller {
  public function index() {
    $this->migrate_database();
    $this->Login_service->must_not_login();
    $data['title'] = 'เข้าสู่ระบบ';
    $this->load->view('login/index', $data);
  }
  public function login_post() {
    $admin_username = $this->input->post('txtUsername');
    $admin_password = $this->input->post('txtPassword');
    $this->Login_service->login($admin_username, $admin_password);
    redirect('dashboard');
  }

  public function logout() {
    $this->Login_service->logout();
    redirect('login');
  }

  public function migrate_database(){
    ini_set('max_execution_time', 300);
    $this->load->library('migration');
    if ($this->migration->current() === FALSE){
      show_error($this->migration->error_string());
    }
  }
}
