<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shipping_type extends MY_Controller{
  public $page_name = 'วิธีการจัดส่ง';
  public $controller = 'Shipping_type';
  public $nav = 'shipping_type';
  public $parent_nav_name = 'การจัดการ';
  public $list_display = array('shipping_type_name');
  public $display_form = array(
    array(
      'panel_name' => 'ข้อมูลวิธีการจัดส่ง',
      'item_list' => array(
        'shipping_type_name',
        'sort_priority',
        'create_date',
        'update_date'
      ),
    )
  );
  public $breadcrumb_list = array(
    array(
      'url' => '',
      'name' => 'การจัดการ',
    ),
    array(
      'url' => 'shipping_type',
      'name' => 'วิธีการจัดส่ง',
    )
  );
  public function __construct(){
    parent::__construct();
    $this->load->model('order/Shipping_type_model');
    $this->Main_model = $this->Shipping_type_model;
    parent::prepare_data();
  }
}
