<?php
class Promotion extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model(array(
      'product/Product_model',
      'category/Category_model',
      'product/Product_image_model',
      'product/Product_relate_model',
      'product/Product_bundle_model',
      'product/Enable_status_model',
    ));
  }
  public function index(){
    $this->Login_service->must_login();
    $category_list = $this->Category_model->get_list(false);
    $content = $this->load->view('promotion/list', array(), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/promotion/list.js');
    $this->Masterpage_service->display($content, 'โปรโมชั่น', 'promotion');
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('search_val');
    $enable_status = $this->input->post('enable_status');
    $page = $this->input->post('page');
    $per_page = $this->input->post('per_page');
    $this->load->model('product/Promotion_filter_service');
    $data = $this->Promotion_filter_service->get_data_content(
      $search_val,
      $enable_status,
      $page,
      $per_page);
    $this->load->view('promotion/list/content', $data);
  }
  public function form($product_id = 0){
    $this->Login_service->must_login();
    $title = (($product_id == 0)?'เพิ่ม':'แก้ไข').'โปรโมชั่น';
    $product = $this->Product_model->get_data($product_id);
    $content = $this->load->view('promotion/form', array(
      'product_id' => $product_id,
      'product' => $product,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_css('assets_admin/vendor/summernote/summernote.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/summernote/summernote.min.js');
    $this->Masterpage_service->add_js('assets_admin/vendor/summernote-image-attributes/summernote-image-attributes.js');
    $this->Masterpage_service->add_css('assets_admin/vendor/jquery.multiple-select/css/multi-select.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/jquery.multiple-select/js/jquery.multi-select.js');
    $this->Masterpage_service->add_js('assets_admin/vendor/jquery.multiple-select/js/jquery.quicksearch.js');
    $this->Masterpage_service->add_js('assets_admin/js/promotion/form.js?v=2');
    $this->Masterpage_service->display($content, $title, 'promotion');
  }
  public function form_post($product_id){
    $this->Login_service->must_login();
    $expire_date = $this->input->post('expire_date').' '.$this->input->post('expire_time');
    $data = array(
      'product_type' => 'promotion',
      'category_id' => 0,
      'is_bundle' => $this->input->post('is_bundle'),
      'is_expire' => $this->input->post('is_expire'),
      'expire_date' => $expire_date,
      'name_th' => $this->input->post('name_th'),
      'name_en' => $this->input->post('name_en'),
      'slug' => $this->input->post('slug'),
      'price' => $this->input->post('price'),
      'price_before_discount' => $this->input->post('price_before_discount'),
      'price_discount_rate' => $this->input->post('price_discount_rate'),
      'main_image' => '',
      'thumb_image' => '',
      'detail_th' => $this->input->post('detail_th'),
      'detail_en' => $this->input->post('detail_en'),
      'enable_status' => $this->input->post('enable_status'),
      'sort_priority' => $this->input->post('sort_priority'),
    );
    $this->load->model(array('Upload_image_model'));
    $this->Upload_image_model->set_thumb(400, 400);
    $image_data = $this->Upload_image_model->upload_image('promotion','thumb_image');
    if($image_data['success']){
      $data['main_image'] = $image_data['image_medium'];
      $data['thumb_image'] = $image_data['image_small'];
    }
    if($product_id == 0){
      $product_id = $this->Product_model->insert($data);
    }else{
      $this->Product_model->update($product_id, $data);
    }
    $product_relate = $this->input->post('product_relate');
    $this->Product_relate_model->add_list($product_id, $product_relate);
    $this->db->where('product_type', 'product');
    $product_list = $this->Product_model->get_list();
    $this->Product_bundle_model->remove_list($product_id);
    foreach($product_list->result() as $product_item){
      $qty = $this->input->post('bundle_qty_'.$product_item->product_id);
      if($qty != '' && $qty >0 ){
        $this->Product_bundle_model->insert(array(
          'product_id' => $product_id,
          'bundle_id' => $product_item->product_id,
          'qty' => $qty
        ));
      }
    }
    redirect('promotion');
  }
  public function detail($product_id){
    $this->Login_service->must_login();
    $product = $this->Product_model->get_data($product_id);
    $product_image_list = $this->Product_image_model->get_list($product_id);
    $title = $product->name_th;
    $content = $this->load->view('promotion/detail', array(
      'product_id' => $product_id,
      'product' => $product,
      'product_image_list' => $product_image_list,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_css('assets_admin/vendor/dropzone/basic.css');
    $this->Masterpage_service->add_css('assets_admin/vendor/dropzone/dropzone.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/dropzone/dropzone.js');
    $this->Masterpage_service->add_css('assets_admin/vendor/jquery-ui/jquery-ui.min.css');
    $this->Masterpage_service->add_css('assets_admin/vendor/jquery-ui/jquery-ui.theme.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/jquery-ui/jquery-ui.min.js');
    $this->Masterpage_service->add_js('assets_admin/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js');
    $this->Masterpage_service->add_js('assets_admin/js/promotion/detail.js?v=1');
    $this->Masterpage_service->display($content, $title, 'promotion');
  }
  public function delete($product_id){
    $this->Login_service->must_login();
    $this->Product_model->delete($product_id);
    redirect('promotion/');
  }
  public function upload_image(){
    $content_image = '';
    $this->load->model('Upload_image_model');
    $image_data = $this->Upload_image_model->upload_single_image('promotion_detail', 'file');
    if($image_data['success']){
      $content_image = $image_data['image_file'];
    }
    echo base_url('uploads/'.$content_image);
  }
  public function image_add($product_id){
    $data = array(
      'product_id' => $product_id,
      'main_image' => '',
      'thumb_image' => ''
    );
    $this->load->model(array('Upload_image_model'));
    $this->Upload_image_model->set_thumb(400, 400);
    $image_data = $this->Upload_image_model->upload_image('promotion_image','file');
    if($image_data['success']){
      $data['main_image'] = $image_data['image_medium'];
      $data['thumb_image'] = $image_data['image_small'];
    }
    $this->Product_image_model->insert($data);
    redirect('promotion/detail/'.$product_id);
  }
  public function image_delete($product_id, $product_image_id){
    $this->Product_image_model->delete($product_image_id);
    redirect('promotion/detail/'.$product_id);
  }
  public function moveup($product_id){
    $this->Product_model->move_up($product_id);
    redirect('promotion');
  }
  public function movedown($product_id){
    $this->Product_model->move_down($product_id);
    redirect('promotion');
  }
}
