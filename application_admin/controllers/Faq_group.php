<?php
class Faq_group extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->Login_service->must_login();
    $this->load->model(array('faq/Faq_group_model'));
  }
  public function index(){
    $content = $this->load->view('faq_group/list', array(
      'faq_group_list' => $this->Faq_group_model->get_list()
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/faq_group/list.js');
    $this->Masterpage_service->display($content, 'หมวดคำถาม', 'faq_group');
  }
  public function form($faq_group_id = 0){
    $this->Login_service->must_login();
    $title = (($faq_group_id == 0)?'เพิ่ม':'แก้ไข').'หมวดคำถาม';
    $faq_group = $this->Faq_group_model->get_data($faq_group_id);
    $content = $this->load->view('faq_group/form', array(
      'faq_group_id' => $faq_group_id,
      'faq_group' => $faq_group,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/faq_group/form.js');
    $this->Masterpage_service->display($content, $title, 'faq_group');
  }
  public function form_post($faq_group_id){
    $data = array(
      'faq_group_th' => $this->input->post('faq_group_th'),
      'faq_group_en' => $this->input->post('faq_group_en'),
      'enable_status' => $this->input->post('enable_status'),
      'sort_priority' => $this->input->post('sort_priority')
    );
    if($faq_group_id == 0){
      $faq_group_id = $this->Faq_group_model->insert($data);
    }else{
      $this->Faq_group_model->update($faq_group_id, $data);
    }
    redirect('faq_group');
  }
  public function delete($faq_group_id){
    $this->Faq_group_model->delete($faq_group_id);
    redirect('faq_group/');
  }
}
