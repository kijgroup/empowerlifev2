<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('contact/Contact_model', 'contact/Contact_group_model'));
  }
  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('contact/list', array(), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/contact/list.js');
    $this->Masterpage_service->display($content, 'รายการติดต่อเจ้าหน้าที่', 'contact');
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('searchVal');
    $start_date = $this->input->post('startDate');
    $end_date = $this->input->post('endDate');
    $page = $this->input->post('page');
    $perPage = $this->input->post('perPage');
    $this->load->model('contact/Contact_filter_service');
    $data = $this->Contact_filter_service->get_data_content(
      $search_val
      , $start_date
      , $end_date
      , $page
      , $perPage);
    $this->load->view('contact/listcontent', $data);
  }
  public function detail($contact_id){
    $this->Login_service->must_login();
    $contact = $this->Contact_model->get_data($contact_id);
    $this->Contact_model->set_to_read($contact_id);
    $data_content = array();
    $data_content['contact_id'] = $contact_id;
    $data_content['contact'] = $contact;
    $title = 'ข้อมูลติดต่อเจ้าหน้าที่';
    $data_content['title'] = $title;
    $content = $this->load->view('contact/detail', $data_content, TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/contact/detail.js');
    $this->Masterpage_service->display($content, $title, 'contact');
  }
  public function delete($contact_id){
    $this->Login_service->must_login();
    $this->Contact_model->delete($contact_id);
    redirect('contact/');
  }
}
