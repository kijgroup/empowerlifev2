<?php
class Order extends CI_Controller{
  function __construct() {
    parent::__construct();
    $this->load->model(array(
      'order/Order_model',
      'order/Order_filter_service',
      'order/Order_item_model',
      'order/Order_status_model',
      'order/Order_print_status_model',
      'order/Payment_type_model',
      'member/Member_model',
      'member/Member_type_model',
      'product/Product_model',
      'order_channel/Order_channel_model',
      'order_channel/Order_sub_channel_model',
      'order/Contact_channel_model',
      'order/Shipping_type_model',
      'order/Send_time_status_model',
      'order/Payment_status_model',
      'order/Order_discount_point_model',
      'stock/Stock_model',
      'discount_point/Discount_point_model',
      'bank/Bank_model',
      'Number_service',
    ));
  }
  public function index($order_status_page = ''){
    $this->Login_service->must_login();
    $content = $this->load->view('order/list', array(
      'order_status_page' => $order_status_page
    ), TRUE);
    $nav = 'order'.(($order_status_page != '')?'-'.$order_status_page:'');
    $this->Masterpage_service->add_js('/assets_admin/js/order/list.js?v=3');
    $this->Masterpage_service->display($content, 'รายการสั่งซื้อ', $nav);
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('search_val');
    $payment_type = $this->input->post('payment_type');
    $order_channel_id = $this->input->post('order_channel_id');
    $send_time_status = $this->input->post('send_time_status');
    $create_by = $this->input->post('create_by');
    $start_date = $this->input->post('start_date');
    $end_date = $this->input->post('end_date');
    $order_status = $this->input->post('order_status');
    $page = $this->input->post('page');
    $per_page = $this->input->post('per_page');
    $this->load->model('order/Order_filter_service');
    $data = $this->Order_filter_service->get_data_content(
      $search_val,
      $payment_type,
      $order_channel_id,
      $send_time_status,
      $create_by,
      $start_date,
      $end_date,
      $order_status,
      $page,
      $per_page);
    $this->load->view('order/list/content', $data);
  }
  public function form($order_id = 0){
    $this->Login_service->must_login();
    $member_id = $this->input->post('member_id');
    $member_id = ($member_id > 0)?$member_id:0;
    $title = (($order_id == 0)?'เพิ่ม':'แก้ไข').'สินค้า';
    $order = $this->Order_model->get_data($order_id);
    $order_item_list = $this->Order_item_model->get_list($order_id);
    $content = $this->load->view('order/form', array(
      'order_id' => $order_id,
      'order' => $order,
      'order_item_list' => $order_item_list,
      'member_id' => $member_id,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_css('assets_admin/vendor/select2/css/select2.css');
    $this->Masterpage_service->add_css('assets_admin/vendor/select2-bootstrap-theme/select2-bootstrap.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/select2/js/select2.js');
    $this->Masterpage_service->add_css('assets_admin/vendor/summernote/summernote.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/summernote/summernote.min.js');
    $this->Masterpage_service->add_js('assets_admin/js/address/popup.js');
    $this->Masterpage_service->add_js('assets_admin/js/order/form.js?v=5');
    $this->Masterpage_service->display($content, $title, 'order');
  }
  public function form_post($order_id){
    $this->Login_service->must_login();
    $data = array();
    $field_list = array(
      'member_id',
      'order_status',
      'order_channel_id',
      'order_sub_channel_id',
      'order_channel_text',
      'contact_channel_id',
      'shipping_type_id',
      'stock_id',
      'send_time_status',
      'payment_status',
      'postoffice',
      'bank_id',
      'discount_point_amount',
      'discount_by_point_amount',
      'shipping_name',
      'shipping_address',
      'shipping_post_code',
      'shipping_district',
      'shipping_amphur',
      'shipping_province',
      'shipping_mobile',
      'is_tax',
      'tax_name',
      'tax_address',
      'tax_post_code',
      'tax_district',
      'tax_amphur',
      'tax_province',
      'tax_mobile',
      'tax_code',
      'tax_branch',
      'payment_type',
      'remark'
    );
    foreach($field_list as $field_name){
      $data[$field_name] = trim($this->input->post($field_name));
    }
    $product_id_list = $this->input->post('product_id');
    if(count($product_id_list) == 0){
      redirect('order/form');
    }
    $order = false;
    if($order_id == 0){
      $order_id = $this->Order_model->insert($data);
      if($data['discount_point_amount'] > 0){
        $this->Member_model->add_point($data['member_id'], - $data['discount_point_amount']);
      }
    }else{
      $order = $this->Order_model->get_data($order_id);
      $this->Order_model->update($order_id, $data);
      $this->Order_item_model->delete_order($order_id);//clear order item
      if((int)$order->discount_point_amount > 0){
        $this->Member_model->add_point($order->member_id, $order->discount_point_amount);
      }
      if((int)$data['discount_point_amount'] > 0){
        $this->Member_model->add_point($data['member_id'], - $data['discount_point_amount']);
      }
    }
    $qty_list = $this->input->post('qty');
    $price_per_item_list = $this->input->post('price_per_item');
    $items_total_price = 0;
    $total_item = 0;
    foreach($product_id_list as $index => $product_id){
      if($qty_list[$index] > 0){
        $item_total_price = $price_per_item_list[$index] * $qty_list[$index];
        $items_total_price += $item_total_price;
        $total_item += $qty_list[$index];
        $this->Order_item_model->insert(array(
          'order_id' => $order_id,
          'product_id' => $product_id,
          'price_per_item' => $price_per_item_list[$index],
          'qty' => $qty_list[$index],
          'price_total' => $item_total_price
        ));
      }
    }
    $discount_price = $this->input->post('discount_price');
    $shipping_price = $this->input->post('shipping_price');
    $grand_total = $items_total_price - $discount_price - $data['discount_by_point_amount'] + $shipping_price;
    $this->Order_model->update_price($order_id, array(
      'total_item' => $total_item,
      'item_price' => $items_total_price,
      'discount_price' => $discount_price,
      'shipping_price' => $shipping_price,
      'grand_total' => $grand_total
    ));
    if($order && $order->order_status == 'complete'){
      $old_member = $this->Member_model->get_data($order->member_id);
      $old_member_type = $this->Member_type_model->get_data($old_member->member_type);
      //reduce member point
      if((int)$order->discount_point_amount == 0){
        $member_point = ceil($order->grand_total / $old_member_type->convert_point);
        $this->Member_model->add_point($old_member->member_id, -$member_point);
      }
      //change buy total
      $this->Member_model->add_buy_total($old_member->member_id, -$order->grand_total);
    }
    if($data['order_status'] == 'complete'){
      $new_member = $this->Member_model->get_data($data['member_id']);
      $new_member_type = $this->Member_type_model->get_data($new_member->member_type);
      //add member point
      if((int)$data['discount_point_amount'] == 0){
        $member_point = ceil($grand_total / $new_member_type->convert_point);
        $date_now = new DateTime();
        $expire_double_point = new DateTime("01/09/2018");
        if ($date_now < $expire_double_point) {
          $member_point *= 2;
        }
        $this->Member_model->add_point($new_member->member_id, $member_point);
      }
      //change buy total
      $this->Member_model->add_buy_total($new_member->member_id, $grand_total);
    }
    $this->add_order_discount_point_list($order_id);
    redirect('order/detail/'.$order_id);
  }
  private function add_order_discount_point_list($order_id){
    $discount_point_list = $this->Discount_point_model->get_list();
    foreach($discount_point_list->result() as $discount_point){
      $qty = $this->input->post('discount_point_'.$discount_point->discount_point_id);
      $total_use_point = $discount_point->use_point * $qty;
      $total_discount_amount = $discount_point->discount_amount * $qty;
      $this->Order_discount_point_model->add($order_id, $discount_point->discount_point_id, array(
        'qty' => $qty,
        'use_point' => $discount_point->use_point,
        'discount_amount' => $discount_point->discount_amount,
        'total_use_point' => $total_use_point,
        'total_discount_amount' => $total_discount_amount,
      ));
    }
  }
  public function detail($order_id){
    $this->Login_service->must_login();
    $order = $this->Order_model->get_data($order_id);
    $order_item_list = $this->Order_item_model->get_list($order_id);
    $title = 'รายละเอียดการสั่งซื้อ';
    $content = $this->load->view('order/detail', array(
      'order_id' => $order_id,
      'order' => $order,
      'order_item_list' => $order_item_list,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/order/detail.js');
    $this->Masterpage_service->display($content, $title, 'order');
  }
  public function delete($order_id){
    $this->Login_service->must_login();
    $this->Order_model->delete($order_id);
    redirect('order');
  }
  public function update_status($order_id, $order_status, $is_redirect = true){
    $this->Login_service->must_login();
    $order = $this->Order_model->get_data($order_id);
    if($order){
      $member = $this->Member_model->get_data($order->member_id);
      $member_type = $this->Member_type_model->get_data($member->member_type);
      if($order->order_status != 'complete' && $order_status == 'complete'){
        if($order->discount_point_id == 0){
          $member_point = ceil($order->grand_total / $member_type->convert_point);
          $date_now = new DateTime();
          $expire_double_point = new DateTime("01/09/2018");
          if ($date_now < $expire_double_point) {
            $member_point *= 2;
          }
          $this->Member_model->add_point($member->member_id, $member_point);
        }
        $this->Member_model->add_buy_total($member->member_id, $order->grand_total);
      }
      if($order->order_status == 'complete' && $order_status == 'cancel'){
        $member_point = ceil($order->grand_total / $member_type->convert_point);
        $this->Member_model->add_point($member->member_id, -$member_point);
        $this->Member_model->add_buy_total($member->member_id, -$order->grand_total);
      }elseif($order->order_status != 'cancel' && $order_status == 'cancel'){
        if($order->discount_point_id == 0){
          $this->Member_model->add_point($member->member_id, $order->discount_point_amount);
        }
      }
      $this->Order_model->update_status($order_id, $order_status);
    }
    if($is_redirect){
      redirect('order/detail/'.$order_id);
    }
  }
  public function update_unprint($order_id){
    $this->Login_service->must_login();
    $this->Order_model->update_print_status($order_id, 'false');
    redirect('order/detail/'.$order_id);
  }
  public function print_pdf($order_id, $is_print = 'false'){
    $this->Login_service->must_login();
    $order = $this->Order_model->get_data($order_id);
    if($is_print == 'true'){
      $this->Order_model->update_print_status($order_id, 'true');
    }
    $html = $this->load->view('order/pdf', array(
      'order_id' => $order_id,
      'order' => $order,
    ), true);
    $stylesheet = file_get_contents(FCPATH.'assets_admin/css/pdf.css');
    $file_name = 'order_'.$order->order_code;
    $this->load->model(array('Pdf_service'));
    //echo $html;
    $this->Pdf_service->view_pdf_file($html, $stylesheet, '', '', $file_name);
  }
  public function pdf_list($id_list, $is_print){
    $this->Login_service->must_login();
    //$id_list = $this->input->post('order_id');
    if($id_list == NULL){
      echo '<h1>ไม่มีรายการที่ถูกเลือก</h1>';
      exit();
    }
    $html = '';
    foreach($id_list as $index => $order_id){
      $order = $this->Order_model->get_data($order_id);
      if($index > 0){
        $html .= '<pagebreak>';
      }
      $html .= $this->load->view('order/pdf', array(
        'order_id' => $order_id,
        'order' => $order,
      ), true);
    }
    $stylesheet = file_get_contents(FCPATH.'assets_admin/css/pdf.css');
    $footer = '';
    $file_name = 'order.pdf';
    $this->load->model(array('Pdf_service'));
    $this->Pdf_service->view_pdf_file($html, $stylesheet, '', $footer, $file_name);
  }
  public function action_list($order_status_page = ''){
    $this->Login_service->must_login();
    $id_list = $this->input->post('order_id');
    if($id_list == NULL){
      echo '<h1>ไม่มีรายการที่ถูกเลือก</h1>';
      exit();
    }
    $action = $this->input->post('action');
    if($action == 'print_true'){
      $this->Order_model->update_print_status_list($id_list, 'true');
      $this->pdf_list($id_list);
      exit(0);
    }elseif($action == 'print_false'){
      $this->pdf_list($id_list);
      exit(0);
    }elseif($action == 'update_unprint'){
      $this->Order_model->update_print_status_list($id_list, 'false');
    }elseif($action == 'status_open'){
      $this->update_status_list($id_list, 'open');
    }elseif($action == 'status_confirm'){
      $this->update_status_list($id_list, 'confirm');
    }elseif($action == 'status_complete'){
      $this->update_status_list($id_list, 'complete');
    }elseif($action == 'status_cancel'){
      $this->update_status_list($id_list, 'cancel');
    }elseif($action == 'delete'){
      $this->delete_list($id_list);
    }
    redirect('order/index/'.$order_status_page);
  }
  private function update_status_list($id_list, $status){
    foreach($id_list as $order_id){
      $this->update_status($order_id, $status, false);
    }
  }
  private function delete_list($id_list){
    $this->db->set('is_delete', 'delete');
    $this->db->where_in('order_id', $id_list);
    $this->db->update('tbl_order');
  }
  public function download_excel(){
    $this->Login_service->must_login();
    $search_val = $this->input->post('search_val');
    $payment_type = $this->input->post('payment_type');
    $order_channel_id = $this->input->post('order_channel_id');
    $send_time_status = $this->input->post('send_time_status');
    $create_by = $this->input->post('create_by');
    $start_date = $this->input->post('start_date');
    $end_date = $this->input->post('end_date');
    $order_status = $this->input->post('order_status');
    $this->load->model(array('order/Order_excel_service'));
    $this->Order_excel_service->export_list(
      $search_val,
      $payment_type,
      $order_channel_id,
      $send_time_status,
      $create_by,
      $start_date,
      $end_date,
      $order_status
    );
  }
}
