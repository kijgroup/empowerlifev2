<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Member_loss extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model(array(
      'member/Member_model',
      'member/Member_type_model',
      'member/Member_enable_status_model',
      'member/Member_gender_model',
      'order/Order_model',
      'order/Order_status_model',
      'reward_transaction/Reward_transaction_model',
      'reward_transaction/Reward_transaction_status_model',
      'member/User_model'
    ));
  }
  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('report/member_loss/index', array(
      'member_type_list' => $this->Member_type_model->get_list()
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/report/member_loss/index.js');
    $this->Masterpage_service->display($content, 'Report Member Loss', 'report_member_loss');
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('search_val');
    $date_after = $this->input->post('date_after');
    $member_type_code = $this->input->post('member_type_code');
    $enable_status = $this->input->post('enable_status');
    $sort = $this->input->post('sort');
    $page = $this->input->post('page');
    $per_page = $this->input->post('per_page');
    if(trim($date_after) === ''){
      $date_after = '2017-01-01';
    }
    if(trim($member_type_code) === ''){
      $member_type_code = 'all';
    }
    if(trim($enable_status) === ''){
      $enable_status = 'all';
    }
    if(trim($sort) === ''){
      $sort = 'member_id';
    }
    if($page <= 0){
      $page = 1;
    }
    if($per_page <= 0){
      $per_page = 20;
    }
    $this->load->model('report/Member_loss_filter_service');
    $data = $this->Member_loss_filter_service->get_data_content(array(
      'search_val' => $search_val,
      'date_after' => $date_after,
      'member_type_code' => $member_type_code,
      'enable_status' => $enable_status,
      'sort' => $sort,
      'page' => $page,
      'per_page' => $per_page
    ));
    $this->load->view('report/member_loss/content', $data);
  }
  public function export(){
    $this->Login_service->must_login();
    $search_val = $this->input->post('search_val');
    $date_after = $this->input->post('date_after');
    $member_type_code = $this->input->post('member_type_code');
    $enable_status = $this->input->post('enable_status');
    if(trim($date_after) === ''){
      $date_after = '2017-01-01';
    }
    if(trim($member_type_code) === ''){
      $member_type_code = 'all';
    }
    if(trim($enable_status) === ''){
      $enable_status = 'all';
    }
    $this->load->model('report/Member_loss_csv_service');
    $data = $this->Member_loss_csv_service->export_list(array(
      'search_val' => $search_val,
      'date_after' => $date_after,
      'member_type_code' => $member_type_code,
      'enable_status' => $enable_status,
    ));
  }
}