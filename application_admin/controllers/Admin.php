<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
  function __construct() {
    parent::__construct();
  }
  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('admin/list', array(), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/admin/list.js');
    $this->Masterpage_service->display($content, 'รายการเจ้าหน้าที่', 'admin');
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('searchVal');
    $enable_status = $this->input->post('enableStatus');
    $page = $this->input->post('page');
    $perPage = $this->input->post('perPage');
    $this->load->model('admin/Admin_filter_service');
    $data = $this->Admin_filter_service->get_data_content(
      $search_val
      , $enable_status
      , $page
      , $perPage);
    $this->load->view('admin/listcontent', $data);
  }
  public function form($admin_id = 0){
    $this->Login_service->must_login();
    $data_content = array();
    $data_content['admin_id'] = $admin_id;
    $data_content['admin'] = $this->Admin_model->get_data($admin_id);
    $title = (($admin_id == 0)?'เพิ่ม':'แก้ไข').'ข้อมูลเจ้าหน้าที่';
    $data_content['title'] = $title;
    $content = $this->load->view('admin/form', $data_content, TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/admin/form.js');
    $this->Masterpage_service->display($content, $title, 'admin');
  }
  public function form_post($admin_id){
    $this->Login_service->must_login();
    $data = array();
    $data['admin_type'] = $this->input->post('admin_type');
    $data['admin_name'] = $this->input->post('txtName');
    $data['admin_username'] = $this->input->post('txtUsername');
    $enable_status = $this->input->post('chkEnableStatus');
    $data['enable_status'] = ($enable_status != '')?$enable_status:'0';
    if($admin_id == 0){
      $data['admin_password'] = $this->input->post('txtPassword');
      $admin_id = $this->Admin_model->insert($data);
    }else{
      $this->Admin_model->update($admin_id, $data);
    }
    redirect('admin/detail/'.$admin_id);
  }
  public function detail($admin_id){
    $this->Login_service->must_login();
    $admin = $this->Admin_model->get_data($admin_id);
    $data_content = array();
    $data_content['admin_id'] = $admin_id;
    $data_content['admin'] = $admin;
    $title = 'ข้อมูลเจ้าหน้าที่';
    $data_content['title'] = $title;
    $content = $this->load->view('admin/detail', $data_content, TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/admin/detail.js');
    $this->Masterpage_service->display($content, $title, 'admin');
  }
  public function form_change_password($admin_id){
    $this->Login_service->must_login();
    $login_password = $this->input->post('txtPassword');
    $this->Admin_model->update_password($admin_id, $login_password);
    redirect('admin/detail/'.$admin_id);
  }
  public function delete($admin_id){
    $this->Login_service->must_login();
    $this->Admin_model->delete($admin_id);
    redirect('admin/');
  }
}
