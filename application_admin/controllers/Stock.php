<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Stock extends MY_Controller{
  public $page_name = 'ตัด Stock';
  public $controller = 'Stock';
  public $nav = 'stock';
  public $parent_nav_name = 'การจัดการ';
  public $list_display = array('stock_name');
  public $display_form = array(
    array(
      'panel_name' => 'ข้อมูลตัด Stock',
      'item_list' => array(
        'stock_name',
        'sort_priority',
        'create_date',
        'update_date'
      ),
    )
  );
  public $breadcrumb_list = array(
    array(
      'url' => '',
      'name' => 'การจัดการ',
    ),
    array(
      'url' => 'stock',
      'name' => 'ตัด Stock',
    )
  );
  public function __construct(){
    parent::__construct();
    $this->load->model('stock/Stock_model');
    $this->Main_model = $this->Stock_model;
    parent::prepare_data();
  }
}
