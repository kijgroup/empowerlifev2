<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribe extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model('subscribe/Subscribe_model');
  }
  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('subscribe/list', array(), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/subscribe/list.js');
    $this->Masterpage_service->display($content, 'ผู้ติดตามข่าวสาร', 'subscribe');
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('searchVal');
    $start_date = $this->input->post('startDate');
    $end_date = $this->input->post('endDate');
    $page = $this->input->post('page');
    $perPage = $this->input->post('perPage');
    $this->load->model('subscribe/Subscribe_filter_service');
    $data = $this->Subscribe_filter_service->get_data_content(
      $search_val
      , $start_date
      , $end_date
      , $page
      , $perPage);
    $this->load->view('subscribe/listcontent', $data);
  }

  public function form($subscribe_id = 0){
    $this->Login_service->must_login();
    $data_content = array();
    $data_content['subscribe_id'] = $subscribe_id;
    $data_content['subscribe'] = $this->Subscribe_model->get_data($subscribe_id);
    $title = (($subscribe_id == 0)?'เพิ่ม':'แก้ไข').'ผู้ติดตามข่าวสาร';
    $data_content['title'] = $title;
    $content = $this->load->view('subscribe/form', $data_content, TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/subscribe/form.js');
    $this->Masterpage_service->display($content, $title, 'subscribe');
  }
  public function form_post($subscribe_id){
    $this->Login_service->must_login();
    $data = array();
    $data['subscribe_email'] = $this->input->post('subscribe_email');
    $subscribe_status = $this->input->post('subscribe_status');
    $data['subscribe_status'] = ($subscribe_status != '')?$subscribe_status:'0';
    if($subscribe_id == 0){
      $subscribe_id = $this->Subscribe_model->insert($data);
    }else{
      $this->Subscribe_model->update($subscribe_id, $data);
    }
    redirect('subscribe/detail/'.$subscribe_id);
  }

  public function detail($subscribe_id){
    $this->Login_service->must_login();
    $subscribe = $this->Subscribe_model->get_data($subscribe_id);
    $data_content = array();
    $data_content['subscribe_id'] = $subscribe_id;
    $data_content['subscribe'] = $subscribe;
    $title = 'ผู้ติดตามข่าวสาร';
    $data_content['title'] = $title;
    $content = $this->load->view('subscribe/detail', $data_content, TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/subscribe/detail.js');
    $this->Masterpage_service->display($content, $title, 'subscribe');
  }

  public function delete($subscribe_id){
    $this->Login_service->must_login();
    $this->Subscribe_model->delete($subscribe_id);
    redirect('subscribe/');
  }
}
