<?php
class Content extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->load->model(array('content/Content_model', 'contentgroup/Content_group_model', 'content/Content_image_model'));
  }
  public function index(){
    $this->Login_service->must_login();
    $content_group_list = $this->Content_group_model->get_list(false);
    $content = $this->load->view('content/list', array(
      'content_group_list' => $content_group_list
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/content/list.js?v=1');
    $this->Masterpage_service->display($content, 'บทความ', 'content');
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('search_val');
    $content_group_id = $this->input->post('content_group_id');
    $display_contact = $this->input->post('display_contact');
    $enable_status = $this->input->post('enable_status');
    $page = $this->input->post('page');
    $per_page = $this->input->post('per_page');
    $this->load->model('content/Content_filter_service');
    $data = $this->Content_filter_service->get_data_content(
      $search_val,
      $content_group_id,
      $display_contact,
      $enable_status,
      $page,
      $per_page);
    $this->load->view('content/list/content', $data);
  }
  public function form($content_id = 0){
    $this->Login_service->must_login();
    $title = (($content_id == 0)?'เพิ่ม':'แก้ไข').'บทความ';
    $content_data = $this->Content_model->get_data($content_id);
    $content = $this->load->view('content/form', array(
      'content_id' => $content_id,
      'content' => $content_data,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_css('assets_admin/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js');
    $this->Masterpage_service->add_css('assets_admin/vendor/summernote/summernote.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/summernote/summernote.min.js');
    $this->Masterpage_service->add_js('assets_admin/vendor/summernote-image-attributes/summernote-image-attributes.js');
    $this->Masterpage_service->add_js('assets_admin/js/content/form.js?v=2');
    $this->Masterpage_service->display($content, $title, 'content');
  }
  public function form_post($content_id){
    $this->Login_service->must_login();
    $data = array(
      'content_group_id' => $this->input->post('content_group_id'),
      'subject_th' => $this->input->post('subject_th'),
      'subject_en' => $this->input->post('subject_en'),
      'content_image' => '',
      'thumb_image' => '',
      'short_content_th' => $this->input->post('short_content_th'),
      'short_content_en' => $this->input->post('short_content_en'),
      'content_th' => $this->input->post('content_th'),
      'content_en' => $this->input->post('content_en'),
      'tags' => $this->input->post('tags'),
      'display_contact' => $this->input->post('display_contact'),
      'enable_status' => $this->input->post('enable_status'),
      'sort_priority' => $this->input->post('sort_priority')
    );
    $this->load->model(array('Upload_image_model'));
    $this->Upload_image_model->set_thumb(400, 400);
    $image_data = $this->Upload_image_model->upload_image('content','content_image');
    if($image_data['success']){
      $data['content_image'] = $image_data['image_large'];
      $data['thumb_image'] = $image_data['image_small'];
    }
    if($content_id == 0){
      $content_id = $this->Content_model->insert($data);
    }else{
      $this->Content_model->update($content_id, $data);
    }
    redirect('content');
  }
  public function detail($content_id){
    $this->Login_service->must_login();
    $content_data = $this->Content_model->get_data($content_id);
    $content_image_list = $this->Content_image_model->get_list($content_id);
    $title = $content_data->subject_th;
    $content = $this->load->view('content/detail', array(
      'content_id' => $content_id,
      'content' => $content_data,
      'content_image_list' => $content_image_list,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_css('assets_admin/vendor/dropzone/basic.css');
    $this->Masterpage_service->add_css('assets_admin/vendor/dropzone/dropzone.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/dropzone/dropzone.js');
    $this->Masterpage_service->add_css('assets_admin/vendor/jquery-ui/jquery-ui.min.css');
    $this->Masterpage_service->add_css('assets_admin/vendor/jquery-ui/jquery-ui.theme.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/jquery-ui/jquery-ui.min.js');
    $this->Masterpage_service->add_js('assets_admin/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js');
    $this->Masterpage_service->add_js('assets_admin/js/content/detail.js?v=1');
    $this->Masterpage_service->display($content, $title, 'content');
  }
  public function delete($content_id){
    $this->Login_service->must_login();
    $this->Content_model->delete($content_id);
    redirect('content/');
  }
  public function upload_image(){
    $this->Login_service->must_login();
    $content_image = '';
    $this->load->model('Upload_image_model');
    $image_data = $this->Upload_image_model->upload_single_image('content_detail', 'file');
    if($image_data['success']){
      $content_image = $image_data['image_file'];
    }
    echo base_url('uploads/'.$content_image);
  }
  public function image_add($content_id){
    $this->Login_service->must_login();
    $data = array(
      'content_id' => $content_id,
      'main_image' => '',
      'thumb_image' => ''
    );
    $this->load->model(array('Upload_image_model'));
    $this->Upload_image_model->set_thumb(400, 400);
    $image_data = $this->Upload_image_model->upload_image('content_image','file');
    if($image_data['success']){
      $data['main_image'] = $image_data['image_medium'];
      $data['thumb_image'] = $image_data['image_small'];
      $this->Content_image_model->insert($data);
      echo 'success';
    }else{
      $this->output->set_status_header('400', $image_data['message']);
    }
  }
  public function update_image_priority(){
    $this->Login_service->must_login();
    $content_image_id = $this->input->post('image_id');
    $sort_priority = $this->input->post('sort_priority');
    $this->Content_image_model->sort_priority($content_image_id, $sort_priority);
  }
  public function image_update($content_id, $content_image_id){
    $this->Content_image_model->update($content_image_id, array(
      'content_image_name' => $this->input->post('content_image_name')
    ));
    redirect('content/detail/'.$content_id);
  }
  public function image_delete($content_id, $content_image_id){
    $this->Content_image_model->delete($content_image_id);
    redirect('content/detail/'.$content_id);
  }
}
