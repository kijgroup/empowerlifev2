<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array('Config_model'));
  }
  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('config/index', array(), true);
    $this->Masterpage_service->add_js('/assets_admin/js/config/form.js');
    $this->Masterpage_service->display($content, 'Config', 'config');
  }
  public function form_post(){
    $this->update_config_text('web_title_th');
    $this->update_config_text('web_title_en');
    $this->update_config_text('meta_description');
    $this->update_config_text('meta_keyword');
    $this->update_config_text('email_list');
    $this->update_config_text('email_sender');
    $this->update_config_text('facebook_code');
    $this->update_config_text('google_analytic');
    $this->update_config_text('youtube_iframe');
    redirect('config');
  }
  private function update_config_text($code){
    $this->Config_model->update($code, $this->input->post($code));
  }
}
