<?php
class Reward extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model(array('reward/Reward_model'));
  }
  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('reward/list', array(), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/reward/list.js');
    $this->Masterpage_service->display($content, 'ของรางวัล', 'reward');
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('search_val');
    $enable_status = $this->input->post('enable_status');
    $page = $this->input->post('page');
    $per_page = $this->input->post('per_page');
    $this->load->model('reward/Reward_filter_service');
    $data = $this->Reward_filter_service->get_data_content(
      $search_val,
      $enable_status,
      $page,
      $per_page);
    $this->load->view('reward/list/content', $data);
  }
  public function form($reward_id = 0){
    $this->Login_service->must_login();
    $title = (($reward_id == 0)?'เพิ่ม':'แก้ไข').'ของรางวัล';
    $reward = $this->Reward_model->get_data($reward_id);
    $content = $this->load->view('reward/form', array(
      'reward_id' => $reward_id,
      'reward' => $reward,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_css('assets_admin/vendor/summernote/summernote.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/summernote/summernote.min.js');
    $this->Masterpage_service->add_css('assets_admin/vendor/jquery.multiple-select/css/multi-select.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/jquery.multiple-select/js/jquery.multi-select.js');
    $this->Masterpage_service->add_js('assets_admin/vendor/jquery.multiple-select/js/jquery.quicksearch.js');
    $this->Masterpage_service->add_js('assets_admin/js/reward/form.js');
    $this->Masterpage_service->display($content, $title, 'reward');
  }
  public function form_post($reward_id){
    $this->Login_service->must_login();
    $data = array(
      'name_th' => $this->input->post('name_th'),
      'name_en' => $this->input->post('name_en'),
      'reward_point' => $this->input->post('reward_point'),
      'main_image' => '',
      'thumb_image' => '',
      'detail_th' => $this->input->post('detail_th'),
      'detail_en' => $this->input->post('detail_en'),
      'enable_status' => $this->input->post('enable_status'),
    );
    $this->load->model(array('Upload_image_model'));
    $this->Upload_image_model->set_thumb(400, 400);
    $image_data = $this->Upload_image_model->upload_image('reward','thumb_image');
    if($image_data['success']){
      $data['main_image'] = $image_data['image_medium'];
      $data['thumb_image'] = $image_data['image_small'];
    }
    if($reward_id == 0){
      $reward_id = $this->Reward_model->insert($data);
    }else{
      $this->Reward_model->update($reward_id, $data);
    }
    redirect('reward');
  }
  public function detail($reward_id){
    $this->Login_service->must_login();
    $reward = $this->Reward_model->get_data($reward_id);
    $title = $reward->name_th;
    $content = $this->load->view('reward/detail', array(
      'reward_id' => $reward_id,
      'reward' => $reward,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/reward/detail.js');
    $this->Masterpage_service->display($content, $title, 'reward');
  }
  public function delete($reward_id){
    $this->Login_service->must_login();
    $this->Reward_model->delete($reward_id);
    redirect('reward/');
  }
  public function upload_image(){
    $content_image = '';
    $this->load->model('Upload_image_model');
    $image_data = $this->Upload_image_model->upload_single_image('reward_detail', 'file');
    if($image_data['success']){
      $content_image = $image_data['image_file'];
    }
    echo base_url('uploads/'.$content_image);
  }
}
