<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Popup extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model(array('Config_model', 'Upload_image_model'));
  }
  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('popup/index', array(), true);
    $this->Masterpage_service->add_js('/assets_admin/js/popup/form.js');
    $this->Masterpage_service->display($content, 'Popup', 'popup');
  }
  public function form_post(){
    $this->Config_model->update('popup_url', $this->input->post('popup_url'));
    $this->Config_model->update('popup_status', $this->input->post('popup_status'));
    $upload_data = $this->Upload_image_model->upload_single_image('popup_th', 'popup_image_th');
    if($upload_data['success']){
      $this->Config_model->update('popup_image_th', $upload_data['image_file']);
    }
    $upload_data = $this->Upload_image_model->upload_single_image('popup_en', 'popup_image_en');
    if($upload_data['success']){
      $this->Config_model->update('popup_image_en', $upload_data['image_file']);
    }
    redirect('popup/index');
  }
}
