<?php
class Contact_group extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->Login_service->must_login();
    $this->load->model(array('contact/Contact_group_model'));
  }
  public function index(){
    $content = $this->load->view('contact_group/list', array(
      'contact_group_list' => $this->Contact_group_model->get_list()
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/contact_group/list.js');
    $this->Masterpage_service->display($content, 'หมวดหัวข้อคำถาม', 'contact_group');
  }
  public function form($contact_group_id = 0){
    $this->Login_service->must_login();
    $title = (($contact_group_id == 0)?'เพิ่ม':'แก้ไข').'หมวดหัวข้อคำถาม';
    $contact_group = $this->Contact_group_model->get_data($contact_group_id);
    $content = $this->load->view('contact_group/form', array(
      'contact_group_id' => $contact_group_id,
      'contact_group' => $contact_group,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/contact_group/form.js');
    $this->Masterpage_service->display($content, $title, 'contact_group');
  }
  public function form_post($contact_group_id){
    $data = array(
      'contact_group_th' => $this->input->post('contact_group_th'),
      'contact_group_en' => $this->input->post('contact_group_en'),
      'enable_status' => $this->input->post('enable_status'),
      'sort_priority' => $this->input->post('sort_priority')
    );
    if($contact_group_id == 0){
      $contact_group_id = $this->Contact_group_model->insert($data);
    }else{
      $this->Contact_group_model->update($contact_group_id, $data);
    }
    redirect('contact_group');
  }
  public function delete($contact_group_id){
    $this->Contact_group_model->delete($contact_group_id);
    redirect('contact_group/');
  }
}
