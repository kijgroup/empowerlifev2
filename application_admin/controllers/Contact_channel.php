<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_channel extends MY_Controller{
  public $page_name = 'ช่องทางการติดต่อ';
  public $controller = 'Contact_channel';
  public $nav = 'contact_channel';
  public $parent_nav_name = 'การจัดการ';
  public $list_display = array('contact_channel_name');
  public $display_form = array(
    array(
      'panel_name' => 'ข้อมูลช่องทางการติดต่อ',
      'item_list' => array(
        'contact_channel_name',
        'sort_priority',
        'create_date',
        'update_date'
      ),
    )
  );
  public $breadcrumb_list = array(
    array(
      'url' => '',
      'name' => 'การจัดการ',
    ),
    array(
      'url' => 'contact_channel',
      'name' => 'ช่องทางการติดต่อ',
    )
  );
  public function __construct(){
    parent::__construct();
    $this->load->model('order/Contact_channel_model');
    $this->Main_model = $this->Contact_channel_model;
    parent::prepare_data();
  }
}
