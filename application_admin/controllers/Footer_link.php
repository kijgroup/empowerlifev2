<?php
class Footer_link extends CI_Controller{

  function __construct(){
    parent::__construct();
    $this->Login_service->must_login();
    $this->load->model(array('footer_link/Footer_link_model'));
  }

  public function index(){
    $content = $this->load->view('footer_link/list', array(
      'footer_link_list' => $this->Footer_link_model->get_list()
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/footer_link/list.js');
    $this->Masterpage_service->display($content, 'หมวดคำถาม', 'footer_link');
  }

  public function form($footer_link_id = 0){
    $this->Login_service->must_login();
    $title = (($footer_link_id == 0)?'เพิ่ม':'แก้ไข').'หมวดคำถาม';
    $footer_link = $this->Footer_link_model->get_data($footer_link_id);
    $content = $this->load->view('footer_link/form', array(
      'footer_link_id' => $footer_link_id,
      'footer_link' => $footer_link,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/footer_link/form.js');
    $this->Masterpage_service->display($content, $title, 'footer_link');
  }

  public function form_post($footer_link_id){
    $data = array(
      'name' => $this->input->post('name'),
      'link_icon' => '',
      'link_url' => $this->input->post('link_url'),
      'enable_status' => $this->input->post('enable_status'),
      'sort_priority' => $this->input->post('sort_priority')
    );
    $this->load->model(array('Upload_image_model'));
    $image_data = $this->Upload_image_model->upload_single_image('footer_link','link_icon');
    if($image_data['success']){
      $data['link_icon'] = $image_data['image_file'];
    }
    if($footer_link_id == 0){
      $footer_link_id = $this->Footer_link_model->insert($data);
    }else{
      $this->Footer_link_model->update($footer_link_id, $data);
    }
    redirect('footer_link');
  }

  public function delete($footer_link_id){
    $this->Footer_link_model->delete($footer_link_id);
    redirect('footer_link/');
  }
}
