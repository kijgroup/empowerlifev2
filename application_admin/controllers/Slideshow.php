<?php
class Slideshow extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model(array('slideshow/Slideshow_model', 'slideshow/Enable_status_model'));
  }
  public function index($page){
    $this->Login_service->must_login();
    $title = $page;
    $content = $this->load->view('slideshow/list', array(
      'page' => $page,
      'slideshow_list' => $this->Slideshow_model->get_list($page),
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/slideshow/list.js');
    $this->Masterpage_service->display($content, $title, 'slideshow_'.$page);
  }
  public function form($page, $slideshow_id = 0){
    $this->Login_service->must_login();
    $title = (($slideshow_id == 0)?'เพิ่ม':'แก้ไข').' Slideshow';
    $slideshow = $this->Slideshow_model->get_data($slideshow_id);
    $content = $this->load->view('slideshow/form', array(
      'page' => $page,
      'slideshow_id' => $slideshow_id,
      'slideshow' => $slideshow,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/slideshow/form.js');
    $this->Masterpage_service->display($content, $title, 'slideshow_'.$page);
  }
  public function form_post($page, $slideshow_id){
    $this->Login_service->must_login();
    $data = array(
      'slideshow_text' => $this->input->post('slideshow_text'),
      'slideshow_url' => $this->input->post('slideshow_url'),
      'enable_status' => $this->input->post('enable_status'),
      'sort_priority' => $this->input->post('sort_priority'),
    );
    if($slideshow_id == 0){
      $data['page'] = $page;
      $this->load->model(array('Upload_image_model'));
      $image_data = $this->Upload_image_model->upload_single_image('slideshow_'.$page,'slideshow_image');
      if($image_data['success']){
        $data['slideshow_image'] = $image_data['image_file'];
        $slideshow_id = $this->Slideshow_model->insert($data);
      }
    }else{
      $this->Slideshow_model->update($slideshow_id, $data);
    }
    redirect('slideshow/index/'.$page);
  }
  public function delete($page, $slideshow_id){
    $this->Login_service->must_login();
    $slideshow_id = $this->Slideshow_model->delete($slideshow_id);
    redirect('slideshow/index/'.$page);
  }
}
