<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_member_type extends CI_Migration {
  public function up(){
    $this->create_member_type_table();
    $this->add_member_types();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_member_type');
  }
  private function create_member_type_table(){
    $this->dbforge->add_field(array(
      'member_type_code' => array(
        'type' => 'CHAR',
        'constraint' => 20
      ),
      'member_type_name' => array(
        'type' => 'VARCHAR',
        'constraint' => 255
      ),
      'require_amount' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE
      ),
      'convert_point' => array(
        'type' => 'INT'
      ),
      'discount' => array(
        'type' => 'DECIMAL',
        'constraint' => '19,4',
        'default' => 0.0
      ),
      'min_amount' => array(
        'type' => 'BIGINT',
        'contraint' => 20,
        'unsigned' => TRUE
      ),
      'step' => array(
        'type' => 'INT'
      ),
      'label_class' => array(
        'type' => 'VARCHAR',
        'constraint' => 255
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('member_type_code', TRUE);
    $this->dbforge->add_key(array('step'));
    $this->dbforge->create_table('tbl_member_type');
  }
  private function add_member_types(){
    $this->add_member_type('normal', 'Normal', 0, 25, 5, 300, 1, 'label label-default');
    $this->add_member_type('silver', 'Silver', 30000, 25, 8, 0, 2, 'label label-info');
    $this->add_member_type('gold', 'Gold', 60000, 25, 10, 0, 3, 'label label-warning');
    $this->add_member_type('diamond', 'Diamond', 100000, 20, 12, 0, 4, 'label label-primary');
  }
  private function add_member_type($member_type_code, $member_type_name, $require_amount, $convert_point, $discount, $min_amount, $step, $label_class){
    $this->db->set('member_type_code', $member_type_code);
    $this->db->set('member_type_name', $member_type_name);
    $this->db->set('require_amount', $require_amount);
    $this->db->set('convert_point', $convert_point);
    $this->db->set('discount', $discount);
    $this->db->set('min_amount', $min_amount);
    $this->db->set('step', $step);
    $this->db->set('label_class', $label_class);
    $this->db->insert('tbl_member_type');
  }
}
