<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_order_amphur extends CI_Migration {
  public function up(){
    $this->dbforge->add_column('tbl_order',array(
      'shipping_amphur' => array(
        'type' => 'VARCHAR',
        'constraint' => 255
      )
    ), 'shipping_district');
    $this->dbforge->add_column('tbl_order',array(
      'tax_amphur' => array(
        'type' => 'VARCHAR',
        'constraint' => 255
      )
    ), 'tax_district');
  }
  public function down(){
    $this->dbforge->drop_column('tbl_order', 'shipping_amphur');
    $this->dbforge->drop_column('tbl_order', 'tax_amphur');
  }
}
