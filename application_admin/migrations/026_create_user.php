<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_user extends CI_Migration {
  public function up(){
    $this->create_user_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_user');
  }
  private function create_user_table(){
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'member_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE
      ),
      'email' => array(
        'type' => 'CHAR',
        'constraint' => 255
      ),
      'login_password' => array(
        'type' => 'CHAR',
        'constraint' => 255
      ),
      'facebook_id' => array(
        'type' => 'CHAR',
        'constraint' => 50
      ),
      'facebook_url' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'google_id' => array(
        'type' => 'CHAR',
        'constraint' => 50
      ),
      'google_url' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'avata_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'last_login' => array(
        'type' => 'DATETIME'
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('user_id', TRUE);
    $this->dbforge->add_key(array('email', 'login_password', 'enable_status', 'is_delete'));
    $this->dbforge->create_table('tbl_user');
  }
}
