<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_category extends CI_Migration {
  public function up(){
    $this->create_category_table();
    $this->add_category();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_category');
  }
  private function create_category_table(){
    $this->dbforge->add_field(array(
      'category_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'name_th' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'name_en' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'slug' => array(
        'type' => 'CHAR',
        'constraint' => '50',
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show'
      ),
      'sort_priority' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('category_id', TRUE);
    $this->dbforge->add_key(array('slug', 'enable_status', 'sort_priority', 'is_delete'));
    $this->dbforge->create_table('tbl_category');
  }
  private function add_category(){
    $this->add_to_db(array(
      'name_th' => 'Ze-Mont',
      'name_en' => 'Ze-Mont',
      'slug' => 'ze-mont',
      'sort_priority' => 1,
    ));
    $this->add_to_db(array(
      'name_th' => 'ซีราเคิล',
      'name_en' => 'Ze-Racle',
      'slug' => 'ze-racle',
      'sort_priority' => 2,
    ));
    $this->add_to_db(array(
      'name_th' => 'ซีออยล์',
      'name_en' => 'Ze-Oil',
      'slug' => 'ze-oil',
      'sort_priority' => 3,
    ));
    $this->add_to_db(array(
      'name_th' => 'ซีเบต้า',
      'name_en' => 'Ze-Beta',
      'slug' => 'ze-beta',
      'sort_priority' => 4,
    ));
    $this->add_to_db(array(
      'name_th' => 'ลอริค',
      'name_en' => 'Lauric',
      'slug' => 'lauric',
      'sort_priority' => 5,
    ));
  }
  private function add_to_db($data){
    $this->db->set('name_th', $data['name_th']);
    $this->db->set('name_en', $data['name_en']);
    $this->db->set('slug', $data['slug']);
    $this->db->set('enable_status', 'show');
    $this->db->set('sort_priority', $data['sort_priority']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_category');
  }
}
