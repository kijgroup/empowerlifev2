<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_postcode extends CI_Migration{
  public function up(){
    $sql = $this->load->file('zipcode.sql',true);
    $sqls = explode(';', $sql);
    array_pop($sqls);
    foreach($sqls as $statement){
      $statment = $statement . ";";
      $this->db->query($statement);
    }
  }
  public function down(){
    $this->dbforge->drop_table('zipcode');
  }
}