<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_price_before_discount extends CI_Migration{
  public function up(){
    $this->dbforge->add_column('tbl_product',array(
      'price_before_discount' => array(
        'type' => 'int',
        'default' => 0,
        'after' => 'price'
      ),
      'price_discount_rate' => array(
        'type' => 'float',
        'default' => 0,
        'after' => 'price_before_discount'
      )
    ));
  }
  public function down(){
    $this->dbforge->drop_column('tbl_product', 'price_before_discount');
    $this->dbforge->drop_column('tbl_product', 'price_discount_rate');
  }
  
}