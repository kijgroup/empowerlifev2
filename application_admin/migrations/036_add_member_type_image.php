<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_add_member_type_image extends CI_Migration {
  public function up(){
    $this->update_member_type_table();
  }
  public function down(){
    $this->dbforge->drop_column('tbl_member_type', 'image_member_type');
  }
  private function update_member_type_table(){
    $this->dbforge->add_column('tbl_member_type',array(
      'member_type_image' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
    ));
  }
}
