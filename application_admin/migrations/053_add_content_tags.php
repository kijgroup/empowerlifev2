<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_content_tags extends CI_Migration{
  public function up(){
    $this->dbforge->add_column('tbl_content',array(
      'tags' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
        'after' => 'content_en'
      )
    ));
  }
  public function down(){
    $this->dbforge->drop_column('tbl_content', 'tags');
  }
}