<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_order_fields extends CI_Migration{
  public function up(){
    $this->create_stock_table();
    $this->create_order_sub_channel_table();
    $this->create_contact_channel_table();
    $this->create_shipping_type_table();
    $this->add_order_fields();
    $this->add_stock_list();
    $this->add_order_sub_channel_list();
    $this->add_contact_channel_list();
    $this->add_shipping_type_list();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_stock');
    $this->dbforge->drop_table('tbl_order_sub_channel');
    $this->dbforge->drop_table('tbl_contact_channel');
    $this->dbforge->drop_table('tbl_shipping_type');
    $this->dbforge->drop_column('tbl_order', 'stock_id');
    $this->dbforge->drop_column('tbl_order', 'order_sub_channel_id');
    $this->dbforge->drop_column('tbl_order', 'order_channel_text');
    $this->dbforge->drop_column('tbl_order', 'contact_channel_id');
    $this->dbforge->drop_column('tbl_order', 'send_time_status');
    $this->dbforge->drop_column('tbl_order', 'payment_status');
    $this->dbforge->drop_column('tbl_order', 'shipping_type_id');
  }
  private function add_order_fields(){
    $this->dbforge->add_column('tbl_order',array(
      'stock_id' => array(
        'type' => 'INT',
        'default' => 0,
        'after' => 'bank_id'
      ),
      'order_sub_channel_id' => array(
        'type' => 'INT',
        'default' => 0,
        'after' => 'order_channel_id'
      ),
      'order_channel_text' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
        'default' => '',
        'after' => 'order_sub_channel_id'
      ),
      'contact_channel_id' => array(
        'type' => 'INT',
        'default' => 0,
        'after' => 'order_sub_channel_id'
      ),
      'send_time_status' => array(
        'type' => 'ENUM',
        'constraint' => array('', 'morning'),
        'default' => '',
        'after' => 'contact_channel_id'
      ),
      'payment_status' => array(
        'type' => 'ENUM',
        'constraint' => array('unpaid', 'paid'),
        'default' => 'unpaid',
        'after' => 'send_time_status'
      ),
      'shipping_type_id' => array(
        'type' => 'INT',
        'default' => 0,
        'after' => 'payment_type'
      )
    ));
  }
  private function create_stock_table(){
    $this->dbforge->add_field(array(
      'stock_id' => array(
        'type' => 'INT',
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'stock_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('stock_id', TRUE);
    $this->dbforge->add_key(array('is_delete'));
    $this->dbforge->create_table('tbl_stock');
  }
  private function create_order_sub_channel_table(){
    $this->dbforge->add_field(array(
      'order_sub_channel_id' => array(
        'type' => 'INT',
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'order_sub_channel_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('order_sub_channel_id', TRUE);
    $this->dbforge->add_key(array('is_delete'));
    $this->dbforge->create_table('tbl_order_sub_channel');
  }
  private function create_contact_channel_table(){
    $this->dbforge->add_field(array(
      'contact_channel_id' => array(
        'type' => 'INT',
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'contact_channel_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('contact_channel_id', TRUE);
    $this->dbforge->add_key(array('is_delete'));
    $this->dbforge->create_table('tbl_contact_channel');
  }
  private function create_shipping_type_table(){
    $this->dbforge->add_field(array(
      'shipping_type_id' => array(
        'type' => 'INT',
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'shipping_type_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('shipping_type_id', TRUE);
    $this->dbforge->add_key(array('is_delete'));
    $this->dbforge->create_table('tbl_shipping_type');
  }
  
  private function add_stock_list(){
    $this->add_stock('พกง.', 1);
    $this->add_stock('โอนเงิน', 2);
    $this->add_stock('หน้าร้าน', 3);
    $this->add_stock('Lazada', 4);
    $this->add_stock('KTC', 5);
  }
  private function add_stock($stock_name, $sort_priority){
    $this->db->set('stock_name', $stock_name);
    $this->db->set('sort_priority', $sort_priority);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_stock');
  }
  private function add_order_sub_channel_list(){
    $this->add_order_sub_channel('ลูกค้าเก่า', 1);
    $this->add_order_sub_channel('Line@', 2);
    $this->add_order_sub_channel('Facebook', 3);
    $this->add_order_sub_channel('Website', 4);
    $this->add_order_sub_channel('Youtube', 5);
    $this->add_order_sub_channel('Lazada', 6);
    $this->add_order_sub_channel('Shopee', 7);
    $this->add_order_sub_channel('ลูกค้าถุง', 8);
    $this->add_order_sub_channel('ตัวแทนถุง', 9);
    $this->add_order_sub_channel('เทคโน-สยาม', 10);
    $this->add_order_sub_channel('เบญจวรรณ', 11);
    $this->add_order_sub_channel('บูธ: (โปรดระบุ)', 12);
    $this->add_order_sub_channel('KTC', 13);
    $this->add_order_sub_channel('Smile Green', 14);
    $this->add_order_sub_channel('Thai Bio', 15);
    $this->add_order_sub_channel('Ze-Oil Thailand(คุณส้ม)', 16);
    $this->add_order_sub_channel('ตัวแทนอื่นๆ (โปรดระบุ)', 17);
    $this->add_order_sub_channel('TV', 18);
    $this->add_order_sub_channel('Share and Care', 19);
    $this->add_order_sub_channel('นิตยสาร', 20);
    $this->add_order_sub_channel('Event', 21);
    $this->add_order_sub_channel('เพื่อนแนะนำ', 22);
    $this->add_order_sub_channel('พระ', 23);
    $this->add_order_sub_channel('พนักงาน', 24);
    $this->add_order_sub_channel('ตัวอย่าง', 25);
  }
  private function add_order_sub_channel($order_sub_channel_name, $sort_priority){
    $this->db->set('order_sub_channel_name', $order_sub_channel_name);
    $this->db->set('sort_priority', $sort_priority);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_order_sub_channel');
  }
  private function add_contact_channel_list(){
    $this->add_contact_channel('เบอร์ออฟฟิส', 1);
    $this->add_contact_channel('เครื่องแดง', 2);
    $this->add_contact_channel('Facebook Chat', 3);
    $this->add_contact_channel('Line@', 4);
  }
  private function add_contact_channel($contact_channel_name, $sort_priority){
    $this->db->set('contact_channel_name', $contact_channel_name);
    $this->db->set('sort_priority', $sort_priority);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_contact_channel');
  }
  private function add_shipping_type_list(){
    $this->add_shipping_type('พกง.(ชำระหน้าบ้าน)', 1);
    $this->add_shipping_type('DHL', 2);
    $this->add_shipping_type('EMS', 3);
    $this->add_shipping_type('หน้าร้าน', 4);
    $this->add_shipping_type('EMPL จัดส่ง', 5);
  }
  private function add_shipping_type($shipping_type_name, $sort_priority){
    $this->db->set('shipping_type_name', $shipping_type_name);
    $this->db->set('sort_priority', $sort_priority);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_shipping_type');
  }
}