<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_product_image extends CI_Migration {
  public function up(){
    $this->create_product_image_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_product_image');
  }
  private function create_product_image_table(){
    $this->dbforge->add_field(array(
      'product_image_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'product_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE
      ),
      'main_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'thumb_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'sort_priority' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('product_image_id', TRUE);
    $this->dbforge->add_key(array('product_id', 'enable_status', 'sort_priority', 'is_delete'));
    $this->dbforge->create_table('tbl_product_image');
  }
}
