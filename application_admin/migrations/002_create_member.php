<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_member extends CI_Migration {
  public function up(){
    $this->create_member_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_member');
  }
  private function create_member_table(){
    $this->dbforge->add_field(array(
      'member_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'member_type' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
        'default' => 'normal'
      ),
      'expire_date' => array(
        'type' => 'DATE',
      ),
      'member_point' => array(
        'type' => 'INT',
        'constraint' => 12,
        'unsigned' => TRUE,
      ),
      'facebook_key' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'facebook_email' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'member_email' => array(
        'type' => 'CHAR',
        'constraint' => '50',
      ),
      'member_password' => array(
        'type' => 'CHAR',
        'constraint' => '50',
      ),
      'member_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'member_mobile' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
      ),
      'member_address' => array(
        'type' => 'TEXT'
      ),
      'enable_status' => array(
          'type' => 'ENUM',
          'constraint' => array('active', 'inactive'),
          'default' => 'active'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'last_login_date' => array(
        'type' => 'DATETIME',
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('member_id', TRUE);
    $this->dbforge->add_key(array('member_email', 'member_password', 'is_delete', 'enable_status'));
    $this->dbforge->create_table('tbl_member');
  }
}
