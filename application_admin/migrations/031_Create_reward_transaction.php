<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_reward_transaction extends CI_Migration {
  public function up(){
    $this->create_reward_transaction_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_reward_transaction');
  }
  private function create_reward_transaction_table(){
    $this->dbforge->add_field(array(
      'reward_transaction_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'member_id' => array(
        'type' => 'BIGINT',
        'constraint' => '20',
      ),
      'total_reward_point' => array(
        'type' => 'INT',
        'constraint' => '20',
      ),
      'shipping_name' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'shipping_address' => array(
        'type' => 'TEXT',
      ),
      'shipping_post_code' => array(
        'type' => 'VARCHAR',
        'constraint' => 5,
      ),
      'shipping_district' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'shipping_province' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'shipping_mobile' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'note' => array(
        'type' => 'TEXT'
      ),
      'reward_transaction_status' => array(
        'type' => 'ENUM',
        'constraint' => array('checking', 'done'),
        'default' => 'checking',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active',
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('reward_transaction_id');
    $this->dbforge->add_key(array('member_id','reward_transaction_status','is_delete'));
    $this->dbforge->create_table('tbl_reward_transaction');
  }
}
