<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_content_group extends CI_Migration {
  public function up(){
    $this->create_content_group_table();
    $this->add_content_group();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_content_group');
  }
  private function create_content_group_table(){
    $this->dbforge->add_field(array(
      'content_group_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'name_th' => array(
        'type' => 'VARCHAR',
        'constraint' => '255'
      ),
      'name_en' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'slug' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show'
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('content_group_id', TRUE);
    $this->dbforge->add_key(array('slug', 'enable_status', 'sort_priority', 'is_delete'));
    $this->dbforge->create_table('tbl_content_group');
  }
  private function add_content_group(){
    $this->add_to_db(array(
      'name_th' => 'ข่าวสาร',
      'name_en' => 'News',
      'slug' => 'news',
      'sort_priority' => 1
    ));
    $this->add_to_db(array(
      'name_th' => 'Share&amp;Care',
      'name_en' => 'Share&amp;Care',
      'slug' => 'sharecare',
      'sort_priority' => 2
    ));
  }
  private function add_to_db($data){
    $this->db->set('name_th', $data['name_th']);
    $this->db->set('name_en', $data['name_en']);
    $this->db->set('slug', $data['slug']);
    $this->db->set('enable_status', 'show');
    $this->db->set('sort_priority', $data['sort_priority']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_content_group');
  }
}
