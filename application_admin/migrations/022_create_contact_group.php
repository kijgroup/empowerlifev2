<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_contact_group extends CI_Migration {
  public function up(){
    $this->create_contact_group_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_contact_group');
  }
  private function create_contact_group_table(){
    $this->dbforge->add_field(array(
      'contact_group_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'contact_group_th' => array(
        'type' => 'TEXT',
      ),
      'contact_group_en' => array(
        'type' => 'TEXT',
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show',
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('contact_group_id', TRUE);
    $this->dbforge->add_key(array('enable_status','is_delete'));
    $this->dbforge->create_table('tbl_contact_group');
  }
}
