<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Update_member_code_data extends CI_Migration{
  public function up(){
    $this->update_normal_member();
    $this->update_silver_member();
  }
  public function down(){
  }
  private function update_normal_member(){
    $this->db->set('running_code', 'CONVERT(SUBSTR(member_code, 2),UNSIGNED INTEGER)', false);
    $this->db->like('member_code', 'E', 'after');
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_member');
  }
  private function get_last_running_code(){
    $this->db->select('MAX(running_code) AS max_running_code');
    $query = $this->db->get('tbl_member');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row()->max_running_code;
  }
  private function update_silver_member(){
    $member_list = $this->get_silver_list();
    $next_running_code = $this->get_last_running_code();
    foreach($member_list->result() as $member){
      $next_running_code++;
      $member_code = 'M'.$next_running_code;
      $this->update_member_data($member->member_id, $member_code, $next_running_code);
    }
  }
  private function get_silver_list(){
    $this->db->like('member_code', 'M', 'after');
    $this->db->where('is_delete', 'active');
    $this->db->order_by('member_code', 'ASC');
    return $this->db->get('tbl_member');
  }
  private function update_member_data($member_id, $member_code, $running_code){
    $this->db->set('member_code', $member_code);
    $this->db->set('pre_code', 'M');
    $this->db->set('running_code', $running_code);
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
  }
}