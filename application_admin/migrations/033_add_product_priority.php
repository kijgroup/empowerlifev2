<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_product_priority extends CI_Migration {
  public function up(){
    $this->dbforge->add_column('tbl_product',array(
      'sort_priority' => array(
        'type' => 'INT',
        'default' => 1,
      )
    ), 'enable_status');
  }
  public function down(){
    $this->dbforge->drop_column('tbl_product', 'sort_priority');
  }
}
