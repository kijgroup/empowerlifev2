<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Update_member_offline extends CI_Migration {
  public function up(){
    $this->update_member_table();
  }
  public function down(){
  }
  private function update_member_table(){
    $this->dbforge->modify_column('tbl_member', array(
      'member_name' => array(
        'name' => 'member_firstname',
        'type' => 'VARCHAR',
        'constraint' => 50
      )
    ));
    $this->dbforge->add_column('tbl_member',array(
      'member_code' => array(
        'type' => 'CHAR',
        'constraint' => 50
      )
    ), 'member_id');
    $this->dbforge->add_column('tbl_member',array(
      'buy_total' => array(
        'type' => 'INT'
      )
    ), 'expire_date');
    $this->dbforge->add_column('tbl_member',array(
      'member_lastname' => array(
        'type' => 'VARCHAR',
        'constraint' => 255
      ),
      'member_gender' => array(
        'type' => 'ENUM',
        'constraint' => array('male', 'female', 'undefined'),
        'default' => 'undefined',
      ),
      'member_birthdate' => array(
        'type' => 'DATE'
      ),
      'member_phone' => array(
        'type' => 'VARCHAR',
        'constraint' => 50
      ),
    ),'member_firstname');
    $this->dbforge->add_column('tbl_member',array(
      'member_postcode' => array(
        'type' => 'VARCHAR',
        'constraint' => 50
      )
    ), 'member_address');
    $this->dbforge->add_column('tbl_member',array(
      'last_order_date' => array(
        'type' => 'DATE'
      )
    ), 'is_delete');
  }
}
