<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_bank_data extends CI_Migration {
  public function up(){
    $this->add_banks();
  }
  public function down(){
  }
  private function add_banks(){
    $this->insert_bank('ธนาคารกสิกรไทย','KBANK','kbank_icon.png','ชื่อบัญชี : บจก. เอ็มพาวเวอร์ไลฟ์ เลขที่บัญชี : 765-2-38383-6','ชื่อบัญชี : บจก. เอ็มพาวเวอร์ไลฟ์ เลขที่บัญชี : 765-2-38383-6','show', 1);
    $this->insert_bank('ธนาคารไทยพาณิชย์','SCB','scb_icon.png','ชื่อบัญชี : บจก. เอ็มพาวเวอร์ไลฟ์ เลขที่บัญชี : 170-2-43070-1','ชื่อบัญชี : บจก. เอ็มพาวเวอร์ไลฟ์ เลขที่บัญชี : 170-2-43070-1','show', 2);
    $this->insert_bank('ธนาคารกรุงเทพ','BBL','bbl_icon.png','ชื่อบัญชี : บจก. เอ็มพาวเวอร์ไลฟ์ เลขที่บัญชี : 245-0-52883-7','ชื่อบัญชี : บจก. เอ็มพาวเวอร์ไลฟ์ เลขที่บัญชี : 245-0-52883-7','show', 3);
    $this->insert_bank('ธนาคารกรุงไทย','KTB','ktb_icon.png','ชื่อบัญชี : บจก. เอ็มพาวเวอร์ไลฟ์ เลขที่บัญชี : 878-0-31539-9','ชื่อบัญชี : บจก. เอ็มพาวเวอร์ไลฟ์ เลขที่บัญชี : 878-0-31539-9','show', 4);
  }
  private function insert_bank($bank_name_th, $bank_name_en, $bank_name_icon, $bank_info_th, $bank_info_en, $enable_status, $sort_priority){
    $this->db->set('bank_name_th', $bank_name_th);
    $this->db->set('bank_name_en', $bank_name_en);
    $this->db->set('bank_name_icon', $bank_name_icon);
    $this->db->set('bank_info_th', $bank_info_th);
    $this->db->set('bank_info_en', $bank_info_en);
    $this->db->set('enable_status', $enable_status);
    $this->db->set('sort_priority', $sort_priority);
    $this->db->insert('tbl_bank');
  }
}
