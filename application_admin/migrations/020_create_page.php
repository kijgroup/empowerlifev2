<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_page extends CI_Migration {
  public function up(){
    $this->create_page_table();
    $this->create_page_data();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_page');
  }
  private function create_page_table(){
    $this->dbforge->add_field(array(
      'page_code' => array(
        'type' => 'CHAR',
        'constraint' => 50,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'page_name_th' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'page_name_en' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'page_content_th' => array(
        'type' => 'TEXT',
      ),
      'page_content_en' => array(
        'type' => 'TEXT',
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('page_code', TRUE);
    $this->dbforge->create_table('tbl_page');
  }
  private function create_page_data(){
    $this->insert_page('about','เกี่ยวกับเรา','About US');
    $this->insert_page('privacy','Privacy policy','Privacy policy');
    $this->insert_page('terms','Terms of Service','Terms of Service');
  }
  private function insert_page($page_code, $page_name_th, $page_name_en){
    $this->db->set('page_code', $page_code);
    $this->db->set('page_name_th', $page_name_th);
    $this->db->set('page_name_en', $page_name_en);
    $this->db->insert('tbl_page');
  }
}
