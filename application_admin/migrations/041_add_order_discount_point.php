<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_order_discount_point extends CI_Migration {
  public function up(){
    $this->dbforge->add_column('tbl_order',array(
      'order_channel_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'default' => 1
      )
    ), 'order_code_running');
    $this->dbforge->add_column('tbl_order',array(
      'discount_point_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'default' => 0
      )
    ), 'order_channel_id');
    $this->dbforge->add_column('tbl_order',array(
      'discount_point_amount' => array(
        'type' => 'FLOAT',
        'default' => 0
      )
    ), 'discount_point_id');
    $this->dbforge->add_column('tbl_order',array(
      'discount_by_point_amount' => array(
        'type' => 'FLOAT',
        'default' => 0
      )
    ), 'discount_price');
  }
  public function down(){
    $this->dbforge->drop_column('tbl_order', 'order_channel_id');
    $this->dbforge->drop_column('tbl_order', 'discount_point_id');
    $this->dbforge->drop_column('tbl_order', 'discount_point_amount');
    $this->dbforge->drop_column('tbl_order', 'discount_by_point_amount');
  }
}
