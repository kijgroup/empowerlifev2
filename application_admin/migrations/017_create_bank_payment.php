<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_bank_payment extends CI_Migration {
  public function up(){
    $this->create_payment_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_bank_payment');
  }
  private function create_payment_table(){
    $this->dbforge->add_field(array(
      'bank_payment_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'payment_id' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'bank_id' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'order_id' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'member_id' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'bank_payment_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'bank_payment_date' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'bank_payment_time' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'bank_payment_amount' => array(
        'type' => 'FLOAT',
      ),
      'bank_payment_phone' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'order_code' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'bank_payment_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'bank_payment_status' => array(
          'type' => 'ENUM',
          'constraint' => array('checking', 'complete','cancel'),
          'default' => 'checking',
      ),
      'bank_payment_note' => array(
        'type' => 'TEXT',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('bank_payment_id', TRUE);
    $this->dbforge->add_key(array('order_id','bank_id','bank_payment_status','is_delete'));
    $this->dbforge->create_table('tbl_bank_payment');
  }
}
