<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_page_2 extends CI_Migration {
  public function up(){
    $this->create_page_data();
  }
  public function down(){
  }
  private function create_page_data(){
    $this->insert_page('verify','รับประกันคุณภาพ','รับประกันคุณภาพ');
    $this->insert_page('oem','OEM','OEM');
    $this->insert_page('joinus','ร่วมงานกับเรา','ร่วมงานกับเรา');
    $this->insert_page('send_policy','นโยบายการส่ง','นโยบายการส่ง');
    $this->insert_page('return_policy','เงื่อนไขการส่งคืนสินค้า','เงื่อนไขการส่งคืนสินค้า');
  }
  private function insert_page($page_code, $page_name_th, $page_name_en){
    $this->db->set('page_code', $page_code);
    $this->db->set('page_name_th', $page_name_th);
    $this->db->set('page_name_en', $page_name_en);
    $this->db->insert('tbl_page');
  }
}
