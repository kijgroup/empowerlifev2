<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_add_member_note extends CI_Migration {
  public function up(){
    $this->update_member_table();
  }
  public function down(){
    $this->dbforge->drop_column('tbl_member', 'note');
  }
  private function update_member_table(){
    $this->dbforge->add_column('tbl_member',array(
      'note' => array(
        'type' => 'TEXT'
      )
    ), 'member_postcode');
  }
}
