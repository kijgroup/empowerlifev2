<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_member_type_pre_code extends CI_Migration{
  public function up(){
    $this->dbforge->add_column('tbl_member_type',array(
      'pre_code' => array(
        'type' => 'VARCHAR',
        'constraint' => 5,
        'default' => 'E',
        'after' => 'member_type_code'
      )
    ));
    $this->update_data();
  }
  public function down(){
    $this->dbforge->drop_column('tbl_member_type', 'pre_code');
  }
  private function update_data(){
    $this->update_type('normal', 'E');
    $this->update_type('silver', 'M');
    $this->update_type('gold', 'P');
    $this->update_type('diamond', 'L');
  }
  private function update_type($member_type_code, $pre_code){
    $this->db->set('pre_code', $pre_code);
    $this->db->where('member_type_code', $member_type_code);
    $this->db->update('tbl_member_type');
  }
}