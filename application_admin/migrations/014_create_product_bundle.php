<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_product_bundle extends CI_Migration {
  public function up(){
    $this->create_product_bundle_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_product_bundle');
  }
  private function create_product_bundle_table(){
    $this->dbforge->add_field(array(
      'product_bundle_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'product_id' => array(
        'type' => 'BIGINT',
        'constraint' => '20',
      ),
      'bundle_id' => array(
        'type' => 'BIGINT',
        'constraint' => '20',
      ),
      'qty' => array(
        'type' => 'INT',
        'constraint' => '12',
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('product_bundle_id', TRUE);
    $this->dbforge->add_key(array('product_id'));
    $this->dbforge->create_table('tbl_product_bundle');
  }
}
