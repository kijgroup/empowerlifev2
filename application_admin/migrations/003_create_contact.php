<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_contact extends CI_Migration {
  public function up(){
    $this->create_contact_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_contact');
  }
  private function create_contact_table(){
    $this->dbforge->add_field(array(
      'contact_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'member_id' => array(
        'type' => 'INT',
        'default' => '0'
      ),
      'contact_group_id' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'contact_name' => array(
        'type' => 'CHAR',
        'constraint' => '255',
      ),
      'contact_email' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'contact_phone' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
      ),
      'contact_detail' => array(
        'type' => 'TEXT',
      ),
      'is_read' => array(
        'type' => 'ENUM',
        'constraint' => array('unread', 'read'),
        'default' => 'unread'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('contact_id', TRUE);
    $this->dbforge->add_key(array('member_id', 'is_delete'));
    $this->dbforge->create_table('tbl_contact');
  }
}
