<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_content extends CI_Migration {
  public function up(){
    $this->create_content_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_content');
  }
  private function create_content_table(){
    $this->dbforge->add_field(array(
      'content_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'content_group_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE
      ),
      'subject_th' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'subject_en' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'thumb_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'short_content_th' => array(
        'type' => 'TEXT'
      ),
      'short_content_en' => array(
        'type' => 'TEXT'
      ),
      'content_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'content_th' => array(
        'type' => 'TEXT'
      ),
      'content_en' => array(
        'type' => 'TEXT'
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show'
      ),
      'sort_priority' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('content_id', TRUE);
    $this->dbforge->add_key(array('enable_status', 'sort_priority', 'is_delete'));
    $this->dbforge->create_table('tbl_content');
  }
}
