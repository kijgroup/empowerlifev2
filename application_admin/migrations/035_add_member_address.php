<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_add_member_address extends CI_Migration {
  public function up(){
    $this->update_member_table();
  }
  public function down(){
    $this->dbforge->drop_column('tbl_member', 'member_district');
    $this->dbforge->drop_column('tbl_member', 'member_amphur');
    $this->dbforge->drop_column('tbl_member', 'member_province');
  }
  private function update_member_table(){
    $this->dbforge->add_column('tbl_member',array(
      'member_district' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'member_amphur' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'member_province' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
    ));
  }
}
