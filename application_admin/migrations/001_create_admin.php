<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_admin extends CI_Migration {
  public function up(){
    $this->create_log_table();
    $this->create_admin_table();
    $this->add_admin();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_log_action');
    $this->dbforge->drop_table('tbl_admin');
  }
  private function create_log_table(){
    $this->dbforge->add_field(array(
      'log_action_id' => array(
        'type' => 'BIGINT',
        'constraint' => '20',
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'admin_id' => array(
        'type' => 'INT',
        'constraint' => '5',
        'unsigned' => TRUE,
      ),
      'log_action_page' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'page_id' => array(
        'type' => 'BIGINT',
        'constraint' => '20',
        'unsigned' => TRUE,
      ),
      'log_action_type' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'log_action_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'log_action_detail' => array(
        'type' => 'TEXT',
      ),
      'log_action_query' => array(
        'type' => 'TEXT',
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
    ));
    $this->dbforge->add_key('log_action_id', TRUE);
    $this->dbforge->add_key(array('log_action_page', 'page_id'));
    $this->dbforge->create_table('tbl_log_action');
  }
  private function create_admin_table(){
    $this->dbforge->add_field(array(
      'admin_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'admin_username' => array(
        'type' => 'CHAR',
        'constraint' => '50',
      ),
      'admin_password' => array(
        'type' => 'CHAR',
        'constraint' => '50',
      ),
      'admin_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'last_login_date' => array(
        'type' => 'DATETIME',
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('admin_id', TRUE);
    $this->dbforge->add_key(array('admin_username', 'is_delete'));
    $this->dbforge->create_table('tbl_admin');
  }
  private function add_admin(){
    $this->load->model('admin/Admin_model');
    $admin_password = $this->Admin_model->encrypt_password('emp0wer');
    $this->db->set('admin_username', 'admin');
    $this->db->set('admin_password', $admin_password);
    $this->db->set('admin_name', 'Administrator');
    $this->db->set('enable_status', 'show');
    $this->db->set('create_date', 'now()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_admin');
  }
}
