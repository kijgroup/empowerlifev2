<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_product extends CI_Migration {
  public function up(){
    $this->create_product_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_product');
  }
  private function create_product_table(){
    $this->dbforge->add_field(array(
      'product_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'product_type' => array(
        'type' => 'ENUM',
        'constraint' => array('product', 'promotion'),
        'default' => 'product'
      ),
      'category_id' => array(
        'type' => 'INT',
        'default' => '0'
      ),
      'is_bundle' => array(
        'type' => 'ENUM',
        'constraint' => array('true', 'false'),
        'default' => 'false'
      ),
      'is_expire' => array(
        'type' => 'ENUM',
        'constraint' => array('true', 'false'),
        'default' => 'false'
      ),
      'expire_date' => array(
        'type' => 'DATETIME',
      ),
      'name_th' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'name_en' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'slug' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'main_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
      ),
      'thumb_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
      ),
      'detail_th' => array(
        'type' => 'TEXT'
      ),
      'detail_en' => array(
        'type' => 'TEXT'
      ),
      'price' => array(
        'type' => 'INT',
        'constraint' => 20,
        'unsigned' => TRUE,
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('product_id', TRUE);
    $this->dbforge->add_key(array('product_type', 'enable_status', 'is_delete'));
    $this->dbforge->create_table('tbl_product');
  }
}
