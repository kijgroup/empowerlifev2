<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_reward_transaction_item extends CI_Migration {
  public function up(){
    $this->create_reward_transaction_item_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_reward_transaction_item');
  }
  private function create_reward_transaction_item_table(){
    $this->dbforge->add_field(array(
      'reward_transaction_item_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'reward_transaction_id' => array(
        'type' => 'BIGINT',
        'constraint' => '20',
      ),
      'reward_id' => array(
        'type' => 'BIGINT',
        'constraint' => '20',
      ),
      'reward_name_en' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'reward_name_th' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'reward_point' => array(
        'type' => 'INT',
        'constraint' => '20',
      ),
      'qty' => array(
        'type' => 'INT',
        'constraint' => '20',
      ),
      'total_reward_point' => array(
        'type' => 'INT',
        'constraint' => '20',
      ),
    ));
    $this->dbforge->add_key('reward_transaction_item_id');
    $this->dbforge->add_key(array('reward_transaction_id','reward_id'));
    $this->dbforge->create_table('tbl_reward_transaction_item');
  }
}
