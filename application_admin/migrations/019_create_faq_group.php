<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_faq_group extends CI_Migration {
  public function up(){
    $this->create_faq_group_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_faq_group');
  }
  private function create_faq_group_table(){
    $this->dbforge->add_field(array(
      'faq_group_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'faq_group_th' => array(
        'type' => 'TEXT',
      ),
      'faq_group_en' => array(
        'type' => 'TEXT',
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show',
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('faq_group_id', TRUE);
    $this->dbforge->add_key(array('enable_status','is_delete'));
    $this->dbforge->create_table('tbl_faq_group');
  }
}
