<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_admin_type extends CI_Migration {
  public function up(){
    $this->dbforge->add_column('tbl_admin',array(
      'admin_type' => array(
        'type' => 'ENUM',
        'constraint' => array('admin', 'agent'),
        'default' => 'admin'
      )
    ), 'admin_id');
  }
  public function down(){
    $this->dbforge->drop_column('tbl_admin', 'admin_type');
  }
}
