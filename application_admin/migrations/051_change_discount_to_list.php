<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Change_discount_to_list extends CI_Migration{
  public function up(){
    $str_sql = '
    INSERT INTO tbl_order_discount_point(order_id, discount_point_id, qty, use_point, discount_amount, total_use_point, total_discount_amount)
    SELECT tbl_order.order_id, tbl_order.discount_point_id, 1, tbl_order.discount_point_amount, tbl_order.discount_by_point_amount, tbl_order.discount_point_amount, tbl_order.discount_by_point_amount
    FROM tbl_order
    WHERE tbl_order.discount_point_id > 0
    AND tbl_order.is_delete = "active"
    ';
    $this->db->query($str_sql);
  }
  public function down(){
  }
  
}