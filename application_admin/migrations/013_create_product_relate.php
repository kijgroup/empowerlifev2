<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_product_relate extends CI_Migration {
  public function up(){
    $this->create_product_relate_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_product_relate');
  }
  private function create_product_relate_table(){
    $this->dbforge->add_field(array(
      'product_relate_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'product_id' => array(
        'type' => 'BIGINT',
        'constraint' => '20',
      ),
      'relate_id' => array(
        'type' => 'BIGINT',
        'constraint' => '20',
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('product_relate_id', TRUE);
    $this->dbforge->add_key(array('product_id'));
    $this->dbforge->create_table('tbl_product_relate');
  }
}
