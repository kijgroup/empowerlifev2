<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_content_image extends CI_Migration {
  public function up(){
    $this->create_content_image_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_content_image');
  }
  private function create_content_image_table(){
    $this->dbforge->add_field(array(
      'content_image_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'content_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE
      ),
      'main_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'thumb_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'sort_priority' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('content_image_id', TRUE);
    $this->dbforge->add_key(array('content_id', 'enable_status', 'sort_priority', 'is_delete'));
    $this->dbforge->create_table('tbl_content_image');
  }
}
