<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_reward_transaction_amphur extends CI_Migration {
  public function up(){
    $this->dbforge->add_column('tbl_reward_transaction',array(
      'shipping_amphur' => array(
        'type' => 'VARCHAR',
        'constraint' => 255
      )
    ), 'shipping_district');
  }
  public function down(){
    $this->dbforge->drop_column('tbl_reward_transaction', 'shipping_amphur');
  }
}
