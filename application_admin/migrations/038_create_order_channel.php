<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_order_channel extends CI_Migration {
  public function up(){
    $this->create_order_channel_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_order_channel');
  }
  private function create_order_channel_table(){
    $this->dbforge->add_field(array(
      'order_channel_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'order_channel_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('order_channel_id', TRUE);
    $this->dbforge->add_key(array('order_channel_name', 'is_delete'));
    $this->dbforge->create_table('tbl_order_channel');
  }
}
