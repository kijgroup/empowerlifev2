<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_postoffice extends CI_Migration{
  public function up(){
    $this->dbforge->add_column('tbl_order',array(
      'postoffice' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
        'after' => 'order_channel_id'
      )
    ));
  }
  public function down(){
    $this->dbforge->drop_column('tbl_order', 'postoffice');
  }
  
}