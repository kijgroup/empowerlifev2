<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_vip_generator_db extends CI_Migration{
  public function up(){
    $this->dbforge->add_column('tbl_member',array(
      'pre_code' => array(
        'type' => 'VARCHAR',
        'constraint' => 10,
        'default' => 'E',
        'after' => 'member_code'
      ),
      'running_code' => array(
        'type' => 'INT',
        'constraint' => 11,
        'default' => 0,
        'after' => 'pre_code'
      )
    ));
  }
  public function down(){
    $this->dbforge->drop_column('tbl_member', 'pre_code');
    $this->dbforge->drop_column('tbl_member', 'running_code');
  }
  
}