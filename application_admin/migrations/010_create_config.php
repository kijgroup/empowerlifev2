<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_config extends CI_Migration {
  public function up(){
    $this->create_config_table();
    $this->create_config_value();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_config');
  }
  private function create_config_table(){
    $this->dbforge->add_field(array(
      'config_code' => array(
        'type' => 'CHAR',
        'constraint' => 50
      ),
      'config_value' => array(
        'type' => 'TEXT'
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('config_code', TRUE);
    $this->dbforge->create_table('tbl_config');
  }
  private function create_config_value(){
    $this->load->model(array('Config_model'));
    $this->Config_model->insert('web_title_th', 'Empower Life');
    $this->Config_model->insert('web_title_en', 'Empower Life');
    $this->Config_model->insert('meta_description', 'Empower Life');
    $this->Config_model->insert('meta_keyword', 'Empower Life');
    $this->Config_model->insert('email_list', 'info@empowerlife.co.th');
    $this->Config_model->insert('email_sender', 'noreply@empowerlife.co.th');
    $this->Config_model->insert('facebook_code', '');
    $this->Config_model->insert('google_analytic', '');
    $this->Config_model->insert('youtube_iframe', '');
    $this->Config_model->insert('popup_image_en', '');
    $this->Config_model->insert('popup_image_th', '');
    $this->Config_model->insert('popup_status', '');
    $this->Config_model->insert('popup_url', '');
  }
}
