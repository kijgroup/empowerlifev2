<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_bank_in_order extends CI_Migration{
  public function up(){
    $this->dbforge->add_column('tbl_order',array(
      'bank_id' => array(
        'type' => 'INT',
        'constraint' => 11,
        'default' => 0
      )
    ), 'order_code_running');
  }
  public function down(){
    $this->dbforge->drop_column('tbl_order', 'bank_id');
  }
}