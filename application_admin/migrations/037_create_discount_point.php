<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_discount_point extends CI_Migration {
  public function up(){
    $this->create_discount_point_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_discount_point');
  }
  private function create_discount_point_table(){
    $this->dbforge->add_field(array(
      'discount_point_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'use_point' => array(
        'type' => 'INT',
        'default' => '0'
      ),
      'discount_amount' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('discount_point_id', TRUE);
    $this->dbforge->add_key(array('enable_status', 'is_delete'));
    $this->dbforge->create_table('tbl_discount_point');
  }
}
