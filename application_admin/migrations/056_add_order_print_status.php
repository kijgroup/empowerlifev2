<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_order_print_status extends CI_Migration{
  public function up(){
    $this->dbforge->add_column('tbl_order',array(
      'is_print' => array(
        'type' => 'ENUM',
        'constraint' => array('true', 'false'),
        'default' => 'false',
        'after' => 'order_status'
      )
    ));
  }
  public function down(){
    $this->dbforge->drop_column('tbl_order', 'is_print');
  }
}