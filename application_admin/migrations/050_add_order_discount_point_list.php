<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_order_discount_point_list extends CI_Migration{
  public function up(){
    $this->create_order_discount_point_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_order_discount_point');
  }
  private function create_order_discount_point_table(){
    $this->dbforge->add_field(array(
      'order_id' => array(
        'type' => 'BIGINT',
        'unsigned' => TRUE,
      ),
      'discount_point_id' => array(
        'type' => 'INT',
        'unsigned' => TRUE,
      ),
      'qty' => array(
        'type' => 'INT',
        'default' => '0'
      ),
      'use_point' => array(
        'type' => 'INT',
        'default' => '0'
      ),
      'discount_amount' => array(
        'type' => 'INT',
        'constraint' => '5',
      ),
      'total_use_point' => array(
        'type' => 'INT',
        'default' => 0
      ),
      'total_discount_amount' => array(
        'type' => 'INT',
        'default' => 0
      ),
    ));
    $this->dbforge->add_key('order_id', TRUE);
    $this->dbforge->add_key('discount_point_id', TRUE);
    $this->dbforge->create_table('tbl_order_discount_point');
  }
}