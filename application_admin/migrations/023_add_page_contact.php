<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_page_contact extends CI_Migration {
  public function up(){
    $this->create_page_data();
  }
  public function down(){
  }
  private function create_page_data(){
    $this->insert_page('contact','ติดต่อเรา','ติดต่อเรา');
  }
  private function insert_page($page_code, $page_name_th, $page_name_en){
    $this->db->set('page_code', $page_code);
    $this->db->set('page_name_th', $page_name_th);
    $this->db->set('page_name_en', $page_name_en);
    $this->db->insert('tbl_page');
  }
}
