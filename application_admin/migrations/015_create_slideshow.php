<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_slideshow extends CI_Migration {
  public function up(){
    $this->create_slideshow_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_slideshow');
  }
  private function create_slideshow_table(){
    $this->dbforge->add_field(array(
      'slideshow_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'page' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'slideshow_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'slideshow_text' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'slideshow_url' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'enable_status' => array(
          'type' => 'ENUM',
          'constraint' => array('show', 'hide'),
          'default' => 'show',
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'constraint' => '12',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('slideshow_id', TRUE);
    $this->dbforge->add_key(array('page','slideshow_image','enable_status','is_delete'));
    $this->dbforge->create_table('tbl_slideshow');
  }
}
