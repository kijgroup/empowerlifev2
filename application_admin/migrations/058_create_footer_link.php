<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_footer_link extends CI_Migration {

  public function up(){
    $this->create_footer_link_table();
  }

  public function down(){
    $this->dbforge->drop_table('tbl_footer_link');
  }

  private function create_footer_link_table(){
    $this->dbforge->add_field(array(
      'footer_link_id' => array(
        'type' => 'INT',
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'link_icon' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'link_url' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'unsigned' => TRUE,
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('footer_link_id', TRUE);
    $this->dbforge->add_key(array('enable_status', 'sort_priority', 'is_delete'));
    $this->dbforge->create_table('tbl_footer_link');
  }
}
