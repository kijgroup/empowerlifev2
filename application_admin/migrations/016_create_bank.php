<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_bank extends CI_Migration {
  public function up(){
    $this->create_bank_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_bank');
  }
  private function create_bank_table(){
    $this->dbforge->add_field(array(
      'bank_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'bank_name_th' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'bank_name_en' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'bank_name_icon' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'bank_info_th' => array(
        'type' => 'TEXT',
      ),
      'bank_info_en' => array(
        'type' => 'TEXT',
      ),
      'enable_status' => array(
          'type' => 'ENUM',
          'constraint' => array('show', 'hide'),
          'default' => 'show',
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'constraint' => '12',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('bank_id', TRUE);
    $this->dbforge->add_key(array('bank_name_th','bank_name_en','enable_status','is_delete'));
    $this->dbforge->create_table('tbl_bank');
  }
}
