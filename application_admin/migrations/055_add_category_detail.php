<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_category_detail extends CI_Migration{
  public function up(){
    $this->dbforge->add_column('tbl_category',array(
      'thumb_image' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
        'default' => '',
        'after' => 'slug'
      ),
      'icon_image' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
        'default' => '',
        'after' => 'thumb_image'
      ),
      'desc_th' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
        'default' => '',
        'after' => 'icon_image'
      ),
      'desc_en' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
        'default' => '',
        'after' => 'desc_th'
      ),
      'content_th' => array(
        'type' => 'TEXT',
        'default' => '',
        'after' => 'desc_en'
      ),
      'content_en' => array(
        'type' => 'TEXT',
        'default' => '',
        'after' => 'content_th'
      ),
      'style' => array(
        'type' => 'TEXT',
        'default' => '',
        'after' => 'content_en'
      )
    ));
  }
  public function down(){
    $this->dbforge->drop_column('tbl_category', 'thumb_image');
    $this->dbforge->drop_column('tbl_category', 'desc_th');
    $this->dbforge->drop_column('tbl_category', 'desc_en');
    $this->dbforge->drop_column('tbl_category', 'content_th');
    $this->dbforge->drop_column('tbl_category', 'content_en');
    $this->dbforge->drop_column('tbl_category', 'style');
  }
}