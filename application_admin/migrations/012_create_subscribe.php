<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_subscribe extends CI_Migration {
  public function up(){
    $this->create_subscribe_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_subscribe');
  }
  private function create_subscribe_table(){
    $this->dbforge->add_field(array(
      'subscribe_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'subscribe_email' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'subscribe_status' => array(
          'type' => 'ENUM',
          'constraint' => array('enable', 'disable'),
          'default' => 'enable'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('subscribe_id', TRUE);
    $this->dbforge->add_key(array('subscribe_email', 'is_delete', 'subscribe_status'));
    $this->dbforge->create_table('tbl_subscribe');
  }
}
