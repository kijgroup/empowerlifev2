<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_thailand_data extends CI_Migration{
  public function up(){
    $sql = $this->load->file('thailand_db.sql',true);
    $sqls = explode(';', $sql);
    array_pop($sqls);
    foreach($sqls as $statement){
      $statment = $statement . ";";
      $this->db->query($statement);
    }
  }
  public function down(){
    $this->dbforge->drop_table('geography');
    $this->dbforge->drop_table('province');
    $this->dbforge->drop_table('amphur');
    $this->dbforge->drop_table('district');
  }
}