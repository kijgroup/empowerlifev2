<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_content_contact extends CI_Migration {

  public function up(){
    $this->add_display_contact();
    $this->create_content_contact_table();
  }

  public function down(){
    $this->dbforge->drop_column('tbl_content', 'display_contact');
    $this->dbforge->drop_table('tbl_content_contact');
  }
  
  private function add_display_contact(){
    $this->dbforge->add_column('tbl_content',array(
      'display_contact' => array(
        'type' => 'ENUM',
        'constraint' => array('true', 'false'),
        'default' => 'false',
        'after' => 'enable_status'
      )
    ));
  }

  private function create_content_contact_table(){
    $this->dbforge->add_field(array(
      'content_contact_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'content_id' => array(
        'type' => 'BIGINT',
        'unsigned' => TRUE,
        'constraint' => '20',
      ),
      'name' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'mobile' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'remark' => array(
        'type' => 'TEXT',
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('content_contact_id', TRUE);
    $this->dbforge->add_key(array('content_id', 'is_delete'));
    $this->dbforge->create_table('tbl_content_contact');
  }
}
