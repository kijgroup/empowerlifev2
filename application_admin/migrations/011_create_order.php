<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_order extends CI_Migration {
  public function up(){
    $this->create_order_table();
    $this->create_order_item_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_order');
    $this->dbforge->drop_table('tbl_order_item');
  }
  private function create_order_table(){
    $this->dbforge->add_field(array(
      'order_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'member_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE
      ),
      'order_status' => array(
        'type' => 'ENUM',
        'constraint' => array('open', 'confirm', 'complete', 'cancel'),
        'default' => 'open'
      ),
      'order_note' => array(
        'type' => 'CHAR',
        'constraint' => 20,
      ),
      'order_note' => array(
        'type' => 'CHAR',
        'constraint' => 20,
      ),
      'order_code' => array(
        'type' => 'CHAR',
        'constraint' => 20,
      ),
      'order_code_month' => array(
        'type' => 'CHAR',
        'constraint' => 4,
      ),
      'order_code_running' => array(
        'type' => 'INT',
        'constraint' => 11,
      ),
      'total_item' => array(
        'type' => 'INT',
        'constraint' => 11,
        'default' => 0
      ),
      'item_price' => array(
        'type' => 'FLOAT',
        'default' => 0
      ),
      'discount_price' => array(
        'type' => 'FLOAT',
        'default' => 0
      ),
      'shipping_price' => array(
        'type' => 'FLOAT',
        'default' => 0
      ),
      'grand_total' => array(
        'type' => 'FLOAT',
        'default' => 0
      ),
      'shipping_name' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'shipping_address' => array(
        'type' => 'TEXT',
      ),
      'shipping_post_code' => array(
        'type' => 'VARCHAR',
        'constraint' => 5,
      ),
      'shipping_district' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'shipping_province' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'shipping_mobile' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'is_tax' => array(
        'type' => 'ENUM',
        'constraint' => array('true', 'false'),
        'default' => 'true'
      ),
      'tax_name' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'tax_address' => array(
        'type' => 'TEXT'
      ),
      'tax_post_code' => array(
        'type' => 'VARCHAR',
        'constraint' => 5,
      ),
      'tax_district' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'tax_province' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'tax_mobile' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'tax_code' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'tax_branch' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'payment_type' => array(
        'type' => 'VARCHAR',
        'constraint' => 255,
      ),
      'remark' => array(
        'type' => 'TEXT'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('order_id', TRUE);
    $this->dbforge->add_key(array('member_id', 'order_status', 'is_delete'));
    $this->dbforge->create_table('tbl_order');
  }
  private function create_order_item_table(){
    $this->dbforge->add_field(array(
      'order_item_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'order_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
      ),
      'product_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
      ),
      'price_per_item' => array(
        'type' => 'FLOAT'
      ),
      'qty' => array(
        'type' => 'INT'
      ),
      'price_total' => array(
        'type' => 'FLOAT'
      )
    ));
    $this->dbforge->add_key('order_item_id', TRUE);
    $this->dbforge->add_key(array('order_id', 'product_id'));
    $this->dbforge->create_table('tbl_order_item');
  }
}
