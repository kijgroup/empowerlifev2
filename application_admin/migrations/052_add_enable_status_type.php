<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_enable_status_type extends CI_Migration{
  public function up(){
    $this->dbforge->modify_column('tbl_product', array(
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide', 'admin'),
        'default' => 'show'
      ),
    ));
  }
  public function down(){
    $this->dbforge->modify_column('tbl_product', array(
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show'
      ),
    ));
  }
  
}