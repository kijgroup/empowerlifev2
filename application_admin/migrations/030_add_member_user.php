<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_add_member_user extends CI_Migration {
  public function up(){
    $this->update_member_table();
  }
  public function down(){
    $this->dbforge->drop_column('tbl_member', 'user_id');
  }
  private function update_member_table(){
    $this->dbforge->add_column('tbl_member',array(
      'user_id' => array(
        'type' => 'INT',
        'default' => 0,
      )
    ), 'member_id');
  }
}
