<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_reward extends CI_Migration {
  public function up(){
    $this->create_reward_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_reward');
  }
  private function create_reward_table(){
    $this->dbforge->add_field(array(
      'reward_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'name_th' => array(
        'type' => 'VARCHAR',
        'constraint' => 255
      ),
      'name_en' => array(
        'type' => 'VARCHAR',
        'constraint' => 255
      ),
      'main_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
      ),
      'thumb_image' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
      ),
      'detail_th' => array(
        'type' => 'TEXT'
      ),
      'detail_en' => array(
        'type' => 'TEXT'
      ),
      'reward_point' => array(
        'type' => 'INT',
        'constraint' => 20,
        'unsigned' => TRUE,
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('reward_id', TRUE);
    $this->dbforge->add_key(array('enable_status', 'is_delete'));
    $this->dbforge->create_table('tbl_reward');
  }
}
