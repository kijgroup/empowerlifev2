<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>สินค้า</span></li>
    <li><span><?php echo anchor('promotion','โปรโมชั่น'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open_multipart('promotion/form_post/'.$product_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'promotion-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="name_th" class="col-md-3 control-label">ชื่อสินค้า (th) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="name_th" id="name_th" value="<?php echo ($product_id != 0)?$product->name_th:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="name_en" class="col-md-3 control-label">ชื่อสินค้า (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="name_en" id="name_en" value="<?php echo ($product_id != 0)?$product->name_en:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="slug" class="col-md-3 control-label">slug <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="slug" id="slug" value="<?php echo ($product_id != 0)?$product->slug:''; ?>" placeholder="ชื่อที่แสดงบน url ** โปรดระบุเฉพาะภาษาอังกฤษ" required>
      </div>
    </div>
    <div class="form-group">
      <label for="thumb_image" class="col-md-3 control-label">รูปสินค้า <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        if($product_id != 0 && $product->thumb_image != ''){
          echo '<img src="'.base_url('uploads/'.$product->thumb_image).'" class="img-responsive" style="max-width:300px;" required>';
        }
        ?>
        <input type="file" class="form-control" name="thumb_image" id="thumb_image">
        <span class="help-block">400X400px</span>
      </div>
    </div>
    <div class="form-group">
      <label for="price" class="col-md-3 control-label">ราคา <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="price" id="price" value="<?php echo ($product_id != 0)?$product->price:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="price_before_discount" class="col-md-3 control-label">ราคาก่อนหักส่วนลด <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="price_before_discount" id="price_before_discount" value="<?php echo ($product_id != 0)?$product->price_before_discount:'0'; ?>" required>        
        <span class="help-block">ใส่ 0 ถ้าไม่ต้องการให้แสดงในหน้าเวบไซต์</span>
      </div>
    </div>
    <div class="form-group">
      <label for="price_discount_rate" class="col-md-3 control-label">อัตราการลดราคาจากราคาเต็ม <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="price_discount_rate" id="price_discount_rate" value="<?php echo ($product_id != 0)?$product->price_discount_rate:'0'; ?>" required>
        <span class="help-block">ใส่ 0 ถ้าไม่ต้องการให้แสดงในหน้าเวบไซต์</span>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">วันหมดอายุโปรโมชั่น <span class="required">*</span></label>
      <div class="col-md-6">
        <label class="radio-inline">
          <input type="radio" name="is_expire" id="is_expire_false" value="false" <?php echo ($product_id == 0 OR $product->is_expire == 'false')?'checked':'' ?>>
          <span class="label label-primary">ไม่มีวันหมดอายุ</span>
        </label>
        <label class="radio-inline">
          <input type="radio" name="is_expire" id="is_expire_true" value="true" <?php echo ($product_id != 0 && $product->is_expire == 'true')?'checked':'' ?>>
          <span class="label label-danger">มีวันหมดอายุ</span>
        </label>
      </div>
    </div>
    <div class="form-group" id="expire_form">
      <label for="expire_date" class="col-md-3 control-label">วันที่หมดอายุ <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        $expire_date = '';
        $expire_time = '';
        if($product_id != 0 && $product->is_expire == 'true'){
          list($expire_date, $expire_time) = explode(" ", $product->expire_date);
        }
        ?>
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          <input type="text" class="form-control form-control-date" name="expire_date" id="expire_date" value="<?php echo $expire_date; ?>" data-plugin-datepicker />
          <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
          <input type="text" class="form-control" name="expire_time" id="expire_time" value="<?php echo $expire_time; ?>" data-plugin-timepicker data-plugin-options='{ "showMeridian": false }' />
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ประเภทสินค้า <span class="required">*</span></label>
      <div class="col-md-6">
        <label class="radio-inline">
          <input type="radio" name="is_bundle" id="is_bundle_false" value="false" <?php echo ($product_id == 0 OR $product->is_bundle == 'false')?'checked':'' ?>>
          <span class="label label-primary">สินค้าเดี่ยว</span>
        </label>
        <label class="radio-inline">
          <input type="radio" name="is_bundle" id="is_bundle_true" value="true" <?php echo ($product_id != 0 && $product->is_bundle == 'true')?'checked':'' ?>>
          <span class="label label-info">สินค้าจัดชุด</span>
        </label>
      </div>
    </div>
    <div id="bundle_form">
      <div class="table table-striped table-bordered table-hover">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>สินค้า</th>
              <th>จำนวน</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $this->db->where('product_type', 'product');
            $product_list = $this->Product_model->get_list();
            foreach($product_list->result() as $product_item){
              $product_item_qty = ($product_id == 0)?0:$this->Product_bundle_model->get_product_bundle_qty($product_id, $product_item->product_id);
              ?>
              <tr>
                <td><?php echo $product_item->name_th; ?> <span class="required">*</span></td>
                <td><input type="text" class="form-control" name="bundle_qty_<?php echo $product_item->product_id; ?>" value="<?php echo $product_item_qty; ?>"></td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
    <div class="form-group">
      <label for="detail_th" class="col-md-3 control-label">ข้อมูลสินค้า (th) <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="summernote" name="detail_th" id="detail_th" rows="3" data-upload="<?php echo site_url('promotion/upload_image'); ?>"><?php echo ($product_id != 0)?$product->detail_th:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="detail_en" class="col-md-3 control-label">ข้อมูลสินค้า (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="summernote" name="detail_en" id="detail_en" rows="3" data-upload="<?php echo site_url('promotion/upload_image'); ?>"><?php echo ($product_id != 0)?$product->detail_en:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        $enable_status_list = $this->Enable_status_model->get_list();
        foreach($enable_status_list as $enable_status){
          $checked = (($product_id != 0 && $product->enable_status == $enable_status['status_code']) || ($product_id == 0 || $enable_status['status_code'] == 'show'))?'checked':'';
          ?>
          <label class="radio-inline">
            <input type="radio" name="enable_status" id="enable_status_show" value="<?php echo $enable_status['status_code'] ?>" <?php echo $checked; ?>>
            <?php echo $this->Enable_status_model->get_label($enable_status['status_code']); ?>
          </label>
          <?php
        }
        ?>
      </div>
    </div>
    <div class="form-group">
      <label for="sort_priority" class="col-md-3 control-label">แสดงลำดับที่</label>
      <div class="col-md-6">
        <select name="sort_priority" id="sort_priority" class="form-control">
          <?php
          $max_priority = $this->Product_model->get_max_priority('promotion');
          $max_priority += ($product_id == 0)?1:0;
          for($i = 1; $i<=$max_priority; $i++){
            $checked = ($product_id != 0 && $product->sort_priority == $i)?'selected="selected"':'';
            echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สินค้าใกล้เคียง</label>
      <div class="col-md-9">
        <select name="product_relate[]" id="product_relate" class="form-control multi-select" multiple="multiple">
          <?php
          $product_list = $this->Product_model->get_list();
          $relate_list = $this->Product_relate_model->get_array($product_id);
          foreach($product_list->result() as $product_data){
            $selected = (in_array($product_data->product_id, $relate_list))?'selected="selected"':'';
            echo '<option value="'.$product_data->product_id.'" '.$selected.'>'.$product_data->name_th.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
