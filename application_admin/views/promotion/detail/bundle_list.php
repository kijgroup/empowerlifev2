<div class="panel panel-default">
  <div class="panel-heading">
    สินค้าในชุด
  </div>
  <div class="panel-body" id="result-list-image">
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th width="120">รูปสินค้า</th>
            <th>ข้อมูลสินค้า</th>
            <th>จำนวน</th>
          </tr>
        </thead>
        <?php
        $this->db->where('qty >', 0);
        $product_bundle_list = $this->Product_bundle_model->get_list($product->product_id);
        foreach($product_bundle_list->result() as $bundle){
          $product_bundle = $this->Product_model->get_data($bundle->bundle_id);
          if($product_bundle){
            ?>
            <tr>
              <td>
                <?php
                if($product_bundle->thumb_image != ''){
                  echo '<img src="'.base_url('uploads/'.$product_bundle->thumb_image).'" style="width:120px;" />';
                }
                ?>
              </td>
              <td>
                <div><b>TH:</b> <?php echo $product_bundle->name_th; ?></div>
                <div><b>EN:</b> <?php echo $product_bundle->name_en; ?></div>
                <div><?php echo ($product_bundle->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ซ่อน</span>'; ?></div>
              </td>
              <th><?php echo $bundle->qty; ?></th>
            </tr>
            <?php
          }
        }
        ?>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>
