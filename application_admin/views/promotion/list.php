<header class="page-header">
<h2>โปรโมชั่น</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>ของขาย</span></li>
    <li><span>โปรโมชั่น</span></li>
  </ol>
</div>
</header>
<div class="search-content">
  <div class="search-control-wrapper">
    <?php echo form_open('promotion/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'searchForm')); ?>
    <div class="form-group">
      <input type="text" class="form-control" name="search_val" id="search_val" placeholder="ข้อความที่ต้องการค้นหา" />
    </div>
    <div class="form-group">
      <select class="form-control" name="enable_status" id="enable_status">
        <option value="all" selected="selected">สถานะทั้งหมด</option>
        <option value="show">แสดง</option>
        <option value="hide">ซ่อน</option>
      </select>
    </div>
    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> ค้นหา</button>
    <?php echo form_close(); ?>
  </div>
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('promotion/form', '<i class="fa fa-plus"></i> เพิ่มโปรโมชั่น', array('class'=>'btn btn-success')); ?>
    </div>
    <div id="result">
    </div>
    <br />
  </div>
</div>
