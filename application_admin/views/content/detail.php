<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>บทความ</span></li>
    <li><span><?php echo anchor('content','บทความ'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('content', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('content/form/', '<i class="glyphicon glyphicon-plus"></i> สร้างสมาชิกใหม่'); ?></li>
    <li><?php echo anchor('content/form/'.$content->content_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
  <div class="panel-body form-horizontal">
    <div class="form-group">
      <label class="col-md-3 control-label">ประเภทบทความ</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $this->Content_group_model->get_name($content->content_group_id); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ชื่อบทความ (th)</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $content->subject_th; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ชื่อบทความ (en)</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $content->subject_en; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">รูปบทความ</label>
      <div class="col-md-3">
        <p class="form-control-static"><?php echo ($content->thumb_image != '')?'<img src="'.base_url('uploads/'.$content->thumb_image).'" class="img-responsive" />':''; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">เนื้อหาโดยย่อ (th)</label>
      <div class="col-md-6">
        <pre><?php echo $content->short_content_th; ?></pre>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">เนื้อหาโดยย่อ (en)</label>
      <div class="col-md-6">
        <pre><?php echo $content->short_content_en; ?></pre>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">บทความ (th)</label>
      <div class="col-md-9">
        <pre><?php echo $content->content_th; ?></pre>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">บทความ (en)</label>
      <div class="col-md-9">
        <pre><?php echo $content->content_en; ?></pre>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">Tags</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php
        $tag_list = explode(",", $content->tags);
        foreach($tag_list as $tag){
          echo ' <span class="label label-info">'.$tag.'</span> ';
        }
        ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">แสดงช่องกรอกเบอร์</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo ($content->display_contact == 'true')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ซ่อน</span>'; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo ($content->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ซ่อน</span>'; ?></p>
      </div>
    </div>
    <div class="row form-group">
      <label class="col-md-3 col-print-3 control-label">สร้างวันที่</label>
      <div class="col-md-3 col-print-3 xs-margin-bottom">
        <p class="form-control-static"><?php echo $content->create_date; ?></p>
      </div>
      <label class="col-md-3 col-print-3 control-label">โดย</label>
      <div class="col-md-3 col-print-3">
        <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($content->create_by); ?></p>
      </div>
    </div>
    <?php if($content->update_by > 0){ ?>
      <div class="row form-group">
        <label class="col-md-3 col-print-3 control-label">ปรับปรุงวันที่</label>
        <div class="col-md-3 col-print-3 xs-margin-bottom">
          <p class="form-control-static"><?php echo $content->update_date; ?></p>
        </div>
        <label class="col-md-3 col-print-3 control-label">โดย</label>
        <div class="col-md-3 col-print-3">
          <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($content->update_by); ?></p>
        </div>
      </div>
    <?php } ?>
  </div>
</div>
<?php
$this->load->view('content/detail/delete_popup');
$this->load->view('content/detail/imagelist');
$this->load->view('content/detail/image_add');
