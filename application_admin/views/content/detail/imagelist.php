<div class="panel panel-default">
  <div class="panel-heading">
    รูปบทความ
    <a hef="#" data-toggle="modal" data-target="#addImageModal" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon-plus"></i> เพิ่มรูป</a>
  </div>
  <div class="panel-body sortable" id="result-list-image" data-sort_url="<?php echo site_url('content/update_image_priority/'); ?>" style="position:relative;">
    <?php
    foreach($content_image_list->result() as $content_image){
      ?>
      <div class="col-md-3" data-image_id="<?php echo $content_image->content_image_id; ?>" style="height:280px;">
        <img src="<?php echo base_url('uploads/'.$content_image->thumb_image); ?>" alt="" class="img-thumbnail" />
        <div class="pt-sm">
          <?php echo anchor('content/image_delete/'.$content->content_id.'/'.$content_image->content_image_id, 'ลบรูป', array('class'=>'btn btn-danger')); ?>
        </div>
      </div>
      <?php
    }
    ?>
  </div>
</div>
