<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>บทตวาม</span></li>
    <li><span><?php echo anchor('content','บทตวาม'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open_multipart('content/form_post/'.$content_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'content-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="content_group_id" class="col-md-3 control-label">ประเภทบทความ <span class="required">*</span></label>
      <div class="col-md-6">
        <select class="form-control" name="content_group_id" id="content_group_id">
          <?php
          $content_group_list = $this->Content_group_model->get_list(false);
          foreach($content_group_list->result() as $content_group){
            $selected = ($content_id != 0 && $content_group->content_group_id == $content->content_group_id)?'selected="selected"':'';
            echo '<option value="'.$content_group->content_group_id.'" '.$selected.'>'.$content_group->name_th.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="subject_th" class="col-md-3 control-label">ชื่อบทความ (th) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="subject_th" id="subject_th" value="<?php echo ($content_id != 0)?$content->subject_th:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="subject_en" class="col-md-3 control-label">ชื่อบทความ (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="subject_en" id="subject_en" value="<?php echo ($content_id != 0)?$content->subject_en:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="subject_en" class="col-md-3 control-label">รูปบทความ <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        if($content_id != 0 && $content->thumb_image != ''){
          echo '<img src="'.base_url('uploads/'.$content->thumb_image).'" class="img-responsive" style="max-width:300px;">';
        }
        ?>
        <input type="file" class="form-control" name="content_image" id="content_image">
        <span class="help-block">400X400px</span>
      </div>
    </div>
    <div class="form-group">
      <label for="short_content_th" class="col-md-3 control-label">เนื้อหาโดยย่อ (th) <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="form-control" name="short_content_th" id="short_content_th" rows="3" maxlength="100" required><?php echo ($content_id != 0)?$content->short_content_th:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="short_content_en" class="col-md-3 control-label">เนื้อหาโดยย่อ (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="form-control" name="short_content_en" id="short_content_en" rows="3" maxlength="100" required><?php echo ($content_id != 0)?$content->short_content_en:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="content_th" class="col-md-3 control-label">บทความ (th) <span class="required">*</span></label>
      <div class="col-md-9">
        <textarea class="summernote" name="content_th" id="content_th" data-upload="<?php echo site_url('content/upload_image'); ?>"><?php echo ($content_id != 0)?$content->content_th:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="content_en" class="col-md-3 control-label">บทความ (en) <span class="required">*</span></label>
      <div class="col-md-9">
        <textarea class="summernote" name="content_en" id="content_en" data-upload="<?php echo site_url('content/upload_image'); ?>"><?php echo ($content_id != 0)?$content->content_en:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="tags" class="col-md-3 control-label">Tags</label>
      <div class="col-md-6">
        <input name="tags" id="tags" data-role="tagsinput" data-tag-class="label label-primary" class="form-control" value="<?php echo ($content_id != 0)?$content->tags:''; ?>" />
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">แสดงช่องกรอกเบอร์ <span class="required">*</span></label>
      <div class="col-md-6">
        <label class="radio-inline">
          <input type="radio" name="display_contact" id="display_contact_true" value="true"
          <?php echo ($content_id != 0 && $content->display_contact == 'true')?'checked':'' ?>>
          <span class="label label-success">แสดง</span>
        </label>
        <label class="radio-inline">
          <input type="radio" name="display_contact" id="display_contact_false" value="false" <?php echo ($content_id == 0 OR $content->display_contact == 'false')?'checked':'' ?>>
          <span class="label label-danger">ซ่อน</span>
        </label>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ <span class="required">*</span></label>
      <div class="col-md-6">
        <label class="radio-inline">
          <input type="radio" name="enable_status" id="enable_status_show" value="show" <?php echo ($content_id == 0 OR $content->enable_status == 'show')?'checked':'' ?>>
          <span class="label label-success">แสดง</span>
        </label>
        <label class="radio-inline">
          <input type="radio" name="enable_status" id="enable_status_hide" value="hide" <?php echo ($content_id != 0 && $content->enable_status == 'hide')?'checked':'' ?>>
          <span class="label label-danger">ซ่อน</span>
        </label>
      </div>
    </div>
    <div class="form-group">
      <label for="sort_priority" class="col-md-3 control-label">แสดงลำดับที่</label>
      <div class="col-md-6">
        <select name="sort_priority" id="sort_priority" class="form-control">
          <?php
          $max_priority = $this->Content_model->get_max_priority();
          $max_priority += ($content_id == 0)?1:0;
          for($i = 1; $i<=$max_priority; $i++){
            $checked = ($content_id != 0 && $content->sort_priority == $i)?'selected="selected"':'';
            echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
