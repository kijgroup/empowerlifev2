<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span><?php echo anchor('subscribe','ผู้ติดตามข่าวสาร'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>

<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('subscribe', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('subscribe/form/0', '<i class="glyphicon glyphicon-plus"></i> สร้างผู้ติดตามข่าวสารใหม่'); ?></li>
    <li><?php echo anchor('subscribe/form/'.$subscribe_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>

<div class="panel panel-default">
  <div class="panel-body form-horizontal">
    <div class="form-group">
      <label class="col-sm-3 control-label">E-Mail</label>
      <div class="col-sm-9">
        <p class="form-control-static"><?php echo $subscribe->subscribe_email; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-3 control-label">สถานะ</label>
      <div class="col-sm-9">
        <p class="form-control-static"><?php echo $subscribe->subscribe_status; ?></p>
      </div>
    </div>
    <div class="row form-group">
        <label class="col-sm-3 col-print-3 control-label">สร้างวันที่</label>
        <div class="col-sm-3 col-print-3 xs-margin-bottom">
            <p class="form-control-static"><?php echo $subscribe->create_date; ?></p>
        </div>
    </div>
    <?php if($subscribe->update_by > 0){ ?>
    <div class="row form-group">
        <label class="col-sm-3 col-print-3 control-label">ปรับปรุงวันที่</label>
        <div class="col-sm-3 col-print-3 xs-margin-bottom">
            <p class="form-control-static"><?php echo $subscribe->update_date; ?></p>
        </div>
        <label class="col-sm-3 col-print-3 control-label">โดย</label>
        <div class="col-sm-3 col-print-3">
            <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($subscribe->update_by); ?></p>
        </div>
    </div>
    <?php } ?>
</div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="deleteModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('subscribe/delete/'.$subscribe->subscribe_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
