<header class="page-header">
<h2>ผู้ติดตามข่าวสาร</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>ผู้ติดตามข่าวสาร</span></li>
    </ol>
</div>
</header>
<div class="search-content">
    <div class="search-control-wrapper">
        <?php echo form_open('subscribe/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'searchForm')); ?>
        <div class="form-group">
            <input type="text" class="form-control" name="txtSearchVal" id="txtSearchVal" placeholder="ข้อความที่ต้องการค้นหา" />
        </div>
        <div class="form-group">
          <input type="text" class="form-control" name="txtStartDate" id="txtStartDate" placeholder="ค้นหาตั้งแต่วันที่" />
        </div>
        <div class="form-group">
          <input type="text" class="form-control" name="txtEndDate" id="txtEndDate" placeholder="ถึงวันที่" />
        </div>
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> ค้นหา</button>
        <?php echo form_close(); ?>
    </div>
    <div class="tab-content pt-lg">
      <div class="pb-lg">
          <?php echo anchor('subscribe/form/0', '<i class="fa fa-plus"></i> เพิ่มผู้ติดตามข่าวสาร', array('class'=>'btn btn-success')); ?>
      </div>
      <div id="result">
      </div>
      <br />
    </div>
</div>
