<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>ของขาย</span></li>
    <li><span><?php echo anchor('reward','ของรางวัล'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open_multipart('reward/form_post/'.$reward_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'reward-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="name_th" class="col-md-3 control-label">ชื่อของรางวัล (th) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="name_th" id="name_th" value="<?php echo ($reward_id != 0)?$reward->name_th:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="name_en" class="col-md-3 control-label">ชื่อของรางวัล (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="name_en" id="name_en" value="<?php echo ($reward_id != 0)?$reward->name_en:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="thumb_image" class="col-md-3 control-label">รูปของรางวัล <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        if($reward_id != 0 && $reward->thumb_image != ''){
          echo '<img src="'.base_url('uploads/'.$reward->thumb_image).'" class="img-responsive" style="max-width:300px;" required>';
        }
        ?>
        <input type="file" class="form-control" name="thumb_image" id="thumb_image">
      </div>
    </div>
    <div class="form-group">
      <label for="reward_point" class="col-md-3 control-label">แต้มแลกของ <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="reward_point" id="reward_point" value="<?php echo ($reward_id != 0)?$reward->reward_point:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="detail_th" class="col-md-3 control-label">ข้อมูลสินค้า (th) <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="summernote" name="detail_th" id="detail_th" rows="3" data-upload="<?php echo site_url('reward/upload_image'); ?>"><?php echo ($reward_id != 0)?$reward->detail_th:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="detail_en" class="col-md-3 control-label">ข้อมูลสินค้า (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="summernote" name="detail_en" id="detail_en" rows="3" data-upload="<?php echo site_url('reward/upload_image'); ?>"><?php echo ($reward_id != 0)?$reward->detail_en:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ</label>
      <div class="col-md-6">
        <label class="radio-inline">
          <input type="radio" name="enable_status" id="enable_status_show" value="show" <?php echo ($reward_id == 0 OR $reward->enable_status == 'show')?'checked':'' ?>>
          <span class="label label-success">แสดง</span>
        </label>
        <label class="radio-inline">
          <input type="radio" name="enable_status" id="enable_status_hide" value="hide" <?php echo ($reward_id != 0 && $reward->enable_status == 'hide')?'checked':'' ?>>
          <span class="label label-danger">ซ่อน</span>
        </label>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
