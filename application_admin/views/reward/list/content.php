<?php
if($total_transaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $total_page = ceil($total_transaction/$per_page);
  $pager_data = array(
    'total_transaction' => $total_transaction,
    'search_val' => $search_val,
    'enable_status' => $enable_status,
    'per_page' => $per_page,
    'page' => $page,
    'total_page' => $total_page,
    'action' => site_url('reward/filter')
  );
?>
<div class="form-group">
    <?php $this->load->view('reward/list/pager', $pager_data); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th width="120">รูปของรางวัล</th>
        <th>ข้อมูล</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($reward_list->result() as $reward){
        $detail_url = 'reward/detail/'.$reward->reward_id;
        ?>
        <tr>
          <td>
            <?php
            if($reward->thumb_image != ''){
              $str_img = '<img src="'.base_url('uploads/'.$reward->thumb_image).'" style="width:120px;" />';
              echo anchor('reward/detail/'.$reward->reward_id, $str_img);
            }
            ?>
          </td>
          <td>
            <div><b>TH:</b> <?php echo anchor('reward/detail/'.$reward->reward_id, $reward->name_th); ?></div>
            <div><b>EN:</b> <?php echo anchor('reward/detail/'.$reward->reward_id, $reward->name_en); ?></div>
            <div><b>แต้มแลกของ:</b> <?php echo anchor('reward/detail/'.$reward->reward_id, $reward->reward_point); ?></div>
            <div><?php echo ($reward->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ซ่อน</span>'; ?></div>
            <small><?php echo $reward->create_date.' <b>by</b> '.$this->Admin_model->get_login_name_by_id($reward->create_by); ?></small>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
  <?php $this->load->view('reward/list/pager', $pager_data); ?>
</div>
<?php
}
