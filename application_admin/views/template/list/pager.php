<div class="row form-inline">
  <div class="col-xs-7">
    <div class="input-group">
      <span class="input-group-addon hidden-sm hidden-xs">
        แสดง
      </span>
      <select name="ddl_per_page" class="ddl_per_page form-control"
        data-search_val="<?php echo $search_val; ?>"
        data-action="<?php echo $action; ?>"
        >
        <?php
        $arr_per_page = array(10,20,50,100,200);
        foreach($arr_per_page as $per_page_val){
          $selected = ($per_page_val == $per_page)?'selected="selected"':'';
          echo '<option val="'.$per_page_val.'" '.$selected.'>'.$per_page_val.'</option>';
        }
        ?>
      </select>
      <span class="input-group-addon">
        จาก <?php echo number_format($total_transaction); ?><span class="hidden-xs"> รายการ</span>
      </span>
    </div>
  </div>
  <div class="col-xs-5 text-right">
    <div class="input-group">
      <span class="input-group-addon">หน้า</span>
      <select name="ddl_pager" class="ddl_pager form-control"
      data-search_val="<?php echo $search_val; ?>"
      data-per_page="<?php echo $per_page; ?>"
      data-action="<?php echo $action; ?>"
      >
        <?php
        for($i = 1; $i <= $total_page; $i++){
          $selected = ($i == $page)?'selected="selected"':'';
          echo '<option val="'.$i.'" '.$selected.'>'.$i.'</option>';
        }
        ?>
      </select>
      <span class="input-group-addon hidden-xs hidden-sm">จาก <?php echo number_format($total_page); ?> หน้า</span>
    </div>
  </div>
</div>
