<?php
$this->load->view('masterpage/component/header', array('title'=>$title));
?>
<?php
echo form_open_multipart($this->controller.'/form_post/'.$key_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'main-form'));
$last_panel = end($this->display_form);
foreach($this->display_form as $display_form_item){
  ?>
  <div class="panel">
    <div class="panel-heading"><?php echo $display_form_item['panel_name'] ?></div>
    <div class="panel-body">
      <?php
      foreach($display_form_item['item_list'] as $display_form_item_name){
        if(!$main_data && in_array($display_form_item_name, array('code', 'create_date', 'update_date'))){
          continue;
        }
        if($display_form_item_name == 'code'){
          $this->Field_type_model->display_detail(array(
            'field_name' => 'รหัสรายการ',
            'field_type' => 'text',
          ), $main_data['code']);
        }elseif($display_form_item_name == 'create_date'){
          $this->Field_type_model->display_detail(array(
            'field_name' => 'ทำรายการวันที่',
            'field_type' => 'text',
          ), $this->Datetime_service->display_datetime($main_data['create_date']));
        }elseif($display_form_item_name == 'update_date'){
          if($main_data['update_date'] != '0000-00-00 00:00:00'){
            $this->Field_type_model->display_detail(array(
              'field_name' => 'แก้ไขล่าสุดวันที่',
              'field_type' => 'text',
            ), $this->Datetime_service->display_datetime($main_data['update_date']));
          }
        }elseif($display_form_item_name == 'sort_priority'){
          if($this->Main_model->is_sort_priority){
            $value = ($main_data)?$main_data['sort_priority']:0;
            $this->load->view('field_type/form/sort_priority', array('value' => $value));
          }
        }else{
          $structure_item = $this->structure[$display_form_item_name];
          $value = ($main_data)?$main_data[$structure_item['field_key']]:'';
          if($structure_item['field_type'] == 'richtext'){
            $structure_item['controller'] = $this->controller;
          }
          $this->Field_type_model->display_form($structure_item, $value);
        }
      }
      ?>
    </div>
    <?php if($last_panel == $display_form_item){
      ?>
      <div class="panel-footer">
        <div class="row">
          <div class="col-md-offset-3 col-md-6">
            <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
            <a class="btn btn-warning" href="#" onclick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
          </div>
        </div>
      </div>
      <?php
    }?>
  </div>
  <?php
}
echo form_close();
