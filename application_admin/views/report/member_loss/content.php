<?php
if($total_transaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $this->load->view('report/member_loss/action');
  $total_page = ceil($total_transaction/$per_page);
  $pager_data = array();
  $pager_data['total_transaction'] = $total_transaction;
  $pager_data['search_val'] = $search_val;
  $pager_data['date_after'] = $date_after;
  $pager_data['member_type_code'] = $member_type_code;
  $pager_data['enable_status'] = $enable_status;
  $pager_data['sort'] = $sort;
  $pager_data['per_page'] = $per_page;
  $pager_data['page'] = $page;
  $pager_data['total_page'] = $total_page;
  $pager_data['action'] = site_url('report/member_loss/filter');
?>
<div class="form-group">
  <?php $this->load->view('report/member_loss/pager', $pager_data); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>วันที่สั่งซื้อล่าสุด</th>
        <th>รหัสสมาชิก</th>
        <th>ชื่อ</th>
        <th>อีเมล</th>
        <th>ประเภทสมาชิก</th>
        <th>สถานะ</th>
        <th>วันที่สร้าง</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($member_list->result() as $member){
        $detail_url = 'member/detail/'.$member->member_id;
        ?>
        <tr>
          <td><?php echo $this->Member_loss_filter_service->get_last_order_date($member->member_id); ?></td>
          <td>
            <?php echo anchor($detail_url, ($member->member_code != '')?$member->member_code:'-', array('target'=>'_blank')); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, ($member->member_firstname.$member->member_lastname != '')?$member->member_firstname.' '.$member->member_lastname:'-', array('target'=>'_blank')); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, ($member->member_email != '')?$member->member_email:'-', array('target'=>'_blank')); ?>
          </td>
          <td>
            <?php echo $this->Member_type_model->get_label($member->member_type); ?>
          </td>
          <td>
            <?php echo $this->Member_enable_status_model->get_label($member->enable_status); ?>
          </td>
          <td>
            <?php echo $member->create_date.' <b>by</b> '.$this->Admin_model->get_login_name_by_id($member->create_by); ?>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
  <?php $this->load->view('report/member_loss/pager', $pager_data); ?>
</div>
<?php
}
