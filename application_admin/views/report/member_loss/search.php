<div class="search-control-wrapper">
  <?php echo form_open('report/member_loss/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'searchForm')); ?>
  <div class="form-group">
    <input type="text" class="form-control" name="search_val" id="search_val" placeholder="ข้อความที่ต้องการค้นหา" />
  </div>
  <div class="form-group">
    <input type="text" class="form-control form-control-date" name="date_after" id="date_after" placeholder="หยุดซื้อหลังวันที่" />
  </div>
  <div class="form-group">
    <select class="form-control" name="member_type_code" id="member_type_code">
      <option value="all" selected="selected">ทุกประเภทสมาชิก</option>
      <?php
      foreach($member_type_list->result() as $member_type){
        echo '<option value="'.$member_type->member_type_code.'">'.$member_type->member_type_name.'</option>';
      }
      ?>
    </select>
  </div>
  <div class="form-group">
    <select class="form-control" name="enable_status" id="enable_status">
      <option value="all" selected="selected">สถานะทั้งหมด</option>
      <option value="active">Active</option>
      <option value="inactive">Inactive</option>
    </select>
  </div>
  <div class="form-group">
    <select class="form-control" name="sort" id="sort">
      <option value="member_id">วันที่สร้าง</option>
      <option value="member_code">รหัสสมาชิก</option>
      <option value="member_firstname">ชื่อสมาชิก</option>
    </select>
  </div>
  <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> ค้นหา</button>
  <?php echo form_close(); ?>
</div>