<header class="page-header">
<h2>Member Loss</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>Report</span></li>
    <li><span>Member Loss</span></li>
  </ol>
</div>
</header>
<div class="search-content">
  <?php $this->load->view('report/member_loss/search'); ?>
  <div class="tab-content pt-lg">
    <div id="result">
      รายงานนี้จะแสดงรายชื่อสมาชิกที่เคยมีประวัติการสั่งซื้อสำเร็จอย่างน้อย 1 ครั้งเท่านั้น
    </div>
    <br />
  </div>
</div>
