<div style="padding-bottom:10px">
  <?php echo form_open('report/member_loss/export', array('method' => 'post', 'target' => '_blank')); ?>
  <input type="hidden" name="search_val" value="<?php echo $search_val; ?>" />
  <input type="hidden" name="date_after" value="<?php echo $date_after; ?>" />
  <input type="hidden" name="member_type_code" value="<?php echo $member_type_code; ?>" />
  <input type="hidden" name="enable_status" value="<?php echo $enable_status; ?>" />
  <button type="submit" class="btn btn-primary">Download Excel</button>
  <?php echo form_close(); ?>
</div>