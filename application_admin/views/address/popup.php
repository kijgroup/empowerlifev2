<?php
$province_list = $this->Thailand_model->get_province_list();
?>
<!-- Modal -->
<div class="modal fade" id="<?php echo $popup_name; ?>" role="dialog" aria-labelledby="<?php echo $popup_name; ?>_label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="<?php echo $popup_name; ?>_label">เลือกที่อยู่</h4>
      </div>
      <div class="modal-body form form-horizontal">
        <div class="form-group">
          <label for="<?php echo $popup_name; ?>_province" class="col-md-3 control-label">จังหวัด</label>
          <div class="col-md-9">
            <select class="form-control select-province" id="<?php echo $popup_name; ?>_province" data-plugin-selectTwo data-target="<?php echo $popup_name; ?>_amphur" data-action="<?php echo site_url('address/amphur_list') ?>">
              <option value="" disabled selected>โปรดระบุ</option>
              <?php
              foreach($province_list->result() as $province){
                echo '<option value="'.$province->PROVINCE_ID.'">'.$province->PROVINCE_NAME.'</option>';
              }
              ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="<?php echo $popup_name; ?>_amphur" class="col-md-3 control-label">อำเภอ/เขต</label>
          <div class="col-md-9">
            <select class="form-control select-amphur" id="<?php echo $popup_name; ?>_amphur" data-plugin-selectTwo data-target="<?php echo $popup_name; ?>_district" data-action="<?php echo site_url('address/district_list') ?>">
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="<?php echo $popup_name; ?>_district" class="col-md-3 control-label">ตำบล/แขวง</label>
          <div class="col-md-9">
            <select class="form-control select-district" id="<?php echo $popup_name; ?>_district" data-plugin-selectTwo data-target="<?php echo $popup_name; ?>_postcode" data-action="<?php echo site_url('address/postcode') ?>">
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="<?php echo $popup_name; ?>_postcode" class="col-md-3 control-label">รหัสไปรษณีย์</label>
          <div class="col-md-9">
            <p class="form-control-static" id="<?php echo $popup_name; ?>_postcode"></p>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <button type="button" class="btn btn-primary copy-address" data-source="<?php echo $popup_name; ?>" data-dismiss="modal" data-target_province="<?php echo $province_field; ?>" data-target_amphur="<?php echo $amphur_field; ?>" data-target_district="<?php echo $district_field; ?>" data-target_postcode="<?php echo $postcode_field; ?>">ยืนยัน</button>
      </div>
    </div>
  </div>
</div>