<?php
$required = ($required)?'required="required"':'';
foreach($source_data as $data){
  $checked = ($data['source_key'] == $value)?'checked=""':'';
  echo '<div class="radio">
    <label>
      <input type="radio" name="'.$field_key.'" id="'.$field_key.'_'.$data['source_key'].'" value="'.$data['source_key'].'" '.$required.' '.$checked.'>
      <span class="'.$data['source_label'].'">'.$data['source_name'].'</span>
    </label>
  </div>';
}
