<div class="form-group">
  <label for="<?php echo $field_key; ?>" class="col-md-3 control-label"><?php
  echo $field_name;
  echo ($required)?' <span class="required" aria-required="true">*</span>':'';
  ?></label>
  <div class="col-md-6">
    <?php
    if($value != ''){
      echo '<img src="'.base_url('uploads/'.$value).'" alt="" style="max-width:200px;" />';
    }
    ?>
    <div class="fileupload fileupload-new" data-provides="fileupload">
      <div class="input-append">
        <div class="uneditable-input">
          <i class="fa fa-file fileupload-exists"></i>
          <span class="fileupload-preview"></span>
        </div>
        <span class="btn btn-default btn-file">
          <span class="fileupload-exists">Change</span>
          <span class="fileupload-new">Select file</span>
          <input type="file" name="<?php echo $field_key; ?>" id="<?php echo $field_key; ?>" <?php echo ($required)?'required="required"':''; ?> />
        </span>
        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
      </div>
    </div>
    <label class="error" for="<?php echo $field_key; ?>" style="display:none;"></label>
    <?php echo ($help_text != '')?'<span class="help-block">'.$help_text.'</span>':''; ?>
  </div>
</div>
