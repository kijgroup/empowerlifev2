<div class="form-group">
  <label for="<?php echo $field_key; ?>" class="col-md-3 control-label"><?php
  echo $field_name;
  echo ($required)?' <span class="required" aria-required="true">*</span>':'';
  ?></label>
  <div class="col-md-6">
    <?php
    if($source_type == 'enum'){
      $this->load->view('field_type/form/radio_enum');
    }else{
      $this->load->view('field_type/form/radio_database');
    }
    ?>
    <label class="error" for="<?php echo $field_key; ?>" style="display:none;"></label>
    <?php echo ($help_text != '')?'<span class="help-block">'.$help_text.'</span>':''; ?>
  </div>
</div>
