<?php
$source_data_list = $source_data->get_list();
$required = ($required)?'required="required"':'';
foreach($source_data_list->result_array() as $data){
  $checked = ($data[$source_data->field_key] == $value)?'checked=""':'';
  echo '<div class="radio">
    <label>
      <input type="radio" name="'.$field_key.'" id="'.$field_key.'_'.$data[$source_data->field_key].'" value="'.$data[$source_data->field_key].'" '.$required.' '.$checked.'>
      '.$data[$source_data->field_name].'
    </label>
  </div>';

}
