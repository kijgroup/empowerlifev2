<div class="form-group">
  <label for="sort_priority" class="col-md-3 control-label">แสดงลำดับที่ <span class="required" aria-required="true">*</span></label>
  <div class="col-md-6">
    <select class="form-control" name="sort_priority" id="sort_priority">
      <?php
      $max_priority = $this->Main_model->get_max_priority();
      $max_priority += ($value == 0)?1:0;
      for($i = 1; $i <= $max_priority; $i++){
        $selected = ($i == $value)?'selected="selected"':'';
        echo '<option value="'.$i.'" '.$selected.'>'.$i.'</option>';
      }
      ?>
    </select>
  </div>
</div>
