<div class="form-group">
  <label class="col-md-3 control-label"><?php echo $field_name; ?></label>
  <div class="col-md-6">
    <?php echo ($value != '')?anchor(base_url('uploads/'.$value), '<i class="fa fa-download"></i> Download', array('class'=>'btn btn-info', 'target'=>'_blank', 'download'=>'')):''; ?>
  </div>
</div>
