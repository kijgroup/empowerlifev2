<div class="form-group">
  <label class="col-md-3 control-label"><?php echo $field_name; ?></label>
  <div class="col-md-6">
    <p class="form-control-static"><?php echo $this->Datetime_service->display_date($value); ?></p>
  </div>
</div>
