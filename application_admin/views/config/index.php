<header class="page-header">
  <h2>ตั้งค่าเวบไซต์</h2>
  <div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
      <li>
        <a href="index.html">
          <i class="fa fa-home"></i>
        </a>
      </li>
      <li><span>การจัดการ</span></li>
      <li><span>ตั้งค่าเวบไซต์</span></li>
    </ol>
  </div>
</header>
<?php echo form_open_multipart('config/form_post/', array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'config-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">การตั้งค่า</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="web_title_th" class="col-md-3 control-label">Web Title (th) <span class="required">*</span></label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="web_title_th" id="web_title_th" value="<?php echo $this->Config_model->get_value('web_title_th'); ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="web_title_en" class="col-md-3 control-label">Web Title (en) <span class="required">*</span></label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="web_title_en" id="web_title_en" value="<?php echo $this->Config_model->get_value('web_title_en'); ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="meta_description" class="col-md-3 control-label">Meta Description <span class="required">*</span></label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="meta_description" id="meta_description" value="<?php echo $this->Config_model->get_value('meta_description'); ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="meta_keyword" class="col-md-3 control-label">Meta Keyword <span class="required">*</span></label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="meta_keyword" id="meta_keyword" value="<?php echo $this->Config_model->get_value('meta_keyword'); ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="email_list" class="col-md-3 control-label">อีเมลเจ้าหน้าที่เวบไซต์ <span class="required">*</span></label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="email_list" id="email_list" value="<?php echo $this->Config_model->get_value('email_list'); ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="email_sender" class="col-md-3 control-label">อีเมลที่ใช้ส่งออก <span class="required">*</span></label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="email_sender" id="email_sender" value="<?php echo $this->Config_model->get_value('email_sender'); ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="facebook_code" class="col-md-3 control-label">Facebook Code <span class="required">*</span></label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="facebook_code" id="facebook_code" value="<?php echo $this->Config_model->get_value('facebook_code'); ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="google_analytic" class="col-md-3 control-label">Google Analytic <span class="required">*</span></label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="google_analytic" id="google_analytic" value="<?php echo $this->Config_model->get_value('google_analytic'); ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="youtube_iframe" class="col-md-3 control-label">Video Youtube หน้าแรก <span class="required">*</span></label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="youtube_iframe" id="youtube_iframe" value="<?php echo $this->Config_model->get_value('youtube_iframe'); ?>" required>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
