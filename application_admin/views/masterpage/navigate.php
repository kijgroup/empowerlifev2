<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">
  <div class="sidebar-header">
    <div class="sidebar-title">Navigation</div>
    <div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
        <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
    </div>
  </div>
  <div class="nano">
    <div class="nano-content">
      <nav id="menu" class="nav-main" role="navigation">
        <ul class="nav nav-main">
          <li <?php echo ($nav == 'dashboard')?'class="nav-active"':''; ?>>
            <?php echo anchor('dashboard','<i class="fa fa-home" aria-hidden="true"></i><span>Dashboard</span>'); ?>
          </li>
          <li <?php echo ($nav == 'member')?'class="nav-active"':''; ?>>
            <?php echo anchor('member','<i class="fa fa-group" aria-hidden="true"></i><span>สมาชิก</span>'); ?>
          </li>
          <?php if($this->session->userdata('admin_type') == 'admin'){ ?>
          <li <?php echo ($nav == 'member_type')?'class="nav-active"':''; ?>>
            <?php echo anchor('member_type','<i class="fa fa-group" aria-hidden="true"></i><span>ประเภทสมาชิก</span>'); ?>
          </li>
          <li class="nav-parent <?php echo (in_array($nav, array('category', 'product', 'promotion', 'reward')))?'nav-expanded nav-active':''; ?>">
            <a>
              <i class="fa fa-dropbox" aria-hidden="true"></i>
              <span>ของขาย</span>
            </a>
            <ul class="nav nav-children">
              <li <?php echo ($nav == 'category')?'class="nav-active"':''; ?>><?php echo anchor('category','หมวดหมู่'); ?></li>
              <li <?php echo ($nav == 'product')?'class="nav-active"':''; ?>><?php echo anchor('product','สินค้า'); ?></li>
              <li <?php echo ($nav == 'promotion')?'class="nav-active"':''; ?>><?php echo anchor('promotion','โปรโมชั่น'); ?></li>
              <li <?php echo ($nav == 'reward')?'class="nav-active"':''; ?>><?php echo anchor('reward','ของรางวัล'); ?></li>
              <li <?php echo ($nav == 'discount_point')?'class="nav-active"':''; ?>><?php echo anchor('discount_point','ใช้แต้มแลกส่วนลด'); ?></li>
            </ul>
          </li>
          <?php } ?>
          <li class="nav-parent <?php echo (in_array($nav, array('order', 'order-web', 'order-open', 'order-confirm', 'order-complete', 'order-cancel')))?'nav-expanded nav-active':''; ?>">
            <a>
              <i class="fa fa-shopping-cart" aria-hidden="true"></i>
              <span>รายการสั่งซื้อ</span>
            </a>
            <ul class="nav nav-children">
              <li <?php echo ($nav == 'order')?'class="nav-active"':''; ?>><?php echo anchor('order','ทั้งหมด'); ?></li>
              <li <?php echo ($nav == 'order-web')?'class="nav-active"':''; ?>><?php echo anchor('order/index/web','รายการรอดำเนินการ-Website'); ?></li>
              <li <?php echo ($nav == 'order-open')?'class="nav-active"':''; ?>><?php echo anchor('order/index/open','รายการรอชำระเงิน'); ?></li>
              <li <?php echo ($nav == 'order-confirm')?'class="nav-active"':''; ?>><?php echo anchor('order/index/confirm','รายการรอส่งสินค้า'); ?></li>
              <li <?php echo ($nav == 'order-complete')?'class="nav-active"':''; ?>><?php echo anchor('order/index/complete','รายการเสร็จสิ้น'); ?></li>
              <li <?php echo ($nav == 'order-cancel')?'class="nav-active"':''; ?>><?php echo anchor('order/index/cancel','รายการยกเลิก'); ?></li>
            </ul>
          </li>
          <?php if($this->session->userdata('admin_type') == 'admin'){ ?>
          <li <?php echo ($nav == 'payment')?'class="nav-active"':''; ?>>
            <?php echo anchor('payment','<i class="fa fa-dollar" aria-hidden="true"></i><span>รายการแจ้งชำระเงิน</span>'); ?>
          </li>
          <?php } ?>
          <li <?php echo ($nav == 'reward_transaction')?'class="nav-active"':''; ?>>
            <?php echo anchor('reward_transaction','<i class="fa fa-gift" aria-hidden="true"></i><span>รายการแลกของรางวัล</span>'); ?>
          </li>
          <?php if($this->session->userdata('admin_type') == 'admin'){ ?>
          <li class="nav-parent <?php echo (in_array($nav, array('content', 'contentgroup')))?'nav-expanded nav-active':''; ?>">
            <a>
              <i class="fa fa-newspaper-o" aria-hidden="true"></i>
              <span>บทความ</span>
            </a>
            <ul class="nav nav-children">
              <li <?php echo ($nav == 'content')?'class="nav-active"':''; ?>><?php echo anchor('content','บทความ'); ?></li>
              <li <?php echo ($nav == 'contentgroup')?'class="nav-active"':''; ?>><?php echo anchor('contentgroup','ประเภทบทความ'); ?></li>
            </ul>
          </li>
          <li <?php echo ($nav == 'content_contact')?'class="nav-active"':''; ?>>
            <?php echo anchor('content_contact','<i class="fa fa-book" aria-hidden="true"></i><span>เบอร์ติดต่อกลับ</span>'); ?>
          </li>
          <li <?php echo ($nav == 'subscribe')?'class="nav-active"':''; ?>>
            <?php echo anchor('subscribe','<i class="fa fa-book" aria-hidden="true"></i><span>ผู้ติดตามข่าวสาร</span>'); ?>
          </li>

          <li class="nav-parent <?php echo (in_array($nav, array('contact', 'contact_group',)))?'nav-expanded nav-active':''; ?>">
            <a>
              <i class="fa fa-comments" aria-hidden="true"></i>
              <span>ติดต่อเจ้าหน้าที่</span>
            </a>
            <ul class="nav nav-children">
              <li <?php echo ($nav == 'contact')?'class="nav-active"':''; ?>><?php echo anchor('contact','ติดต่อเจ้าหน้าที่'); ?></li>
              <li <?php echo ($nav == 'contact_group')?'class="nav-active"':''; ?>><?php echo anchor('contact_group','หมวดหัวข้อคำถาม'); ?></li>
            </ul>
          </li>


          <li class="nav-parent <?php echo (in_array($nav, array('faq', 'faq_group',)))?'nav-expanded nav-active':''; ?>">
            <a>
              <i class="fa fa-comments" aria-hidden="true"></i>
              <span>FAQ</span>
            </a>
            <ul class="nav nav-children">
              <li <?php echo ($nav == 'faq_group')?'class="nav-active"':''; ?>><?php echo anchor('faq_group','หมวดคำถาม'); ?></li>
              <li <?php echo ($nav == 'faq')?'class="nav-active"':''; ?>><?php echo anchor('faq','ถาม-ตอบ'); ?></li>
            </ul>
          </li>

          <li class="nav-parent <?php echo (in_array($nav, array('about', 'privacy', 'terms')))?'nav-expanded nav-active':''; ?>">
            <a>
              <i class="fa fa-desktop" aria-hidden="true"></i>
              <span>หน้าเวบ</span>
            </a>
            <ul class="nav nav-children">
              <li <?php echo ($nav == 'about')?'class="nav-active"':''; ?>><?php echo anchor('page/index/about','เกี่ยวกับเรา'); ?></li>
              <li <?php echo ($nav == 'privacy')?'class="nav-active"':''; ?>><?php echo anchor('page/index/privacy','Privacy policy'); ?></li>
              <li <?php echo ($nav == 'terms')?'class="nav-active"':''; ?>><?php echo anchor('page/index/terms','Terms of Service'); ?></li>
              <li <?php echo ($nav == 'verify')?'class="nav-active"':''; ?>><?php echo anchor('page/index/verify','รับประกันคุณภาพ'); ?></li>
              <li <?php echo ($nav == 'oem')?'class="nav-active"':''; ?>><?php echo anchor('page/index/oem','OEM'); ?></li>
              <li <?php echo ($nav == 'joinus')?'class="nav-active"':''; ?>><?php echo anchor('page/index/joinus','ร่วมงานกับเรา'); ?></li>
              <li <?php echo ($nav == 'send_policy')?'class="nav-active"':''; ?>><?php echo anchor('page/index/send_policy','นโยบายการส่ง'); ?></li>
              <li <?php echo ($nav == 'return_policy')?'class="nav-active"':''; ?>><?php echo anchor('page/index/return_policy','เงื่อนไขการส่งคืนสินค้า'); ?></li>
              <li <?php echo ($nav == 'contact')?'class="nav-active"':''; ?>><?php echo anchor('page/index/contact','ติดต่อเรา'); ?></li>
            </ul>
          </li>

          <li class="nav-parent <?php echo (in_array($nav, array('slideshow_home', 'slideshow_about','slideshow_oem')))?'nav-expanded nav-active':''; ?>">
            <a>
              <i class="fa fa-desktop" aria-hidden="true"></i>
              <span>Slideshow</span>
            </a>
            <ul class="nav nav-children">
              <li <?php echo ($nav == 'slideshow_home')?'class="nav-active"':''; ?>><?php echo anchor('slideshow/index/home','หน้าแรก'); ?></li>
              <li <?php echo ($nav == 'slideshow_about')?'class="nav-active"':''; ?>><?php echo anchor('slideshow/index/about','เกี่ยวกับเรา'); ?></li>
              <li <?php echo ($nav == 'slideshow_oem')?'class="nav-active"':''; ?>><?php echo anchor('slideshow/index/oem','OEM'); ?></li>
            </ul>
          </li>
          <li class="nav-parent <?php echo (in_array($nav, array('report_member_loss',)))?'nav-expanded nav-active':''; ?>">
            <a>
              <i class="fa fa-file" aria-hidden="true"></i>
              <span>Report</span>
            </a>
            <ul class="nav nav-children">
              <li <?php echo ($nav == 'report_member_loss')?'class="nav-active"':''; ?>><?php echo anchor('report/member_loss','Member Loss'); ?></li>
            </ul>
          </li>
          <li class="nav-parent <?php echo (in_array($nav, array(
            'admin',
            'footer_link',
            'popup',
            'config',
            'order_channel',
            'order_sub_channel',
            'contact_channel',
            'shipping_type',
            'stock',
            )))?'nav-expanded nav-active':''; ?>">
            <a>
              <i class="fa fa-cog" aria-hidden="true"></i>
              <span>การจัดการ</span>
            </a>
            <ul class="nav nav-children">
              <li <?php echo ($nav == 'admin')?'class="nav-active"':''; ?>><?php echo anchor('admin','เจ้าหน้าที่เวบไซต์'); ?></li>
              <li <?php echo ($nav == 'footer_link')?'class="nav-active"':''; ?>><?php echo anchor('footer_link','Footer link'); ?></li>
              <li <?php echo ($nav == 'popup')?'class="nav-active"':''; ?>><?php echo anchor('popup','ป๊อปอัพ'); ?></li>
              <li <?php echo ($nav == 'config')?'class="nav-active"':''; ?>><?php echo anchor('config','ตั้งค่าเวบไซต์'); ?></li>
              <li <?php echo ($nav == 'order_channel')?'class="nav-active"':''; ?>><?php echo anchor('order_channel','ช่องทางการสั่งซื้อ'); ?></li>
              <li <?php echo ($nav == 'order_sub_channel')?'class="nav-active"':''; ?>><?php echo anchor('order_sub_channel','Source'); ?></li>
              <li <?php echo ($nav == 'contact_channel')?'class="nav-active"':''; ?>><?php echo anchor('contact_channel','ช่องทางการติดต่อ'); ?></li>
              <li <?php echo ($nav == 'shipping_type')?'class="nav-active"':''; ?>><?php echo anchor('shipping_type','วิธีการจัดส่ง'); ?></li>
              <li <?php echo ($nav == 'stock')?'class="nav-active"':''; ?>><?php echo anchor('stock','ตัด Stock'); ?></li>
            </ul>
          </li>
          <?php } ?>
        </ul>
      </nav>
      <hr class="separator" />
    </div>
  </div>
</aside>
<!-- end: sidebar -->
