<!doctype html>
<html class="fixed">
  <head>
    <!-- Basic -->
    <meta charset="UTF-8">
    <title><?php echo $title; ?> - EMPOWERLIFE Administrator</title>
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('favicon.ico'); ?>">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Kanit&subset=thai" rel="stylesheet">
    <!--link href='//fonts.googleapis.com/css?family=Kanit:400,700&subset=thai,latin' rel='stylesheet' type='text/css'>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/vendor/bootstrap/css/bootstrap.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/vendor/font-awesome/css/font-awesome.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/vendor/magnific-popup/magnific-popup.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/vendor/bootstrap-timepicker/css/bootstrap-timepicker.css'); ?>" />
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/stylesheets/theme.css'); ?>" />
    <!-- Skin CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/stylesheets/skins/default.css'); ?>" />
    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/stylesheets/css/theme-custom.css'); ?>">
    <!-- Head Libs -->
    <script src="<?php echo base_url('assets_admin/vendor/modernizr/modernizr.js'); ?>"></script>
    <?php echo $this->Masterpage_service->print_css(); ?>
  </head>
  <body>
    <section class="body">
      <?php $this->load->view('masterpage/header'); ?>
      <div class="inner-wrapper">
        <?php $this->load->view('masterpage/navigate'); ?>
        <section role="main" class="content-body">
          <input type="hidden" id="base_url" value="<?php echo base_url(); ?>" />
          <?php echo $content; ?>
        </section>
      </div>
    </section>
    <!-- Vendor -->
    <script src="<?php echo base_url('assets_admin/vendor/jquery/jquery.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/jquery-browser-mobile/jquery.browser.mobile.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/bootstrap/js/bootstrap.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/nanoscroller/nanoscroller.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/bootstrap-timepicker/bootstrap-timepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/magnific-popup/jquery.magnific-popup.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/jquery-placeholder/jquery-placeholder.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/jquery-validation/jquery.validate.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/jquery-sortable/jquery-sortable.js'); ?>"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCrZN_hFwEb1Cy0LemtHNf1RDTX3-epNjY"></script>
    <?php echo $this->Masterpage_service->print_js(); ?>
    <!-- Theme Base, Components and Settings -->
    <script src="<?php echo base_url('assets_admin/javascripts/theme.js'); ?>"></script>
    <!-- Theme Custom -->
    <script src="<?php echo base_url('assets_admin/javascripts/theme.custom.js'); ?>"></script>
    <!-- Theme Initialization Files -->
    <script src="<?php echo base_url('assets_admin/javascripts/theme.init.js'); ?>"></script>
  </body>
</html>
