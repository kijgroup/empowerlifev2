<!-- start: header -->
<header class="header">
  <div class="logo-container">
    <?php echo anchor('', '<h2 style="margin:0;color:#696458;">EMPOWERLIFE</h2>', array('class'=>'logo')); ?>
    <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
      <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
    </div>
  </div>
  <!-- start: search & user box -->
  <div class="header-right">
    <span class="separator"></span>
    <div id="userbox" class="userbox">
      <a href="#" data-toggle="dropdown">
        <div class="profile-info">
          <span class="name"><?php echo $this->session->userdata('admin_name'); ?></span>
          <span class="email"><?php echo $this->session->userdata('admin_username'); ?></span>
        </div>
        <i class="fa custom-caret"></i>
      </a>
      <div class="dropdown-menu">
        <ul class="list-unstyled">
          <li class="divider"></li>
          <li>
            <?php echo anchor('editprofile','<i class="fa fa-user"></i> My Profile</a>', array('role'=>'menuitem','tabindex'=>'-1')); ?>
          </li>
          <li>
            <?php echo anchor('changepassword','<i class="fa fa-lock"></i> Change Password', array('role'=>'menuitem','tabindex'=>'-1')); ?>
          </li>
          <li>
            <?php echo anchor('login/logout','<i class="fa fa-power-off"></i> Logout', array('role'=>'menuitem', 'tabindex'=>'-1')); ?>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <!-- end: search & user box -->
</header>
<!-- end: header -->
