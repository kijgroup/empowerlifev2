<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <?php
    foreach($this->breadcrumb_list as $breadcrumb_item){
      echo '<li><span>';
      echo ($breadcrumb_item['url'] != '')?anchor($breadcrumb_item['url'], $breadcrumb_item['name']):$breadcrumb_item['name'];
      echo '</span></li>';
    }
    ?>
  </ol>
</div>
