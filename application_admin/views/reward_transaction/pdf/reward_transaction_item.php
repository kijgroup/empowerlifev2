<table width="100%" cellpadding="0" cellspacing="0" class="borderlist partlist">
  <tr>
    <th class="first-child">ลำดับ</th>
    <th>ของรางวัล</th>
    <th>จำนวนที่แลก</th>
    <th>แต้มต่อหน่วย</th>
    <th>รวม (แต้ม)</th>
  </tr>
  <?php
  $count_item = 0;
  foreach($reward_transaction_item_list->result() as $reward_transaction_item){
    $count_item++;
    ?>
    <tr>
      <td width="10%" class="first-child text-center"><?php echo $count_item; ?></td>
      <td width="40%"><?php echo $reward_transaction_item->reward_name_th; ?></td>
      <td width="20%" class="text-center"><?php echo $reward_transaction_item->qty; ?></td>
      <td width="15%" style="text-align:right;"><?php echo number_format($reward_transaction_item->reward_point, 0); ?></td>
      <td width="15%" style="text-align:right;"><?php echo number_format($reward_transaction_item->total_reward_point, 0); ?></td>
    </tr>
    <?php
  }
  ?>
  <tr>
    <td class="first-child" colspan="3" rowspan="7">
      <div style="font-size:5px;">&nbsp;</div>
      &nbsp;&nbsp;ไปรษณีย์ (EMS)<br/>
      &nbsp;&nbsp;บจก.เอ็มพาวเวอร์ไลฟ์ KBANK 765-2-38383-6<br/>
    </td>
    <td class="text-right">แต้มรวม</td>
    <td class="text-right"><?php echo number_format($reward_transaction->total_reward_point, 0); ?></td>
  </tr>
</table>
<hr/>
