<?php
$member = $this->Member_model->get_data($reward_transaction->member_id);
?>
<table width="100%">
  <tr>
    <td width="60%" style="padding-right:50px; padding-top:10px;">
      <table>
        <tr>
          <td>ชื่อลูกค้า </td>
          <td>
            <?php echo $this->Member_model->get_name($reward_transaction->member_id); ?>
            (รหัสลูกค้า <?php echo $member->member_code; ?>)
          </td>
        </tr>
        <tr>
          <td>ที่อยู่ </td>
          <td><?php echo $member->member_address; ?> <?php echo $member->member_postcode; ?></td>
        </tr>
        <tr>
          <td>โทร. </td>
          <td><?php echo $member->member_mobile; ?></td>
        </tr>
        <br/><br/>
        <tr>
          <td>*หมายเหตุ : </td>
          <td></td>
        </tr>
        <tr>
          <td colspan="2"><?php echo $reward_transaction->note; ?></td>
        </tr>
      </table>
    </td>
    <td width="40%" style="padding-left:80px; padding-top:10px;">
      <table>
        <tr>
          <td>วันที่สั่งซื้อ </td>
          <td><?php echo $this->Datetime_service->display_datetime($reward_transaction->create_date); ?></td>
        </tr>
        <tr>
          <td>วันที่เบิก </td>
          <td>...............................</td>
        </tr>
        <tr>
          <td>วันที่จัดส่ง </td>
          <td>...............................</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
