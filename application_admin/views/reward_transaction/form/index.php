<header class="page-header">
  <h2><?php echo $title; ?></h2>
  <div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
      <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
      <li><span><?php echo anchor('reward_transaction','รายการแลกของรางวัล'); ?></span></li>
      <li><span><?php echo $title; ?></span></li>
    </ol>
  </div>
</header>
<?php echo form_open('reward_transaction/form_post/'.$reward_transaction_id, array('class'=>'form-horizontal','id' => 'transaction-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลผู้แลกของรางวัล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="member_name" class="col-md-3 control-label">ชื่อ <span class="required">*</span></label>
      <div class="col-md-6">
        <select class="form-control" name="member_id" id="member_id">
          <option value="">โปรดระบุ</option>
          <?php
          $member_list = $this->Member_model->get_list();
          foreach($member_list->result() as $member){
            $selected = ($reward_transaction_id != 0 && $reward_transaction->member_id == $member->member_id)?'selected="selected"':'';
            echo '<option value="'.$member->member_id.'" '.$selected.'>'.$member->member_firstname.' '.$member->member_lastname.' - '.$member->member_code.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ <span class="required">*</span></label>
      <div class="col-md-6">
        <select class="form-control" name="reward_transaction_status" id="reward_transaction_status">
          <?php
          $reward_transaction_status_list = $this->Reward_transaction_status_model->get_list();
          foreach($reward_transaction_status_list as $reward_transaction_status){
            $selected = ($reward_transaction_id != 0 && $reward_transaction_status['status_code'] == $reward_transaction->reward_transaction_status)?'selected="selected"':'';
            echo '<option value="'.$reward_transaction_status['status_code'].'" '.$selected.'>'.$reward_transaction_status['status_name'].'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('reward_transaction/form/item_list'); ?>
<?php $this->load->view('reward_transaction/form/shipping'); ?>
<div class="panel-footer">
  <div class="row">
    <div class="col-md-offset-3 col-md-6">
      <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
      <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
    </div>
  </div>
</div>
<?php
echo form_close();
$this->load->view('reward_transaction/form/item_template');
$this->load->view('address/popup', array(
  'popup_name' => 'select_address_shipping',
  'province_field' => 'shipping_province',
  'amphur_field' => 'shipping_amphur',
  'district_field' => 'shipping_district',
  'postcode_field' => 'shipping_post_code',
));