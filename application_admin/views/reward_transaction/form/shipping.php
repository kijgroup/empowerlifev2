<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลผู้จัดส่งสินค้า</div>
  <div class="panel-body">
    <div class="form-group">
      <label class="col-sm-4 control-label">ชื่อ-นามสกุล <span class="required">*</span></label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="shipping_name" id="shipping_name" value="<?php echo ($reward_transaction_id)? $reward_transaction->shipping_name:''; ?>"required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">โทรศัพท์ <span class="required">*</span></label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="shipping_mobile" id="shipping_mobile" value="<?php echo ($reward_transaction_id !=0)? $reward_transaction->shipping_mobile:''; ?>"required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">ที่อยู่ในการจัดส่ง <span class="required">*</span></label>
      <div class="col-sm-8">
        <textarea type="text" class="form-control" name="shipping_address" id="shipping_address" required><?php echo ($reward_transaction_id !=0)? $reward_transaction->shipping_address:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">เขต/ตำบล <span class="required">*</span></label>
      <div class="col-sm-8">
        <div class="input-group">
          <input type="text" class="form-control" name="shipping_district" id="shipping_district" value="<?php echo ($reward_transaction_id !=0)? $reward_transaction->shipping_district:''; ?>"required>
          <span class="input-group-btn">
            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#select_address_shipping"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">แขวง/อำเภอ <span class="required">*</span></label>
      <div class="col-sm-8">
        <div class="input-group">
          <input type="text" class="form-control" name="shipping_amphur" id="shipping_amphur" value="<?php echo ($reward_transaction_id !=0)? $reward_transaction->shipping_amphur:''; ?>"required>
          <span class="input-group-btn">
            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#select_address_shipping"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">จังหวัด <span class="required">*</span></label>
      <div class="col-sm-8">
        <div class="input-group">
          <input type="text" class="form-control" name="shipping_province" id="shipping_province" value="<?php echo ($reward_transaction_id !=0)? $reward_transaction->shipping_province:''; ?>"required>
          <span class="input-group-btn">
            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#select_address_shipping"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">รหัสไปรษณีย์ <span class="required">*</span>์</label>
      <div class="col-sm-8">
        <div class="input-group">
          <input type="text" class="form-control" name="shipping_post_code" id="shipping_post_code" value="<?php echo ($reward_transaction_id !=0)? $reward_transaction->shipping_post_code:''; ?>"required>
          <span class="input-group-btn">
            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#select_address_shipping"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">หมายเหตุ</label>
      <div class="col-sm-8">
        <textarea type="text" class="form-control" name="note" id="note"><?php echo ($reward_transaction_id !=0)? $reward_transaction->note:''; ?></textarea>
      </div>
    </div>
  </div>
</div>
