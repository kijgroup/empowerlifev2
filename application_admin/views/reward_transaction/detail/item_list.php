<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>สินค้า</th>
        <th class="text-right">มูลค่าแต้ม</th>
        <th class="text-right">จำนวน</th>
        <th class="text-right">รวมแต้ม</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($reward_transaction_item_list->result() as $reward_transaction_item){
        ?>
        <tr>
          <td><?php echo $reward_transaction_item->reward_name_th; ?></td>
          <td class="text-right"><?php echo number_format($reward_transaction_item->reward_point, 0); ?></td>
          <td class="text-right"><?php echo number_format($reward_transaction_item->qty, 0); ?></td>
          <td class="text-right"><?php echo number_format($reward_transaction_item->total_reward_point, 0); ?></td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
</div>
