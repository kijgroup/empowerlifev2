<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลการแลกของรางวัล</div>
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">สมาชิก</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo anchor('member/detail/'.$reward_transaction->member_id, $this->Member_model->get_name($reward_transaction->member_id)); ?></p>
      </div>
    </div>
      <div class="form-group">
        <label class="col-md-3 control-label">แลกแต้ม</label>
        <div class="col-md-6">
          <p class="form-control-static"><?php echo number_format($reward_transaction->total_reward_point ,0); ?></p>
        </div>
      </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Reward_transaction_status_model->get_label($reward_transaction->reward_transaction_status); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">หมายเหตุ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward_transaction->note; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">วันที่ทำรายการ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward_transaction->create_date; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">แก้ไขล่าสุด</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward_transaction->update_date; ?></p>
      </div>
    </div>
  </div>
</div>
