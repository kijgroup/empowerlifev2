<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลผู้จัดส่งสินค้า</div>
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">ชื่อผู้รับ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward_transaction->shipping_name; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">โทรศัพท์</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward_transaction->shipping_mobile; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ที่อยู่</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward_transaction->shipping_address; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">เขต/ตำบล</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward_transaction->shipping_district; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">แขวง/อำเภอ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward_transaction->shipping_amphur; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">จังหวัด</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward_transaction->shipping_province; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ไปรษณีย์</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward_transaction->shipping_post_code; ?></p>
      </div>
    </div>
  </div>
</div>
