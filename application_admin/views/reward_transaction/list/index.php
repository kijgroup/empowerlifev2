<header class="page-header">
  <h2>รายการแลกของรางวัล</h2>
  <div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
      <li><?php echo anchor('', '<i class="fa fa-home"></i>'); ?></li>
      <li><span>รายการแลกของรางวัล</span></li>
    </ol>
  </div>
</header>
<div class="search-content">
  <?php $this->load->view('reward_transaction/list/search'); ?>
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('reward_transaction/form', '<i class="fa fa-plus"></i> เพิ่มรายการแลกของรางวัล', array('class'=>'btn btn-success')); ?>
    </div>
    <div id="result">
    </div>
    <br />
  </div>
</div>
