<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span><?php echo anchor('member_type','ประเภทสมาชิก'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open_multipart('member_type/form_post/'.$member_type_code, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'member_type-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="member_type_name" class="col-md-3 control-label">ชื่อประเภทสมาชิก<span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="member_type_name" id="member_type_name" value="<?php echo $member_type->member_type_name; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="require_amount" class="col-md-3 control-label">ยอดซื้อ (บาท)<span class="required">*</span></label>
      <div class="col-md-6">
        <input type="number" class="form-control" name="require_amount" id="require_amount" value="<?php echo $member_type->require_amount; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="convert_point" class="col-md-3 control-label">สะสมคะแนน (บาท)<span class="required">*</span></label>
      <div class="col-md-6">
        <input type="number" class="form-control" name="convert_point" id="convert_point" value="<?php echo $member_type->convert_point; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="discount" class="col-md-3 control-label">ส่วนลด (%)<span class="required">*</span></label>
      <div class="col-md-6">
        <input type="number" class="form-control" name="discount" id="discount" value="<?php echo $member_type->discount; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="min_amount" class="col-md-3 control-label">ยอดซื้อขั้นต่ำ (บาท)<span class="required">*</span></label>
      <div class="col-md-6">
        <input type="number" class="form-control" name="min_amount" id="min_amount" value="<?php echo $member_type->min_amount; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="member_type_image" class="col-md-3 control-label">รูปบัตร </label>
      <div class="col-md-6">
        <?php
         if($member_type->member_type_image != ''){
           echo '<img src="'.base_url('uploads/'.$member_type->member_type_image).'" alt="" width="100" />';
         }
        ?>
        <input type="file" class="form-control" name="member_type_image" id="member_type_image">
        <span class="help-block">200X200px</span>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
