<header class="page-header">
<h2>ประเภทสมาชิก</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>ประเภทสมาชิก</span></li>
  </ol>
</div>
</header>
<div class="search-content">
  <div class="tab-content pt-lg">
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
            <tr>
              <th>ประเภทสมาชิก</th>
              <th class="text-right">ยอดซื้อ</th>
              <th class="text-right">สะสมคะแนน</th>
              <th class="text-right">ลดราคา</th>
              <th class="text-right">ยอดซื้อขั้นต่ำ</th>
            </tr>
        </thead>
        <tbody>
          <?php
          foreach($member_type_list->result() as $member_type){
            $form_url = 'member_type/form/'.$member_type->member_type_code;
            ?>
            <tr>
              <td><?php echo anchor($form_url, $this->Member_type_model->get_label($member_type->member_type_code)); ?></td>
              <td class="text-right"><?php echo anchor($form_url, number_format($member_type->require_amount)); ?></td>
              <td class="text-right"><?php echo anchor($form_url, $member_type->convert_point.'บาท = 1 คะแนน'); ?></td>
              <td class="text-right"><?php echo anchor($form_url, number_format($member_type->discount, 2).' %'); ?></td>
              <td class="text-right"><?php echo anchor($form_url, $member_type->min_amount); ?></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <br />
  </div>
</div>
