<!doctype html>
<html class="fixed">
  <head>
    <title>เข้าสู่ระบบ - Empowerlife Administration</title>
    <!-- Basic -->
    <meta charset="UTF-8">
    <meta name="keywords" content="BanOkay" />
    <meta name="description" content="Empowerlife - Administrator page">
    <meta name="author" content="">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="icon" sizes="16x16 32x32" type="image/x-icon" href="<?php echo base_url('favicon.ico'); ?>">
    <!-- Web Fonts  -->
    <!--link href='//fonts.googleapis.com/css?family=Kanit:400,700&subset=thai,latin' rel='stylesheet' type='text/css'>
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">
    <!-- Vendor CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/vendor/bootstrap/css/bootstrap.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/vendor/font-awesome/css/font-awesome.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/vendor/magnific-popup/magnific-popup.css'); ?>" />
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css'); ?>" />
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/stylesheets/theme.css'); ?>" />
    <!-- Skin CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/stylesheets/skins/default.css'); ?>" />
    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="<?php echo base_url('assets_admin/stylesheets/theme-custom.css'); ?>">
    <!-- Head Libs -->
    <script src="<?php echo base_url('assets_admin/vendor/modernizr/modernizr.js'); ?>"></script>
  </head>
  <body>
    <!-- start: page -->
    <section class="body-sign">
      <div class="center-sign">
        <a href="#" class="logo pull-left">
          <h1>EMPOWERLIFE</h1>
        </a>

        <div class="panel panel-sign">
          <div class="panel-title-sign mt-xl text-right">
            <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> ADMINISTRATOR</h2>
          </div>
          <div class="panel-body">
            <?php echo form_open('login/login_post'); ?>
            <div class="form-group mb-lg">
              <label for="txtUsername">ชื่อผู้ใช้</label>
              <div class="input-group input-group-icon">
                <input name="txtUsername" id="txtUsername" type="text" class="form-control input-lg" tabindex="1" />
                <span class="input-group-addon">
                  <span class="icon icon-lg"><i class="fa fa-user"></i></span>
                </span>
              </div>
            </div>
            <div class="form-group mb-lg">
              <div class="clearfix">
                <label class="pull-left" for="txtPassword">รหัสผ่าน</label>
              </div>
              <div class="input-group input-group-icon">
                <input name="txtPassword" id="txtPassword" type="password" class="form-control input-lg" tabindex="2" />
                <span class="input-group-addon">
                  <span class="icon icon-lg"><i class="fa fa-lock"></i></span>
                </span>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-offset-8 col-sm-4 text-right">
                <button type="submit" class="btn btn-primary hidden-xs" tabindex="3">เข้าสู่ระบบ</button>
                <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg" tabindex="4">เข้าสู่ระบบ</button>
              </div>
            </div>
            <?php echo form_close(); ?>
          </div>
        </div>

        <p class="text-center text-muted mt-md mb-md">&copy; Copyright 2016. All Rights Reserved.</p>
      </div>
    </section>
    <!-- end: page -->
    <!-- Vendor -->
    <script src="<?php echo base_url('assets_admin/vendor/jquery/jquery.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/jquery-browser-mobile/jquery.browser.mobile.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/bootstrap/js/bootstrap.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/nanoscroller/nanoscroller.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/magnific-popup/jquery.magnific-popup.js'); ?>"></script>
    <script src="<?php echo base_url('assets_admin/vendor/jquery-placeholder/jquery-placeholder.js'); ?>"></script>
    <!-- Theme Base, Components and Settings -->
    <script src="<?php echo base_url('assets_admin/javascripts/theme.js'); ?>"></script>
    <!-- Theme Custom -->
    <script src="<?php echo base_url('assets_admin/javascripts/theme.custom.js'); ?>"></script>
    <!-- Theme Initialization Files -->
    <script src="<?php echo base_url('assets_admin/javascripts/theme.init.js'); ?>"></script>
  </body>
</html>
