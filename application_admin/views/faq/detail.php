<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>FAQ</span></li>
    <li><span><?php echo anchor('faq','ถาม-ตอบ'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('faq', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('faq/form/', '<i class="glyphicon glyphicon-plus"></i> สร้างสมาชิกใหม่'); ?></li>
    <li><?php echo anchor('faq/form/'.$faq_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
  <div class="panel-body form-horizontal">
    <div class="form-group">
      <label class="col-md-3 control-label">หมวดคำถาม</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $this->Faq_group_model->get_name($faq->faq_group_id); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">คำถาม (th)</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $faq->faq_question_th; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">คำถาม (en)</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $faq->faq_question_en; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">คำตอบ (th)</label>
      <div class="col-md-6">
        <pre><?php echo $faq->faq_answer_th; ?></pre>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">คำตอบ (en)</label>
      <div class="col-md-6">
        <pre><?php echo $faq->faq_answer_en; ?></pre>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo ($faq->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ซ่อน</span>'; ?></p>
      </div>
    </div>
    <div class="row form-group">
      <label class="col-md-3 col-print-3 control-label">สร้างวันที่</label>
      <div class="col-md-3 col-print-3 xs-margin-bottom">
        <p class="form-control-static"><?php echo $faq->create_date; ?></p>
      </div>
      <label class="col-md-3 col-print-3 control-label">โดย</label>
      <div class="col-md-3 col-print-3">
        <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($faq->create_by); ?></p>
      </div>
    </div>
    <?php if($faq->update_by > 0){ ?>
      <div class="row form-group">
        <label class="col-md-3 col-print-3 control-label">ปรับปรุงวันที่</label>
        <div class="col-md-3 col-print-3 xs-margin-bottom">
          <p class="form-control-static"><?php echo $faq->update_date; ?></p>
        </div>
        <label class="col-md-3 col-print-3 control-label">โดย</label>
        <div class="col-md-3 col-print-3">
          <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($faq->update_by); ?></p>
        </div>
      </div>
    <?php } ?>
  </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="deleteModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('faq/delete/'.$faq->faq_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
