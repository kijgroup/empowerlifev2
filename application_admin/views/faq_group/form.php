<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>FAQ</span></li>
    <li><span><?php echo anchor('faq_group','หมวดคำถาม'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open('faq_group/form_post/'.$faq_group_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'faq-group-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="faq_group_th" class="col-md-3 control-label">หมวดคำถาม (th) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="faq_group_th" id="faq_group_th" value="<?php echo ($faq_group_id != 0)?$faq_group->faq_group_th:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="faq_group_en" class="col-md-3 control-label">หมวดคำถาม (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="faq_group_en" id="faq_group_en" value="<?php echo ($faq_group_id != 0)?$faq_group->faq_group_en:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ <span class="required">*</span></label>
      <div class="col-md-6">
        <label class="radio-inline">
          <input type="radio" name="enable_status" id="enable_status_show" value="show" <?php echo ($faq_group_id == 0 OR $faq_group->enable_status == 'show')?'checked':'' ?>>
          <span class="label label-success">แสดง</span>
        </label>
        <label class="radio-inline">
          <input type="radio" name="enable_status" id="enable_status_hide" value="hide" <?php echo ($faq_group_id != 0 && $faq_group->enable_status == 'hide')?'checked':'' ?>>
          <span class="label label-danger">ซ่อน</span>
        </label>
      </div>
    </div>
    <div class="form-group">
      <label for="sort_priority" class="col-md-3 control-label">แสดงลำดับที่</label>
      <div class="col-md-6">
        <select name="sort_priority" id="sort_priority" class="form-control">
          <?php
          $max_priority = $this->Faq_group_model->get_max_priority();
          $max_priority += ($faq_group_id == 0)?1:0;
          for($i = 1; $i<=$max_priority; $i++){
            $checked = ($faq_group_id != 0 && $faq_group->sort_priority == $i)?'selected="selected"':'';
            echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
