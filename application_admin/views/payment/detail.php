<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span><?php echo anchor('payment','รายการแจ้งชำระเงิน'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('payment', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li class="divider"></li>
    <?php
    if($payment->bank_payment_status != 'waiting'){
      ?>
      <li><a href="#" data-toggle="modal" data-target="#waitingModal"><i class="glyphicon glyphicon-pencil"></i> ปรับสถานะเป็นรอตรวจสอบการแจ้งชำระ</a></li>
      <?php
    }
    if($payment->bank_payment_status != 'confirm'){
      ?>
      <li><a href="#" data-toggle="modal" data-target="#confirmModal"><i class="glyphicon glyphicon-pencil"></i> ยืนยันการแจ้งชำระ</a></li>
      <?php
    }
    if($payment->bank_payment_status != 'cancel'){
      ?>
      <li><a href="#" data-toggle="modal" data-target="#cancelModal"><i class="glyphicon glyphicon-pencil"></i> ยกเลิกการแจ้งชำระ</a></li>
      <?php
    }
    if($payment->bank_payment_status != 'complete'){
      ?>
      <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
      <?php
    }
    ?>
  </ul>

</div>

<?php
$this->load->view('payment/detail/detailinfo');
$this->load->view('payment/detail/confirm');
$this->load->view('payment/detail/cancel');
$this->load->view('payment/detail/delete_popup');
