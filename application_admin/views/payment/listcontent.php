<?php
if($total_transaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $total_page = ceil($total_transaction/$per_page);
  $pager_data = array();
  $pager_data['total_transaction'] = $total_transaction;
  $pager_data['searchVal'] = $search_val;
  $pager_data['startDate'] = $start_date;
  $pager_data['endDate'] = $end_date;
  $pager_data['per_page'] = $per_page;
  $pager_data['page'] = $page;
  $pager_data['total_page'] = $total_page;
  $pager_data['action'] = site_url('payment/filter');
?>
  <div class="form-group">
    <?php $this->load->view('payment/listpager', $pager_data); ?>
  </div>
  <div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>รหัสการแจ้งชำระเงิน</th>
        <th>ชื่อ</th>
        <th>วันที่ชำระเงิน</th>
        <th>มูลค่าการสั่งซื้อ</th>
        <th>ธนาคาร</th>
        <th class="text-center">สถานะ</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($payment_list->result() as $payment){
        $detail_url = 'payment/detail/'.$payment->bank_payment_id;
        ?>
        <tr>
          <td>
            <?php echo anchor($detail_url, $payment->order_code); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, $payment->bank_payment_name); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, $payment->bank_payment_date); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, $payment->bank_payment_amount); ?>
          </td>
          <td>
            <?php echo $this->Bank_model->get_name($payment->bank_id); ?>
          </td>
          <td>
            <?php echo $payment->bank_payment_status; ?>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
  </div>
  <div class="form-group">
    <?php $this->load->view('payment/listpager', $pager_data); ?>
  </div>
<?php
}
