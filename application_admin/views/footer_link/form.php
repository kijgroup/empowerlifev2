<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>การจัดการ</span></li>
    <li><span><?php echo anchor('footer_link','Footer link'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open_multipart('footer_link/form_post/'.$footer_link_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'footer-link-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="name" class="col-md-3 control-label">ชื่อ Link <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="name" id="name" value="<?php echo ($footer_link_id != 0)?$footer_link->name:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="link_icon" class="col-md-3 control-label">รูป Icon <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        if($footer_link_id != 0 && $footer_link->link_icon != ''){
          echo '<img src="'.base_url('uploads/'.$footer_link->link_icon).'" class="img-responsive" style="max-width:300px;" required>';
        }
        ?>
        <input type="file" class="form-control" name="link_icon" id="link_icon">
        <span class="help-block">40X40px</span>
      </div>
    </div>
    <div class="form-group">
      <label for="link_url" class="col-md-3 control-label">URL <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="link_url" id="link_url" value="<?php echo ($footer_link_id != 0)?$footer_link->link_url:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ <span class="required">*</span></label>
      <div class="col-md-6">
        <label class="radio-inline">
          <input type="radio" name="enable_status" id="enable_status_show" value="show" <?php echo ($footer_link_id == 0 OR $footer_link->enable_status == 'show')?'checked':'' ?>>
          <span class="label label-success">แสดง</span>
        </label>
        <label class="radio-inline">
          <input type="radio" name="enable_status" id="enable_status_hide" value="hide" <?php echo ($footer_link_id != 0 && $footer_link->enable_status == 'hide')?'checked':'' ?>>
          <span class="label label-danger">ซ่อน</span>
        </label>
      </div>
    </div>
    <div class="form-group">
      <label for="sort_priority" class="col-md-3 control-label">แสดงลำดับที่</label>
      <div class="col-md-6">
        <select name="sort_priority" id="sort_priority" class="form-control">
          <?php
          $max_priority = $this->Footer_link_model->get_max_priority();
          $max_priority += ($footer_link_id == 0)?1:0;
          for($i = 1; $i<=$max_priority; $i++){
            $checked = ($footer_link_id != 0 && $footer_link->sort_priority == $i)?'selected="selected"':'';
            echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
