<header class="page-header">
<h2>Footer link</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>การจัดการ</span></li>
    <li><span>Footer link</span></li>
  </ol>
</div>
</header>
<div class="search-content">
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('footer_link/form', '<i class="fa fa-plus"></i> เพิ่ม Link', array('class'=>'btn btn-success')); ?>
    </div>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
            <tr>
              <th width="40">Icon</th>
              <th>ชื่อ Link</th>
              <th>สถานะ</th>
              <th>URL</th>
              <th></th>
            </tr>
        </thead>
        <tbody>
          <?php
          foreach($footer_link_list->result() as $footer_link){
            $form_url = 'footer_link/form/'.$footer_link->footer_link_id;
            $delete_url = 'footer_link/delete/'.$footer_link->footer_link_id;
            ?>
            <tr>
              <td>
                <?php
                if($footer_link->link_icon !== ''){
                  $str_img = '<img src="'.base_url('uploads/'.$footer_link->link_icon).'" style="width:40px;" />';
                  echo anchor($form_url, $str_img);
                }
                ?>
              </td>
              <td><?php echo anchor($form_url, $footer_link->name); ?></td>
              <td>
                <?php echo ($footer_link->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ไม่แสดง</span>'; ?>
              </td>
              <td><?php echo anchor($form_url, $footer_link->link_url); ?></td>
              <td>
                <?php echo anchor($delete_url, '<i class="fa fa-trash"></i> ลบข้อมูล', array('class'=>'btn btn-sm btn-danger')); ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <br />
  </div>
</div>
