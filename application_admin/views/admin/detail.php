<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>การจัดการ</span></li>
    <li><span><?php echo anchor('admin','เจ้าหน้าที่เวบไซต์'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('admin', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('admin/form/', '<i class="glyphicon glyphicon-plus"></i> สร้างเจ้าหน้าที่ใหม่'); ?></li>
    <li><?php echo anchor('admin/form/'.$admin->admin_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li><a href="#" data-toggle="modal" data-target="#passwordModal"><i class="glyphicon glyphicon-pencil"></i> เปลี่ยนรหัสผ่าน</a></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
    <div class="panel-body form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label">ประเภทผู้เข้าระบบ </label>
            <div class="col-sm-9">
                <p class="form-control-static"><?php echo $admin->admin_type; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Login Name</label>
            <div class="col-sm-9">
                <p class="form-control-static"><?php echo $admin->admin_username; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">ชื่อ</label>
            <div class="col-sm-9">
                <p class="form-control-static"><?php echo $admin->admin_name; ?></p>
            </div>
        </div>
        <fieldset disabled>
        <div class="form-group">
            <label class="col-sm-offset-3 col-sm-4 col-print-6 xs-margin-bottom">
                <input type="checkbox" <?php echo ($admin->enable_status == 'show')?'checked="checked"':''; ?> />
                ใช้งานได้
            </label>
        </div>
        </fieldset>
        <div class="row form-group">
            <label class="col-sm-3 col-print-3 control-label">สร้างวันที่</label>
            <div class="col-sm-3 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $admin->create_date; ?></p>
            </div>
            <label class="col-sm-3 col-print-3 control-label">โดย</label>
            <div class="col-sm-3 col-print-3">
                <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($admin->create_by); ?></p>
            </div>
        </div>
        <div class="row form-group">
            <label class="col-sm-3 col-print-3 control-label">ปรับปรุงวันที่</label>
            <div class="col-sm-3 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $admin->update_date; ?></p>
            </div>
            <label class="col-sm-3 col-print-3 control-label">โดย</label>
            <div class="col-sm-3 col-print-3">
                <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($admin->update_by); ?></p>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="deleteModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('admin/delete/'.$admin->admin_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="passwordModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="passwordModalLabel">เปลี่ยนรหัสผ่าน</h4>
      </div>
      <?php echo form_open('admin/form_change_password/'.$admin->admin_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'password-form')); ?>
      <div class="modal-body">
        <div class="form-group">
            <label for="txtPassword" class="col-sm-3 control-label">Password</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" name="txtPassword" id="txtPassword" placeholder="" maxlength="20">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <button type="submit" class="btn btn-primary">เปลี่ยนรหัสผ่าน</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>
