<header class="page-header">
<h2>เจ้าหน้าที่เวบไซต์</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>การจัดการ</span></li>
        <li><span>เจ้าหน้าที่เวบไซต์</span></li>
    </ol>
</div>
</header>
<div class="search-content">
    <div class="search-control-wrapper">
        <?php echo form_open('admin/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'searchForm')); ?>
        <div class="form-group">
            <input type="text" class="form-control" name="txtSearchVal" id="txtSearchVal" placeholder="ข้อความที่ต้องการค้นหา" />
        </div>
        <div class="form-group">
            <select class="form-control" name="ddlEnableStatus" id="ddlEnableStatus">
                <option value="all" selected="selected">สถานะทั้งหมด</option>
                <option value="show">ใช้งานได้</option>
                <option value="hide">ไม่สามารถใช้งานได้</option>
            </select>
        </div>
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> ค้นหา</button>
        <?php echo form_close(); ?>
    </div>
    <div class="tab-content pt-lg">
        <div class="pb-lg">
            <?php echo anchor('admin/form/0', '<i class="fa fa-plus"></i> เพิ่มผู้ใช้', array('class'=>'btn btn-success')); ?>
        </div>
        <div id="result">
        </div>
        <br />
    </div>
</div>
