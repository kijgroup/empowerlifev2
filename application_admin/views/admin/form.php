<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>การจัดการ</span></li>
    <li><span><?php echo anchor('admin', 'เจ้าหน้าที่เวบไซต์'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open('admin/form_post/'.$admin_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'admin-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลระบบ</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="admin_type" class="col-md-3 control-label">ประเภทผู้เข้าระบบ <span class="required">*</span></label>
      <div class="col-md-6">
        <select class="form-control" name="admin_type" id="admin_type">
          <option value="admin" <?php echo ($admin_id != 0 && $admin->admin_type == 'admin')?'selected="selected"':''; ?>>admin</option>
          <option value="agent" <?php echo ($admin_id != 0 && $admin->admin_type == 'agent')?'selected="selected"':''; ?>>agent</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="txtUsername" class="col-md-3 control-label">Login Name <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="txtUsername" id="txtUsername" placeholder="" maxlength="20" value="<?php echo ($admin_id != 0)?$admin->admin_username:''; ?>"required>
      </div>
    </div>
    <?php if($admin_id == 0){ ?>
    <div class="form-group">
      <label for="txtPassword" class="col-md-3 control-label">Password <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="password" class="form-control" name="txtPassword" id="txtPassword" placeholder="" maxlength="20" required>
      </div>
    </div>
    <div class="form-group">
      <label for="txtConfirmPassword" class="col-md-3 control-label">Confirm Password <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="password" class="form-control" name="txtConfirmPassword" id="txtConfirmPassword" placeholder="" maxlength="20" required>
      </div>
    </div>
    <?php } ?>
    <div class="form-group">
      <label for="txtName" class="col-md-3 control-label">ชื่อ <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="txtName" id="txtName" value="<?php echo ($admin_id != 0)?$admin->admin_name:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-offset-3 col-md-6 xs-margin-bottom">
        <input type="checkbox" name="chkEnableStatus" value="1" <?php echo ($admin_id == 0 || $admin->enable_status == 'show')?'checked="checked"':''; ?> />
        ใช้งานได้
      </label>
    </div>
  </div>
  <div class="panel-footer">
    <div class="form-group">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
