<header class="page-header">
<h2>ช่องทางการสั่งซื้อ</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li>
            <a href="index.html">
                <i class="fa fa-home"></i>
            </a>
        </li>
        <li><span>ช่องทางการสั่งซื้อ</span></li>
    </ol>
</div>
</header>

<div class="search-content">
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('order_channel/form', '<i class="fa fa-plus"></i> เพิ่มช่องทางการสั่งซื้อ', array('class'=>'btn btn-success')); ?>
    </div>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
            <tr>
              <th>ช่องทางการสั่งซื้อ</th>
              <th></th>
            </tr>
        </thead>
        <tbody>
          <?php
          foreach($order_channel_list->result() as $order_channel){
            $form_url = 'order_channel/form/'.$order_channel->order_channel_id;
            $delete_url = 'order_channel/delete/'.$order_channel->order_channel_id;
            ?>
            <tr>
              <td><?php echo anchor($form_url, $order_channel->order_channel_name); ?></td>
              <td>
                <?php echo anchor($delete_url, '<i class="fa fa-trash"></i> ลบข้อมูล', array('class'=>'btn btn-sm btn-danger')); ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <br />
  </div>
</div>
