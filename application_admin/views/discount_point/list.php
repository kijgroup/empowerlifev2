<header class="page-header">
<h2>แต้มแลกส่วนลด</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li>
            <a href="index.html">
                <i class="fa fa-home"></i>
            </a>
        </li>
        <li><span>แต้มแลกส่วนลด</span></li>
    </ol>
</div>
</header>

<div class="search-content">
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('discount_point/form', '<i class="fa fa-plus"></i> เพิ่มข้อมูลแต้มแลกส่วนลด', array('class'=>'btn btn-success')); ?>
    </div>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
            <tr>
              <th>คะแนนสะสม</th>
              <th>ส่วนลด (บาท)</th>
              <th>สถานะ</th>
              <th></th>
            </tr>
        </thead>
        <tbody>
          <?php
          foreach($discount_point_list->result() as $discount_point){
            $form_url = 'discount_point/form/'.$discount_point->discount_point_id;
            $delete_url = 'discount_point/delete/'.$discount_point->discount_point_id;
            ?>
            <tr>
              <td><?php echo anchor($form_url, $discount_point->use_point); ?></td>
              <td><?php echo anchor($form_url, $discount_point->discount_amount); ?></td>
              <td>
                <?php echo ($discount_point->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ไม่แสดง</span>'; ?>
              </td>
              <td>
                <?php echo anchor($delete_url, '<i class="fa fa-trash"></i> ลบข้อมูล', array('class'=>'btn btn-sm btn-danger')); ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <br />
  </div>
</div>
