<header class="page-header">
<h2>หมวดหมู่</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>ของขาย</span></li>
    <li><span>หมวดหมู่</span></li>
  </ol>
</div>
</header>

<div class="search-content">
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('category/form', '<i class="fa fa-plus"></i> เพิ่มหมวดหมู่', array('class'=>'btn btn-success')); ?>
    </div>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
            <tr>
              <th>ชื่อหมวดหมู่ (th)</th>
              <th>ชื่อหมวดหมู่ (en)</th>
              <th>slug</th>
              <th class="text-center">บทความ</th>
              <th>สถานะ</th>
              <th></th>
            </tr>
        </thead>
        <tbody>
          <?php
          foreach($category_list->result() as $category){
            $form_url = 'category/form/'.$category->category_id;
            $delete_url = 'category/delete/'.$category->category_id;
            ?>
            <tr>
              <td><?php echo anchor($form_url, $category->name_th); ?></td>
              <td><?php echo anchor($form_url, $category->name_en); ?></td>
              <td><?php echo $category->slug; ?></td>
              <td class="text-center"><?php echo $this->Category_model->count_product($category->category_id); ?></td>
              <td>
                <?php echo ($category->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ไม่แสดง</span>'; ?>
              </td>
              <td>
                <?php echo anchor($delete_url, '<i class="fa fa-trash"></i> ลบข้อมูล', array('class'=>'btn btn-sm btn-danger')); ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <br />
  </div>
</div>
