<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>บทความ</span></li>
    <li><span><?php echo anchor('Category','หมวดหมู่'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open_multipart('category/form_post/'.$category_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'category-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="name_th" class="col-md-3 control-label">ชื่อหมวดหมู่ (th) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="name_th" id="name_th" value="<?php echo($category_id !=0)? $category->name_th:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="name_en" class="col-md-3 control-label">ชื่อหมวดหมู่ (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="name_en" id="name_en" value="<?php echo($category_id != 0)?$category->name_en:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="slug" class="col-md-3 control-label">slug <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="slug" id="slug" value="<?php echo ($category_id != 0)?$category->slug:''; ?>" placeholder="ชื่อที่แสดงบน url ** โปรดระบุเฉพาะภาษาอังกฤษ" required>
      </div>
    </div>
    <div class="form-group">
      <label for="desc_th" class="col-md-3 control-label">รายละเอียด (th)<span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="desc_th" id="desc_th" value="<?php echo($category_id !=0)? $category->desc_th:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="desc_en" class="col-md-3 control-label">รายละเอียด (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="desc_en" id="desc_en" value="<?php echo($category_id != 0)?$category->desc_en:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="thumb_image" class="col-md-3 control-label">Thumbnail <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        if($category_id != 0 && $category->thumb_image != ''){
          echo '<img src="'.base_url('uploads/'.$category->thumb_image).'" class="img-responsive" style="max-width:300px;">';
        }
        ?>
        <input type="file" class="form-control" name="thumb_image" id="thumb_image">
        <span class="help-block">400X400px</span>
      </div>
    </div>
    <div class="form-group">
      <label for="icon_image" class="col-md-3 control-label">Icon <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        if($category_id != 0 && $category->icon_image != ''){
          echo '<img src="'.base_url('uploads/'.$category->icon_image).'" class="img-responsive" style="max-width:300px;">';
        }
        ?>
        <input type="file" class="form-control" name="icon_image" id="icon_image">
        <span class="help-block">200X40px</span>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ <span class="required">*</span></label>
      <div class="col-md-6">
        <label class="radio-inline">
          <input type="radio" name="enable_status" id="enable_status_show" value="show" <?php echo ($category_id == 0 OR $category->enable_status == 'show')?'checked':'' ?>>
          <span class="label label-success">แสดง</span>
        </label>
        <label class="radio-inline">
          <input type="radio" name="enable_status" id="enable_status_hide" value="hide" <?php echo ($category_id != 0 && $category->enable_status == 'hide')?'checked':'' ?>>
          <span class="label label-danger">ซ่อน</span>
        </label>
      </div>
    </div>
    <div class="form-group">
      <label for="sort_priority" class="col-md-3 control-label">แสดงลำดับที่</label>
      <div class="col-md-6">
        <select name="sort_priority" id="sort_priority" class="form-control">
          <?php
          $max_priority = $this->Category_model->get_max_priority();
          $max_priority += ($category_id == 0)?1:0;
          for($i = 1; $i<=$max_priority; $i++){
            $checked = ($category_id != 0 && $category->sort_priority == $i)?'selected="selected"':'';
            echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-heading">เนื้อหาหน้าหมวดหมู่</div>
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-2 control-label" for="content_th">HTML (th)</label>
      <div class="col-md-10">
        <textarea rows="10" class="form-control" id="content_th" name="content_th" data-plugin-codemirror data-plugin-options='{ "mode": "text/html" }'><?php echo ($category_id != 0)?$category->content_th:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="content_en">HTML (en)</label>
      <div class="col-md-10">
        <textarea rows="10" class="form-control" id="content_en" name="content_en" data-plugin-codemirror data-plugin-options='{ "mode": "text/html" }'><?php echo ($category_id != 0)?$category->content_en:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-2 control-label" for="style">Style</label>
      <div class="col-md-10">
        <textarea rows="10" class="form-control" id="style" name="style" data-plugin-codemirror data-plugin-options='{ "mode": "text/css" }'><?php echo ($category_id != 0)?$category->style:''; ?></textarea>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
