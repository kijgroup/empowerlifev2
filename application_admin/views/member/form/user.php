<?php
if($member_id != 0 && $member->user_id != 0){
  $user = $this->User_model->get_data($member->user_id);
}
?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลผู้ใช้งานเวบไซต์</div>
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">บัญชีเวบไซต์</label>
      <div class="col-md-6">
        <select class="form-control" name="has_user" id="has_user">
          <option value="false" <?php echo ($member_id != 0 && $member->user_id == 0)?'selected="selected"':''; ?>>ไม่มี</option>
          <?php ?>
          <option value="true" <?php echo ($member_id != 0 && $member->user_id != 0)?'selected="selected"':''; ?>>มี</option>
        </select>
      </div>
    </div>
    <div class="form-group user_form">
      <label for="email" class="col-md-3 control-label">อีเมลเข้าสู่ระบบ</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="email" id="email" value="<?php echo ($member_id != 0 && $member->user_id != 0)?$user->email:''; ?>" required>
      </div>
    </div>
    <?php if($member_id == 0 || $member->user_id == 0){ ?>
    <div class="form-group user_form">
      <label for="login_password" class="col-md-3 control-label">รหัสผ่าน </label>
      <div class="col-md-6">
        <input type="password" class="form-control" name="login_password" id="login_password" placeholder="" maxlength="20" required>
      </div>
    </div>
    <div class="form-group user_form">
      <label for="login_confirm_password" class="col-md-3 control-label">ยืนยันรหัสผ่าน </label>
      <div class="col-md-6">
        <input type="password" class="form-control" name="login_confirm_password" id="login_confirm_password" placeholder="" maxlength="20" required>
      </div>
    </div>
    <?php } ?>
  </div>
</div>
