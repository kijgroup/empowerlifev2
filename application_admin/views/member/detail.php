<header class="page-header">
  <h2><?php echo $title; ?></h2>
  <div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
      <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
      <li><span><?php echo anchor('member','สมาชิกเวบไซต์'); ?></span></li>
      <li><span><?php echo $title; ?></span></li>
    </ol>
  </div>
</header>
<div class="pb-lg">
  <div class="btn-group hide-print">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
      การกระทำ <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
      <li><?php echo anchor('member', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
      <li><?php echo anchor('member/form/', '<i class="glyphicon glyphicon-plus"></i> สร้างสมาชิกใหม่'); ?></li>
      <li><?php echo anchor('member/form/'.$member->member_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
      <li><a href="#" data-toggle="modal" data-target="#passwordModal"><i class="glyphicon glyphicon-pencil"></i> เปลี่ยนรหัสผ่าน</a></li>
      <li class="divider"></li>
      <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
    </ul>
  </div>
  <?php echo form_open('order/form', array('style'=>'display:inline-block;')); ?>
    <input type="hidden" name="member_id" value="<?php echo $member_id; ?>" />
    <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> เพิ่มรายการสั่งซื้อ</button>
  <?php echo form_close(); ?>
</div>
<div class="panel panel-default">
  <div class="panel-body form-horizontal">
    <div class="form-group">
      <label class="col-md-3 control-label">รหัสสมาชิก</label>
      <div class="col-md-6" id="detail_member_code">
        <p class="form-control-static">
          <?php
          if($member->member_code != ''){
            echo $member->member_code;
            echo '<a href="#" data-fieldname="member_code" class="pull-right btn-edit-field">แก้ไข</a>';
          }else{
            echo anchor('member/create_member_code/'.$member->member_id, '<i class="fa fa-user-plus"></i> สร้างอัตโนมัติ', array('class'=>'btn btn-primary btn-xs'));
            echo ' <a href="#" data-fieldname="member_code" class="btn btn-warning btn-xs btn-edit-field"><i class="fa fa-user-plus"></i> ระบุเอง</a>';
          }
          ?>
        </p>
      </div>
      <div class="col-md-6 form_edit" id="form_member_code">
        <?php echo form_open('member/update_field/member_code/'.$member_id); ?>
        <div class="input-group input-group-icon" style="padding-bottom:10px;">
          <input type="text" class="form-control" name="input_value" id="input_member_code" value="<?php echo $member->member_code; ?>">
          <span class="input-group-addon">
            <span class="icon"><i class="fa fa-asterisk"></i></span>
          </span>
        </div>
        <div>
          <button type="submit" class="btn btn-primary">บันทึก</button>
          <button type="button" class="btn btn-warning btn-cancel-form" data-fieldname="member_code">ยกเลิก</button>
        </div>
        <?php echo form_close(); ?>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ประเภทสมาชิก</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Member_type_model->get_label($member->member_type); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ยอดซื้อสะสม</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->buy_total; ?></p>
      </div>
    </div>
    <?php if($member->member_type != 'normal'){ ?>
    <div class="form-group">
      <label class="col-md-3 control-label">วันที่หมดอายุ vip</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->expire_date; ?></p>
      </div>
    </div>
    <?php } ?>
    <div class="form-group">
      <label class="col-md-3 control-label">แต้มสะสม</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_point; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ชื่อ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_firstname; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">นามสกุล</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_lastname; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">เพศ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_gender; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">วันเกิด</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_birthdate; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ที่อยู่</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_address; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ตำบล/แขวง</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_district; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">เขต/อำเภอ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_amphur; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">จังหวัด</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_province; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">รหัสไปรษณีย์</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_postcode; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">เบอร์โทรศัพท์</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_phone; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">เบอร์มือถือ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_mobile; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">อีเมล</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_email; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Member_enable_status_model->get_label($member->enable_status); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">เข้าใช้งานล่าสุด</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->last_login_date; ?></p>
      </div>
    </div>
    <div class="row form-group">
      <label class="col-md-3 col-print-3 control-label">สร้างวันที่</label>
      <div class="col-md-3 col-print-3 xs-margin-bottom">
        <p class="form-control-static"><?php echo $member->create_date; ?></p>
      </div>
      <label class="col-md-3 col-print-3 control-label">โดย</label>
      <div class="col-md-3 col-print-3">
        <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($member->create_by); ?></p>
      </div>
    </div>
    <?php if($member->update_by > 0){ ?>
      <div class="row form-group">
        <label class="col-md-3 col-print-3 control-label">ปรับปรุงวันที่</label>
        <div class="col-md-3 col-print-3 xs-margin-bottom">
          <p class="form-control-static"><?php echo $member->update_date; ?></p>
        </div>
        <label class="col-md-3 col-print-3 control-label">โดย</label>
        <div class="col-md-3 col-print-3">
          <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($member->update_by); ?></p>
        </div>
      </div>
    <?php } ?>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="deleteModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('member/delete/'.$member->member_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="passwordModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="passwordModalLabel">เปลี่ยนรหัสผ่าน</h4>
      </div>
      <?php echo form_open('member/form_change_password/'.$member->member_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'password-form')); ?>
      <div class="modal-body">
        <div class="form-group">
            <label for="txtPassword" class="col-md-3 control-label">Password</label>
            <div class="col-sm-6">
                <input type="password" class="form-control" name="txtPassword" id="txtPassword" placeholder="" maxlength="20">
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <button type="submit" class="btn btn-primary">เปลี่ยนรหัสผ่าน</button>
      </div>
      <?php echo form_close(); ?>
    </div>
  </div>
</div>


<?php
$this->load->view('member/detail/order_list');
$this->load->view('member/detail/reward_list');
?>
