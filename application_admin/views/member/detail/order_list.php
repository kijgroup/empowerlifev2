<div class="table-responsive">
  <div class="panel-heading">ประวัติการสั่งซื้อ</div>
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>ชื่อผู้รับสินค้า</th>
        <th>ราคารวม</th>
        <th>สถานะ</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($order_list->result() as $order){
        $detail_url = 'order/detail/'.$order->order_id;
        ?>
        <tr>
          <td>
            <?php echo anchor($detail_url, ($order->shipping_name != '')?$order->shipping_name:'-'); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, ($order->grand_total != '')?$order->grand_total:'-'); ?>
          </td>
          <td>
            <?php echo $this->Order_status_model->get_label($order->order_status); ?>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
