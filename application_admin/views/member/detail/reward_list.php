<div class="table-responsive">
  <div class="panel-heading">ข้อมูลแลกของรางวัล</div>
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>วันที่ส่งข้อมูล</th>
        <th>แต้มแลก</th>
        <th>สถานะ</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($reward_transaction_list->result() as $reward_transaction){
        $detail_url = 'reward_transaction/detail/'.$reward_transaction->reward_transaction_id;
        ?>
        <tr>
          <td>
            <?php echo anchor($detail_url, $reward_transaction->create_date); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, number_format($reward_transaction->total_reward_point, 0)); ?>
          </td>
          <td>
            <?php echo $this->Reward_transaction_status_model->get_label($reward_transaction->reward_transaction_status); ?>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
