<header class="page-header">
<h2>เบอร์ติดต่อกลับ</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>เบอร์ติดต่อกลับ</span></li>
  </ol>
</div>
</header>
<div class="search-content">
  <div class="search-control-wrapper">
    <?php echo form_open('content_contact/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'searchForm')); ?>
    <div class="form-group">
      <input type="text" class="form-control" name="search_val" id="search_val" placeholder="ข้อความที่ต้องการค้นหา" />
    </div>
    <div class="form-group">
      <select class="form-control" name="content_id" id="content_id">
        <option value="all" selected="selected">ทุกบทความ</option>
        <?php
        $option_list = $this->Content_model->get_display_contact_list();
        foreach($option_list->result() as $option){
          echo '<option value="'.$option->content_id.'">'.$option->subject_th.'</option>';
        }
        ?>
      </select>
    </div>
    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> ค้นหา</button>
    <?php echo form_close(); ?>
  </div>
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('content_contact/form', '<i class="fa fa-plus"></i> เพิ่มเบอร์โทรศัพท์', array('class'=>'btn btn-success')); ?>
    </div>
    <div id="result">
    </div>
    <br />
  </div>
</div>
