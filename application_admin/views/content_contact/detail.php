<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span><?php echo anchor('content_contact','เบอร์ติดต่อกลับ'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('content_contact', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('content_contact/form/', '<i class="glyphicon glyphicon-plus"></i> เพิ่มช้อมูล'); ?></li>
    <li><?php echo anchor('content_contact/form/'.$content_contact->content_contact_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
  <div class="panel-body form-horizontal">
    <div class="form-group">
      <label class="col-md-3 control-label">บทความ</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $this->Content_model->get_name($content_contact->content_id); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ชื่อ</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $content_contact->name; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">เบอร์โทร</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $content_contact->mobile; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">หมายเหตุ</label>
      <div class="col-md-6">
        <pre><?php echo $content_contact->remark; ?></pre>
      </div>
    </div>
    <div class="row form-group">
      <label class="col-md-3 col-print-3 control-label">สร้างวันที่</label>
      <div class="col-md-3 col-print-3 xs-margin-bottom">
        <p class="form-control-static"><?php echo $content_contact->create_date; ?></p>
      </div>
      <label class="col-md-3 col-print-3 control-label">โดย</label>
      <div class="col-md-3 col-print-3">
        <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($content_contact->create_by); ?></p>
      </div>
    </div>
    <?php if($content_contact->update_by > 0){ ?>
      <div class="row form-group">
        <label class="col-md-3 col-print-3 control-label">ปรับปรุงวันที่</label>
        <div class="col-md-3 col-print-3 xs-margin-bottom">
          <p class="form-control-static"><?php echo $content_contact->update_date; ?></p>
        </div>
        <label class="col-md-3 col-print-3 control-label">โดย</label>
        <div class="col-md-3 col-print-3">
          <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($content_contact->update_by); ?></p>
        </div>
      </div>
    <?php } ?>
  </div>
</div>
<?php
$this->load->view('content_contact/detail/delete_popup');
