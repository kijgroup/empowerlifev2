<?php
if($total_transaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $total_page = ceil($total_transaction/$per_page);
  $pager_data = array(
    'total_transaction' => $total_transaction,
    'search_val' => $search_val,
    'content_id' => $content_id,
    'per_page' => $per_page,
    'page' => $page,
    'total_page' => $total_page,
    'action' => site_url('content_contact/filter')
  );
?>
<div class="form-group">
    <?php $this->load->view('content_contact/list/pager', $pager_data); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th width="60">#</th>
        <th width="120">เบอร์โทร</th>
        <th width="120">ชื่อ</th>
        <th>จากบทความ</th>
        <th>หมายเหตุ</th>
        <th>วันที่บันทึกข้อมูล</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($content_list->result() as $content_contact){
        $detail_url = 'content_contact/detail/'.$content_contact->content_contact_id;
        ?>
        <tr>
          <td><?php echo anchor($detail_url, $content_contact->content_contact_id); ?></td>
          <td><?php echo anchor($detail_url, $content_contact->mobile); ?></td>
          <td><?php echo anchor($detail_url, $content_contact->name); ?></td>
          <td><?php echo anchor($detail_url, $this->Content_model->get_name($content_contact->content_id)); ?></td>
          <td><?php echo anchor($detail_url, ($content_contact->remark !== "")?$content_contact->remark:'-'); ?></td>
          <td><?php echo anchor($detail_url, $content_contact->create_date); ?></td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
  <?php $this->load->view('content_contact/list/pager', $pager_data); ?>
</div>
<?php
}
