<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span><?php echo anchor('content_contact','เบอร์ติดต่อกลับ'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open_multipart('content_contact/form_post/'.$content_contact_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'content-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="content_group_id" class="col-md-3 control-label">บทความ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo ($content_contact_id !== 0 && $content_contact->content_id !== 0 )?$this->Content_model->get_name($content_contact->content_id):'-'; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label for="name" class="col-md-3 control-label">ชื่อ <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="name" id="name" value="<?php echo ($content_contact_id != 0)?$content_contact->name:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="mobile" class="col-md-3 control-label">เบอร์โทรศัพท์ <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="mobile" id="mobile" value="<?php echo ($content_contact_id != 0)?$content_contact->mobile:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="remark" class="col-md-3 control-label">หมายเหตุ</label>
      <div class="col-md-6">
        <textarea class="form-control" name="remark" id="remark" rows="3" maxlength="100" required><?php echo ($content_contact_id != 0)?$content_contact->remark:''; ?></textarea>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
