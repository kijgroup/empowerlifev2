<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>สินค้า</span></li>
    <li><span><?php echo anchor('product','สินค้า'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('product', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('product/form/', '<i class="glyphicon glyphicon-plus"></i> เพิ่มสินค้าใหม่'); ?></li>
    <li><?php echo anchor('product/form/'.$product->product_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
  <div class="panel-body form-horizontal">
    <div class="form-group">
      <label class="col-md-3 control-label">ชื่อหมวดหมู่</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $this->Category_model->get_name($product->category_id); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ชื่อสินค้า (th)</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $product->name_th; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ชื่อสินค้า (en)</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $product->name_en; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">slug</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $product->slug; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">รูปสินค้า</label>
      <div class="col-md-3">
        <p class="form-control-static"><?php echo ($product->thumb_image != '')?'<img src="'.base_url('uploads/'.$product->thumb_image).'" class="img-responsive" />':''; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ข้อมูลสินค้า (th)</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $product->detail_th; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ข้อมูลสินค้า (en)</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $product->detail_en; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ราคา</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo number_format($product->price); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ราคาก่อนหักส่วนลด</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo ($product->price_before_discount > 0 && $product->price_before_discount != $product->price)?number_format($product->price_before_discount):'ไม่ระบุ'; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">อัตราการลดราคาจากราคาเต็ม</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo ($product->price_discount_rate != 0)?number_format($product->price_discount_rate, 2):'ไม่ระบุ'; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $this->Enable_status_model->get_label($product->enable_status); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">แสดงลำดับที่</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $product->sort_priority; ?></p>
      </div>
    </div>
    <div class="row form-group">
      <label class="col-md-3 col-print-3 control-label">สร้างวันที่</label>
      <div class="col-md-3 col-print-3 xs-margin-bottom">
        <p class="form-control-static"><?php echo $product->create_date; ?></p>
      </div>
      <label class="col-md-3 col-print-3 control-label">โดย</label>
      <div class="col-md-3 col-print-3">
        <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($product->create_by); ?></p>
      </div>
    </div>
    <?php if($product->update_by > 0){ ?>
      <div class="row form-group">
        <label class="col-md-3 col-print-3 control-label">ปรับปรุงวันที่</label>
        <div class="col-md-3 col-print-3 xs-margin-bottom">
          <p class="form-control-static"><?php echo $product->update_date; ?></p>
        </div>
        <label class="col-md-3 col-print-3 control-label">โดย</label>
        <div class="col-md-3 col-print-3">
          <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($product->update_by); ?></p>
        </div>
      </div>
    <?php } ?>
  </div>
</div>
<?php
$this->load->view('product/detail/delete_popup');
$this->load->view('product/detail/image_list');
$this->load->view('product/detail/image_add');
$this->load->view('product/detail/relate_list');
