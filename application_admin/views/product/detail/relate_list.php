<div class="panel panel-default">
  <div class="panel-heading">
    สินค้าใกล้เคียง
  </div>
  <div class="panel-body" id="result-list-image">
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th width="120">รูปสินค้า</th>
            <th>ข้อมูลสินค้า</th>
          </tr>
        </thead>
        <?php
        $product_relate_list = $this->Product_relate_model->get_list($product->product_id);
        foreach($product_relate_list->result() as $relate){
          $product_relate = $this->Product_model->get_data($relate->relate_id);
          if($product_relate){
            ?>
            <tr>
              <td>
                <?php
                if($product_relate->thumb_image != ''){
                  echo '<img src="'.base_url('uploads/'.$product_relate->thumb_image).'" style="width:120px;" />';
                }
                ?>
              </td>
              <td>
                <div><b>TH:</b> <?php echo $product_relate->name_th; ?></div>
                <div><b>EN:</b> <?php echo $product_relate->name_en; ?></div>
                <div><?php echo ($product_relate->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ซ่อน</span>'; ?></div>
              </td>
            </tr>
            <?php
          }
        }
        ?>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>
