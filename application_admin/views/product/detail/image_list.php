<div class="panel panel-default">
  <div class="panel-heading">
    รูปภาพ
    <a hef="#" data-toggle="modal" data-target="#addImageModal" class="btn btn-success btn-xs pull-right"><i class="glyphicon glyphicon-plus"></i> เพิ่มรูป</a>
  </div>
  <div class="panel-body sortable" id="result-list-image" data-sort_url="<?php echo site_url('product/update_image_priority/'); ?>" style="position:relative;">
    <?php
    foreach($product_image_list->result() as $product_image){
      ?>
      <div class="col-md-3" data-image_id="<?php echo $product_image->product_image_id; ?>" style="height:280px;">
        <img src="<?php echo base_url('uploads/'.$product_image->thumb_image); ?>" alt="" class="img-thumbnail" />
        <div class="pt-sm">
          <?php echo anchor('product/image_delete/'.$product->product_id.'/'.$product_image->product_image_id, 'ลบรูป', array('class'=>'btn btn-danger')); ?>
        </div>
      </div>
      <?php
    }
    ?>
  </div>
</div>
