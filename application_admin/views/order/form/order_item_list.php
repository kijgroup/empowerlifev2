<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>สินค้า</th>
        <th class="text-right" style="width:100px;">จำนวน</th>
        <th class="text-right" style="width:150px;">ราคาขาย</th>
        <th class="text-right" style="width:150px;">รวมราคา</th>
        <th style="width:120px;"></th>
      </tr>
    </thead>
    <tbody id="order_item_list">
      <?php
      if($order_item_list){
        foreach($order_item_list->result() as $order_item){
          $this->load->view('order/form/order_item', array('order_item' => $order_item));
        }
      }
      ?>
    </tbody>
  </table>
</div>
<div class="pb-sm">
  <button type="button" class="btn btn-primary" id="btn-plus"><i class="fa fa-plus"></i> เพิ่มรายการ</button>
</div>
