<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลผู้สั่งซื้อ</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="member_name" class="col-md-3 control-label">ชื่อ <span class="required">*</span></label>
      <div class="col-md-6">
        <select class="form-control" data-plugin-selectTwo name="member_id" id="member_id">
          <option value="">โปรดระบุ</option>
          <?php
          $member_list = $this->Member_model->get_list();
          foreach($member_list->result() as $member){
            $selected = ($order_id != 0 && $order->member_id == $member->member_id)?'selected="selected"':'';
            echo '<option value="'.$member->member_id.'" '.$selected.'
            data-member_type="'.$member->member_type.'"
            data-member_point="'.$member->member_point.'"
            data-member_firstname="'.$member->member_firstname.'"
            data-member_lastname="'.$member->member_lastname.'"
            data-member_mobile="'.$member->member_mobile.'"
            data-member_address="'.$member->member_address.'"
            data-member_district="'.$member->member_district.'"
            data-member_amphur="'.$member->member_amphur.'"
            data-member_province="'.$member->member_province.'"
            data-member_postcode="'.$member->member_postcode.'"
            >'.$member->member_firstname.' '.$member->member_lastname.' - '.$member->member_code.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group" id="member_type_detail">
    </div>
    <div class="form-group" id="member_point_detail">
    </div>
    <div class="form-group" id="member_mobile_detail">
    </div>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="form-group">
      <label for="order_channel_id" class="col-md-3 control-label">ช่องทางการสั่งซื้อ</label>
      <div class="col-md-6">
        <select class="form-control" name="order_channel_id" id="order_channel_id">
          <?php
          $order_channel_list = $this->Order_channel_model->get_list();
          foreach($order_channel_list->result() as $order_channel){
            $selected = ($order_id != 0 && $order_channel->order_channel_id == $order->order_channel_id)?'selected="selected"':'';
            echo '<option value="'.$order_channel->order_channel_id.'" '.$selected.'>'.$order_channel->order_channel_name.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="order_sub_channel_id" class="col-md-3 control-label">Source</label>
      <div class="col-md-6">
        <select class="form-control" name="order_sub_channel_id" id="order_sub_channel_id">
          <option value="0">--โปรดระบุ--</option>
        <?php
        $order_sub_channel_list = $this->Order_sub_channel_model->get_list();
        foreach($order_sub_channel_list->result() as $order_sub_channel){
          $selected = ($order_id != 0 && $order_sub_channel->order_sub_channel_id == $order->order_sub_channel_id)?'selected="selected"':'';
          echo '<option value="'.$order_sub_channel->order_sub_channel_id.'" '.$selected.'>'.$order_sub_channel->order_sub_channel_name.'</option>';
        }
        ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="order_channel_text" class="col-md-3 control-label">รายละเอียด Source</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="order_channel_text" id="order_channel_text" value="<?php echo ($order_id != 0)?$order->order_channel_text:''; ?>" />
      </div>
    </div>
    <div class="form-group">
      <label for="contact_channel_id" class="col-md-3 control-label">Show</label>
      <div class="col-md-6">
        <select class="form-control" name="contact_channel_id" id="contact_channel_id">
          <option value="0">--โปรดระบุ--</option>
          <?php
          $contact_channel_list = $this->Contact_channel_model->get_list();
          foreach($contact_channel_list->result() as $contact_channel){
            $selected = ($order_id != 0 && $contact_channel->contact_channel_id == $order->contact_channel_id)?'selected="selected"':'';
            echo '<option value="'.$contact_channel->contact_channel_id.'" '.$selected.'>'.$contact_channel->contact_channel_name.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="form-group">
      <label for="payment_type" class="col-md-3 control-label">วิธีการชำระเงิน <span class="required">*</span></label>
      <div class="col-md-6">
        <select class="form-control" name="payment_type" id="payment_type">
          <?php
          $payment_type_list = $this->Payment_type_model->get_list();
          foreach($payment_type_list as $payment_type){
            $selected = ($order_id != 0 && $payment_type['status_code'] == $order->payment_type)?'selected="selected"':'';
            echo '<option value="'.$payment_type['status_code'].'" '.$selected.'>'.$payment_type['status_name'].'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group" id="postoffice-form">
      <label for="postoffice" class="col-md-3 control-label">ชื่อสาขาไปรษณีย์</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="postoffice" id="postoffice" value="<?php echo ($order_id != 0 && isset($order->postoffice))?$order->postoffice:''; ?>" />
      </div>
    </div>
    <div class="form-group">
      <label for="shipping_type_id" class="col-md-3 control-label">วิธีการจัดส่ง</label>
      <div class="col-md-6">
        <select class="form-control" name="shipping_type_id" id="shipping_type_id">
          <option value="0">--โปรดระบุ--</option>
          <?php
          $shipping_type_list = $this->Shipping_type_model->get_list();
          foreach($shipping_type_list->result() as $shipping_type){
            $selected = ($order_id != 0 && $shipping_type->shipping_type_id == $order->shipping_type_id)?'selected="selected"':'';
            echo '<option value="'.$shipping_type->shipping_type_id.'" '.$selected.'>'.$shipping_type->shipping_type_name.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="bank_id" class="col-md-3 control-label">ธนาคารที่ชำระเงิน</label>
      <div class="col-md-6">
        <select class="form-control" name="bank_id" id="bank_id">
          <option value="0">ไม่ระบุ</option>
          <?php
          $bank_list = $this->Bank_model->get_list();
          foreach($bank_list->result() as $bank){
            $selected = ($order_id != 0 && $order->bank_id == $bank->bank_id)?' selected="selected"':'';
            echo '<option value="'.$bank->bank_id.'" '.$selected.'>'.$bank->bank_name_th.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะการส่ง <span class="required">*</span></label>
      <div class="col-md-6">
        <select class="form-control" name="order_status" id="order_status">
          <?php
          $order_status_list = $this->Order_status_model->get_list();
          foreach($order_status_list as $order_status){
            $selected = ($order_id != 0 && $order_status['status_code'] == $order->order_status)?'selected="selected"':'';
            echo '<option value="'.$order_status['status_code'].'" '.$selected.'>'.$order_status['status_name'].'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="stock_id" class="col-md-3 control-label">ตัด Stock</label>
      <div class="col-md-6">
        <select class="form-control" name="stock_id" id="stock_id">
          <option value="0">--โปรดระบุ--</option>
          <?php
          $stock_list = $this->Stock_model->get_list();
          foreach($stock_list->result() as $stock){
            $selected = ($order_id != 0 && $stock->stock_id == $order->stock_id)?'selected="selected"':'';
            echo '<option value="'.$stock->stock_id.'" '.$selected.'>'.$stock->stock_name.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">ส่งเช้า</label>
      <div class="col-md-6">
        <select class="form-control" name="send_time_status" id="send_time_status">
          <?php
          $send_time_status_list = $this->Send_time_status_model->get_list();
          foreach($send_time_status_list as $send_time_status){
            $selected = ($order_id != 0 && $send_time_status['status_code'] == $order->send_time_status)?'selected="selected"':'';
            echo '<option value="'.$send_time_status['status_code'].'" '.$selected.'>'.$send_time_status['status_name'].'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะการโอนเงิน</label>
      <div class="col-md-6">
        <select class="form-control" name="payment_status" id="payment_status">
          <?php
          $payment_status_list = $this->Payment_status_model->get_list();
          foreach($payment_status_list as $payment_status){
            $selected = ($order_id != 0 && $payment_status['status_code'] == $order->payment_status)?'selected="selected"':'';
            echo '<option value="'.$payment_status['status_code'].'" '.$selected.'>'.$payment_status['status_name'].'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
</div>