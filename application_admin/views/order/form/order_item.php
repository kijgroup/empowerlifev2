<tr class="row_item">
  <td>
    <select class="form-control product_id" name="product_id[]">
      <?php
      $product_list = $this->Product_model->get_list();
      foreach($product_list->result() as $product){
        $selected = ($order_item->product_id == $product->product_id)?'selected="selected"':'';
        echo '<option value="'.$product->product_id.'" '.$selected.' data-price="'.$product->price.'">'.$product->name_th.'</option>';
      }
      ?>
    </select>
  </td>
  <td><input type="text" class="form-control text-right qty" name="qty[]" value="<?php echo $order_item->qty; ?>" /></td>
  <td><input type="text" class="form-control text-right price_per_item" name="price_per_item[]" value="<?php echo $order_item->price_per_item; ?>" /></td>
  <td class="text-right price_total"><?php echo number_format($order_item->price_total, 2); ?></td>
  <td class="text-center"><button type="button" class="btn btn-danger btn-remove-product"><i class="fa fa-trash"></i> Remove</button></td>
</tr>
