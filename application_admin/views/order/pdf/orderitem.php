<?php
$table_width = 100;
$order_item_list = $this->Order_item_model->get_list($order->order_id);
$total_rows = $order_item_list->num_rows();
if($total_rows > 3){
  $table_width = 100 + ($total_rows - 3)*8;
}
?>
<table width="<?php echo $table_width; ?>%" cellpadding="0" cellspacing="0" class="borderlist partlist" >
  <tr>
    <th class="first-child">ลำดับ</th>
    <th>รายการสินค้า</th>
    <th>จำนวน</th>
    <th>ราคา<br />ต่อหน่วย</th>
    <th>ส่วนลด<br />ต่อหน่วย</th>
    <th>ราคาต่อหน่วย<br />(หลังลด)</th>
    <th>รวม (บาท)</th>
  </tr>
  <?php
  $count_item = 0;
  foreach($order_item_list->result() as $order_item){
    $count_item++;
    $product = $this->Product_model->get_data($order_item->product_id);
    $discount_item = 0;
    if($order->item_price > 0){
      $discount_item = ($order->discount_price + $order->discount_by_point_amount) * $order_item->price_total / $order->item_price;
    }
    $discount_per_item = ($order_item->qty > 0)?$discount_item/$order_item->qty:0;
    $price_after_discount = $order_item->price_per_item - $discount_per_item;
    ?>
    <tr>
      <td width="10%" class="first-child text-center"><?php echo $count_item; ?></td>
      <td width="35%"><?php echo $product->name_th; ?></td>
      <td width="10%" class="text-center"><?php echo $order_item->qty; ?></td>
      <td width="10%" style="text-align:right;"><?php echo number_format($order_item->price_per_item, 2); ?></td>
      <td width="10%" style="text-align:right;"><?php echo number_format($discount_per_item, 2); ?></td>
      <td width="15%" style="text-align:right;"><?php echo number_format($price_after_discount, 2); ?></td>
      <td width="10%" style="text-align:right;"><?php echo number_format($order_item->price_total, 2); ?></td>
    </tr>
    <?php
  }
  ?>
  <tr>
    <td class="first-child" colspan="5" rowspan="8">
      <div style="font-size:5px;">&nbsp;</div>
      <div style="font-size:1.1em;">&nbsp;&nbsp;<?php
      echo $this->Payment_type_model->get_name($order->payment_type);
      if($order->payment_type == 'post'){
        echo ' '.$order->postoffice;
      }
      ?></div>
      <div style="font-size:1.2em;color:red;">&nbsp;&nbsp;<?php echo $this->Shipping_type_model->get_name($order->shipping_type_id). ' '.$this->Send_time_status_model->get_name($order->send_time_status); ?></div>
      &nbsp;&nbsp;<?php
      if($order->bank_id != 0){
        $bank = $this->Bank_model->get_data($order->bank_id);
        echo $bank->bank_name_th.'<br />&nbsp;&nbsp;'.$bank->bank_info_th;
      }
      ?><br />
      &nbsp;&nbsp;<?php echo $order->remark; ?>
    </td>
    <td class="text-right">ราคารวม</td>
    <td class="text-right"><?php echo number_format($order->item_price, 2); ?></td>
  </tr>
  <tr>
    <td class="text-right">แต้มแลกส่วนลด(<?php echo number_format($order->discount_point_amount, 0); ?> แต้ม)</td>
    <td class="text-right"><?php echo number_format($order->discount_by_point_amount, 2); ?></td>
  </tr>
  <tr>
    <td class="text-right">ส่วนลดเพิ่ม</td>
    <td class="text-right"><?php echo number_format($order->discount_price, 2); ?></td>
  </tr>
  <tr>
    <td class="text-right">ราคาสุทธิ</td>
    <td class="text-right"><?php echo number_format($order->item_price - $order->discount_by_point_amount - $order->discount_price, 2); ?></td>
  </tr>
  <tr>
    <td class="text-right">VAT 0%</td>
    <td class="text-right">0.00</td>
  </tr>
  <tr>
    <td class="text-right">ราคารวม(VAT)</td>
    <td class="text-right"><?php echo number_format($order->item_price - $order->discount_by_point_amount - $order->discount_price, 2); ?></td>
  </tr>
  <tr>
    <td class="text-right">ค่าจัดส่ง</td>
    <td class="text-right"><?php echo number_format($order->shipping_price, 2); ?></td>
  </tr>
  <tr>
    <td class="text-right">ราคารวม (ค่าจัดส่ง)</td>
    <td class="text-right"><?php echo number_format($order->grand_total, 2); ?></td>
  </tr>
</table>
<hr/>