<div style="border:1px solid #000; padding:20px 20px;page-break-inside: avoid;">
  <table width="100%" style="page-break-inside: avoid;">
    <tr style="page-break-inside: avoid;">
      <td width="35%">
        <div>
          ผู้ส่ง. บริษัท เอ็มพาวเวอร์ ไลฟ์ จำกัด<br/>
          <div style="padding-left:32px;">
            87/14-15 ถ.รัชดาภิเษก(ท่าพระ-ตากสิน) แขวงตลาดพลู เขตธนบุรี กรุงเทพฯ 10600 โทร. 021067999
          </div>
        </div>
      </td>
      <td width="17%"></td>
      <td>
        <?php if($order->payment_type == 'post'){ ?>
        <div>ตู้ปณ. <?php echo $order->postoffice; ?></div>
        <div>**ยอดเงินชำระ <?php echo number_format($order->grand_total, 2); ?> บาท</div>
        <div>(<?php echo $this->Number_service->number_to_words_thai($order->grand_total); ?>)</div>
        <?php } ?>
      </td>
    </tr>
  </table>
  <div>
  </div>
  <div style="padding-left:300px; font-size:15px; padding-top:10px;">
    <b>ผู้รับ. </b><br/>
  </div>
  <div style="padding-left:350px;" width="35%">
    คุณ <?php echo $order->shipping_name; ?><br/>
    <?php
    echo $order->shipping_address.' แขวง/ตำบล '.$order->shipping_district.' เขต/อำเภอ '.$order->shipping_amphur.' จังหวัด '.$order->shipping_province.' '.$order->shipping_post_code; ?><br/>
    โทร. <?php echo $order->shipping_mobile; ?>
  </div>
</div>
