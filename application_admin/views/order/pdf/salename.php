
<table width="100%">
  <tr>
    <td width="60%" style="padding-right:20px; padding-top:10px; ">
      <table>
        <tr>
          <td class="text-center">ผู้แทนขาย </td>
          <td><?php echo $this->Admin_model->get_login_name_by_id($this->session->userdata('admin_id')); ?></td>
          <td>วันที่ <?php

          $now = new DateTime();
          $now->setTimezone(new DateTimeZone("Asia/Bangkok"));
          echo $this->Datetime_service->display_datetime($now->format('Y-m-d H:i:s'));
          ?>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<hr/>
