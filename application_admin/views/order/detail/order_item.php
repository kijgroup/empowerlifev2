<?php
$product = $this->Product_model->get_data($order_item->product_id);
?>
<tr>
  <td><?php echo $product->name_th; ?></td>
  <td class="text-right"><?php echo number_format($order_item->qty, 0); ?></td>
  <td class="text-right"><?php echo number_format($order_item->price_per_item, 2); ?></td>
  <td class="text-right"><?php echo number_format($order_item->price_total, 2); ?></td>
</tr>
