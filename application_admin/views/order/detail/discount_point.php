<div class="panel panel-default">
  <div class="panel-heading">คะแนนแลกส่วนลด</div>
  <div class="panel-body">
    <?php
    $order_discount_point_list = $this->Order_discount_point_model->get_list($order_id);
    foreach($order_discount_point_list->result() as $order_discount_point){
      ?>
      <div class="form-group">
        <label class="col-sm-6 control-label"><?php echo $order_discount_point->use_point.' คะแนน แลก '.$order_discount_point->discount_amount; ?> บาท</label>
        <div class="col-sm-6">
          <div class="input-group">
            <input type="text" class="form-control text-right" value="<?php echo $order_discount_point->qty; ?>" readonly />
            <span class="input-group-addon">ครั้ง</span>
          </div>
        </div>
      </div>
      <?php
    }
    ?>
    <div class="form-group">
      <label class="col-sm-6 control-label">รวมคะแนนที่ใช้</label>
      <div class="col-sm-6">
        <div class="input-group">
          <input type="text" class="form-control text-right" id="discount_point_amount" name="discount_point_amount" value="<?php echo ($order_id != 0)?$order->discount_point_amount:0; ?>" readonly />
          <span class="input-group-addon">คะแนน</span>
        </div>
      </div>
    </div>
  </div>
</div>