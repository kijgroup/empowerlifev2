<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>สินค้า</th>
        <th class="text-right">จำนวน</th>
        <th class="text-right">ราคาขาย</th>
        <th class="text-right">รวมราคา</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($order_item_list->result() as $order_item){
        $this->load->view('order/detail/order_item', array('order_item' => $order_item));
      }
      ?>
    </tbody>
  </table>
</div>
