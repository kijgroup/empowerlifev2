<!-- Modal -->
<div class="modal fade" id="unprintModal" tabindex="-1" role="dialog" aria-labelledby="unprintModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="unprintModalLabel">ปรับสถานะ</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ปรับสถานะ" เพื่อยืนยันการปรับสถานะข้อมูลนี้
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <button type="button" class="btn btn-primary btn-action" data-action="update_unprint"><i class="glyphicon glyphicon-edit"></i> ปรับสถานะ</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
