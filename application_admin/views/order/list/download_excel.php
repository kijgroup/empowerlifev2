<?php
echo form_open('order/download_excel', array('style' => 'display:inline-block;', 'target' => '_blank'));
$field_list = array(
  'total_transaction' => $total_transaction,
  'search_val' => $search_val,
  'payment_type' => $payment_type,
  'order_channel_id' => $order_channel_id,
  'send_time_status' => $send_time_status,
  'create_by' => $create_by,
  'start_date' => $start_date,
  'end_date' => $end_date,
  'order_status' => $order_status,
);
foreach($field_list as $field_name => $field_value){
  echo form_hidden($field_name, $field_value, array('class' => 'btn btn-primary'));
}
echo '<button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-download"></i> ดาวน์โหลด Excel</button>';
echo form_close();
?>