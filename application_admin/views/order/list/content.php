<?php
if($total_transaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $per_page = ($per_page <= 0)?20:$per_page;
  $total_page = ceil($total_transaction/$per_page);
  $pager_data = array(
    'total_transaction' => $total_transaction,
    'search_val' => $search_val,
    'payment_type' => $payment_type,
    'order_channe_id' => $order_channel_id,
    'send_time_status' => $send_time_status,
    'create_by' => $create_by,
    'start_date' => $start_date,
    'end_date' => $end_date,
    'order_status' => $order_status,
    'per_page' => $per_page,
    'page' => $page,
    'total_page' => $total_page,
    'action' => site_url('order/filter')
  );
  $order_status_page = $order_status;
?>
<div class="pb-lg">
  <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#pdfModal"><i class="glyphicon glyphicon-download"></i> ดาวน์โหลด PDF</button>
  <?php $this->load->view('order/list/download_excel'); ?>
  <div class="btn-group hide-print">
    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown">
      ปรับสถานะรายการ <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
      <?php
      $order_status_list = $this->Order_status_model->get_list();
      foreach($order_status_list as $order_status){
        echo '<li><a href="#" class="btn-action" data-action="status_'.$order_status['status_code'].'" ><i class="glyphicon glyphicon-send"></i> ปรับสถานะ'.$order_status['status_name'].'</a></li>';
      }
      ?>
    </ul>
  </div>
  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#unprintModal"><i class="glyphicon glyphicon-edit"></i> ปรับเป็นรอพิมพ์</button>
  <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบรายการ</button>
</div>
<?php $this->load->view('order/list/delete_popup'); ?>
<div class="form-group">
  <?php $this->load->view('order/list/pager', $pager_data); ?>
</div>
<?php echo form_open('order/action_list/'.$order_status_page, array('id'=>'action-form')); ?>
<input type="hidden" id="action" name="action" value="" />
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th width="50"><input type="checkbox" class="chk_all" onClick="selectall(this)"></th>
      <th>สมาชิก</th>
      <th>ชื่อผู้รับสินค้า</th>
      <th>ราคารวม</th>
      <th>การชำระเงิน</th>
      <th>ช่องทาง</th>
      <th>Source</th>
      <th>Show</th>
      <th>ธนาคารที่ชำระเงิน</th>
      <th>ตัด Stock</th>
      <th>ส่งเช้า</th>
      <th>สถานะ</th>
      <th>print</th>
      <th>วันที่สั่งซื้อ</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach($order_list->result() as $order){
      $detail_url = 'order/detail/'.$order->order_id;
      ?>
      <tr>
        <td><input type="checkbox" name="order_id[]" value="<?php echo $order->order_id; ?>"></td>
        <td>
          <?php echo anchor($detail_url, $this->Member_model->get_name($order->member_id)); ?>
        </td>
        <td>
          <?php echo anchor($detail_url, ($order->shipping_name != '')?$order->shipping_name:'-'); ?>
        </td>
        <td>
          <?php echo anchor($detail_url, ($order->grand_total != '')?$order->grand_total:'-'); ?>
        </td>
        <td>
          <?php echo $this->Payment_type_model->get_label($order->payment_type); ?>
          <div><?php echo ($order->bank_id > 0)?$this->Bank_model->get_name($order->bank_id):''; ?></div>
        </td>
        <td>
          <?php echo $this->Order_channel_model->get_name($order->order_channel_id); ?>
        </td>
        <td>
          <?php echo $this->Order_sub_channel_model->get_name($order->order_sub_channel_id); ?>
        </td>
        <td>
          <?php echo $this->Contact_channel_model->get_name($order->contact_channel_id); ?>
        </td>
        <td>
          <?php echo $this->Bank_model->get_name($order->bank_id); ?>
        </td>
        <td>
          <?php echo $this->Stock_model->get_name($order->stock_id); ?>
        </td>
        <td>
          <?php echo $this->Send_time_status_model->get_name($order->send_time_status); ?>
        </td>
        <td>
          <?php echo $this->Order_status_model->get_label($order->order_status); ?>
        </td>
        <td>
          <?php echo $this->Order_print_status_model->get_label($order->is_print); ?>
        </td>
        <td>
          <?php echo $order->create_date; ?><br />โดย <?php echo $this->Admin_model->get_login_name_by_id($order->create_by); ?>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>
</div>
<?php echo form_close(); ?>
<div class="form-group">
  <?php $this->load->view('order/list/pager', $pager_data); ?>
</div>
<?php
}