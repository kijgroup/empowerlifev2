<!-- Modal -->
<div class="modal fade" id="pdfModal" tabindex="-1" role="dialog" aria-labelledby="pdfModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="pdfModalLabel">ดาวน์โหลด PDF</h4>
      </div>
      <div class="modal-body">
        กรุณาเลือกสถานะการพิมพ์ก่อน download file PDF
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <button type="button" class="btn btn-primary btn-action" data-action="print_true"><i class="glyphicon glyphicon-edit"></i> ปรับสถานะเป็น พิมพ์</button>
        <button type="button" class="btn btn-danger btn-action" data-action="print_false"><i class="glyphicon glyphicon-edit"></i> ไม่ปรับสถานะ</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
