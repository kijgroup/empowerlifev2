<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model{
  public $table_name = '';
  public $field_key = '';
  public $password_key = '';
  public $field_name = '';
  public $sort_field = '';
  public $sort_type = 'DESC';
  public $is_sort_priority = false;
  public $code_pre = '';
  public $code_length = 4;
  public $structure = array();
  public $field_search_val_list = array();
  public function __construct(){
    parent::__construct();
    $this->update_structure();
    if($this->is_sort_priority == true){
      $this->sort_field = 'sort_priority';
      $this->sort_type = 'ASC';
    }else{
      $this->sort_field = $this->field_key;
    }
  }
  private function update_structure(){
    $structure = array();
    foreach($this->structure as $structure_item){
      $structure[$structure_item['field_key']] = $structure_item;
    }
    $this->structure = $structure;
  }
  public function get_data($key_id){
    $this->db->where($this->field_key, $key_id);
    $query = $this->db->get($this->table_name);
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row_array();
  }
  public function get_name($key_id){
    $main_data = $this->get_data($key_id);
    return ($main_data)?$main_data[$this->field_name]:'-';
  }
  public function get_list(){
    $this->db->where('is_delete', 'active');
    $this->db->order_by($this->sort_field, $this->sort_type);
    return $this->db->get($this->table_name);
  }
  public function search_content_data($search_data){
    $search_data['total_transaction'] = $this->search_count($search_data);
    $search_data['main_data_list'] = $this->search_list($search_data);
    return $search_data;
  }
  public function insert($data){
    if($this->is_sort_priority){
      $this->move_out_priority($data['sort_priority']);
    }
    if($this->code_pre != ''){
      $this->set_code();
    }
    if($this->is_sort_priority){
      $this->db->set('sort_priority', $data['sort_priority']);
    }
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 1);
    $this->db->insert($this->table_name);
    $key_id = $this->db->insert_id();
    $this->on_insert($key_id);
    return $key_id;
  }
  public function update($key_id, $data){
    $old_data = $this->get_data($key_id);
    if(!$old_data || $old_data['is_delete'] == 'delete'){
      return false;
    }
    if($this->is_sort_priority){
      $this->move_in_priority($old_data['sort_priority']);
      $this->move_out_priority($data['sort_priority']);
      $this->db->set('sort_priority', $data['sort_priority']);
    }
    $this->set_to_db($data);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 1);
    $this->db->where($this->field_key, $key_id);
    $this->db->update($this->table_name);
    $this->on_update($key_id);
  }
  public function delete($key_id){
    $old_data = $this->get_data($key_id);
    if(!$old_data || $old_data['is_delete'] == 'delete'){
      return false;
    }
    if($this->is_sort_priority){
      $this->move_in_priority($old_data['sort_priority']);
    }
    $this->db->set('is_delete', 'delete');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 1);
    $this->db->where($this->field_key, $key_id);
    $this->db->update($this->table_name);
    $this->on_delete($key_id);
  }
  public function on_insert($key_id){
  }
  public function on_update($key_id){
  }
  public function on_delete($key_id){
  }
  private function set_to_db($data){
    $field_list = array();
    $field_file_list = array();
    foreach($this->structure as $structure_item){
      if(in_array($structure_item['field_type'], array('file', 'image'))){
        $field_file_list[] = $structure_item['field_key'];
      }elseif(in_array($structure_item['field_type'], array('text', 'date', 'number', 'email', 'url', 'textarea', 'richtext', 'radio', 'dropdown'))){
        $field_list[] = $structure_item['field_key'];
      }elseif($structure_item['field_type'] == 'password'){
        $password_field = $structure_item['field_key'];
        if($data[$password_field] != ''){
          $this->db->set($password_field, $this->encrypt_password($data[$password_field]));
        }
      }
    }
    foreach($field_list as $field_name){
      $this->db->set($field_name, $data[$field_name]);
    }
    foreach($field_file_list as $field_file_name){
      if($data[$field_file_name] != ''){
        $this->db->set($field_file_name, $data[$field_file_name]);
      }
    }
  }
  private function search_count($search_data){
    $this->search_where($search_data);
    $this->db->select($this->field_key);
    $query = $this->db->get($this->table_name);
    return $query->num_rows();
  }
  private function search_list($search_data){
    $this->search_where($search_data);
    $this->db->order_by($this->table_name.'.'.$this->sort_field, $this->sort_type);
    $offset = ($search_data['page'] - 1) * $search_data['per_page'];
    return $this->db->get($this->table_name, $search_data['per_page'], $offset);
  }
  private function search_where($search_data){
    if($search_data['search_val'] != ''){
      $search_val = $search_data['search_val'];
      $str_where = '(';
      foreach($this->field_search_val_list as $index => $field_search_val){
        $str_where .= ($index > 0)?' OR ':'';
        $str_where .= $field_search_val." like '%$search_val%' ";
      }
      $str_where .= ')';
      $this->db->where($str_where, NULL,FALSE);
    }
    $this->db->where($this->table_name.'.is_delete', 'active');
  }
  public function get_max_priority(){
    $query = $this->get_list();
    return $query->num_rows();
  }
  private function move_in_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority - 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->update($this->table_name);
  }
  private function move_out_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->update($this->table_name);
  }
  public function set_code(){
    $code_pre = $this->get_code_pre();
    $code_running = $this->get_next_running($code_pre);
    $code = $this->code_pre.$code_pre.str_pad($code_running, $this->code_length, '0', STR_PAD_LEFT);
    $this->db->set('code', $code);
    $this->db->set('code_pre', $code_pre);
    $this->db->set('code_running', $code_running);
  }
  private function get_code_pre(){
    return date('ym');
  }
  public function get_next_running($code_pre){
    $this->db->select('code_running');
    $this->db->where('code_pre', $code_pre);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get($this->table_name);
    if($query->num_rows() == 0){
      return 1;
    }
    return $query->row()->code_running + 1;
  }
  public function encrypt_password($password_value){
    return md5($this->password_key. $password_value);
  }
}
