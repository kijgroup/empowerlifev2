<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller{
  public $page_name = 'MY Controller';
  public $controller = 'MY_Controller';
  public $has_code = false;
  public $structure = array();
  public $list_display = array();
  public $list_display_structure = array();
  public $nav = 'my_controller';
  public $breadcrumb_list = array();
  public $Main_model;

  public function __construct(){
    parent::__construct();
    $this->load->model('field_type/Field_type_model');
  }
  protected function prepare_data(){
    $this->structure = $this->Main_model->structure;
    $this->primary_key = $this->Main_model->field_key;
    $this->has_code = ($this->Main_model->code_pre != '')?true:false;
  }
  public function index(){
    $this->Login_service->must_login();
    $content = $this->load->view('template/list/index', array(), true);
    $this->Masterpage_service->add_js('assets_admin/js/template/list.js');
    $this->Masterpage_service->display($content, $this->page_name, $this->nav);
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $map_structure = array();
    foreach($this->structure as $structure_item){
      $map_structure[$structure_item['field_key']] = $structure_item;
    }
    foreach($this->list_display as $display_key){
      $this->list_display_structure[] = $map_structure[$display_key];
    }
    $arr_post_name = array('search_val', 'page', 'per_page');
    $search_data = array();
    foreach($arr_post_name as $post_name){
      $search_data[$post_name] = $this->input->post($post_name);
    }
    $data = $this->Main_model->search_content_data($search_data);
    $this->load->view('template/list/content', $data);
  }
  public function detail($key_id = 0){
    $this->Login_service->must_login();
    $main_data = $this->Main_model->get_data($key_id);
    $title = 'รายละเอียด'.$this->page_name;
    $this->breadcrumb_list[] = array(
      'url' => '',
      'name' => $title
    );
    $content = $this->load->view('template/detail/index', array(
      'key_id' => $key_id,
      'main_data' => $main_data,
      'title' => $title,
    ), TRUE);
    $this->Masterpage_service->display($content, $title, $this->nav);
  }
  public function form($key_id = 0){
    $this->Login_service->must_login();
    $main_data = $this->Main_model->get_data($key_id);
    $title = (($key_id == 0)?'เพิ่ม':'แก้ไข').$this->page_name;
    $this->breadcrumb_list[] = array(
      'url' => '',
      'name' => $title
    );
    $content = $this->load->view('template/form/index', array(
      'key_id' => $key_id,
      'main_data' => $main_data,
      'title' => $title,
    ), TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/template/form.js');
    $this->Masterpage_service->display($content, $title, $this->nav);
  }
  public function form_post($key_id){
    $this->Login_service->must_login();
    $data = array();
    $upload_field_list = array();
    foreach($this->structure as $structure_item){
      $field_name = $structure_item['field_key'];
      if(in_array($structure_item['field_type'], array('image', 'file'))){
        $upload_field_list[] = $structure_item;
        $data[$field_name] = '';
      }else{
        $data[$field_name] = $this->input->post($field_name);
      }
    }
    if(count($upload_field_list) > 0){
      $this->load->model(array(
        'Upload_file_model',
        'Upload_image_model'
      ));
      foreach($upload_field_list as $upload_field){
        if($upload_field['field_type'] == 'image'){
          $upload_result = $this->Upload_image_model->upload_single_image($upload_field['prefix'], $upload_field['field_key']);
          if($upload_result['success']){
            $data[$upload_field['field_key']] = $upload_result['image_file'];
          }
        }else{
          $upload_result = $this->Upload_file_model->upload($upload_field['prefix'], $upload_field['field_key']);
          if($upload_result['success']){
            $data[$upload_field['field_key']] = $upload_result['file_name'];
          }
        }
      }
    }
    if($this->Main_model->is_sort_priority){
      $data['sort_priority'] = $this->input->post('sort_priority');
    }
    if($key_id == 0){
      $key_id = $this->Main_model->insert($data);
    }else{
      $this->Main_model->update($key_id, $data);
    }
    redirect($this->controller.'/detail/'.$key_id);
  }
  public function delete($key_id){
    $this->Login_service->must_login();
    $this->Main_model->delete($key_id);
    redirect($this->controller);
  }
  public function upload_image(){
    $content_image = '';
    $this->load->model('Upload_image_model');
    $image_data = $this->Upload_image_model->upload_single_image($this->controller.'_richtext', 'file');
    if($image_data['success']){
      $content_image = $image_data['image_file'];
    }
    echo base_url('uploads/'.$content_image);
  }
}
