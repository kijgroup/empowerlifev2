<?php
class Content_model extends CI_Model{

  public function get_display_contact_list(){
    $this->db->where('display_contact', 'true');
    $this->db->order_by('sort_priority', 'asc');
    return $this->db->get('tbl_content');
  }

  public function get_name($content_id){
    $content = $this->get_data($content_id);
    if(!$content){
      return '-';
    }
    return $content->subject_th;
  }
  public function get_data($content_id){
    $this->db->where('content_id', $content_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_content');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_max_priority(){
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_content');
    return $query->num_rows();
  }
  public function insert($data){
    $this->move_up_priority($data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_content');
    $content_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('content',$content_id,'insert', 'insert new content ', $data, $this->db->last_query());
    return $content_id;
  }
  public function update($content_id, $data){
    $content = $this->get_data($content_id);
    if($content){
      $this->move_down_priority($content->sort_priority);
      $this->move_up_priority($data['sort_priority']);
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('content_id', $content_id);
      $this->db->update('tbl_content');
      $this->Log_action_model->insert_log('content', $content_id,'update', 'update content', $data, $this->db->last_query());
    }
  }
  public function delete($content_id){
    $content = $this->get_data($content_id);
    if($content){
      $this->move_down_priority($content->sort_priority);
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('content_id', $content_id);
      $this->db->update('tbl_content');
      $this->Log_action_model->insert_log('content', $content_id,'delete', 'delete content group', array(), $this->db->last_query());
    }
  }
  private function set_to_db($data){
    $this->db->set('content_group_id', $data['content_group_id']);
    $this->db->set('subject_th', $data['subject_th']);
    $this->db->set('subject_en', $data['subject_en']);
    $this->db->set('short_content_th', $data['short_content_th']);
    $this->db->set('short_content_en', $data['short_content_en']);
    $this->db->set('content_th', $data['content_th']);
    $this->db->set('content_en', $data['content_en']);
    $this->db->set('tags', $data['tags']);
    $this->db->set('display_contact', $data['display_contact']);
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('sort_priority', $data['sort_priority']);
    if($data['thumb_image'] != ''){
      $this->db->set('thumb_image', $data['thumb_image']);
    }
    if($data['content_image'] != ''){
      $this->db->set('content_image', $data['content_image']);
    }
  }
  public function move_up_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_content');
  }
  public function move_down_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority - 1', false);
    $this->db->where('sort_priority >', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_content');
  }
}
