<?php
class Content_filter_service extends CI_Model{
  public function get_data_content($search_val, $content_group_id, $display_contact, $enable_status, $page = 1, $per_page = 20) {
    $data = array();
    $data['search_val'] = $search_val;
    $data['content_group_id'] = $content_group_id;
    $data['display_contact'] = $display_contact;
    $data['enable_status'] = $enable_status;
    $data['page'] = $page;
    $data['per_page'] = $per_page;
    $data['content_list'] = $this->get_list($search_val, $content_group_id, $display_contact, $enable_status, $page, $per_page);
    $data['total_transaction'] = $this->get_count($search_val, $content_group_id, $display_contact, $enable_status);
    return $data;
  }
  private function get_count($search_val, $content_group_id, $display_contact, $enable_status){
    $this->db->select('tbl_content.content_id');
    $this->where_transaction($search_val, $content_group_id, $display_contact, $enable_status);
    $query = $this->db->get('tbl_content');
    return $query->num_rows();
  }
  private function get_list($search_val, $content_group_id, $display_contact, $enable_status, $page, $per_page){
    $this->db->select('tbl_content.*');
    $this->where_transaction($search_val, $content_group_id, $display_contact, $enable_status);
    $this->db->order_by('tbl_content.content_id', 'DESC');
    $offset = ($page - 1) * $per_page;
    return $this->db->get('tbl_content', $per_page, $offset);
  }
  private function where_transaction($search_val, $content_group_id, $display_contact, $enable_status){
    if($search_val != ''){
      $str_where = "(subject_th like '%$search_val%' ";
      $str_where .= "OR subject_en like '%$search_val%' ";
      $str_where .= "OR short_content_th like '%$search_val%' ";
      $str_where .= "OR short_content_en like '%$search_val%' ";
      $str_where .= "OR content_th like '%$search_val%' ";
      $str_where .= "OR content_en like '%$search_val%' ";
      $str_where .= "OR tags like '%$search_val%' ";
      $str_where .= ')';
      $this->db->where($str_where, NULL,FALSE);
    }
    if($content_group_id != 'all'){
      $this->db->where('tbl_content.content_group_id', $content_group_id);
    }
    if($display_contact != 'all'){
      $this->db->where('tbl_content.display_contact', $display_contact);
    }
    if($enable_status != 'all'){
      $this->db->where('tbl_content.enable_status', $enable_status);
    }
    $this->db->where('tbl_content.is_delete', 'active');
  }
}
