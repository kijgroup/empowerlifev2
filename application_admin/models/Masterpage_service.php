<?php
class Masterpage_service extends CI_Model {
    var $css_list;
    var $js_list;
    function __construct() {
        parent::__construct();
        $this->css_list = array();
        $this->js_list = array();
    }
    function display($content, $title = '', $nav = ''){
        $data['content'] = $content;
        $data['title'] = $title;
        $data['nav'] = $nav;
        $this->load->view('masterpage/masterpage', $data);
    }
    function add_css($css_file){
        $this->css_list[] = base_url($css_file);
    }
    function add_js($js_file){
        $this->js_list[] = base_url($js_file);
    }
    function add_outer_css($css_file){
        $this->css_list[] = $css_file;
    }
    function add_outer_js($js_file){
        $this->js_list[] = $js_file;
    }
    public function print_css(){
        return $this->load->view('masterpage/cssloader', array('css_list'=>$this->css_list), TRUE);
    }
    public function print_js(){
        return $this->load->view('masterpage/jsloader', array('js_list'=>$this->js_list), TRUE);
    }
}
