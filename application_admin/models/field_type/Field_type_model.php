<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field_type_model extends CI_Model{
  public function display_form($structure, $value){
    $field_type_model = $this->get_field_model($structure['field_type']);
    $option = $structure;
    $option['value'] = $value;
    $field_type_model->display_form($option);
  }
  public function display_detail($structure, $value){
    $field_type_model = $this->get_field_model($structure['field_type']);
    $option = $structure;
    $option['value'] = $value;
    $field_type_model->display_detail($option);
  }
  public function display_value($structure, $value){
    $field_type_model = $this->get_field_model($structure['field_type']);
    $option = $structure;
    $option['value'] = $value;
    return $field_type_model->display_value($option);
  }
  private function get_field_model($field_type){
    switch($field_type){
      case 'text':
        $this->load->model('field_type/Field_type_text_model');
        return $this->Field_type_text_model;
      case 'password':
        $this->load->model('field_type/Field_type_password_model');
        return $this->Field_type_password_model;
      case 'date':
        $this->load->model('field_type/Field_type_date_model');
        return $this->Field_type_date_model;
      case 'number':
        $this->load->model('field_type/Field_type_number_model');
        return $this->Field_type_number_model;
      case 'email':
        $this->load->model('field_type/Field_type_email_model');
        return $this->Field_type_email_model;
      case 'url':
        $this->load->model('field_type/Field_type_url_model');
        return $this->Field_type_url_model;
      case 'textarea':
        $this->load->model('field_type/Field_type_textarea_model');
        return $this->Field_type_textarea_model;
      case 'richtext':
        $this->load->model('field_type/Field_type_richtext_model');
        return $this->Field_type_richtext_model;
      case 'radio':
        $this->load->model('field_type/Field_type_radio_model');
        return $this->Field_type_radio_model;
      case 'dropdown':
        $this->load->model('field_type/Field_type_dropdown_model');
        return $this->Field_type_dropdown_model;
      case 'image':
        $this->load->model('field_type/Field_type_image_model');
        return $this->Field_type_image_model;
      case 'file':
        $this->load->model('field_type/Field_type_file_model');
        return $this->Field_type_file_model;
      case 'display_text':
        $this->load->model('field_type/Field_type_display_text_model');
        return $this->Field_type_display_text_model;
      case 'display_number':
        $this->load->model('field_type/Field_type_display_number_model');
        return $this->Field_type_display_number_model;
      case 'display_enum':
        $this->load->model('field_type/Field_type_display_enum_model');
        return $this->Field_type_display_enum_model;
      case 'display_database':
        $this->load->model('field_type/Field_type_display_database_model');
        return $this->Field_type_display_database_model;
    }
  }
}
