<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field_type_display_number_model extends CI_Model{
  protected $default_config = array(
    'field_name' => 'ไม่ระบุ',
    'value' => '',
    'help_text' => ''
  );
  public function display_form($my_config){
    if($my_config['value'] != ''){
      $field_config = array_merge($this->default_config, $my_config);
      $this->load->view('field_type/detail/number', $field_config);
    }
  }
  public function display_detail($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    $this->load->view('field_type/detail/number', $field_config);
  }
  public function display_value($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    return number_format($field_config['value']);
  }
}
