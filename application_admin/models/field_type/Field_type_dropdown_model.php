<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field_type_dropdown_model extends CI_Model{
  protected $default_config = array(
    'field_name' => 'ไม่ระบุ',
    'field_key' => '',
    'placeholder' => '',
    'value' => '',
    'source_type' => 'enum',
    'source_data' => array(
      'active' => array(
        'source_key' => 'active',
        'source_name' => 'ปกติ',
        'source_label' => 'label label-success'
      ),
      'inactive' => array(
        'source_key' => 'inactive',
        'source_name' => 'ไม่ปกติ',
        'source_label' => 'label label-danger'
      ),
    ),
    'source_model_path' => '',
    'source_model_name' => '',
    'source_model' => '',
    'required' => false,
    'help_text' => ''
  );
  public function display_form($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    $this->update_source_data($field_config);
    $this->load->view('field_type/form/dropdown', $field_config);
  }
  public function display_detail($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    $field_config['value'] = $this->get_display_value($field_config);
    $this->load->view('field_type/detail/text', $field_config);
  }
  private function update_source_data(&$field_config){
    if($field_config['source_type'] != 'enum'){
      $this->load->model($field_config['source_model_path']);
      eval('$source_data = $this->'.$field_config['source_model_name'].';');
      $field_config['source_data'] = $source_data;
    }
  }
  private function get_display_value($field_config){
    if($field_config['source_type'] == 'enum'){
      $selected_data = $field_config['source_data'][$field_config['value']];
      if(!isset($selected_data)){
        return '-';
      }
      return '<span class="'.$selected_data['source_label'].'">'.$selected_data['source_name'].'</span>';
    }else{
      $this->load->model($field_config['source_model_path']);
      eval('$value = $this->'.$field_config['source_model_name'].'->get_name('.$field_config['value'].');');
      return $value;
    }
  }
  public function display_value($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    return $this->get_display_value($field_config);
  }
}
