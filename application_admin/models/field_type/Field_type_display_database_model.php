<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field_type_display_database_model extends CI_Model{
  protected $default_config = array(
    'field_name' => 'ไม่ระบุ',
    'value' => '',
    'source_model_path' => '',
    'source_model_name' => '',
    'source_model' => '',
    'help_text' => ''
  );
  public function display_form($my_config){
    if($my_config['value'] != ''){
      $field_config = array_merge($this->default_config, $my_config);
      $field_config['value'] = $this->get_display_value($field_config);
      $this->load->view('field_type/detail/text', $field_config);
    }
  }
  public function display_detail($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    $field_config['value'] = $this->get_display_value($field_config);
    $this->load->view('field_type/detail/text', $field_config);
  }
  private function update_source_data(&$field_config){
    $this->load->model($field_config['source_model_path']);
    eval('$source_data = $this->'.$field_config['source_model_name'].';');
    $field_config['source_data'] = $source_data;
  }
  private function get_display_value($field_config){
    $this->load->model($field_config['source_model_path']);
    eval('$value = $this->'.$field_config['source_model_name'].'->get_name('.$field_config['value'].');');
    return $value;
  }
  public function display_value($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    return $this->get_display_value($field_config);
  }
}
