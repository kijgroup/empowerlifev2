<?php
class Payment_filter_service extends CI_Model{
  public function get_data_content($search_val, $start_date, $end_date, $page = 1, $per_page = 20) {
    $data = array();
    $data['search_val'] = $search_val;
    $data['start_date'] = $start_date;
    $data['end_date'] = $end_date;
    $data['page'] = $page;
    $data['per_page'] = $per_page;
    $data['payment_list'] = $this->get_list($search_val, $start_date, $end_date, $page, $per_page);
    $data['total_transaction'] = $this->get_count($search_val, $start_date, $end_date);
    return $data;
  }
  private function get_count($search_val, $payment_id, $bank_payment_status){
    $this->db->select('tbl_bank_payment.bank_payment_id');
    $this->where_transaction($search_val, $payment_id, $bank_payment_status);
    $query = $this->db->get('tbl_bank_payment');
    return $query->num_rows();
  }
  private function get_list($search_val, $start_date, $end_date, $page, $per_page){
    $this->db->select('tbl_bank_payment.*');
    $this->where_transaction($search_val, $start_date, $end_date);
    $this->db->order_by('tbl_bank_payment.bank_payment_id', 'DESC');
    $offset = ($page - 1) * $per_page;
    return $this->db->get('tbl_bank_payment', $per_page, $offset);
  }
  private function where_transaction($search_val, $start_date, $end_date){
    if($search_val != ''){
      $str_where = "(bank_payment_name like '%$search_val%' ";
      $str_where .= "OR bank_id like '%$search_val%' ";
      $str_where .= ')';
      $this->db->where($str_where, NULL,FALSE);
    }
    /*
    if($payment_id != 'all'){
      $this->db->where('tbl_bank_payment.payment_id', $payment_id);
    }
    if($bank_payment_status != 'all'){
      $this->db->where('tbl_bank_payment.bank_payment_status', $bank_payment_status);
    }
    */
    $this->db->where('tbl_bank_payment.is_delete', 'active');
  }
}
