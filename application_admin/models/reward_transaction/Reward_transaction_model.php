<?php
class Reward_transaction_model extends CI_Model{

  public function get_list_by_member($member_id){
    $this->db->where('member_id', $member_id);
    $this->db->where('is_delete', 'active');
    return $this->db->get('tbl_reward_transaction');
  }
  public function get_data($reward_transaction_id){
    $this->db->where('reward_transaction_id', $reward_transaction_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_reward_transaction');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function insert($data){
    $this->set_to_db($data);
    $this->db->set('member_id', $data['member_id']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_reward_transaction');
    $reward_transaction_id = $this->db->insert_id();
    return $reward_transaction_id;
  }
  public function update($reward_transaction_id, $data){
    $this->set_to_db($data);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('reward_transaction_id', $reward_transaction_id);
    $this->db->update('tbl_reward_transaction');
  }
  public function delete($reward_transaction_id){
    $this->db->set('is_delete', 'delete');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('reward_transaction_id', $reward_transaction_id);
    $this->db->update('tbl_reward_transaction');
  }
  public function update_point($reward_transaction_id, $total_reward_point){
    $this->db->set('total_reward_point', $total_reward_point);
    $this->db->where('reward_transaction_id', $reward_transaction_id);
    $this->db->update('tbl_reward_transaction');
  }
  private function set_to_db($data){
    $field_list = array('member_id', 'shipping_name', 'shipping_address', 'shipping_post_code', 'shipping_district', 'shipping_amphur', 'shipping_province', 'shipping_mobile', 'note', 'reward_transaction_status');
    foreach($field_list as $field_name){
      $this->db->set($field_name, $data[$field_name]);
    }
  }
}
