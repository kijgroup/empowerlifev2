<?php

Class Discount_point_model extends CI_Model{

  public function get_list($all_status = true){
    if(!$all_status){
      $this->db->where('enable_status', 'show');
    }
    $this->db->where('is_delete','active');
    $this->db->order_by('sort_priority','ASC');
    return $this->db->get('tbl_discount_point');
  }

  public function count_discount_point($discount_point_id){
    $this->db->where('discount_point_id', $discount_point_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_discount_point');
    return $query->num_rows();
  }

  public function get_data($discount_point_id){
    $this->db->where('discount_point_id', $discount_point_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_discount_point');
    if($query->num_rows()==0){
      return false;
    }
    return $query->row();
  }

  public function get_name($discount_point_id){
    $discount_point = $this->get_data($discount_point_id);
    if(!$discount_point){
      return 'ไม่ระบุ';
    }
    return $discount_point->use_point;
  }

  public function get_max_priority(){
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_discount_point');
    return $query->num_rows();
  }

  public function insert($data){
    $this->move_up_priority($data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_discount_point');
    $discount_point_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('discount_point',$discount_point_id,'insert','insert new discount_point ', $data, $this->db->last_query());
    return $discount_point_id;
  }

  public function update($discount_point_id, $data){
    $discount_point = $this->get_data($discount_point_id);
    if($discount_point){
      $this->move_down_priority($discount_point->sort_priority);
      $this->move_up_priority($data['sort_priority']);
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('discount_point_id',$discount_point_id);
      $this->db->update('tbl_discount_point');
      $this->Log_action_model->insert_log('discount_point', $discount_point_id,'update','update discount_point', $data, $this->db->last_query());
    }
  }

  public function delete($discount_point_id){
    $discount_point = $this->get_data($discount_point_id);
    if($discount_point){
      $this->move_down_priority($discount_point->sort_priority);
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('discount_point_id', $discount_point_id);
      $this->db->update('tbl_discount_point');
      $this->Log_action_model->insert_log('discount_point', $discount_point_id, 'delete','detele discount_point', array(), $this->db->last_query());
    }
  }

  private function set_to_db($data){
    $this->db->set('use_point', $data['use_point']);
    $this->db->set('discount_amount', $data['discount_amount']);
    $this->db->set('sort_priority', $data['sort_priority']);
    $this->db->set('enable_status', $data['enable_status']);
  }

  private function move_up_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_discount_point');
  }

  private function move_down_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority -1', false);
    $this->db->where('sort_priority >', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_discount_point');
  }

}
