<?php
class Bank_model extends CI_model{
  public function get_list(){
    $this->db->where('is_delete', 'active');
    return $this->db->get('tbl_bank');
  }
  public function get_name($bank_id){
    $bank = $this->get_data($bank_id);
    if(!$bank){
      return 'ไม่ระบุ';
    }
    return $bank->bank_name_th;
  }
  public function get_data($bank_id){
    $this->db->where('bank_id', $bank_id);
    $query = $this->db->get('tbl_bank');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
}
