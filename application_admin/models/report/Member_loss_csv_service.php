<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Member_loss_csv_service extends CI_Model{
  public function export_list($data){
    $filename = 'member_'.date('Ymd_his', time()).'.csv';
    list($order_list, $member_list) = $this->get_list($data);
    $file = $this->set_header($filename);
    $column_list = array(
      'last_order_date' => 'Last Order Date',
      'member_code' => 'Member Code',
      'member_type' => 'Member Type',
      'expire_date' => 'Expire Date',
      'buy_total' => 'Buy Total',
      'member_point' => 'Member Point',
      'email' => 'Email Login',
      'create_date' => 'Register Date',
      'member_email' => 'Email',
      'member_firstname' => 'Firstname',
      'member_lastname' => 'Lastname',
      'member_phone' => 'Phone Number',
      'member_mobile' => 'Mobile',
      'member_birthdate' => 'Birthday',
      'member_gender' => 'Gender',
      'member_address' => 'Address',
      'member_district' => 'District',
      'member_amphur' => 'Amphur',
      'member_province' => 'Province',
      'member_postcode' => 'Postcode',
      'note' => 'Note',
    );
    $this->set_head($file, $column_list);
    flush();
    $this->set_content($file, $column_list, $order_list, $member_list);
    $this->set_close($file);
  }
  private function set_header($filename){
    ob_clean();
    ini_set('max_execution_time', 1200);
    date_default_timezone_set('Asia/Bangkok');
    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-Description: File Transfer');
    header("Content-type: text/csv");
    header("Content-Disposition: attachment; filename={$filename}");
    header("Expires: 0");
    header("Pragma: public");
    $file = @fopen( 'php://output', 'w' );
    fwrite($file, "\xEF\xBB\xBF");
    return $file;
  }
  private function set_head($file, $column_list){
    $csv_list = array('วันที่บันทึกข้อมูล');
    foreach($column_list as $column_code => $column_name){
      $csv_list[] = $column_name;
    }
    fputcsv($file, $csv_list);
  }
  private function set_content($file, $column_list, $order_list, $item_list){
    $counter = 0;
    foreach($item_list->result_array() as $item){
      $counter++;
      if($counter%1000 === 0){
        flush();
      }
      $csv_list = array($item['create_date']);
      foreach($column_list as $column_code => $column_name){
        if($column_code === 'last_order_date'){
          $csv_list[] = $order_list[$item['member_id']];
          continue;
        }
        $csv_list[] = $item[$column_code];
      }
      fputcsv($file, $csv_list);
    }
  }
  private function set_close($file){
    fclose($file);
    exit;
  }
  public function get_list($data){
    $order_list = $this->where($data);
    $this->db->select('tbl_member.*');
    $this->db->select('tbl_user.email');
    $this->db->join('tbl_user', 'tbl_user.member_id = tbl_member.member_id', 'left');
    $this->db->where('tbl_member.is_delete', 'active');
    return array($order_list, $this->db->get('tbl_member'));
  }
  private function where($data){
    list($order_list, $member_id_list) = $this->filter_by_date($data['date_after']);
    if(count($member_id_list) > 0){
      $this->db->where_in('tbl_member.member_id', $member_id_list);
    }else{
      $this->db->where('tbl_member.member_id', -1);
    }
    if($data['search_val'] != ''){
      $this->db->group_start();
      $this->db->like('tbl_member.member_firstname', $data['search_val']);
      $this->db->or_like('tbl_member.member_lastname', $data['search_val']);
      $this->db->or_like('tbl_member.member_email', $data['search_val']);
      $this->db->or_like('tbl_member.member_mobile', $data['search_val']);
      $this->db->or_like('tbl_member.member_phone', $data['search_val']);
      $this->db->or_like('tbl_member.member_code', $data['search_val']);
      $this->db->group_end();
    }
    if($data['member_type_code'] != 'all'){
      $this->db->where('tbl_member.member_type', $data['member_type_code']);
    }
    if($data['enable_status'] != 'all'){
      $this->db->where('tbl_member.enable_status', $data['enable_status']);
    }
    $this->db->where('tbl_member.is_delete', 'active');
    return $order_list;
  }
  private function filter_by_date($date_after){
    $this->db->select('tbl_order.member_id');
    $this->db->select('MAX(create_date) as last_order_date');
    $this->db->where('order_status', 'complete');
    $this->db->group_by('tbl_order.member_id');
    $this->db->having('MAX(create_date) <', $date_after);
    $query = $this->db->get('tbl_order');
    $member_id_list = array();
    $order_list = array();
    foreach($query->result() as $order){
      $member_id_list[] = $order->member_id;
      $order_list[$order->member_id] = $order->last_order_date;
    }
    return array($order_list, $member_id_list);
  }
}