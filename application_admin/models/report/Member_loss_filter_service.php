<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Member_loss_filter_service extends CI_Model{
  public function get_data_content($data){
    $data['member_list'] = $this->get_list($data);
    $data['total_transaction'] = $this->get_count($data);
    return $data;
  }
  private function get_count($data){
    $this->where_transaction($data);
    $this->db->select('tbl_member.member_id');
    $query = $this->db->get('tbl_member');
    return $query->num_rows();
  }
  private function get_list($data){
    $this->where_transaction($data);
    $this->db->select('tbl_member.*');
    if($data['sort'] == 'member_firstname'){
      $this->db->order_by($data['sort'], 'ASC');
    }else{
      $this->db->order_by($data['sort'], 'DESC');
    }
    $offset = ($data['page'] - 1) * $data['per_page'];
    return $this->db->get('tbl_member', $data['per_page'], $offset);
  }
  private function where_transaction($data){
    $member_id_list = $this->filter_by_date($data['date_after']);
    if(count($member_id_list) > 0){
      $this->db->where_in('member_id', $member_id_list);
    }else{
      $this->db->where('member_id', -1);
    }
    if($data['search_val'] != ''){
      $this->db->group_start();
      $this->db->like('member_firstname', $data['search_val']);
      $this->db->or_like('member_lastname', $data['search_val']);
      $this->db->or_like('member_email', $data['search_val']);
      $this->db->or_like('member_mobile', $data['search_val']);
      $this->db->or_like('member_phone', $data['search_val']);
      $this->db->or_like('member_code', $data['search_val']);
      $this->db->group_end();
    }
    if($data['member_type_code'] != 'all'){
      $this->db->where('tbl_member.member_type', $data['member_type_code']);
    }
    if($data['enable_status'] != 'all'){
      $this->db->where('tbl_member.enable_status', $data['enable_status']);
    }
    $this->db->where('tbl_member.is_delete', 'active');
  }
  private function filter_by_date($date_after){
    $this->db->select('member_id');
    $this->db->where('order_status', 'complete');
    $this->db->group_by('member_id');
    $this->db->having('MAX(create_date) <', $date_after);
    $query = $this->db->get('tbl_order');
    $member_id_list = array();
    foreach($query->result() as $order){
      $member_id_list[] = $order->member_id;
    }
    return $member_id_list;
  }
  public function get_last_order_date($member_id){
    $this->db->select('MAX(create_date) as last_create_date');
    $this->db->where('member_id', $member_id);
    $this->db->where('order_status', 'complete');
    $query = $this->db->get('tbl_order');
    if($query->num_rows() === 0){
      return '';
    }
    return $query->row()->last_create_date;
  }
}