<?php
class Faq_group_model extends CI_Model{
  public function get_list($all_status = true){
    if(!$all_status){
      $this->db->where('enable_status', 'show');
    }
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_faq_group');
  }

  public function get_data($faq_group_id){
    $this->db->where('faq_group_id', $faq_group_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_faq_group');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_name($faq_group_id){
    $faq_group = $this->get_data($faq_group_id);
    if(!$faq_group){
      return 'ไม่ระบุ';
    }
    return $faq_group->faq_group_th;
  }
  public function get_max_priority(){
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_faq_group');
    return $query->num_rows();
  }
  public function insert($data){
    $this->move_up_priority($data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_faq_group');
    $faq_group_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('faq_group',$faq_group_id,'insert', 'insert new faq_group ', $data, $this->db->last_query());
    return $faq_group_id;
  }
  public function update($faq_group_id, $data){
    $faq_group = $this->get_data($faq_group_id);
    if($faq_group){
      $this->move_down_priority($faq_group->sort_priority);
      $this->move_up_priority($data['sort_priority']);
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('faq_group_id', $faq_group_id);
      $this->db->update('tbl_faq_group');
      $this->Log_action_model->insert_log('faq_group', $faq_group_id,'update', 'update faq_group', $data, $this->db->last_query());
    }
  }
  public function delete($faq_group_id){
    $faq_group = $this->get_data($faq_group_id);
    if($faq_group){
      $this->move_down_priority($faq_group->sort_priority);
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('faq_group_id', $faq_group_id);
      $this->db->update('tbl_faq_group');
      $this->Log_action_model->insert_log('faq_group', $faq_group_id,'delete', 'delete faq_group', array(), $this->db->last_query());
    }
  }
  private function set_to_db($data){
    $this->db->set('faq_group_th', $data['faq_group_th']);
    $this->db->set('faq_group_en', $data['faq_group_en']);
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('sort_priority', $data['sort_priority']);
  }
  private function move_up_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_faq_group');
  }
  private function move_down_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority - 1', false);
    $this->db->where('sort_priority >', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_faq_group');
  }
}
