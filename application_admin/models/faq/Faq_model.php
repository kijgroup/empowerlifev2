<?php
class Faq_model extends CI_Model{

  public function get_list(){
    $this->db->where('is_delete','active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_faq');
  }
  public function get_data($faq_id){
    $this->db->where('faq_id', $faq_id);
    $query = $this->db->get('tbl_faq');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_max_priority(){
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_faq');
    return $query->num_rows();
  }
  public function insert($data){
    $this->move_up_priority($data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_faq');
    $faq_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('faq',$faq_id,'insert', 'insert new faq ', $data, $this->db->last_query());
    return $faq_id;
  }
  public function update($faq_id, $data){
    $faq = $this->get_data($faq_id);
    if($faq){
      $this->move_down_priority($faq->sort_priority);
      $this->move_up_priority($data['sort_priority']);
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('faq_id', $faq_id);
      $this->db->update('tbl_faq');
      $this->Log_action_model->insert_log('faq', $faq_id,'update', 'update faq', $data, $this->db->last_query());
    }
  }
  public function delete($faq_id){
    $faq = $this->get_data($faq_id);
    if($faq){
      $this->move_down_priority($faq->sort_priority);
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('faq_id', $faq_id);
      $this->db->update('tbl_faq');
      $this->Log_action_model->insert_log('faq', $faq_id,'delete', 'delete faq group', array(), $this->db->last_query());
    }
  }
  private function set_to_db($data){
    $this->db->set('faq_group_id', $data['faq_group_id']);
    $this->db->set('faq_question_th', $data['faq_question_th']);
    $this->db->set('faq_question_en', $data['faq_question_en']);
    $this->db->set('faq_answer_th', $data['faq_answer_th']);
    $this->db->set('faq_answer_en', $data['faq_answer_en']);
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('sort_priority', $data['sort_priority']);
  }
  public function move_up_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_faq');
  }
  public function move_down_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority - 1', false);
    $this->db->where('sort_priority >', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_faq');
  }
}
