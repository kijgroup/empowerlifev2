<?php
Class Promotion_filter_service extends CI_Model{
  public function get_data_content($search_val, $enable_status, $page = 1, $per_page = 20) {
    $data = array();
    $data['search_val'] = $search_val;
    $data['enable_status'] = $enable_status;
    $data['page'] = $page;
    $data['per_page'] = $per_page;
    $data['product_list'] = $this->get_list($search_val, $enable_status, $page, $per_page);
    $data['total_transaction'] = $this->get_count($search_val, $enable_status);
    return $data;
  }

  private function get_count($search_val, $enable_status){
    $this->db->select('tbl_product.product_id');
    $this->where_transaction($search_val, $enable_status);
    $query = $this->db->get('tbl_product');
    return $query->num_rows();
  }

  private function get_list($search_val, $enable_status, $page, $per_page){
    $this->db->select('tbl_product.*');
    $this->where_transaction($search_val, $enable_status);
    $this->db->order_by('tbl_product.sort_priority', 'ASC');
    $offset = ($page - 1) * $per_page;
    return $this->db->get('tbl_product', $per_page, $offset);
  }

  private function where_transaction($search_val, $enable_status){
    if($search_val != ''){
      $str_where = "(name_th like '%$search_val%' ";
      $str_where .= "OR name_en like '%$search_val%' ";
      $str_where .= "OR detail_th like '%$search_val%' ";
      $str_where .= "OR detail_en like '%$search_val%' ";
      $str_where .= ')';
      $this->db->where($str_where, NULL,FALSE);
    }
    if($enable_status != 'all'){
      $this->db->where('tbl_product.enable_status', $enable_status);
    }
    $this->db->where('product_type', 'promotion');
    $this->db->where('tbl_product.is_delete', 'active');
  }
}
