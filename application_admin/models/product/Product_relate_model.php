<?php
class Product_relate_model extends CI_Model{
  public function get_array($product_id){
    $product_relate_list = $this->get_list($product_id);
    $ret_val = array();
    foreach($product_relate_list->result() as $product_relate){
      $ret_val[] = $product_relate->relate_id;
    }
    return $ret_val;
  }
  public function get_list($product_id){
    $this->db->where('product_id', $product_id);
    return $this->db->get('tbl_product_relate');
  }
  public function add_list($product_id, $relate_list){
    $this->db->where('product_id', $product_id);
    $this->db->delete('tbl_product_relate');
    if(isset($relate_list)){
      foreach($relate_list as $relate_id){
        $this->insert(array(
          'product_id' => $product_id,
          'relate_id' => $relate_id
        ));
      }
    }
  }
  public function insert($data){
    $this->db->set('product_id', $data['product_id']);
    $this->db->set('relate_id', $data['relate_id']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_product_relate');
    $product_relate_id = $this->db->insert_id();
    return $product_relate_id;
  }
  public function delete($product_relate_id){
    $product_relate = $this->get_data($product_relate_id);
    if($product_relate){
      $this->db->where('product_relate_id', $product_relate_id);
      $this->db->delete('tbl_product_relate');
    }
  }
}
