<?php
class Product_bundle_model extends CI_Model{
  public function get_list($product_id){
    $this->db->where('product_id', $product_id);
    return $this->db->get('tbl_product_bundle');
  }
  public function get_product_bundle_qty($product_id, $bundle_id){
    $this->db->where('product_id', $product_id);
    $this->db->where('bundle_id', $bundle_id);
    $query = $this->db->get('tbl_product_bundle');
    if($query->num_rows() === 0){
      return 0;
    }
    return $query->row()->qty;
  }
  public function get_data($product_bundle_id){
    $this->db->where('product_bundle_id', $product_bundle_id);
    $qurey = $this->db->get('tbl_product_bundle');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function remove_list($product_id){
    $this->db->where('product_id', $product_id);
    $this->db->delete('tbl_product_bundle');
  }
  public function insert($data){
    $this->set_to_db($data);
    $this->db->set('product_id', $data['product_id']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_product_bundle');
    $product_bundle_id = $this->db->insert_id();
    return $product_bundle_id;
  }
  public function update($product_bundle_id, $data){
    $product_bundle = $this->get_data($product_bundle_id);
    if($product_bundle){
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('product_bundle_id', $product_bundle_id);
      $this->db->update('tbl_product_bundle');
    }
  }
  public function delete($product_bundle_id){
    $product_bundle = $this->get_data($product_bundle_id);
    if($product_bundle){
      $this->db->where('product_bundle_id', $product_bundle_id);
      $this->db->delete('tbl_product_bundle');
    }
  }
  private function set_to_db($data){
    $this->db->set('bundle_id', $data['bundle_id']);
    $this->db->set('qty', $data['qty']);
  }
}
