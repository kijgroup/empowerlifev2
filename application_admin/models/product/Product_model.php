<?php
class Product_model extends CI_Model{
  public function get_list(){
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_product');
  }
  public function get_data($product_id){
    $this->db->where('product_id', $product_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_product');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function insert($data){
    $this->move_up_priority($data['sort_priority'], $data['product_type']);
    $this->set_to_db($data);
    $this->db->set('product_type', $data['product_type']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_product');
    $product_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('product',$product_id,'insert', 'insert new product ', $data, $this->db->last_query());
    return $product_id;
  }
  public function update($product_id, $data){
    $product = $this->get_data($product_id);
    if($product){
      $this->move_down_priority($product->sort_priority, $product->product_type);
      $this->move_up_priority($data['sort_priority'], $product->product_type);
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('product_id', $product_id);
      $this->db->update('tbl_product');
      $this->Log_action_model->insert_log('product', $product_id,'update', 'update product', $data, $this->db->last_query());
    }
  }
  public function delete($product_id){
    $product = $this->get_data($product_id);
    if($product){
      $this->move_down_priority($product->sort_priority, $product->product_type);
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('product_id', $product_id);
      $this->db->update('tbl_product');
      $this->Log_action_model->insert_log('product', $product_id,'delete', 'delete product', array(), $this->db->last_query());
    }
  }
  public function get_max_priority($product_type){
    $this->db->where('is_delete', 'active');
    $this->db->where('product_type', $product_type);
    $query = $this->db->get('tbl_product');
    return $query->num_rows();
  }
  private function set_to_db($data){
    $this->db->set('category_id', $data['category_id']);
    $this->db->set('is_bundle', $data['is_bundle']);
    $this->db->set('is_expire', $data['is_expire']);
    $this->db->set('expire_date', $data['expire_date']);
    $this->db->set('name_th', $data['name_th']);
    $this->db->set('name_en', $data['name_en']);
    $this->db->set('slug', $data['slug']);
    $this->db->set('price', $data['price']);
    $this->db->set('price_before_discount', $data['price_before_discount']);
    $this->db->set('price_discount_rate', $data['price_discount_rate']);
    $this->db->set('detail_th', $data['detail_th']);
    $this->db->set('detail_en', $data['detail_en']);
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('sort_priority', $data['sort_priority']);
    if($data['thumb_image'] != ''){
      $this->db->set('thumb_image', $data['thumb_image']);
    }
    if($data['main_image'] != ''){
      $this->db->set('main_image', $data['main_image']);
    }
  }

  private function move_up_priority($sort_priority, $product_type){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->where('product_type', $product_type);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_product');
  }

  private function move_down_priority($sort_priority, $product_type){
    $this->db->set('sort_priority', 'sort_priority -1', false);
    $this->db->where('sort_priority >', $sort_priority);
    $this->db->where('product_type', $product_type);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_product');
  }
  public function move_up($product_id){
    $this->move_priority($product_id, -1);
  }
  public function move_down($product_id){
    $this->move_priority($product_id, 1);
  }
  private function move_priority($product_id, $factor){
    $product = $this->get_data($product_id);
    if(!$product){
      return false;
    }
    if($factor === 1){
      $max_priority = $this->get_max_priority($product->product_type);
      if($product->sort_priority >= $max_priority){
        return false;
      }
    }else{
      if($product->sort_priority <= 1){
        return false;
      }
    }
    $new_priority = $product->sort_priority + $factor;
    $max_priority = $this->get_max_priority($product->product_type);
    $this->move_down_priority($product->sort_priority, $product->product_type);
    $this->move_up_priority($new_priority, $product->product_type);
    $this->db->set('sort_priority', $new_priority);
    $this->db->where('product_id', $product_id);
    $this->db->update('tbl_product');
  }
}
