<?php
class Product_image_model extends CI_Model{
  public function get_list($product_id){
    $this->db->where('product_id', $product_id);
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_product_image');
  }
  public function get_data($product_image_id){
    $this->db->where('product_image_id', $product_image_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_product_image');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function insert($data){
    $next_priority = $this->next_priority($data['product_id']);
    $this->set_to_db($data);
    $this->db->set('product_id', $data['product_id']);
    $this->db->set('sort_priority', $next_priority);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_product_image');
    $product_image_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('product_image',$product_image_id,'insert', 'insert new product image ', $data, $this->db->last_query());
    return $product_image_id;
  }
  public function update($product_image_id, $data){
    $product_image = $this->get_data($product_image_id);
    if($product_image){
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('product_image_id', $product_image_id);
      $this->db->update('tbl_product_image');
      $this->Log_action_model->insert_log('product_image', $product_image_id,'update', 'update product image', $data, $this->db->last_query());
    }
  }
  public function delete($product_image_id){
    $product_image = $this->get_data($product_image_id);
    if($product_image){
      $this->move_down_priority($product_image->product_id, $product_image->sort_priority);
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('product_image_id', $product_image_id);
      $this->db->update('tbl_product_image');
      $this->Log_action_model->insert_log('product_image', $product_image_id, 'delete', 'delete product image', array(), $this->db->last_query());
    }
  }
  public function sort_priority($product_image_id, $sort_priority){
    $product_image = $this->get_data($product_image_id);
    if($product_image){
      $this->move_down_priority($product_image->product_id, $product_image->sort_priority);
      $this->move_up_priority($product_image->product_id, $sort_priority);
      $this->db->set('sort_priority', $sort_priority);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('product_image_id', $product_image_id);
      $this->db->update('tbl_product_image');
      $this->Log_action_model->insert_log('product_image', $product_image_id,'sort_priority', 'update content image priority', array(), $this->db->last_query());
    }
  }
  private function set_to_db($data){
    if($data['main_image'] != ''){
      $this->db->set('main_image', $data['main_image']);
    }
    if($data['thumb_image'] != ''){
      $this->db->set('thumb_image', $data['thumb_image']);
    }
  }
  private function next_priority($product_id){
    $content_list = $this->get_list($product_id);
    return $content_list->num_rows() + 1;
  }
  public function move_up_priority($product_id, $sort_priority){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->where('product_id', $product_id);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_product_image');
  }
  public function move_down_priority($product_id, $sort_priority){
    $this->db->set('sort_priority', 'sort_priority - 1', false);
    $this->db->where('sort_priority >', $sort_priority);
    $this->db->where('product_id', $product_id);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_product_image');
  }
}
