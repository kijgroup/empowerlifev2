<?php
class Order_model extends CI_Model{

  public function get_list_by_member($member_id){
    $this->db->where('member_id', $member_id);
    $this->db->where('is_delete', 'active');
    return $this->db->get('tbl_order');
  }
  public function get_data($order_id){
    $this->db->where('order_id', $order_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_order');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function insert($data){
    $this->set_next_code();
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_order');
    $order_id = $this->db->insert_id();
    return $order_id;
  }
  public function update($order_id, $data){
    $this->set_to_db($data);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('order_id', $order_id);
    $this->db->update('tbl_order');
  }
  public function delete($order_id){
    $this->db->set('is_delete', 'delete');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('order_id', $order_id);
    $this->db->update('tbl_order');
  }
  public function update_price($order_id, $data){
    $this->db->set('total_item', $data['total_item']);
    $this->db->set('item_price', $data['item_price']);
    $this->db->set('discount_price', $data['discount_price']);
    $this->db->set('shipping_price', $data['shipping_price']);
    $this->db->set('grand_total', $data['grand_total']);
    $this->db->where('order_id', $order_id);
    $this->db->update('tbl_order');
  }
  private function set_to_db($data){
    $field_list = array(
      'member_id',
      'order_status',
      'order_channel_id',
      'order_sub_channel_id',
      'order_channel_text',
      'contact_channel_id',
      'shipping_type_id',
      'stock_id',
      'send_time_status',
      'payment_status',
      'postoffice',
      'bank_id',
      'discount_point_amount',
      'discount_by_point_amount',
      'shipping_name',
      'shipping_address',
      'shipping_post_code',
      'shipping_district',
      'shipping_amphur',
      'shipping_province',
      'shipping_mobile',
      'is_tax',
      'tax_name',
      'tax_address',
      'tax_post_code',
      'tax_district',
      'tax_amphur',
      'tax_province',
      'tax_mobile',
      'tax_code',
      'tax_branch',
      'payment_type',
      'remark'
    );
    foreach($field_list as $field_name){
      $this->db->set($field_name, $data[$field_name]);
    }
  }
  public function update_status($order_id, $order_status){
    $this->db->set('order_status', $order_status);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('order_id', $order_id);
    $this->db->update('tbl_order');
  }
  public function update_print_status($order_id, $is_print){
    $this->db->set('is_print', $is_print);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('order_id', $order_id);
    $this->db->update('tbl_order');
  }
  public function update_print_status_list($order_id_list, $is_print){
    $this->db->set('is_print', $is_print);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where_in('order_id', $order_id_list);
    $this->db->update('tbl_order');
  }
  public function set_next_code(){
    $code_month = $this->get_order_code_month();
    $code_number = $this->get_next_running_number($code_month);
    $order_code = 'OD'.$code_month.str_pad($code_number, 4, "0", STR_PAD_LEFT);
    $this->db->set('order_code_month', $code_month);
    $this->db->set('order_code_running', $code_number);
    $this->db->set('order_code', $order_code);
    return $order_code;
  }
  public function get_next_running_number($code_month){
    $ret_val = 0;
    $this->db->select('MAX(order_code_running) AS max_number');
    $this->db->where('order_code_month', $code_month);
    $query = $this->db->get('tbl_order');
    if($query->num_rows() > 0){
      $ret_val = $query->row()->max_number;
    }
    $ret_val ++;
    return $ret_val;
  }
  private function get_order_code_month(){
    $ret_val = str_pad(((date('y')+543)%100),2,"0", STR_PAD_LEFT);
    $ret_val .= date('m');
    return $ret_val;
  }
}
