<?php
class Order_filter_service extends CI_Model{
  function __construct() {
    parent::__construct();
  }
  public function get_data_content($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status, $page = 1, $per_page = 20) {
    $data = array();
    $data['search_val'] = $search_val;
    $data['payment_type'] = $payment_type;
    $data['order_channel_id'] = $order_channel_id;
    $data['send_time_status'] = $send_time_status;
    $data['create_by'] = $create_by;
    $data['start_date'] = $start_date;
    $data['end_date'] = $end_date;
    $data['order_status'] = $order_status;
    $data['page'] = $page;
    $data['per_page'] = $per_page;
    $data['order_list'] = $this->get_list($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status, $page, $per_page);
    $data['total_transaction'] = $this->get_count($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status);
    return $data;
  }
  private function get_count($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status){
    $this->where_transaction($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status);
    $this->db->select('tbl_order.order_id');
    $query = $this->db->get('tbl_order');
    return $query->num_rows();
  }
  private function get_list($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status, $page, $per_page){
    $this->where_transaction($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status);
    $this->db->select('tbl_order.*');
    $this->db->order_by('tbl_order.order_id', 'DESC');
    $offset = ($page - 1) * $per_page;
    return $this->db->get('tbl_order', $per_page, $offset);
  }
  private function where_transaction($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status){
    if($search_val != ''){
      $member_id_list = $this->get_member_id_list($search_val);
      $str_where = "(
        order_code like '%$search_val%' OR
        shipping_name like '%$search_val%' OR
        shipping_mobile like '%$search_val%' OR
        tax_name like '%$search_val%' OR
        tax_mobile like '%$search_val%' OR
        tax_code like '%$search_val%'
        )";
      $this->db->where($str_where, NULL,FALSE);
      if(count($member_id_list) > 0){
        $this->db->or_where_in('member_id', $member_id_list);
      }
    }
    if($payment_type != 'all'){
      $this->db->where('tbl_order.payment_type', $payment_type);
    }
    if($order_channel_id != 'all'){
      $this->db->where('tbl_order.order_channel_id', $order_channel_id);
    }
    if($send_time_status != 'all'){
      $this->db->where('tbl_order.send_time_status', $send_time_status);
    }
    if($create_by != 'all'){
      $this->db->where('tbl_order.create_by', $create_by);
    }
    if($start_date!= ''){
      $this->db->where('tbl_order.create_date >=', $start_date.' 00:00:00');
    }
    if($end_date != ''){
      $this->db->where('tbl_order.create_date <=', $end_date.' 23:59:59');
    }
    if($order_status != 'all'){
      if($order_status == 'web'){
        $this->db->where_in('tbl_order.order_channel_id', 1);
        $this->db->where_in('tbl_order.order_status', array('open','confirm'));
      }else{
        $this->db->where('tbl_order.order_status', $order_status);
      }
    }
    $this->db->where('is_delete', 'active');
  }
  private function get_member_id_list($search_val){
    $this->db->select('member_id');
    $str_where = "(
      member_code like '%$search_val%' OR
      member_firstname like '%$search_val%' OR
      member_lastname like '%$search_val%' OR
      member_phone like '%$search_val%' OR
      member_mobile like '%$search_val%'
      )";
    $this->db->where($str_where, NULL,FALSE);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_member');
    $member_id_list = array();
    foreach($query->result() as $member){
      $member_id_list[] = $member->member_id;
    }
    return $member_id_list;
  }
}
