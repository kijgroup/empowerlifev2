<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order_excel_service extends CI_Model{
  public function export_list($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status){
    ini_set('max_execution_time', 300);
    date_default_timezone_set('Asia/Bangkok');
    include(FCPATH."PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
    $today = date('Y-m-d');
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Narathip Harijiratiwong")
    							 ->setLastModifiedBy("Narathip Harijiratiwong")
    							 ->setTitle("Order ".$today)
    							 ->setSubject("Order ".$today)
    							 ->setDescription("Order.")
    							 ->setKeywords("Empowerlife")
                   ->setCategory("Empowerlife Order");
    // Add some data
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', '#')
                ->setCellValue('B1', 'รหัสสั่งซื้อ')
                ->setCellValue('C1', 'Member Code')
                ->setCellValue('D1', 'Member Type')
                ->setCellValue('E1', 'Register Date')
                ->setCellValue('F1', 'Email')
                ->setCellValue('G1', 'Firstname')
                ->setCellValue('H1', 'Lastname')
                ->setCellValue('I1', 'Phone Number')
                ->setCellValue('J1', 'Mobile')
                ->setCellValue('K1', 'Birthday')
                ->setCellValue('L1', 'Gender')
                ->setCellValue('M1', 'ชื่อผู้รับสินค้า')
                ->setCellValue('N1', 'การชำระเงิน')
                ->setCellValue('O1', 'ช่องทาง')
                ->setCellValue('P1', 'Source')
                ->setCellValue('Q1', 'Show')
                ->setCellValue('R1', 'ธนาคารที่ชำระเงิน')
                ->setCellValue('S1', 'ตัด Stock')
                ->setCellValue('T1', 'ส่งเช้า')
                ->setCellValue('U1', 'สถานะ')
                ->setCellValue('V1', 'วันที่สั่งซื้อ')
                ->setCellValue('W1', 'โดย')
                ->setCellValue('X1', 'สินค้า')
                ->setCellValue('Y1', 'จำนวน')
                ->setCellValue('Z1', 'ราคาต่อหน่วย')
                ->setCellValue('AA1', 'ราคาต่อประเภทสินค้า')
                ->setCellValue('AB1', 'ส่วนลดต่อรายการสินค้า')
                ->setCellValue('AC1', 'แต้มที่ใช้')
                ->setCellValue('AD1', 'จำนวนสินค้าทั้งบิล')
                ->setCellValue('AE1', 'ราคาสินค้ารวม')
                ->setCellValue('AF1', 'ส่วนลดสมาชิก')
                ->setCellValue('AG1', 'ส่วนลดแต้ม')
                ->setCellValue('AH1', 'ค่าส่งสินค้า')
                ->setCellValue('AI1', 'ยอดชำระ')
                ;
    $sheet_count = 2;
    list($order_list, $member_id_list, $admin_id_list) = $this->get_order_list($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status);
    $member_list = $this->get_member_list($member_id_list);
    $admin_list = $this->get_admin_list($admin_id_list);
    $order_channel_list = $this->get_order_channel_list();
    $order_sub_channel_list = $this->get_order_sub_channel_list();
    $contact_channel_list = $this->get_contact_channel_list();
    $bank_list = $this->get_bank_list();
    $stock_list = $this->get_stock_list();
    $product_list = $this->get_product_list();
    $counter = 1;
    $last_order_id = 0;
    foreach($order_list->result() as $order){
      $member = array(
        'member_code' => '',
        'member_type' => '',
        'email' => '',
        'create_date' => '',
        'member_email' => '',
        'member_firstname' => '',
        'member_lastname' => '',
        'member_phone' => '',
        'member_mobile' => '',
        'member_birthdate' => '',
        'member_gender' => '',
      );
      $admin_name = '';
      $order_channel = '';
      $order_sub_channel = '';
      $contact_channel = '';
      $bank = '';
      $stock = '';
      $product = '';
      if($order->member_id > 0){
        $member = $member_list[$order->member_id];
      }
      if($order->create_by > 0){
        $admin_name = $admin_list[$order->create_by];
      }
      if($order->order_channel_id > 0){
        $order_channel = $order_channel_list[$order->order_channel_id];
      }
      if($order->order_sub_channel_id > 0){
        $order_sub_channel = $order_sub_channel_list[$order->order_sub_channel_id];
      }
      if($order->contact_channel_id > 0){
        $contact_channel = $contact_channel_list[$order->contact_channel_id];
      }
      if($order->bank_id > 0){
        $bank = $bank_list[$order->bank_id];
      }
      if($order->stock_id > 0){
        $stock = $stock_list[$order->stock_id];
      }
      if($order->product_id > 0){
        $product = $product_list[$order->product_id];
      }
      $is_same_order = ($last_order_id == $order->order_id)?true:false;
      $last_order_id = $order->order_id;
      $discount_item = 0;
      if($order->item_price > 0){
        $discount_item = ($order->discount_price + $order->discount_by_point_amount)*$order->price_total / $order->item_price;
      }
      $objPHPExcel->setActiveSheetIndex(0)
                  ->setCellValue('A'.$sheet_count, $counter)
                  ->setCellValue('B'.$sheet_count, $order->order_code)
                  ->setCellValue('C'.$sheet_count, $member['member_code'])
                  ->setCellValue('D'.$sheet_count, $member['member_type'])
                  ->setCellValue('E'.$sheet_count, $member['create_date'])
                  ->setCellValue('F'.$sheet_count, $member['member_email'])
                  ->setCellValue('G'.$sheet_count, $member['member_firstname'])
                  ->setCellValue('H'.$sheet_count, $member['member_lastname'])
                  ->setCellValue('I'.$sheet_count, "'".$member['member_phone'])
                  ->setCellValue('J'.$sheet_count, "'".$member['member_mobile'])
                  ->setCellValue('K'.$sheet_count, $member['member_birthdate'])
                  ->setCellValue('L'.$sheet_count, $member['member_gender'])
                  ->setCellValue('M'.$sheet_count, $order->shipping_name)
                  ->setCellValue('N'.$sheet_count, $this->Payment_type_model->get_name($order->payment_type))
                  ->setCellValue('O'.$sheet_count, $order_channel)
                  ->setCellValue('P'.$sheet_count, $order_sub_channel)
                  ->setCellValue('Q'.$sheet_count, $contact_channel)
                  ->setCellValue('R'.$sheet_count, $bank)
                  ->setCellValue('S'.$sheet_count, $stock)
                  ->setCellValue('T'.$sheet_count, $this->Send_time_status_model->get_name($order->send_time_status))
                  ->setCellValue('U'.$sheet_count, $this->Order_status_model->get_name($order->order_status))
                  ->setCellValue('V'.$sheet_count, $order->create_date)
                  ->setCellValue('W'.$sheet_count, $admin_name)
                  ->setCellValue('X'.$sheet_count, $product)
                  ->setCellValue('Y'.$sheet_count, $order->qty)
                  ->setCellValue('Z'.$sheet_count, $order->price_per_item)
                  ->setCellValue('AA'.$sheet_count, $order->price_total)
                  ->setCellValue('AB'.$sheet_count, $discount_item)
                  ->setCellValue('AC'.$sheet_count, (!$is_same_order)?$order->discount_point_amount:'')
                  ->setCellValue('AD'.$sheet_count, (!$is_same_order)?$order->total_item:'')
                  ->setCellValue('AE'.$sheet_count, (!$is_same_order)?$order->item_price:'')
                  ->setCellValue('AF'.$sheet_count, (!$is_same_order)?$order->discount_price:'')
                  ->setCellValue('AG'.$sheet_count, (!$is_same_order)?$order->discount_by_point_amount:'')
                  ->setCellValue('AH'.$sheet_count, (!$is_same_order)?$order->shipping_price:'')
                  ->setCellValue('AI'.$sheet_count, (!$is_same_order)?$order->grand_total:'')
                  ;
      $counter++;
      $sheet_count++;
    }
    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('Emportlife Member');
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="order_'.date('Ymd').'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }
  private function get_order_list($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status){
    $member_id_list = array();
    $admin_id_list= array();
    $this->where_order($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status);
    $this->db->join('tbl_order_item', 'tbl_order_item.order_id = tbl_order.order_id');
    $this->db->where('tbl_order.is_delete', 'active');
    $this->db->order_by('tbl_order.order_id', 'DESC');
    $query = $this->db->get('tbl_order');
    foreach($query->result() as $row){
      if($row->member_id > 0){
        $member_id_list[$row->member_id] = $row->member_id;
      }
      if($row->create_by > 0){
        $admin_id_list[$row->create_by] = $row->create_by;
      }
    }
    $query->data_seek(0);
    return array(
      $query,
      $member_id_list,
      $admin_id_list
    );
  }
  private function where_order($search_val, $payment_type, $order_channel_id, $send_time_status, $create_by, $start_date, $end_date, $order_status){
    if($search_val != ''){
      $member_id_list = $this->get_member_id_list($search_val);
      $str_where = "(
        order_code like '%$search_val%' OR
        shipping_name like '%$search_val%' OR
        shipping_mobile like '%$search_val%' OR
        tax_name like '%$search_val%' OR
        tax_mobile like '%$search_val%' OR
        tax_code like '%$search_val%'
        )";
      $this->db->where($str_where, NULL,FALSE);
      if(count($member_id_list) > 0){
        $this->db->or_where_in('member_id', $member_id_list);
      }
    }
    if($payment_type != 'all'){
      $this->db->where('tbl_order.payment_type', $payment_type);
    }
    if($order_channel_id != 'all'){
      $this->db->where('tbl_order.order_channel_id', $order_channel_id);
    }
    if($send_time_status != 'all'){
      $this->db->where('tbl_order.send_time_status', $send_time_status);
    }
    if($create_by != 'all'){
      $this->db->where('tbl_order.create_by', $create_by);
    }
    if($start_date!= ''){
      $this->db->where('tbl_order.create_date >=', $start_date.' 00:00:00');
    }
    if($end_date != ''){
      $this->db->where('tbl_order.create_date <=', $end_date.' 23:59:59');
    }
    if($order_status != 'all'){
      if($order_status == 'web'){
        $this->db->where_in('tbl_order.order_channel_id', 1);
        $this->db->where_in('tbl_order.order_status', array('open','confirm'));
      }else{
        $this->db->where('tbl_order.order_status', $order_status);
      }
    }
  }
  private function get_member_id_list($search_val){
    $this->db->select('member_id');
    $str_where = "(
      member_code like '%$search_val%' OR
      member_firstname like '%$search_val%' OR
      member_lastname like '%$search_val%' OR
      member_phone like '%$search_val%' OR
      member_mobile like '%$search_val%'
      )";
    $this->db->where($str_where, NULL,FALSE);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_member');
    $member_id_list = array();
    foreach($query->result() as $member){
      $member_id_list[] = $member->member_id;
    }
    return $member_id_list;
  }
  private function get_member_list($member_id_list){
    $member_list = array();
    if(count($member_id_list) == 0){
      return array();
    }
    $this->db->select('member_id, member_code, member_type, member_firstname, member_lastname, member_phone, member_birthdate, member_gender, member_email, member_mobile, create_date');
    $this->db->where_in('member_id', $member_id_list);
    $query = $this->db->get('tbl_member');
    foreach($query->result_array() as $member){
      $member_list[$member['member_id']] = $member;
    }
    return $member_list;
  }
  private function get_admin_list($admin_id_list){
    $admin_list = array();
    if(count($admin_id_list) == 0){
      return array();
    }
    $this->db->where_in('admin_id', $admin_id_list);
    $this->db->select('admin_name, admin_id');
    $query = $this->db->get('tbl_admin');
    foreach($query->result() as $admin){
      $admin_list[$admin->admin_id] = $admin->admin_name;
    }
    return $admin_list;
  }
  private function get_order_channel_list(){
    $order_channel_list = array();
    $query = $this->db->get('tbl_order_channel');
    foreach($query->result() as $order_channel){
      $order_channel_list[$order_channel->order_channel_id] = $order_channel->order_channel_name;
    }
    return $order_channel_list;
  }
  private function get_order_sub_channel_list(){
    $order_sub_channel_list = array();
    $query = $this->db->get('tbl_order_sub_channel');
    foreach($query->result() as $order_sub_channel){
      $order_sub_channel_list[$order_sub_channel->order_sub_channel_id] = $order_sub_channel->order_sub_channel_name;
    }
    return $order_sub_channel_list;
  }
  private function get_contact_channel_list(){
    $contact_channel_list = array();
    $query = $this->db->get('tbl_contact_channel');
    foreach($query->result() as $contact_channel){
      $contact_channel_list[$contact_channel->contact_channel_id] = $contact_channel->contact_channel_name;
    }
    return $contact_channel_list;
  }
  private function get_bank_list(){
    $bank_list = array();
    $query = $this->db->get('tbl_bank');
    foreach($query->result() as $bank){
      $bank_list[$bank->bank_id] = $bank->bank_name_th;
    }
    return $bank_list;
  }
  private function get_stock_list(){
    $stock_list = array();
    $query = $this->db->get('tbl_stock');
    foreach($query->result() as $stock){
      $stock_list[$stock->stock_id] = $stock->stock_name;
    }
    return $stock_list;
  }
  private function get_product_list(){
    $product_list = array();
    $query = $this->db->get('tbl_product');
    foreach($query->result() as $product){
      $product_list[$product->product_id] = $product->name_th;
    }
    return $product_list;
  }
}