<?php
class Order_item_model extends CI_Model{
  public function get_data($order_item_id){
    $this->db->where('order_item_id', $order_item_id);
    $query = $this->db->get('tbl_order_item');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_list($order_id){
    $this->db->where('order_id', $order_id);
    return $this->db->get('tbl_order_item');
  }
  public function get_name($order_item_id){
    $order_item = $this->get_data($order_item_id);
    if($order_item){
      return $order_item->order_per_item;
    }
    return '-';
  }
  public function insert($data){
    $this->db->set('order_id', $data['order_id']);
    $this->db->set('product_id', $data['product_id']);
    $this->db->set('price_per_item', $data['price_per_item']);
    $this->db->set('qty', $data['qty']);
    $this->db->set('price_total', $data['price_total']);
    $this->db->insert('tbl_order_item');
    $order_item_id = $this->db->insert_id();
    return $order_item_id;
  }
  public function delete($order_item_id){
    $this->db->where('order_item_id', $order_item_id);
    $this->db->delete('tbl_order_item');
  }
  public function delete_order($order_id){
    $this->db->where('order_id', $order_id);
    $this->db->delete('tbl_order_item');
  }
}
