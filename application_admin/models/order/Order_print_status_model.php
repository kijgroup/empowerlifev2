<?php
class Order_print_status_model extends CI_Model{
  var $status_list = array(
    'true' => array(
      'status_code' => 'true',
      'status_name' => 'พิมพ์',
      'label_class' => 'label label-success'
    ),
    'false' => array(
      'status_code' => 'false',
      'status_name' => 'รอพิมพ์',
      'label_class' => 'label label-danger'
    )
  );
  public function get_list(){
    return $this->status_list;
  }
  public function get_data($status_code){
    return $this->status_list[$status_code];
  }
  public function get_name($status_code){
    $status = $this->get_data($status_code);
    return $status['status_name'];
  }
  public function get_label($status_code){
    $status = $this->get_data($status_code);
    return '<span class="'.$status['label_class'].'">'.$status['status_name'].'</span>';
  }
}
