<?php
class Order_status_model extends CI_Model{
  var $order_status_list = array(
    'open' => array(
      'status_code' => 'open',
      'status_name' => 'รอชำระเงิน',
      'label_class' => 'label label-warning'
    ),
    'confirm' => array(
      'status_code' => 'confirm',
      'status_name' => 'รอส่งสินค้า',
      'label_class' => 'label label-primary'
    ),
    'complete' => array(
      'status_code' => 'complete',
      'status_name' => 'เสร็จสิ้น',
      'label_class' => 'label label-success'
    ),
    'cancel' => array(
      'status_code' => 'cancel',
      'status_name' => 'ยกเลิก',
      'label_class' => 'label label-danger'
    ),
  );
  public function get_list(){
    return $this->order_status_list;
  }
  public function get_data($status_code){
    return $this->order_status_list[$status_code];
  }
  public function get_name($status_code){
    $order_status = $this->get_data($status_code);
    return $order_status['status_name'];
  }
  public function get_label($status_code){
    $order_status = $this->get_data($status_code);
    return '<span class="'.$order_status['label_class'].'">'.$order_status['status_name'].'</span>';
  }
}
