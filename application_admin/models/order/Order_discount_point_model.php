<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order_discount_point_model extends CI_Model{
  public function get_qty($order_id, $discount_point_id){
    $this->db->where('order_id', $order_id);
    $this->db->where('discount_point_id', $discount_point_id);
    $query = $this->db->get('tbl_order_discount_point');
    if($query->num_rows() == 0){
      return 0;
    }
    return $query->row()->qty;
  }
  public function get_list($order_id){
    $this->db->where('order_id', $order_id);
    return $this->db->get('tbl_order_discount_point');
  }
  public function get_data($order_id, $discount_point_id){
    $this->db->where('order_id', $order_id);
    $this->db->where('discount_point_id', $discount_point_id);
    $query = $this->db->get('tbl_order_discount_point');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  private function is_insert($order_id, $discount_point_id){
    $this->db->where('order_id', $order_id);
    $this->db->where('discount_point_id', $discount_point_id);
    $query = $this->db->get('tbl_order_discount_point');
    return ($query->num_rows() == 0);
  }
  public function add($order_id, $discount_point_id, $data){
    if($data['qty'] <= 0){
      $this->delete($order_id, $discount_point_id);
    }elseif($this->is_insert($order_id, $discount_point_id)){
      $data['order_id'] = $order_id;
      $data['discount_point_id'] = $discount_point_id;
      $this->insert($data);
    }else{
      $this->update($order_id, $discount_point_id, $data);
    }
  }
  private function insert($data){
    foreach($data as $field_name => $value){
      $this->db->set($field_name, $value);
    }
    $this->db->insert('tbl_order_discount_point');
  }
  private function update($order_id, $discount_point_id, $data){
    foreach($data as $field_name => $value){
      $this->db->set($field_name, $value);
    }
    $this->db->where('order_id', $order_id);
    $this->db->where('discount_point_id', $discount_point_id);
    $this->db->update('tbl_order_discount_point');
  }
  private function delete($order_id, $discount_point_id){
    $this->db->where('order_id', $order_id);
    $this->db->where('discount_point_id', $discount_point_id);
    $this->db->delete('tbl_order_discount_point');
  }
}