<?php
class Slideshow_model extends CI_Model{
  public function get_list($page){
    $this->db->where('page', $page);
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_slideshow');
  }
  public function get_data($slideshow_id){
    $this->db->where('slideshow_id', $slideshow_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_slideshow');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_max_priority($page){
    $this->db->where('page', $page);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_slideshow');
    return $query->num_rows();
  }
  public function insert($data){
    $this->move_up_priority($data['page'], $data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('page', $data['page']);
    $this->db->set('slideshow_image', $data['slideshow_image']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_slideshow');
    $slideshow_id = $this->db->insert_id();
    return $slideshow_id;
  }
  public function update($slideshow_id, $data){
    $slideshow = $this->get_data($slideshow_id);
    if($slideshow){
      $this->move_down_priority($slideshow->page, $slideshow->sort_priority);
      $this->move_up_priority($slideshow->page, $data['sort_priority']);
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('slideshow_id', $slideshow_id);
      $this->db->update('tbl_slideshow');
    }
  }
  public function delete($slideshow_id){
    $slideshow = $this->get_data($slideshow_id);
    if($slideshow){
      $this->move_down_priority($slideshow->page, $slideshow->sort_priority);
      $this->db->set('is_delete', 'delete');
      $this->db->where('slideshow_id', $slideshow_id);
      $this->db->update('tbl_slideshow');
    }
  }
  public function move_up_priority($page, $sort_priority){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->where('page', $page);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_slideshow');
  }
  public function move_down_priority($page, $sort_priority){
    $this->db->set('sort_priority', 'sort_priority - 1', false);
    $this->db->where('sort_priority >', $sort_priority);
    $this->db->where('page', $page);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_slideshow');
  }
  private function set_to_db($data){
    $this->db->set('slideshow_text', $data['slideshow_text']);
    $this->db->set('slideshow_url', $data['slideshow_url']);
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('sort_priority', $data['sort_priority']);
  }
}
