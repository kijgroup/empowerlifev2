<?php
class Page_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function get_data($page_code){
        $this->db->where('page_code', $page_code);
        $query = $this->db->get('tbl_page');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function get_name($page_code){
        $page = $this->getData($page_code);
        if($page){
            return $page->page_name_th;
        }
        return '-';
    }
    public function update($page_code, $data){
        $this->setToDb($data);
        $this->db->set('update_date', 'now()', false);
        $this->db->set('update_by', $this->session->userdata('admin_id'));
        $this->db->where('page_code', $page_code);
        $this->db->update('tbl_page');
        $this->Log_action_model->insert_log('page/index/'.$page_code,0,'update', 'update page '.$page_code, $data, $this->db->last_query());
    }
    public function setToDb($data){
        $this->db->set('page_name_th', $data['page_name_th']);
        $this->db->set('page_name_en', $data['page_name_en']);
        $this->db->set('page_content_th', $data['page_content_th']);
        $this->db->set('page_content_en', $data['page_content_en']);
    }
}
