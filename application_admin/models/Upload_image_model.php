<?php
class Upload_image_model extends CI_Model {
  var $upload_path = './uploads/';
  var $thumb_width = 200;
  var $thumb_height = 200;
  function __construct() {
    parent::__construct();
  }
  public function set_thumb($width = 200, $height = 200){
    $this->thumb_width = $width;
    $this->thumb_height = $height;
  }
  public function upload_single_image($prefix, $field_name = 'file'){
    $ret_val = array();
    $this->load->library('upload');
    $this->upload->initialize(array(
      'file_name' => $this->gen_image_name($prefix),
      'upload_path' => $this->upload_path,
      'allowed_types' => 'gif|jpg|png|jpeg',
      'max_size' => '8000',
      'max_width' => '4096',
      'max_height' => '4096'
    ));
    if (!$this->upload->do_upload($field_name)){
      $ret_val = array('success'=>false, 'message'=>$this->upload->display_errors(), 'image_file'=>'');
    }else{
      $image_data = $this->upload->data();
      $ret_val = array(
        'success' => true,
        'message' => 'success',
        'image_file' => $image_data['file_name']
      );
    }
    return $ret_val;
  }
  public function upload_image($prefix, $field_name = 'file'){
    $ret_val = array();
    $config['file_name'] = $this->gen_image_name($prefix);
    $config['upload_path'] = $this->upload_path;
    $config['allowed_types'] = 'gif|jpg|png|jpeg';
    $config['max_size'] = '8000';
    $config['max_width']  = '4096';
    $config['max_height']  = '4096';
    $this->load->library('upload', $config);
    if (!$this->upload->do_upload($field_name)){
      $ret_val = array('success'=>false, 'message'=>$this->upload->display_errors(), 'imageData'=>'');
    }else{
      $image_data = $this->upload->data();
      $image_width = $image_data['image_width'];
      $image_height = $image_data['image_height'];
      $image_medium = $image_data['raw_name'].'_m'.$image_data['file_ext'];
      $image_small = $image_data['raw_name'].'_s'.$image_data['file_ext'];
      $this->resize_image($this->upload_path.$image_data['file_name'], $this->upload_path.$image_medium, 960, 960);
      $this->resize_and_crop_image($this->upload_path.$image_data['file_name'], $this->upload_path.$image_small, $image_width, $image_height, $this->thumb_width, $this->thumb_height);
      $ret_val = array(
        'success'=>true,
        'message'=>'success',
        'image_large'=>$image_data['file_name'],
        'image_medium'=>$image_medium,
        'image_small'=>$image_small,
        'image_name'=>$image_data['client_name']
      );
    }
    return $ret_val;
  }
  private function gen_image_name($prefix){
    $date = date('Y/m/d H:i:s');
    $replace = array(":"," ","/");
    return $prefix.'_'.str_ireplace($replace, "", $date);
  }
  private function resize_image($source_image, $new_image, $width, $height){
    $this->load->library('image_lib');
    $this->image_lib->initialize(array(
      'source_image' => $source_image,
      'new_image' => $new_image,
      'width' => $width,
      'height' => $height
    ));
    $this->image_lib->resize();
  }
  private function resize_and_crop_image($source_image, $new_image, $source_width, $source_height, $width, $height){
    $this->load->library('image_lib');
    $dim = (intval($source_width) / intval($source_height)) - ($width / $height);
    $master_dim = ($dim > 0)?'height':'width';
    $this->image_lib->initialize(array(
      'source_image' => $source_image,
      'new_image' => $new_image,
      'width' => $width,
      'height' => $height,
      'master_dim' => $master_dim
    ));
    $this->image_lib->resize();
    $x_axis = ($dim > 0)?(($source_width * $height/$source_height) - $width)/2:0;
    $y_axis = ($dim > 0)?0:(($source_height * $width/$source_width) - $height)/2;
    $this->image_lib->initialize(array(
      'source_image' => $new_image,
      'new_image' => $new_image,
      'width' => $width,
      'height' => $height,
      'maintain_ratio' => false,
      'x_axis' => $x_axis,
      'y_axis' => $y_axis
    ));
    $this->image_lib->crop();
  }
  public function delete_image($file_name){
    $this->load->helper('file');
    $path = FCPATH.'uploads/'.$file_name;
    if (file_exists($path)) {
      unlink($path) or die('failed deleting: ' . $path);
    }
  }
}
