<?php
class Footer_link_model extends CI_Model{

  public function get_list($all_status = true){
    if(!$all_status){
      $this->db->where('enable_status', 'show');
    }
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_footer_link');
  }

  public function get_data($footer_link_id){
    $this->db->where('footer_link_id', $footer_link_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_footer_link');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }

  public function get_max_priority(){
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_footer_link');
    return $query->num_rows();
  }

  public function insert($data){
    $this->move_up_priority($data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_footer_link');
    $footer_link_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('footer_link',$footer_link_id,'insert', 'insert new footer_link ', $data, $this->db->last_query());
    return $footer_link_id;
  }

  public function update($footer_link_id, $data){
    $footer_link = $this->get_data($footer_link_id);
    if($footer_link){
      $this->move_down_priority($footer_link->sort_priority);
      $this->move_up_priority($data['sort_priority']);
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('footer_link_id', $footer_link_id);
      $this->db->update('tbl_footer_link');
      $this->Log_action_model->insert_log('footer_link', $footer_link_id,'update', 'update footer_link', $data, $this->db->last_query());
    }
  }

  public function delete($footer_link_id){
    $footer_link = $this->get_data($footer_link_id);
    if($footer_link){
      $this->move_down_priority($footer_link->sort_priority);
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('footer_link_id', $footer_link_id);
      $this->db->update('tbl_footer_link');
      $this->Log_action_model->insert_log('footer_link', $footer_link_id,'delete', 'delete footer_link', array(), $this->db->last_query());
    }
  }

  private function set_to_db($data){
    $this->db->set('name', $data['name']);
    $this->db->set('link_url', $data['link_url']);
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('sort_priority', $data['sort_priority']);
    if($data['link_icon'] != ""){
      $this->db->set('link_icon', $data['link_icon']);
    }
  }

  private function move_up_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_footer_link');
  }

  private function move_down_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority - 1', false);
    $this->db->where('sort_priority >', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_footer_link');
  }
}
