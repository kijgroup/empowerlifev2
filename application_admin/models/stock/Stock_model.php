<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Stock_model extends MY_Model{
  public $table_name = 'tbl_stock';
  public $field_key = 'stock_id';
  public $field_name = 'stock_name';
  public $field_search_val_list = array('stock_name');
  public $is_sort_priority = true;
  public $structure = array(
    array(
      'field_name' => 'ชื่อตัด Stock',
      'field_key' => 'stock_name',
      'field_type' => 'text',
      'placeholder' => 'โปรดระบุ *',
      'required' => true
    ),
  );
}
