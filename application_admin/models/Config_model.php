<?php
class Config_model extends CI_Model{
  public function get_data($code){
    $this->db->where('config_code', $code);
    $query = $this->db->get('tbl_config');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_value($code){
    $config = $this->get_data($code);
    return ($config)?$config->config_value:'';
  }
  public function insert($code, $value){
    $this->db->set('config_code', $code);
    $this->db->set('config_value', $value);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->insert('tbl_config');
  }
  public function update($code, $value){
    $this->db->set('config_value', $value);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('config_code', $code);
    $this->db->update('tbl_config');
  }
}
