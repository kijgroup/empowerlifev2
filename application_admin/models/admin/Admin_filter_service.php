<?php
class Admin_filter_service extends CI_Model {

  function __construct() {
    parent::__construct();
  }

  public function get_data_content($search_val, $enable_status, $page = 1, $per_page = 20) {
    $data = array();
    $data['search_val'] = $search_val;
    $data['enable_status'] = $enable_status;
    $data['page'] = $page;
    $data['per_page'] = $per_page;
    $data['admin_list'] = $this->get_list($search_val, $enable_status, $page, $per_page);
    $data['total_transaction'] = $this->get_count($search_val, $enable_status);
    return $data;
  }
  private function get_count($search_val, $enable_status){
    $this->db->select('tbl_admin.admin_id');
    $this->where_transaction($search_val, $enable_status);
    $query = $this->db->get('tbl_admin');
    return $query->num_rows();
  }
  private function get_list($search_val, $enable_status, $page, $per_page){
    $this->db->select('tbl_admin.*');
    $this->where_transaction($search_val, $enable_status);
    $offset = ($page - 1) * $per_page;
    return $this->db->get('tbl_admin', $per_page, $offset);
  }
  private function where_transaction($search_val, $enable_status){
    if($search_val != ''){
      $str_where = "(admin_name like '%$search_val%' ";
      $str_where .= "OR admin_username like '%$search_val%' ";
      $str_where .= ')';
      $this->db->where($str_where, NULL,FALSE);
    }
    if($enable_status != 'all'){
      $this->db->where('tbl_admin.enable_status', $enable_status);
    }
    $this->db->where('is_delete', 'active');
  }
}
