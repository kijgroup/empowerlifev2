<?php
class Admin_model extends CI_Model {
  var $login_key = 'emp0wer1ife';
  function __construct() {
    parent::__construct();
  }
  public function get_list(){
    $this->db->where('is_delete', 'active');
    $this->db->order_by('admin_name', 'ASC');
    return $this->db->get('tbl_admin');
  }
  public function get_login_name_by_id($admin_id){
    $admin = $this->get_data($admin_id);
    if($admin){
      return $admin->admin_username;
    }
    return '-';
  }
  public function get_data($admin_id){
    $this->db->where('admin_id', $admin_id);
    $query = $this->get_admin_db();
    return ($query->num_rows() > 0)?$query->row():false;
  }
  public function get_login($username, $password){
    $this->db->where('admin_username', $username);
    $this->db->where('admin_password', $this->encrypt_password($password));
    $query = $this->get_admin_db();
    return ($query->num_rows() > 0)?$query->row():false;
  }
  private function get_admin_db(){
    $this->db->select('tbl_admin.*');
    $this->db->where('is_delete', 'active');
    return $this->db->get('tbl_admin');
  }
  public function set_last_login($admin_id){
    $this->db->set('last_login_date', 'NOW()', false);
    $this->db->where('admin_id', $admin_id);
    $this->db->update('tbl_admin');
  }
  public function insert($data){
    $this->set_to_db($data);
    $this->db->set('admin_password', $this->encrypt_password($data['admin_password']));
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', (isset($data['create_by']))?$data['create_by']:$this->session->userdata('admin_id'));
    $this->db->insert('tbl_admin');
    $admin_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('admin',$admin_id,'insert', 'create new admin', $data, $this->db->last_query());
    return $admin_id;
  }
  public function update($admin_id, $data){
    $this->set_to_db($data);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('admin_id', $admin_id);
    $this->db->update('tbl_admin');
    $this->Log_action_model->insert_log('admin',$admin_id,'update', 'update admin', $data, $this->db->last_query());
  }
  public function delete($admin_id){
    $this->db->where('is_delete', 'delete');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('admin_id', $admin_id);
    $this->db->update('tbl_admin');
    $this->Log_action_model->insert_log('admin',$admin_id,'delete', 'delete admin', array(), $this->db->last_query());
  }
  public function update_password($admin_id, $password){
    $this->db->set('admin_password', $this->encrypt_password($password));
    $this->db->set('update_date', 'now()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('admin_id', $admin_id);
    $this->db->update('tbl_admin');
    $this->Log_action_model->insert_log('admin',$admin_id,'change password', 'change password admin', array(), $this->db->last_query());
  }
  private function set_to_db($data){
    $this->db->set('admin_type', $data['admin_type']);
    $this->db->set('admin_username', $data['admin_username']);
    $this->db->set('admin_name', $data['admin_name']);
    $this->db->set('enable_status', $data['enable_status']);
  }
  public function edit_profile($data){
    $this->db->set('admin_name', $data['admin_name']);
    $this->db->set('update_date', 'now()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('admin_id', $this->session->userdata('admin_id'));
    $this->db->update('tbl_admin');
    $this->Log_action_model->insert_log('admin', $this->session->userdata('admin_id'),'editprofile', 'edit profile', $data, $this->db->last_query());
  }
  public function encrypt_password($login_password){
    return md5($this->login_key.$login_password);
  }
}
