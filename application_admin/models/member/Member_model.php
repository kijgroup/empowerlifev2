<?php
class Member_model extends CI_Model {
  var $login_key = '';
  function __construct() {
    parent::__construct();
    $this->load->model('member/User_model');
  }
  public function get_list(){
    $this->db->where('is_delete', 'active');
    return $this->db->get('tbl_member');
  }
  public function get_data($member_id){
    $this->db->where('member_id', $member_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_member');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
  public function get_name($member_id){
    $member = $this->get_data($member_id);
    if($member){
      return $member->member_firstname.' '.$member->member_lastname;
    }
    return '-';
  }
  public function insert($data){
    $this->set_to_db($data);
    $this->db->set('create_date', 'now()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_member');
    $member_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('member',$member_id,'insert', 'insert new member ', $data, $this->db->last_query());
    return $member_id;
  }
  public function update($member_id, $data){
    $member = $this->get_data($member_id);
    if(!$member){
      return false;
    }
    $this->set_to_db($data);
    $this->db->set('update_date', 'now()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
    $this->Log_action_model->insert_log('member', $member_id,'update', 'update member', $data, $this->db->last_query());
    $this->auto_update_member_code($member_id);
  }
  public function set_user($member_id, $user_id){
    $this->db->set('user_id', $user_id);
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
  }
  public function delete($member_id){
    $this->db->set('is_delete', 'delete');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
    $this->Log_action_model->insert_log('member',$member_id,'delete', 'delete member ', array(), $this->db->last_query());
  }
  public function add_point($member_id, $member_point){
    $member = $this->get_data($member_id);
    $new_member_point = $member->member_point + $member_point;
    $this->db->set('member_point', $new_member_point);
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
  }
  public function add_buy_total($member_id, $buy_amount){
    $member = $this->get_data($member_id);
    $new_buy_total = $member->buy_total + $buy_amount;
    $this->db->set('buy_total', $new_buy_total);
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
    $this->check_change_member_type($member_id);
  }
  public function check_change_member_type($member_id){
    $this->load->model('member/Member_type_model');
    $member = $this->get_data($member_id);
    $next_member_type = $this->Member_type_model->get_next_data($member->member_type);
    if($next_member_type && $member->buy_total >= $next_member_type->require_amount){
      $remain_buy_total = $member->buy_total - $next_member_type->require_amount;
      $this->upgrade_member_type($member_id, $next_member_type->member_type_code, $remain_buy_total);
      $this->auto_update_member_code($member_id);
    }
  }
  private function upgrade_member_type($member_id, $member_type, $remain_buy_total){
    $this->db->set('member_type', $member_type);
    $this->db->set('expire_date', 'CURDATE() + INTERVAL 1 YEAR', false);
    $this->db->set('buy_total', $remain_buy_total);
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
  }
  private function set_to_db($data){
    $columns = array('member_type', 'expire_date', 'buy_total', 'member_point', 'member_email', 'member_firstname', 'member_lastname', 'member_gender', 'member_birthdate', 'member_phone', 'member_mobile', 'member_address', 'member_postcode', 'enable_status', 'member_district', 'member_amphur', 'member_province');
    foreach($columns as $column){
      $this->db->set($column, $data[$column]);
    }
  }
  public function update_field($member_id, $field_name, $input_value){
    $this->db->set($field_name, $input_value);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
  }
  public function encrypt_password($member_password){
      return md5($this->login_key.$member_password);
  }
  public function update_member_code($member_id, $member_code){
    $this->db->set('member_code', $member_code);
    $this->db->set('update_date','NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
  }
  public function create_member_code($member_id){
    $member = $this->get_data($member_id);
    if($member->member_code != ''){
      return false;
    }
    $member_type = $this->Member_type_model->get_data($member->member_type);
    $pre_code = $member_type->pre_code;
    $running_code = $this->get_last_running_code();
    $member_code = $pre_code.$running_code;
    $this->db->set('member_code', $member_code);
    $this->db->set('pre_code', $pre_code);
    $this->db->set('running_code', $running_code);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
  }
  public function auto_update_member_code($member_id){
    $member = $this->Member_model->get_data($member_id);
    $default_member_code = $member->pre_code. $member->running_code;
    if($member->member_code != $default_member_code){
      return false;
    }
    $member_type = $this->Member_type_model->get_data($member->member_type);
    $pre_code = $member_type->pre_code;
    $new_member_code = $pre_code.$member->running_code;
    $this->db->set('member_code', $new_member_code);
    $this->db->set('pre_code', $pre_code);
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
  }
  private function get_last_running_code(){
    $min_code = 5000;
    $this->db->select('MAX(running_code) as MAX_CODE');
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_member');
    if($query->num_rows() == 0){
      return $min_code;
    }
    $max_code = $query->row()->MAX_CODE;
    $max_code = ($max_code < $min_code)?$min_code:$max_code;
    return ++$max_code;
  }
}
