<?php
class User_model extends CI_Model{
  var $login_key = '';
  public function get_list(){
    $this->db->where('is_delete', 'active');
    return $this->db->get('tbl_user');
  }
  public function get_data_member($member_id){
    $this->db->where('member_id', $member_id);
    $this->db->where('is_delete', 'active');
    return $this->query_data();
  }
  public function get_data($user_id){
    $this->db->where('user_id', $user_id);
    return $this->query_data();
  }
  private function query_data(){
    $query = $this->db->get('tbl_user');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function is_valid_email($email, $user_id = 0){
    if($email == ''){
      return false;
    }
    $this->db->where('email', $email);
    $this->db->where('user_id !=', $user_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_user');
    return ($query->num_rows() == 0);
  }
  public function insert($data){
    $this->db->set('member_id', $data['member_id']);
    $this->db->set('email', $data['email']);
    $this->db->set('login_password', $this->encrypt_password($data['login_password']));
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_user');
    $user_id = $this->db->insert_id();
    return $user_id;
  }
  public function update($user_id, $data){
    $this->db->set('email', $data['email']);
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('user_id', $user_id);
    $this->db->update('tbl_user');
  }
  public function update_field($user_id, $field_name, $field_value){
    $this->db->set($field_name, $field_value);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('user_id', $user_id);
    $this->db->update('tbl_user');
  }
  public function update_password($user_id, $login_password){
    $this->update_field($user_id, 'login_password', $this->encrypt_password($login_password));
  }
  public function delete($user_id){
    $this->db->set('is_delete', 'delete');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('user_id', $user_id);
    $this->db->update('tbl_user');
  }
  public function delete_member($member_id){
    $this->db->set('is_delete', 'delete');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_user');
  }
  public function encrypt_password($login_password){
    return md5($this->login_key.$login_password);
  }
}
