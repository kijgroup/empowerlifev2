<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Member_csv_service extends CI_Model{
  public function export_list(){
    $filename = 'member_'.date('Ymd_his', time()).'.csv';
    $member_list = $this->get_list();
    $file = $this->set_header($filename);
    $column_list = array(
      'member_code' => 'Member Code',
      'member_type' => 'Member Type',
      'expire_date' => 'Expire Date',
      'buy_total' => 'Buy Total',
      'member_point' => 'Member Point',
      'email' => 'Email Login',
      'create_date' => 'Register Date',
      'member_email' => 'Email',
      'member_firstname' => 'Firstname',
      'member_lastname' => 'Lastname',
      'member_phone' => 'Phone Number',
      'member_mobile' => 'Mobile',
      'member_birthdate' => 'Birthday',
      'member_gender' => 'Gender',
      'member_address' => 'Address',
      'member_district' => 'District',
      'member_amphur' => 'Amphur',
      'member_province' => 'Province',
      'member_postcode' => 'Postcode',
      'note' => 'Note',
    );
    $this->set_head($file, $column_list);
    flush();
    $this->set_content($file, $column_list, $member_list);
    $this->set_close($file);
  }
  private function set_header($filename){
    ob_clean();
    ini_set('max_execution_time', 1200);
    date_default_timezone_set('Asia/Bangkok');
    header('Content-Encoding: UTF-8');
    header('Content-type: text/csv; charset=UTF-8');
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header('Content-Description: File Transfer');
    header("Content-type: text/csv");
    header("Content-Disposition: attachment; filename={$filename}");
    header("Expires: 0");
    header("Pragma: public");
    $file = @fopen( 'php://output', 'w' );
    fwrite($file, "\xEF\xBB\xBF");
    return $file;
  }
  private function set_head($file, $column_list){
    $csv_list = array('วันที่บันทึกข้อมูล');
    foreach($column_list as $column_code => $column_name){
      $csv_list[] = $column_name;
    }
    fputcsv($file, $csv_list);
  }
  private function set_content($file, $column_list, $item_list){
    $counter = 0;
    foreach($item_list->result_array() as $item){
      $counter++;
      if($counter%1000 === 0){
        flush();
      }
      $csv_list = array($item['create_date']);
      foreach($column_list as $column_code => $column_name){
        $csv_list[] = $item[$column_code];
      }
      fputcsv($file, $csv_list);
    }
  }
  private function set_close($file){
    fclose($file);
    exit;
  }
  public function get_list(){
    $this->db->select('tbl_member.*');
    $this->db->select('tbl_user.email');
    $this->db->join('tbl_user', 'tbl_user.member_id = tbl_member.member_id', 'left');
    $this->db->where('tbl_member.is_delete', 'active');
    return $this->db->get('tbl_member');
  }
}