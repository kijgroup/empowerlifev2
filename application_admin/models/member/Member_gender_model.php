<?php
class Member_gender_model extends CI_Model{
  var $gender_list = array(
    'undefined' => array(
      'status_code' => 'undefined',
      'status_name' => 'ไม่ระบุ',
      'label_class' => 'label label-default'
    ),
    'male' => array(
      'status_code' => 'male',
      'status_name' => 'ผู้ชาย',
      'label_class' => 'label label-primary'
    ),
    'female' => array(
      'status_code' => 'female',
      'status_name' => 'ผู้หญิง',
      'label_class' => 'label label-danger'
    )
  );
  public function get_list(){
    return $this->gender_list;
  }
  public function get_data($status_code){
    return $this->gender_list[$status_code];
  }
  public function get_name($status_code){
    $gender = $this->get_data($status_code);
    return $gender['status_name'];
  }
  public function get_label($status_code){
    $gender = $this->get_data($status_code);
    return '<span class="'.$gender['label_class'].'">'.$gender['status_name'].'</span>';
  }
}
