<?php
class Member_enable_status_model extends CI_Model{
  var $enable_status_list = array(
    'active' => array(
      'status_code' => 'active',
      'status_name' => 'ใช้งานได้',
      'label_class' => 'label label-success'
    ),
    'inactive' => array(
      'status_code' => 'inactive',
      'status_name' => 'ปิดการใช้งาน',
      'label_class' => 'label label-danger'
    )
  );
  public function get_list(){
    return $this->enable_status_list;
  }
  public function get_data($status_code){
    return $this->enable_status_list[$status_code];
  }
  public function get_name($status_code){
    $enable_status = $this->get_data($status_code);
    return $member_type['status_name'];
  }
  public function get_label($status_code){
    $enable_status = $this->get_data($status_code);
    return '<span class="'.$enable_status['label_class'].'">'.$enable_status['status_name'].'</span>';
  }
}
