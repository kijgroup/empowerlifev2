<?php
class Member_type_model extends CI_Model{
  public function get_list(){
    $this->db->order_by('step', 'ASC');
    return $this->db->get('tbl_member_type');
  }
  public function get_data($member_type_code){
    $this->db->where('member_type_code', $member_type_code);
    $query = $this->db->get('tbl_member_type');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_data_by_step($step){
    $this->db->where('step', $step);
    $query = $this->db->get('tbl_member_type');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_next_data($member_type_code){
    $member_type = $this->get_data($member_type_code);
    $next_step = $member_type->step + 1;
    return $this->get_data_by_step($next_step);
  }
  public function get_name($member_type_code){
    $member_type = $this->get_data($member_type_code);
    if(!$member_type_code){
      return '-';
    }
    return $member_type->member_type_name;
  }
  public function get_label($member_type_code){
    $member_type = $this->get_data($member_type_code);
    return '<span class="'.$member_type->label_class.'">'.$member_type->member_type_name.'</span>';
  }
  public function update($member_type_code, $data){
    $columns = array('member_type_name', 'pre_code', 'member_type_code', 'require_amount', 'convert_point', 'discount', 'min_amount');
    foreach($columns as $column){
      $this->db->set($column, $data[$column]);
    }
    if($data['member_type_image'] !=''){
      $this->db->set('member_type_image', $data['member_type_image']);
    }
    $this->db->where('member_type_code', $member_type_code);
    $this->db->update('tbl_member_type');
  }
}
