<?php
class Reward_model extends CI_Model{
  public function get_list(){
    $this->db->where('is_delete', 'active');
    return $this->db->get('tbl_reward');
  }
  public function get_data($reward_id){
    $this->db->where('reward_id', $reward_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_reward');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function insert($data){
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_reward');
    $reward_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('reward',$reward_id,'insert', 'insert new reward ', $data, $this->db->last_query());
    return $reward_id;
  }
  public function update($reward_id, $data){
    $reward = $this->get_data($reward_id);
    if($reward){
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('reward_id', $reward_id);
      $this->db->update('tbl_reward');
      $this->Log_action_model->insert_log('reward', $reward_id,'update', 'update reward', $data, $this->db->last_query());
    }
  }
  public function delete($reward_id){
    $reward = $this->get_data($reward_id);
    if($reward){
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('reward_id', $reward_id);
      $this->db->update('tbl_reward');
      $this->Log_action_model->insert_log('reward', $reward_id,'delete', 'delete reward', array(), $this->db->last_query());
    }
  }
  private function set_to_db($data){
    $this->db->set('name_th', $data['name_th']);
    $this->db->set('name_en', $data['name_en']);
    $this->db->set('reward_point', $data['reward_point']);
    $this->db->set('detail_th', $data['detail_th']);
    $this->db->set('detail_en', $data['detail_en']);
    $this->db->set('enable_status', $data['enable_status']);
    if($data['thumb_image'] != ''){
      $this->db->set('thumb_image', $data['thumb_image']);
    }
    if($data['main_image'] != ''){
      $this->db->set('main_image', $data['main_image']);
    }
  }
}
