<?php
class Contact_group_model extends CI_Model{
  public function get_list($all_status = true){
    if(!$all_status){
      $this->db->where('enable_status', 'show');
    }
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_contact_group');
  }

  public function get_data($contact_group_id){
    $this->db->where('contact_group_id', $contact_group_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_contact_group');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_name($contact_group_id){
    $contact_group = $this->get_data($contact_group_id);
    if(!$contact_group){
      return 'ไม่ระบุ';
    }
    return $contact_group->contact_group_th;
  }
  public function get_max_priority(){
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_contact_group');
    return $query->num_rows();
  }
  public function insert($data){
    $this->move_up_priority($data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_contact_group');
    $contact_group_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('contact_group',$contact_group_id,'insert', 'insert new contact_group ', $data, $this->db->last_query());
    return $contact_group_id;
  }
  public function update($contact_group_id, $data){
    $contact_group = $this->get_data($contact_group_id);
    if($contact_group){
      $this->move_down_priority($contact_group->sort_priority);
      $this->move_up_priority($data['sort_priority']);
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('contact_group_id', $contact_group_id);
      $this->db->update('tbl_contact_group');
      $this->Log_action_model->insert_log('contact_group', $contact_group_id,'update', 'update contact_group', $data, $this->db->last_query());
    }
  }
  public function delete($contact_group_id){
    $contact_group = $this->get_data($contact_group_id);
    if($contact_group){
      $this->move_down_priority($contact_group->sort_priority);
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('contact_group_id', $contact_group_id);
      $this->db->update('tbl_contact_group');
      $this->Log_action_model->insert_log('contact_group', $contact_group_id,'delete', 'delete contact_group', array(), $this->db->last_query());
    }
  }
  private function set_to_db($data){
    $this->db->set('contact_group_th', $data['contact_group_th']);
    $this->db->set('contact_group_en', $data['contact_group_en']);
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('sort_priority', $data['sort_priority']);
  }
  private function move_up_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_contact_group');
  }
  private function move_down_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority - 1', false);
    $this->db->where('sort_priority >', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_contact_group');
  }
}
