<?php
class Contact_model extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function get_data($contact_id){
    $this->db->where('contact_id', $contact_id);
    $query = $this->db->get('tbl_contact');
    return ($query->num_rows() > 0)?$query->row():false;
  }
  public function set_to_read($contact_id){
    $this->db->set('is_read', 'read');
    $this->db->where('contact_id', $contact_id);
    $this->db->update('tbl_contact');
    $this->Log_action_model->insert_log('contact',$contact_id,'read', 'read contact', array(), $this->db->last_query());
  }
  public function delete($contact_id){
    $this->db->set('is_delete', 1);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('contact_id', $contact_id);
    $this->db->update('tbl_contact');
    $this->Log_action_model->insert_log('contact',$contact_id,'delete', 'delete contact', array(), $this->db->last_query());
  }
  private function set_to_db($data){
    $this->db->set('contact_name', $data['contact_name']);
    $this->db->set('contact_email', $data['contact_email']);
    $this->db->set('contact_phone', $data['contact_phone']);
    $this->db->set('contact_subject', $data['contact_subject']);
    $this->db->set('contact_detail', $data['contact_detail']);
  }
}
