<?php
class Login_service extends CI_Model {

  var $NO_USER_NAME = 'require_user_name';
  var $NO_PASSWORD = 'require_password';
  var $NO_USER_FOUND = 'user_and_password_miss_match';
  var $USERTYPE_INVALID = 'unenable_user';

  function __construct() {
    parent::__construct();
  }

  public function must_login() {
    if (!$this->get_login_status()) {
      redirect('login');
    }
  }
  public function must_not_login() {
    if ($this->get_login_status()) {
      redirect('dashboard');
    }
  }

  public function logout() {
    $this->session->sess_destroy();
  }

  public function format_username($username) {
    return ($username !== 'null') ? $username : '';
  }

  public function getErrorMessage($error) {
    $error_message = '';
    if ($error != '') {
      $arr_message[$this->NO_USER_NAME] = 'กรุณาระบุชื่อผู้ใช้';
      $arr_message[$this->NO_PASSWORD] = 'กรุณาระบุรหัสผ่าน';
      $arr_message[$this->NO_USER_FOUND] = 'ไม่พบข้อมูล กรุณาตรวจสอบชื่อผู้ใช้ และรหัสผ่านอีกครั้ง';
      $arr_message[$this->USERTYPE_INVALID] = 'บัญชีนี้ไม่สามารถใช้ในระบบนี้ได้ กรุณาติดต่อเจ้าหน้าที่เพื่อสอบถามข้อมูลเพิ่มเติม';
      $error_message = (isset($arr_message[$error])) ? $arr_message[$error] : $error;
    }
    return $error_message;
  }

  public function login($username, $password) {
    $this->check_username_input($username);
    $this->check_password_input($password);
    $admin = $this->check_user_login($username, $password);
    $this->check_user_allow($admin);
    $this->create_session($admin);
  }

  private function get_login_status() {
    if ($this->session->userdata('logged_in')) {
      return true;
    }
    return false;
  }

  private function check_username_input($username) {
    if (!$username) {
      redirect('login/index/' . $this->NO_USER_NAME);
    }
  }

  private function check_password_input($password) {
    if (!$password) {
      redirect('login/index/'.$this->NO_PASSWORD);
    }
  }

  private function check_user_login($username, $password) {
    $data = $this->Admin_model->get_login($username, $password);
    if (!$data) {
      redirect('login/index/'.$this->NO_USER_FOUND);
    }
    return $data;
  }

  private function check_user_allow($admin) {
    if ($admin->enable_status != 'show') {
      redirect('login/index/'.$this->USERTYPE_INVALID);
    }
  }

  public function update_session(){
    $admin = $this->Admin_model->get_data($this->session->userdata('admin_id'));
    $this->create_session($admin);
  }
  private function create_session($admin) {
    $userdata = array();
    $userdata['admin_type'] = $admin->admin_type;
    $userdata['admin_id'] = $admin->admin_id;
    $userdata['admin_username'] = $admin->admin_username;
    $userdata['admin_name'] = $admin->admin_name;
    $userdata['logged_in'] = TRUE;
    $this->session->set_userdata($userdata);
  }

}
