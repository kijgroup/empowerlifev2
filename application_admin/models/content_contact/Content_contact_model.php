<?php
class Content_contact_model extends CI_Model{

  public function get_data($content_contact_id){
    $this->db->where('content_contact_id', $content_contact_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_content_contact');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function insert($data){
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_content_contact');
    $insert_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('content contact',$insert_id,'insert', 'insert new content contact', $data, $this->db->last_query());
    return $insert_id;
  }

  public function update($content_contact_id, $data){
    $content_contact = $this->get_data($content_contact_id);
    if($content_contact){
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('content_contact_id', $content_contact_id);
      $this->db->update('tbl_content_contact');
      $this->Log_action_model->insert_log('content contact', $content_contact_id,'update', 'update content contact', $data, $this->db->last_query());
    }
  }

  public function delete($content_contact_id){
    $content_contact = $this->get_data($content_contact_id);
    if($content_contact){
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('content_contact_id', $content_contact_id);
      $this->db->update('tbl_content_contact');
      $this->Log_action_model->insert_log('content contact', $content_contact_id,'delete', 'delete content contact', array(), $this->db->last_query());
    }
  }
  private function set_to_db($data){
    $this->db->set('name', $data['name']);
    $this->db->set('mobile', $data['mobile']);
    $this->db->set('remark', $data['remark']);
  }
}
