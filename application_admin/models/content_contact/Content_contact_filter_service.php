<?php
class Content_contact_filter_service extends CI_Model{

  public function get_data_content($search_val, $content_id, $page = 1, $per_page = 20) {
    $data = array();
    $data['search_val'] = $search_val;
    $data['content_id'] = $content_id;
    $data['page'] = $page;
    $data['per_page'] = $per_page;
    $data['content_list'] = $this->get_list($search_val, $content_id, $page, $per_page);
    $data['total_transaction'] = $this->get_count($search_val, $content_id);
    return $data;
  }

  private function get_count($search_val, $content_id){
    $this->db->select('tbl_content_contact.content_contact_id');
    $this->where_transaction($search_val, $content_id);
    $query = $this->db->get('tbl_content_contact');
    return $query->num_rows();
  }

  private function get_list($search_val, $content_id, $page, $per_page){
    $this->db->select('tbl_content_contact.*');
    $this->where_transaction($search_val, $content_id);
    $this->db->select('tbl_content.subject_th');
    $this->db->join('tbl_content', 'tbl_content.content_id = tbl_content_contact.content_id', 'left');
    $this->db->order_by('tbl_content_contact.content_contact_id', 'DESC');
    $offset = ($page - 1) * $per_page;
    return $this->db->get('tbl_content_contact', $per_page, $offset);
  }

  private function where_transaction($search_val, $content_id){
    if($search_val != ''){
      $str_where = "(name like '%$search_val%' ";
      $str_where .= "OR mobile like '%$search_val%' ";
      $str_where .= "OR remark like '%$search_val%' ";
      $str_where .= ')';
      $this->db->where($str_where, NULL,FALSE);
    }
    if($content_id != 'all'){
      $this->db->where('tbl_content_contact.content_id', $content_id);
    }
    $this->db->where('tbl_content_contact.is_delete', 'active');
  }
}
