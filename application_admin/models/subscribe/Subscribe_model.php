<?php
class Subscribe_model extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function get_data($subscribe_id){
    $this->db->where('subscribe_id', $subscribe_id);
    $query = $this->db->get('tbl_subscribe');
    return ($query->num_rows() > 0)?$query->row():false;
  }

  public function insert($data){
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', (isset($data['create_by']))?$data['create_by']:$this->session->userdata('admin_id'));
    $this->db->insert('tbl_subscribe');
    $subscribe_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('subscribe',$subscribe_id,'insert', 'create new subscribe', $data, $this->db->last_query());
    return $subscribe_id;
  }

  public function update($subscribe_id, $data){
    $this->set_to_db($data);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('subscribe_id'));
    $this->db->where('subscribe_id', $subscribe_id);
    $this->db->update('tbl_subscribe');
    $this->Log_action_model->insert_log('subscribe',$subscribe_id,'update', 'update subscribe', $data, $this->db->last_query());
  }

  public function delete($subscribe_id){
    $this->db->set('is_delete', 'delete');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('subscribe_id', $subscribe_id);
    $this->db->update('tbl_subscribe');
    $this->Log_action_model->insert_log('subscribe',$subscribe_id,'delete', 'delete subscribe', array(), $this->db->last_query());
  }
  private function set_to_db($data){
    $this->db->set('subscribe_email', $data['subscribe_email']);
    $this->db->set('subscribe_status', $data['subscribe_status']);

  }
}
