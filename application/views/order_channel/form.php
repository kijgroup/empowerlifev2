<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>การจัดการ</span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open('order_channel/form_post/'.$order_channel_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'order_channel-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="order_channel_name" class="col-md-3 control-label">ช่องทางการสั่งซื้อ <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="order_channel_name" id="order_channel_name" value="<?php echo($order_channel_id !=0)? $order_channel->order_channel_name:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="sort_priority" class="col-md-3 control-label">แสดงลำดับที่</label>
      <div class="col-md-6">
        <select name="sort_priority" id="sort_priority" class="form-control">
          <?php
          $max_priority = $this->Order_channel_model->get_max_priority();
          $max_priority += ($order_channel_id == 0)?1:0;
          for($i = 1; $i<=$max_priority; $i++){
            $checked = ($order_channel_id != 0 && $order_channel->sort_priority == $i)?'selected="selected"':'';
            echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
<?php echo form_close(); ?>
