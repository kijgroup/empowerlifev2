<div class="sidebar">
  <div class="shopping-result">
    <h3 class="heading-helper heading-helper--large shopping-result__heading" style="color:#fff;"><?php echo $this->lang->line('checkout_content_step3_cart_total'); ?></h3>
    <table class="table--short">
      <colgroup class="col-half col--dark">
      </colgroup>
      <colgroup class="col-half col--light">
      </colgroup>
      <tbody>
        <tr>
          <td style="color:#fff;padding:8px 10px;"><?php echo $this->lang->line('checkout_content_step3_cart_subtotal'); ?></td>
          <td class="checkout__result text-right" style="color:#fff;"><?php echo number_format($this->Cart_model->get_items_price(), 2); ?> ฿</td>
        </tr>
        <tr>
          <td style="color:#fff;padding:8px 10px;"><?php echo $this->lang->line('checkout_content_step3_discount_point'); ?></td>
          <td class="checkout__result text-right" style="color:#fff;">- <span class="discount_by_point_amount">0</span>.00 ฿</td>
        </tr>
        <tr>
          <td style="color:#fff;padding:8px 10px;"><?php echo $this->lang->line('checkout_content_step3_discount'); ?></td>
          <td class="checkout__result text-right" style="color:#fff;">- <span class="member_discount"><?php echo number_format($this->Cart_model->get_discount_rate(), 2); ?></span> ฿</td>
        </tr>
        <tr>
          <td style="color: #fff;padding:8px 10px;"><?php echo $this->lang->line('checkout_content_step3_shipping'); ?></td>
          <td class="checkout__result text-right" style="color: #fff;"><?php echo  number_format($this->Cart_model->get_shipping_amount(), 2); ?> ฿</td>
        </tr>
      </tbody>
    </table>
    <div class="checkout__sum" style="color:#fff;"><span class="paid_amount"><?php echo number_format($this->Cart_model->get_price(), 2); ?></span> ฿</div>
  </div>
</div>
