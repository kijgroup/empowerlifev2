<?php echo form_open('checkout/form_post'); ?>
<div class="panel-group accordion accordion--blocks" id="accordion">
  <?php
  $this->load->view('checkout/index/content/step1');
  $this->load->view('checkout/index/content/step2');
  $this->load->view('checkout/index/content/step3');
  ?>
</div>
<?php echo form_close(); ?>
