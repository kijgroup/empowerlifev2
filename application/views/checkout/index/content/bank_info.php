<table style="width:100%;">
  <?php
  $bank_list = $this->Bank_model->get_list();
  foreach($bank_list->result() as $bank){
    ?>
    <tr>
      <td width="100" style="padding:5px 10px;"><label for="bank_<?php echo $bank->bank_id; ?>" style="padding-left:0;"><img src="<?php echo base_url('uploads/'.$bank->bank_name_icon); ?>" /></label></td>
      <td>
        <label for="bank_<?php echo $bank->bank_id; ?>" style="padding-left:0;">
          <div style="font-size:20px;"><?php echo ($this->config->item('language_abbr') == 'th')?$bank->bank_name_th:$bank->bank_name_en; ?></div>
          <div><?php echo ($this->config->item('language_abbr') == 'th')?$bank->bank_info_th:$bank->bank_info_en; ?></div>
        </label>
      </td>
    </tr>
    <?php
  }
  ?>
</table>
