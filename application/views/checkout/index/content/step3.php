<div class="panel panel-default page3">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="collapsed accordion-link" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
        <span class="marker">
          <span class="marker__close"><i class="fa fa-plus"></i></span>
          <span class="marker__open"><i class="fa fa-minus"></i></span>
        </span>
        <?php echo $this->lang->line('checkout_content_step3_order_product'); ?>
        <span class="step__number">3</span>
      </a>
    </h4>
  </div>
  <div id="collapseThree" class="panel-collapse collapse">
    <div class="panel-body">
      <div class="table-responsive">
        <table class="table table-bordered table--wide table--product">
          <colgroup class="col-third">
          <colgroup class="col-thin">
          <colgroup class="col-thin">
          <colgroup class="col-thin">
          <thead>
            <tr>
              <th><?php echo $this->lang->line('checkout_content_step3_product'); ?></th>
              <th><?php echo $this->lang->line('checkout_content_step3_product_price'); ?></th>
              <th><?php echo $this->lang->line('checkout_content_step3_product_unit'); ?></th>
              <th><?php echo $this->lang->line('checkout_content_step3_product_total'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php
            $cart_items = $this->Cart_model->get_list();
            $total_items_price = 0;
            foreach($cart_items as $cart_item){
              $cart_product = $this->Product_model->get_data($cart_item['product_id']);
              if($cart_product){
                $product_name = $this->Product_model->return_name_lang($cart_product, $this->config->item('language_abbr'));
                $product_image_url = base_url('uploads/'.$cart_product->thumb_image);
                $product_image = '<img alt="" src="'.$product_image_url.'" class="product__thumb" style="width:50px;" />';
                $total_item_price = $cart_product->price * $cart_item['quantity'];
                $total_items_price += $total_item_price;
                ?>
                <tr>
                  <td class="table__item">
                    <div>
                      <?php
                      echo $product_image;
                      echo '<p class="product__shortname">'.$product_name.'</p>';
                      ?>
                    </div>
                  </td>
                  <td><?php echo number_format($cart_product->price, 2); ?> ฿</td>
                  <td><?php echo $cart_item['quantity']; ?></td>
                  <td class="text-right"><?php echo number_format($total_item_price, 2); ?> ฿</td>
                </tr>
                <?php
              }
            }
            $discount_rate = 0;
            if($total_items_price >= $member_type->min_amount){
              $discount_rate = $member_type->discount;
            }
            $discount_amount = $total_items_price * $discount_rate / 100;
            ?>
            <tr>
              <td class="contact" colspan="3" style="text-align: left;">
                <style>
                select:focus{outline:0;}
                </style>
                <table>
                  <thead>
                    <tr>
                      <th>คะแนนแลกส่วนลด</th>
                      <th>จำนวนครั้ง</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  $discount_point_list = $this->Discount_point_model->get_list();
                  foreach($discount_point_list->result() as $discount_point){
                    $input_id = 'discount_point_'.$discount_point->discount_point_id;
                    echo '<tr>
                      <td>'.$discount_point->use_point.' คะแนนแลก '.$discount_point->discount_amount.' บาท</td>
                      <td><input type="number" class="contact__field text-right discount_point" name="'.$input_id.'" id="'.$input_id.'" data-use_point="'.$discount_point->use_point.'" data-discount_amount="'.$discount_point->discount_amount.'"  value="0" /></td>
                    </tr>';
                    ?>
                    <?php
                  }
                  ?>
                  </tbody>
                </table>
                <p>รวม <span class="discount_point_amount">0</span> คะแนนแลกส่วนลด <span class="discount_by_point_amount">0</span> บาท</p>
              </td>
              <td class="text-right">- <span class="discount_by_point_amount" id="discount_by_point_amount">0</span>.00 ฿</td>
            </tr>
            <tr>
              <td class="checkout__result" colspan="2"><?php echo $this->lang->line('checkout_content_step3_discount'); ?></td>
              <td><?php echo number_format($discount_rate, 2); ?> %
              </td>
              <td class="text-right">- <span class="member_discount"><?php echo number_format($discount_amount, 2); ?></span> ฿</td>
            </tr>
            <tr>
              <td class="checkout__result" colspan="3">
                <?php echo $this->lang->line('checkout_content_step3_shipping'); ?>
              </td>
              <td class="text-right">
              <span id="shipping_amount"><?php echo number_format($this->Cart_model->get_shipping_amount(), 2); ?></span> ฿</td>
            </tr>
          </tbody>
        </table>
      </div>
      <table class="table-info">
        <colgroup class="col-section-sm">
        <colgroup class="col-section-lg">
        <tr>
          <td class="checkout__result"><?php echo $this->lang->line('checkout_content_step3_order_total'); ?></td>
          <td class="checkout__total"><span id="order_total" class="paid_amount" data-total_price="<?php echo $this->Cart_model->get_price(); ?>"><?php echo number_format($this->Cart_model->get_price(), 2); ?></span> ฿</td>
        </tr>
      </table>
      <div class="order-btn">
        <button type="submit" class="btn btn-info btn--decorated pull-right check__btn"><i class='icon-cart'></i><?php echo $this->lang->line('checkout_content_order'); ?></button>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="item_price" value="<?php echo $this->Cart_model->get_items_price(); ?>" />
<input type="hidden" id="discount_rate" value="<?php echo $discount_rate; ?>" />
<input type="hidden" id="shipping_price" value="<?php echo $this->Cart_model->get_shipping_amount(); ?>" />
