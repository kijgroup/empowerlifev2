<div class="panel panel-default page2">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="collapsed accordion-link" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
        <span class="marker">
          <span class="marker__close"><i class="fa fa-plus"></i></span>
          <span class="marker__open"><i class="fa fa-minus"></i></span>
        </span>
        <?php echo $this->lang->line('checkout_content_step2_payment'); ?>
        <span class="step__number">2</span>
      </a>
    </h4>
  </div>
  <div id="collapseTwo" class="panel-collapse collapse">
    <div class="panel-body">
      <div class="form-control">
        <div class="radio" style="margin-top:0;">
          <input id="payment-bank" type="radio" name="payment_type" value="bank">
          <label for="payment-bank"><?php echo $this->lang->line('checkout_content_step2_payment_bank'); ?></label>
          <div style="text-align:left;padding:0;" id="bank-info">
            <?php $this->load->view('checkout/index/content/bank_info'); ?>
          </div>
          <input id="payment-post" type="radio" name="payment_type" value="post">
          <label for="payment-post"><?php echo $this->lang->line('checkout_content_step2_payment_post'); ?></label>
          <div style="text-align:left;padding:0;" id="post-info" class="contact contact--check">
            <div class="row">
              <div class="col-md-6">
                <input type="text" class="contact__field" name="postoffice" id="postoffice" placeholder="ชื่อสาขาไปรษณีย์" />
              </div>
            </div>
          </div>
          <input id="payment-home" type="radio" name="payment_type" value="home">
          <label for="payment-home"><?php echo $this->lang->line('checkout_content_step2_payment_home'); ?></label>
        </div>
      </div>
      <a class="link link--check checkout-next" data-page="page3" href="#"><i class="fa fa-check-circle"></i><?php echo $this->lang->line('checkout_content_next'); ?></a>
    </div>
  </div>
</div>
