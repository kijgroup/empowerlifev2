<div class="panel panel-default">
  <div class="panel-heading">
    <h4 class="panel-title">
      <a class="accordion-link" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
        <span class="marker">
          <span class="marker__close"><i class="fa fa-plus"></i></span>
          <span class="marker__open"><i class="fa fa-minus"></i></span>
        </span>
        <?php echo $this->lang->line('checkout_content_step1_address'); ?> &amp; <?php echo $this->lang->line('checkout_content_step1_contact'); ?>
        <span class="step__number">1</span>
      </a>
    </h4>
  </div>
  <div id="collapseOne" class="panel-collapse collapse in">
    <div class="panel-body">
      <div class="contact contact--check">
        <h5 style="margin-top:0;"><?php echo $this->lang->line('checkout_content_step1_address_product'); ?></h5>
        <input class="contact__field" name="shipping_name" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_shipping_name'); ?>" value="<?php echo $member->member_firstname.' '.$member->member_lastname; ?>">
        <input class="contact__field" name="shipping_mobile" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_shipping_mobile'); ?>" value="<?php echo $member->member_phone; ?>">
        <input class="contact__field" name="shipping_address" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_shipping_address'); ?>" value="<?php echo $member->member_address; ?>">
        <div class="row">
          <div class="col-md-6">
            <input class="contact__field" name="shipping_district" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_shipping_district'); ?>" value="<?php echo $member->member_district; ?>">
          </div>
          <div class="col-md-6">
            <input class="contact__field" name="shipping_amphur" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_shipping_amphur'); ?>" value="<?php echo $member->member_amphur; ?>">
          </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <input class="contact__field" name="shipping_province" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_shipping_province'); ?>" value="<?php echo $member->member_province; ?>">
          </div>
          <div class="col-md-6">
            <input class="contact__field" name="shipping_post_code" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_shipping_post_code'); ?>" value="<?php echo $member->member_postcode; ?>">
          </div>
        </div>
        <div class="checkbox text-left">
          <input type="checkbox" id="is_tax" name="is_tax" value="true" />
          <label for="is_tax"> <?php echo $this->lang->line('checkout_content_step1_is_tax'); ?></label>
        </div>
        <div id="display-tax">
          <h5><?php echo $this->lang->line('checkout_content_step1_address_tax'); ?></h5>
          <div class="checkbox text-left">
            <input type="checkbox" id="same_address" name="same_address" value="true" checked="checked" />
            <label for="same_address"> <?php echo $this->lang->line('checkout_content_step1_same_address'); ?></label>
          </div>
          <input class="contact__field" name="tax_name" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_tax_name'); ?>">
          <div id="display-tax-address" style="display:none;">
            <input class="contact__field" name="tax_mobile" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_tax_mobile'); ?>">
            <input class="contact__field" name="tax_address" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_tax_address'); ?>">
            <div class="row">
              <div class="col-md-6">
                <input class="contact__field" name="tax_district" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_shipping_district'); ?>">
              </div>
              <div class="col-md-6">
                <input class="contact__field" name="tax_amphur" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_shipping_amphur'); ?>">
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <input class="contact__field" name="tax_province" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_tax_province'); ?>">
              </div>
              <div class="col-md-6">
                <input class="contact__field" name="tax_post_code" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_tax_post_code'); ?>">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <input class="contact__field" name="tax_code" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_tax_code'); ?>">
            </div>
            <div class="col-md-6">
              <input class="contact__field" name="tax_branch" type="text" placeholder="<?php echo $this->lang->line('checkout_content_step1_tax_branch'); ?>">
            </div>
          </div>
        </div>
      </div>
      <a class="link link--check checkout-next" data-page="page2" href="#"><i class="fa fa-check-circle"></i><?php echo $this->lang->line('checkout_content_next'); ?></a>
    </div>
  </div>
</div>
