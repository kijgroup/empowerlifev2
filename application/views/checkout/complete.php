<section class="page-indecator">
  <div class="container">
  <h2 class="heading"><?php echo $this->lang->line('checkout_content_complete_checkout_complete'); ?></h2>
  <!-- Breadcrumb pattern -->
  <ol class="breadcrumb">
    <li><a href="index.html"><?php echo $this->lang->line('checkout_index_home'); ?></a></li>
    <li class="active"><?php echo $this->lang->line('checkout_index_checkout'); ?></li>
    <li class="active"><?php echo $this->lang->line('checkout_content_complete'); ?></li>
  </ol>
  <!-- Default one color devider -->
  <div class="devider devider--bottom-md"></div>
  </div>
</section>
<section class="container">
  <div class="promo promo--border promo-present">
    <h3 class="promo__heading"><?php echo $this->lang->line('checkout_content_complete_checkout_complete'); ?></h3>
    <p><?php echo $this->lang->line('checkout_content_complete_order_number'); ?><b><?php echo $order_code; ?></b></p>
    <?php echo anchor('myorder/detail/'.$order_code, $this->lang->line('checkout_content_complete_order_status_here'), array('class' => 'btn btn-warning btn--decorated promo__btn')); ?>
  </div>
</section>
