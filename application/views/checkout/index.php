<section class="page-indecator">
  <div class="container">
  <h2 class="heading"><?php echo $this->lang->line('checkout_index_checkout'); ?></h2>
  <!-- Breadcrumb pattern -->
  <ol class="breadcrumb">
    <li><a href="index.html"><?php echo $this->lang->line('checkout_index_home'); ?></a></li>
    <li class="active"><?php echo $this->lang->line('checkout_index_checkout'); ?></li>
  </ol>
  <!-- Default one color devider -->
  <div class="devider devider--bottom-md"></div>
  </div>
</section>
<section class="container">
  <div class="row">
    <div class="col-sm-8 cut-section">
      <?php $this->load->view('checkout/index/content'); ?>
    </div>
    <aside class="col-sm-4">
      <?php $this->load->view('checkout/index/side'); ?>
    </aside>
  </div>
</section>
