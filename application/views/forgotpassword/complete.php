<section class="container">
  <h2 class="block-title block-title--bottom"><?php echo $this->lang->line('content_forgotpassword'); ?></h2>
  <div class="login">
    Please check your email to reset password!
  </div>
</section><!-- end container -->
