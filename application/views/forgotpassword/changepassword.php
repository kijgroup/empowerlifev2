<section class="container">
  <h2 class="block-title block-title--bottom"><?php echo $this->lang->line('content_forgotpassword'); ?></h2>
  <div class="login">
    <div class="alert alert-danger" id="alert_login" style="display:none;">
      <span class="alert-market">
        <i class="fa fa-ban"></i>
      </span>
      <span id="alert_login_msg"></span>
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
    </div>
    <?php echo form_open('forgotpassword/changepassword_post/'.$hash_code,array('class'=>'contact', 'id'=>'login-form')); ?>
    <input type="password" class="contact__field" name="password" id="password" placeholder="new password" />
    <input type="password" class="contact__field" name="confirm_password" id="confirm_password" placeholder="confirm new password" />
    <button class="btn btn--decorated btn-warning login__btn" type="submit">SUBMIT</button>
    <?php echo form_close(); ?>
  </div>
</section><!-- end container -->
