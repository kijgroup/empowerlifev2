<section class="container">
  <h2 class="block-title block-title--bottom"><?php echo $this->lang->line('content_forgotpassword'); ?></h2>
  <div class="login">
    <div class="alert alert-danger" id="alert_login" style="display:none;">
      <span class="alert-market">
        <i class="fa fa-ban"></i>
      </span>
      <span id="alert_login_msg"></span>
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
    </div>
    <?php echo form_open('forgotpassword/form_post',array('class'=>'contact', 'id'=>'login-form')); ?>
      <input class="contact__field" id="member_email" name="member_email" type="email" placeholder="<?php echo $this->lang->line('content_forgotpassword_email'); ?>">
      <button class="btn btn--decorated btn-warning login__btn" type="submit"><?php echo $this->lang->line('content_forgotpassword'); ?></button>
      <div>
        <?php echo anchor('login', $this->lang->line('content_forgotpassword_login')); ?>
        <div style="clear:both;"></div>
      </div>
    <?php echo form_close(); ?>
  </div>
  <div class="btn-wrapper">
    <h3 class="heading-helper heading-helper--bottom"><?php echo $this->lang->line('content_login_social'); ?></h3>
    <?php echo anchor('hauth/login/Facebook','<i class="fa fa-facebook"></i>Facebook',array('class' =>'btn btn--decorated btn--facebook')); ?>
    <?php echo anchor('hauth/login/Google','<i class="fa fa-google-plus"></i>Google',array('class' =>'btn btn--decorated btn--google')); ?>
  </div>
</section><!-- end container -->
