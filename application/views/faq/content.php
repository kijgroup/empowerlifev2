<div class="panel-group accordion accordion--blocks" id="accordion">
  <?php foreach($faq_list->result() as $faq){ ?>
  <!-- Accordion panel -->
  <div class="panel panel-default">
  	<!-- accordion panel heading -->
    <div class="panel-heading">
      <h4 class="panel-title">
        <a class="accordion-link" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $faq->faq_id; ?>">
          <span class="marker">
            <span class="marker__close"><i class="fa fa-plus"></i></span>
            <span class="marker__open"><i class="fa fa-minus"></i></span>
          </span>
          <?php echo $faq->faq_question_th; ?>
        </a>
      </h4>
    </div><!-- end accordion panel heading -->
    <!-- accordion panel body -->
    <div id="collapse<?php echo $faq->faq_id; ?>" class="panel-collapse collapse in">
      <div class="panel-body">
      <?php echo $faq->faq_answer_th; ?>
      </div>
    </div><!-- end accordion panel body -->
  </div><!-- end accordion panel -->
  <?php } ?>
</div>
