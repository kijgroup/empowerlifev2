<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>FAQ</span></li>
    <li><span><?php echo anchor('faq','ถาม-ตอบ'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open('faq/form_post/'.$faq_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'faq-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="faq_group_id" class="col-md-3 control-label">หมวดคำถาม <span class="required">*</span></label>
      <div class="col-md-6">
        <select class="form-control" name="faq_group_id" id="faq_group_id">
          <?php
          $faq_group_list = $this->Faq_group_model->get_list(false);
          foreach($faq_group_list->result() as $faq_group){
            $selected = ($faq_id != 0 && $faq_group->faq_group_id == $faq->faq_group_id)?'selected="selected"':'';
            echo '<option value="'.$faq_group->faq_group_id.'" '.$selected.'>'.$faq_group->faq_group_th.'</option>';
          }
          ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="faq_group_th" class="col-md-3 control-label">คำถาม (th) <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="form-control" name="faq_question_th" id="faq_question_th" rows="3" required><?php echo ($faq_id != 0)?$faq->faq_question_th:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="faq_question_en" class="col-md-3 control-label">คำถาม (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="form-control" name="faq_question_en" id="faq_question_en" rows="3" required><?php echo ($faq_id != 0)?$faq->faq_question_en:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="faq_answer_th" class="col-md-3 control-label">คำตอบ (th) <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="summernote" name="faq_answer_th" id="faq_answer_th" data-upload="<?php echo site_url('faq/upload_image'); ?>" ><?php echo ($faq_id != 0)?$faq->faq_answer_th:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="faq_answer_en" class="col-md-3 control-label">คำตอบ (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="summernote" name="faq_answer_en" id="faq_answer_en" data-upload="<?php echo site_url('faq/upload_image'); ?>"><?php echo ($faq_id != 0)?$faq->faq_answer_en:''; ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ <span class="required">*</span></label>
      <div class="col-md-6">
        <label class="radio-inline">
          <input type="radio" name="enable_status" id="enable_status_show" value="show" <?php echo ($faq_id == 0 OR $faq->enable_status == 'show')?'checked':'' ?>>
          <span class="label label-success">แสดง</span>
        </label>
        <label class="radio-inline">
          <input type="radio" name="enable_status" id="enable_status_hide" value="hide" <?php echo ($faq_id != 0 && $faq->enable_status == 'hide')?'checked':'' ?>>
          <span class="label label-danger">ซ่อน</span>
        </label>
      </div>
    </div>
    <div class="form-group">
      <label for="sort_priority" class="col-md-3 control-label">แสดงลำดับที่</label>
      <div class="col-md-6">
        <select name="sort_priority" id="sort_priority" class="form-control">
          <?php
          $max_priority = $this->Faq_model->get_max_priority();
          $max_priority += ($faq_id == 0)?1:0;
          for($i = 1; $i<=$max_priority; $i++){
            $checked = ($faq_id != 0 && $faq->sort_priority == $i)?'selected="selected"':'';
            echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
