<header class="page-header">
<h2>FAQ</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>ถาม-ตอบ</span></li>
  </ol>
</div>
</header>
<div class="search-content">
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('faq/form', '<i class="fa fa-plus"></i> เพิ่มถาม-ตอบ', array('class'=>'btn btn-success')); ?>
    </div>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
            <tr>
              <th>คำถาม (th)</th>
              <th>คำถาม (en)</th>
              <th>หมวดคำถาม (th)</th>
              <th>สถานะ</th>
            </tr>
        </thead>
        <tbody>
          <?php
          foreach($faq_list->result() as $faq){
            $detail_url = 'faq/detail/'.$faq->faq_id;
            $delete_url = 'faq/delete/'.$faq->faq_id;
            ?>
            <tr>
              <td><?php echo anchor($detail_url, $faq->faq_question_th); ?></td>
              <td><?php echo anchor($detail_url, $faq->faq_question_en); ?></td>
              <td><?php echo $this->Faq_group_model->get_name($faq->faq_group_id); ?></td>
              <td>
                <?php echo ($faq->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ไม่แสดง</span>'; ?>
              </td>
              <td>
                <?php echo anchor($delete_url, '<i class="fa fa-trash"></i> ลบข้อมูล', array('class'=>'btn btn-sm btn-danger')); ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <br />
  </div>
</div>
