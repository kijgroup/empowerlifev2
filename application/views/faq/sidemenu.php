<div class="sidebar">
  <h2 class="heading heading--section heading--first">FAQ GROUP</h2>
  <ul class="list list--marker">
    <?php
    $faq_group_list = $this->Faq_group_model->get_list();
    foreach($faq_group_list->result() as $faq_group){
      echo '<li class="list__item">'.anchor('faq/index/'.$faq_group->faq_group_id, $this->Faq_group_model->return_name_lang($faq_group, $this->config->item('language_abbr'))).'</li>';
    }
    ?>
  </ul>
</div>
