<header class="page-header">
<h2>ป๊อปอัพ</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li>
      <a href="index.html">
        <i class="fa fa-home"></i>
      </a>
    </li>
    <li><span>การจัดการ</span></li>
    <li><span>ป๊อปอัพ</span></li>
  </ol>
</div>
</header>
<?php echo form_open_multipart('popup/form_post/', array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'popup-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลป็อปอัพ</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="popup_image_th" class="col-md-3 control-label">รูป popup(TH) <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        $popup_image_th = $this->Config_model->get_value('popup_image_th');
        if($popup_image_th != ''){
          echo '<p style="max-width:200px;"><img src="'.base_url('uploads/'.$popup_image_th).'" class="img-responsive" ></p>';
        }
        ?>
        <input type="file" class="form-control" name="popup_image_th">
      </div>
    </div>
      <div class="form-group">
        <label for="popup_image_en" class="col-md-3 control-label">รูป popup(EN) <span class="required">*</span></label>
        <div class="col-md-6">
          <?php
          $popup_image_en = $this->Config_model->get_value('popup_image_en');
          if($popup_image_en != ''){
            echo '<p style="max-width:200px;"><img src="'.base_url('uploads/'.$popup_image_en).'" class="img-responsive"></p>';
          }
          ?>
            <input type="file" class="form-control" name="popup_image_en">
        </div>
      </div>
    <div class="form-group">
      <label for="popup_url" class="col-md-3 control-label">url</label>
      <div class="col-md-6">
          <input type="text" class="form-control" name="popup_url" id="popup_url" value="<?php echo $this->Config_model->get_value('popup_url'); ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ <span class="required">*</span></label>
      <div class="col-md-6">
        <div class="radio-inline">
          <label>
            <input type="radio" name="popup_status" id="popup_status_show" value="show" <?php echo ($this->Config_model->get_value('popup_status') == 'show')?'checked=""':''; ?>>
            แสดง
          </label>
        </div>
        <div class="radio-inline">
          <label>
            <input type="radio" name="popup_status" id="popup_status_hide" value="hide" <?php echo ($this->Config_model->get_value('popup_status') == 'hide')?'checked=""':''; ?>>
            ซ่อน
          </label>
        </div>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
