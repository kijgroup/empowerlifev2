<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>หน้าเวบ</span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<?php echo form_open('page/form_post/'.$page_code, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'content-form')); ?>
<div class="panel panel-default">
    <div class="panel-body">
        <div class="form-group">
            <label for="page_name_th" class="col-md-3 control-label">ข้อความภาษาไทย</label>
            <div class="col-md-6">
              <input type="text" class="form-control" name="page_name_th" id="page_name_th" value="<?php echo $page->page_name_th; ?>" >
            </div>
        </div>
        <div class="form-group">
            <label for="page_name_en" class="col-md-3 control-label">ข้อความภาษาอังกฤษ</label>
            <div class="col-md-6">
                <input type="text" class="form-control" name="page_name_en" id="page_name_en" placeholder="" value="<?php echo $page->page_name_en; ?>">
            </div>
        </div>
        <div class="form-group">
          <label for="page_content_th" class="col-md-3 control-label">เนื้อหา (th)</label>
          <div class="col-md-6">
            <textarea class="summernote" name="page_content_th" id="page_content_th" data-upload="<?php echo site_url('page/upload_image'); ?>" ><?php echo $page->page_content_th; ?></textarea>
          </div>
        </div>
        <div class="form-group">
          <label for="page_content_en" class="col-md-3 control-label">เนื้อหา (en)</label>
          <div class="col-md-6">
            <textarea class="summernote" name="page_content_en" id="page_content_en" data-upload="<?php echo site_url('page/upload_image'); ?>" ><?php echo $page->page_content_en; ?></textarea>
          </div>
        </div>

        <div class="form-group">
            <div class="col-md-offset-3 col-md-6">
                <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
