<?php
if($page->page_code=='oem'){
  $this->load->view('page/slideshow');
}
?>
<style>
section.container ol, section.container ul{
  color:#737c85;
}
</style>
<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $page->page_name_th; ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $page->page_name_th; ?></li>
    </ol>
    <div class="devider devider--bottom-xs"></div>
  </div>
</section>
<section class="container">
  <?php echo $page->page_content_th; ?>
</section>
