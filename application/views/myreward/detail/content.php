<div>
  <table class="table table--vertical table--space">
    <colgroup class="col-width-one"></colgroup>
    <colgroup class="col-width-two"></colgroup>
    <tbody>
      <tr>
        <td class="vertical-heading"><?php echo $this->lang->line('myreward_content_status'); ?></td>
        <td><?php echo $reward_transaction->reward_transaction_status; ?></td>
      </tr>
      <tr>
        <td class="vertical-heading"><?php echo $this->lang->line('myreward_content_date'); ?></td>
        <td><?php echo $this->Datetime_service->display_datetime($reward_transaction->create_date, $this->config->item('language_abbr')); ?></td>
      </tr>
    </tbody>
  </table>
</div>
<?php
$this->load->view('myreward/detail/item_list');
$this->load->view('myreward/detail/shipping');
?>
