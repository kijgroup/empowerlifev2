<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $this->lang->line('myreward_detail'); ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $this->lang->line('myreward_detail'); ?></li>
    </ol>
    <div class="devider devider--bottom-md"></div>
  </div>
</section>
<section class="container">
  <div class="row">
    <div class="col-sm-3">
      <?php $this->load->view('membership/sidemenu'); ?>
    </div>
    <div class="col-sm-9">
      <?php $this->load->view('myreward/detail/content'); ?>
    </div>
  </div>
</section>
