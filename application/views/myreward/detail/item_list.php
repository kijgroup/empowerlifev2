<div class="table-responsive">
  <table class="table table-bordered table--wide table-present">
    <colgroup class="col-third"></colgroup>
    <colgroup class="col-thin"></colgroup>
    <colgroup class="col-thin"></colgroup>
    <colgroup class="col-thin"></colgroup>
    <thead>
      <tr>
        <th><?php echo $this->lang->line('myreward_detail_reward'); ?></th>
        <th><?php echo $this->lang->line('myreward_detail_reward_point'); ?></th>
        <th><?php echo $this->lang->line('myreward_detail_reward_qty'); ?></th>
        <th><?php echo $this->lang->line('myreward_detail_reward_total'); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($reward_transaction_item_list->result() as $reward_transaction_item){
        $reward = $this->Reward_model->get_data($reward_transaction_item->reward_id);
        $thumb_image = '';
        if($reward && $reward->thumb_image != ''){
          $thumb_image = base_url('uploads/'.$reward->thumb_image);
        }else{
          $thumb_image = 'http://placehold.it/600x600';
        }
        ?>
        <tr>
          <td class="table__item">
            <img src="<?php echo $thumb_image; ?>" alt="" style="max-width:50px;" class="product__thumb" />
            <p class="product__shortname"><?php echo $this->Reward_transaction_item_model->return_name_lang($reward_transaction_item, $this->config->item('language_abbr')); ?></p>
          </td>
          <td class="text-right"><?php echo number_format($reward_transaction_item->reward_point); ?> P</td>
          <td class="text-right"><?php echo number_format($reward_transaction_item->qty); ?></td>
          <td class="text-right"><?php echo number_format($reward_transaction_item->total_reward_point); ?> P</td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
  <table class="table-info">
    <colgroup class="col-section-sm"></colgroup>
    <colgroup class="col-section-lg"></colgroup>
    <tbody>
      <tr>
        <td class="checkout__result"><?php echo $this->lang->line('myreward_detail_reward_grand_total'); ?></td>
        <td class="checkout__total"><?php echo number_format($reward_transaction->total_reward_point); ?> P</td>
      </tr>
    </tbody>
  </table>
</div>
