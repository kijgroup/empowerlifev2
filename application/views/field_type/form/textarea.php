<div class="form-group">
  <label for="<?php echo $field_key; ?>" class="col-md-3 control-label"><?php
  echo $field_name;
  echo ($required)?' <span class="required" aria-required="true">*</span>':'';
  ?></label>
  <div class="col-md-6">
    <textarea
      class="form-control"
      name="<?php echo $field_key; ?>"
      id="<?php echo $field_key; ?>"
      placeholder="<?php echo $placeholder; ?>"
      <?php echo ($required)?'required="required"':''; ?>
      data-plugin-textarea-autosize
      ><?php echo $value; ?></textarea>
    <?php echo ($help_text != '')?'<span class="help-block">'.$help_text.'</span>':''; ?>
  </div>
</div>
