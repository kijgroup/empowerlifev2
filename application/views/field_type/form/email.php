<div class="form-group">
  <label for="<?php echo $field_key; ?>" class="col-md-3 control-label"><?php
  echo $field_name;
  echo ($required)?' <span class="required" aria-required="true">*</span>':'';
  ?></label>
  <div class="col-md-6">
    <input type="email" class="form-control" name="<?php echo $field_key; ?>" id="<?php echo $field_key; ?>" value="<?php echo $value; ?>" placeholder="<?php echo $placeholder; ?>" <?php echo ($required)?'required="required"':''; ?> />
    <?php echo ($help_text != '')?'<span class="help-block">'.$help_text.'</span>':''; ?>
  </div>
</div>
