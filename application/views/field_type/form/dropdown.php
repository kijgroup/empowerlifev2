<div class="form-group">
  <label for="<?php echo $field_key; ?>" class="col-md-3 control-label"><?php
  echo $field_name;
  echo ($required)?' <span class="required" aria-required="true">*</span>':'';
  ?></label>
  <div class="col-md-6">
    <select class="form-control" name="<?php echo $field_key; ?>" id="<?php echo $field_key; ?>">
      <?php
      if($source_type == 'enum'){
        $this->load->view('field_type/form/dropdown_enum');
      }else{
        $this->load->view('field_type/form/dropdown_database');
      }
      ?>
    </select>
    <?php echo ($help_text != '')?'<span class="help-block">'.$help_text.'</span>':''; ?>
  </div>
</div>
