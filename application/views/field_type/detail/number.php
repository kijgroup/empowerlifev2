<div class="form-group">
  <label class="col-md-3 control-label"><?php echo $field_name; ?></label>
  <div class="col-md-6">
    <p class="form-control-static"><?php echo number_format($value); ?></p>
  </div>
</div>
