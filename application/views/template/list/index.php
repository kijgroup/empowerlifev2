<?php
$this->load->view('masterpage/component/header', array('title'=>$this->page_name));
?>
<div class="search-content">
  <?php
  $this->load->view('template/list/search');
  ?>
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor($this->controller.'/form', '<i class="fa fa-plus"></i> เพิ่มรายการ'.$this->page_name, array('class'=>'btn btn-success')); ?>
    </div>
    <div id="result">
    </div>
    <br />
  </div>
</div>
