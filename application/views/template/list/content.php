<?php
if($total_transaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $total_page = ceil($total_transaction/$per_page);
  $pager_data = array(
    'total_transaction' => $total_transaction,
    'search_val' => $search_val,
    'per_page' => $per_page,
    'page' => $page,
    'total_page' => $total_page,
    'action' => site_url($this->controller.'/filter')
  );
?>
<div class="form-group">
  <?php $this->load->view('template/list/pager', $pager_data); ?>
</div>
<div class="table-responsive">
  <table class="table table-no-more table-bordered table-striped">
    <thead>
      <tr>
        <?php
        foreach($this->list_display_structure as $structure_item){
          echo '<th>'.$structure_item['field_name'].'</th>';
        }
        echo '<th>วันที่ทำรายการ</th>';
        ?>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($main_data_list->result_array() as $main_data){
        $detail_url = $this->controller.'/detail/'.$main_data[$this->primary_key];
        ?>
        <tr>
          <?php
          foreach($this->list_display_structure as $structure_item){
            $field_value = $this->Field_type_model->display_value($structure_item, $main_data[$structure_item['field_key']]);
            $display_td = ($field_value != '')?anchor($detail_url, $field_value):'';
            echo '<td data-title="'.$structure_item['field_name'].'">'.$display_td.'</td>';
          }
          ?>
          <td data-title="วันที่ทำรายการ">
            <?php echo $main_data['create_date']; ?>
            <div>โดย : <?php echo $this->Admin_model->get_login_name_by_id($main_data['create_by']); ?></div>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
  <?php $this->load->view('template/list/pager', $pager_data); ?>
</div>
<?php
}
