<div class="search-control-wrapper">
  <?php echo form_open($this->controller.'/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'search-form')); ?>
  <div class="form-group">
    <input type="text" class="form-control" name="search_val" id="search_val" placeholder="ข้อความที่ต้องการค้นหา" />
  </div>
  <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> ค้นหา</button>
  <?php echo form_close(); ?>
</div>
