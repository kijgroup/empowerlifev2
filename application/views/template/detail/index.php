<?php
$this->load->view('masterpage/component/header', array('title'=>$title));
?>
<div class="pb-sm">
  <div class="btn-group hide-print">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
      การกระทำ <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
      <li><?php echo anchor($this->controller, '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
      <li><?php echo anchor($this->controller.'/form/', '<i class="glyphicon glyphicon-plus"></i> เพิ่มข้อมูล'); ?></li>
      <li><?php echo anchor($this->controller.'/form/'.$key_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
      <li class="divider"></li>
      <li><a href="#" data-toggle="modal" data-target="#delete-modal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
    </ul>
  </div>
</div>
<div class="form-horizontal">
  <?php
  foreach($this->display_form as $display_form_item){
    ?>
    <div class="panel">
      <div class="panel-heading"><?php echo $display_form_item['panel_name'] ?></div>
      <div class="panel-body">
        <?php
        foreach($display_form_item['item_list'] as $display_form_item_name){
          if($display_form_item_name == 'code'){
            $this->Field_type_model->display_detail(array(
              'field_name' => 'รหัสรายการ',
              'field_type' => 'text',
            ), $main_data['code']);
          }elseif($display_form_item_name == 'create_date'){
            $this->Field_type_model->display_detail(array(
              'field_name' => 'ทำรายการวันที่',
              'field_type' => 'text',
            ), $this->Datetime_service->display_datetime($main_data['create_date']).' โดย '.$this->Admin_model->get_login_name_by_id($main_data['create_by']));
          }elseif($display_form_item_name == 'update_date'){
            if($main_data['update_date'] != '0000-00-00 00:00:00'){
              $this->Field_type_model->display_detail(array(
                'field_name' => 'แก้ไขล่าสุดวันที่',
                'field_type' => 'text',
              ), $this->Datetime_service->display_datetime($main_data['update_date']).' โดย '.$this->Admin_model->get_login_name_by_id($main_data['update_by']));
            }
          }elseif($display_form_item_name == 'sort_priority'){
          }else{
            $structure_item = $this->structure[$display_form_item_name];
            $this->Field_type_model->display_detail($structure_item, $main_data[$structure_item['field_key']]);
          }
        }
        ?>
      </div>
    </div>
    <?php
  }
  ?>
</div>
<?php
$this->load->view('template/detail/delete_popup');
