<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>Slideshow</span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<div class="search-content">
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('slideshow/form/'.$page, '<i class="fa fa-plus"></i> เพิ่มรูป Slideshow', array('class'=>'btn btn-success')); ?>
    </div>
    <div class="table-responsive">
      <table class="table table-striped table-bordered table-hover">
        <thead>
          <tr>
            <th>รูป Slideshow</th>
            <th>สถานะ</th>
            <th>สร้างวันที่</th>
            <th>การกระทำ</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach($slideshow_list->result() as $slideshow){
            $form_url = 'slideshow/form/'.$page.'/'.$slideshow->slideshow_id;
            $slideshow_image = '-';
            if($slideshow->slideshow_image != ''){
              $slideshow_image = '<img src="'.base_url('uploads/'.$slideshow->slideshow_image).'" style="max-width:300px;width:100%;" />';
            }
            ?>
            <tr>
              <td>
                <?php echo anchor($form_url, $slideshow_image); ?>
              </td>
              <td>
                <?php echo $this->Enable_status_model->get_label($slideshow->enable_status); ?>
              </td>
              <td>
                <?php echo anchor($form_url, $slideshow->create_date); ?>
              </td>
              <td>
                <?php echo anchor('slideshow/delete/'.$page.'/'.$slideshow->slideshow_id, 'ลบข้อมูล', array('class' => 'btn btn-danger')); ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <br />
  </div>
</div>
