<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>Slideshow</span></li>
    <li><span><?php echo anchor('slideshow/index/'.$page,$page); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open_multipart('slideshow/form_post/'.$page.'/'.$slideshow_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'member-form')); ?>
<div class="panel panel-default">
  <div class="panel-body">
    <?php if($slideshow_id == 0){ ?>
      <div class="form-group">
        <label for="slideshow_image" class="col-md-3 control-label">รูป Slideshow <span class="required">*</span></label>
        <div class="col-md-6">
          <input type="file" class="form-control" name="slideshow_image" id="slideshow_image" required>
          <span class="help-block">2560X400px</span>
        </div>
      </div>
    <?php }else{ ?>
      <div class="form-group">
        <label class="col-md-3 control-label">รูป Slideshow <span class="required">*</span></label>
        <div class="col-md-6">
          <img src="<?php echo base_url('uploads/'.$slideshow->slideshow_image); ?>" alt="" class="img-responsive" />
        </div>
      </div>
    <?php } ?>
    <div class="form-group">
      <label for="slideshow_url" class="col-md-3 control-label">URL</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="slideshow_url" id="slideshow_url" value="<?php echo ($slideshow_id != 0)?$slideshow->slideshow_url:''; ?>" >
      </div>
    </div>
    <div class="form-group">
      <label for="member_point" class="col-md-3 control-label">ชื่อรูป</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="slideshow_text" id="slideshow_text" value="<?php echo ($slideshow_id != 0)?$slideshow->slideshow_text:''; ?>" >
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">แสดงลำดับที่</label>
      <div class="col-md-6">
        <select name="sort_priority" id="sort_priority" class="form-control">
          <?php
          $max_priority = $this->Slideshow_model->get_max_priority($page);
          $max_priority += ($slideshow_id == 0)?1:0;
          for($i = 1; $i<=$max_priority; $i++){
            $checked = ($slideshow_id != 0 && $slideshow->sort_priority == $i)?'selected="selected"':'';
            echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        $enable_status_list = $this->Enable_status_model->get_list();
        foreach($enable_status_list as $enable_status){
          $checked = (($slideshow_id != 0 && $slideshow->enable_status == $enable_status['status_code']) OR ($slideshow_id == 0 && $enable_status['status_code'] == 'show'))?'checked':'';
          echo '
          <div class="radio">
            <label>
              <input type="radio" name="enable_status" id="enable_status_'.$enable_status['status_code'].'" value="'.$enable_status['status_code'].'" '.$checked.'>
              '.$this->Enable_status_model->get_label($enable_status['status_code']).'
            </label>
          </div>';
        }
        ?>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
