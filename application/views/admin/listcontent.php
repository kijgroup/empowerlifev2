<?php
if($total_transaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $total_page = ceil($total_transaction/$per_page);
  $pager_data = array();
  $pager_data['total_transaction'] = $total_transaction;
  $pager_data['search_val'] = $search_val;
  $pager_data['enable_status'] = $enable_status;
  $pager_data['per_page'] = $per_page;
  $pager_data['page'] = $page;
  $pager_data['total_page'] = $total_page;
  $pager_data['action'] = site_url('admin/filter');
?>
  <div class="form-group">
    <?php $this->load->view('admin/listpager', $pager_data); ?>
  </div>
  <div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>ประเภทผู้เข้าระบบ</th>
        <th>Login Name</th>
        <th>ชื่อ</th>
        <th>สถานะ</th>
        <th>วันที่สร้าง</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($admin_list->result() as $admin){
        $detail_url = 'admin/detail/'.$admin->admin_id;
        ?>
        <tr>
          <td>
            <?php echo anchor($detail_url, $admin->admin_type); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, $admin->admin_username); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, ($admin->admin_name != '')?$admin->admin_name:'-'); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, ($admin->enable_status == 'show')?'ใช้งานได้':'ไม่สามารถใช้งานได้'); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, $admin->create_date); ?>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
  </div>
  <div class="form-group">
    <?php $this->load->view('admin/listpager', $pager_data); ?>
  </div>
<?php
}
