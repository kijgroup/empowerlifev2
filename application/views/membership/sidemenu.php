<div class="sidebar">
  <?php
  if($this->Login_service->get_login_status()){
  $member = $this->Member_model->get_data($this->session->userdata('member_id'));
  ?>
  <p class="promo__info">
    <?php if($member->member_type != 'normal'){ ?>
    <div><?php echo $this->lang->line('membership_profile_end_date'); ?> <?php echo $this->Datetime_service->display_date($member->expire_date, $this->config->item('language_abbr')); ?></div>
    <?php } ?>
    <div style="line-height:26px;">
      <h5 style="margin-top:0"><?php echo $member->member_firstname.' '.$member->member_lastname; ?></h5>
      <div><?php echo $this->lang->line('membership_profile_code').' '. $member->member_code; ?></div>
      <div><?php echo $this->lang->line('membership_profile_level'); ?> <?php echo $this->Member_type_model->get_name($member->member_type); ?></div>
    </div>
    <div style="line-height:2em;padding-top:10px;">
      <div><?php echo $this->lang->line('membership_profile_buy'); ?> <?php echo number_format($member->buy_total, 2).' '. $this->lang->line('membership_profile_baht'); ?></div>
      <div><?php echo $this->lang->line('membership_profile_score').' '. number_format($member->member_point).' '. $this->lang->line('membership_profile_point'); ?></div>
    </div>
    <hr />
  </p>
  <?php } ?>
  <ul class="list list--marker">
    <li class="list__item"><?php echo anchor('membership',$this->lang->line('membership_my_account_member'),array('class'=>'list__link')); ?></li>
    <li class="list__item"><?php echo anchor('myorder',$this->lang->line('membership_my_account_myorder'),array('class'=>'list__link')); ?></li>
    <li class="list__item"><?php echo anchor('reward',$this->lang->line('membership_my_account_reward'),array('class'=>'list__link')); ?></li>
    <li class="list__item"><?php echo anchor('myreward',$this->lang->line('membership_my_account_my_reward'),array('class'=>'list__link')); ?></li>
    <!--li class="list__item"><?php echo anchor('myaddress','ที่อยู่',array('class'=>'list__link')); ?></li-->
    <li class="list__item"><?php echo anchor('profile',$this->lang->line('membership_my_account_profile'),array('class'=>'list__link')); ?></li>
    <li class="list__item"><?php echo anchor('changepassword',$this->lang->line('membership_my_account_change_password'),array('class'=>'list__link')); ?></li>
  </ul>
</div>
