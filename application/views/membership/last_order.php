<h2 class="heading heading--start"><?php echo $this->lang->line('membership_last_order_list'); ?></h2>
<div class="table-responsive">
  <table class="table table-bordered table--wide table-present">
    <colgroup class="col-sm-width"></colgroup>
    <colgroup class="col-sm-width"></colgroup>
    <colgroup class="col-sm-width"></colgroup>
    <colgroup class="col-sm-width"></colgroup>
    <colgroup class="col-sm-width"></colgroup>
    <colgroup class="col-sm-width"></colgroup>
    <thead>
      <tr>
        <th><?php echo $this->lang->line('membership_order_id'); ?></th>
        <th><?php echo $this->lang->line('membership_last_order_date'); ?></th>
        <th><?php echo $this->lang->line('membership_last_order_total'); ?></th>
        <th><?php echo $this->lang->line('membership_last_order_status'); ?></th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($order_list->result() as $order){
        $this->load->view('myorder/list/item', array('order' => $order));
      }
      ?>
    </tbody>
  </table>
</div>
