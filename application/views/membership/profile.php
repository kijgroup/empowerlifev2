<div class="row">
  <!--div class="col-md-6">
    <h3 style="margin-top:0;"><?php echo $this->lang->line('membership_profile_customer'); ?></h3>
    <p class="promo__info">
      <?php if($member->member_type != 'normal'){ ?>
      <div><?php echo $this->lang->line('membership_profile_end_date'); ?> <?php echo $this->Datetime_service->display_date($member->expire_date, $this->config->item('language_abbr')); ?></div>
      <?php } ?>
      <div style="line-height:26px;">
        <div><?php echo $this->lang->line('membership_profile_name').' '.$member->member_firstname.' '.$member->member_lastname; ?></div>
        <div><?php echo $this->lang->line('membership_profile_code').' '. $member->member_code; ?></div>
        <div><?php echo $this->lang->line('membership_profile_level'); ?> <?php echo $this->Member_type_model->get_name($member->member_type); ?></div>
      </div>
      <div style="font-size:1.2em;line-height:2em;padding-top:10px;">
        <div><?php echo $this->lang->line('membership_profile_buy'); ?> <?php echo number_format($member->buy_total, 2).' '. $this->lang->line('membership_profile_baht'); ?></div>
        <div><?php echo $this->lang->line('membership_profile_score').' '. number_format($member->member_point).' '. $this->lang->line('membership_profile_point'); ?></div>
      </div>
    </p>
  </div-->
  <?php
  $current_member_type = $this->Member_type_model->get_data($member->member_type);
  $next_member_type = $this->Member_type_model->get_next_step($member->member_type);
  ?>
  <div class="col-md-6 col-md-offset-3">
    <table width="100%">
      <tr>
        <td width="40%" class="text-center">
          <img class="img-responsive" src="<?php echo base_url('assets\images\member_type\Member Card - Packshot - Normal.png'); ?>" alt="" />
        </td>
        <?php if($next_member_type){ ?>
        <td width="20%" class="text-center">
          <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ3Ny4xNzUgNDc3LjE3NSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgNDc3LjE3NSA0NzcuMTc1OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjY0cHgiIGhlaWdodD0iNjRweCI+CjxnPgoJPHBhdGggZD0iTTM2MC43MzEsMjI5LjA3NWwtMjI1LjEtMjI1LjFjLTUuMy01LjMtMTMuOC01LjMtMTkuMSwwcy01LjMsMTMuOCwwLDE5LjFsMjE1LjUsMjE1LjVsLTIxNS41LDIxNS41ICAgYy01LjMsNS4zLTUuMywxMy44LDAsMTkuMWMyLjYsMi42LDYuMSw0LDkuNSw0YzMuNCwwLDYuOS0xLjMsOS41LTRsMjI1LjEtMjI1LjFDMzY1LjkzMSwyNDIuODc1LDM2NS45MzEsMjM0LjI3NSwzNjAuNzMxLDIyOS4wNzV6ICAgIiBmaWxsPSIjOTk5OTk5Ii8+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPGc+CjwvZz4KPC9zdmc+Cg==" style="height:50px;" />
        </td>
        <td width="40%" class="text-center">
          <img class="img-responsive" src="<?php echo base_url('assets\images\member_type\Member Card - Packshot - Silver.png'); ?>" alt="" />
        </td>
        <?php } ?>
      </tr>
      <tr>
        <td class="text-center"><h4 style="margin:10px 0;"><?php echo $current_member_type->member_type_name; ?></h4></td>
        <?php if($next_member_type){ ?>
        <td></td>
        <td class="text-center"><h4 style="margin:10px 0;"><?php echo $next_member_type->member_type_name; ?></h4></td>
        <?php } ?>
      </tr>
      <tr>
        <td class="text-center"><p><?php echo $this->lang->line('membership_profile_buy_present'); ?><br /><?php echo number_format($member->buy_total, 2).' '.$this->lang->line('membership_profile_baht'); ?></p></td>
        <?php if($next_member_type){ ?>
        <td></td>
        <td class="text-center"><p><?php echo $this->lang->line('membership_profile_want_to_buy'); ?><br /><?php echo number_format($next_member_type->require_amount, 2).' '.$this->lang->line('membership_profile_baht'); ?></p></td>
        <?php } ?>
      </tr>
      <tr>
        <td class="text-center" style="font-weight:bold;color: #0b9444;"><?php echo $this->lang->line('membership_profile_discount').' '. number_format($current_member_type->discount); ?> %</td>
        <?php if($next_member_type){ ?>
        <td></td>
        <td class="text-center" style="font-weight:bold;color: #0b9444;"><?php echo $this->lang->line('membership_profile_discount').' '. number_format($next_member_type->discount); ?> %</td>
        <?php } ?>
      </tr>
    </table>
  </div>
</div>
