<h2 class="heading heading--start"><?php echo $this->lang->line('membership_last_reward_list'); ?></h2>
<div class="table-responsive">
  <table class="table table-bordered table--wide table-present">
    <colgroup class="col-small"></colgroup>
    <colgroup class="col-middle"></colgroup>
    <colgroup class="col-thin"></colgroup>
    <colgroup class="col-thin"></colgroup>
    <colgroup class="col-small"></colgroup>
    <thead>
      <tr>
        <th>#</th>
        <th><?php echo $this->lang->line('membership_last_order_date'); ?></th>
        <th><?php echo $this->lang->line('membership_last_order_total'); ?></th>
        <th><?php echo $this->lang->line('membership_last_order_status'); ?></th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
      $counter = 0;
      foreach($reward_transaction_list->result() as $reward_transaction){
        $counter++;
        $status_col = '';
        switch($reward_transaction->reward_transaction_status){
          case 'checking':
            $status_col = '<td class="table__wait"><i class="fa fa-spinner"></i> Pending</td>';
            break;
          case 'done':
            $status_col = '<td class="table__done"><i class="fa fa-check"></i> Complete</td>';
            break;
        }
        ?>
        <tr>
          <td><?php echo $counter; ?></td>
          <td><?php echo $this->Datetime_service->display_datetime($reward_transaction->create_date, $this->config->item('language_abbr')); ?></td>
          <td><?php echo number_format($reward_transaction->total_reward_point, 0); ?></td>
          <?php echo $status_col; ?>
          <td><?php echo anchor('myreward/detail/'.$reward_transaction->reward_transaction_id, $this->lang->line('membership_last_order_detail'), array('class'=>'btn btn-primary btn-sm')); ?></td>
        </tr>
        <?php
      }
      ?>
    </tbody>
  </table>
</div>
