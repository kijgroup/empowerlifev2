<!-- Revolution full width slider -->
<div class="product-slider-wrapper">
  <div class="swiper-container product-slider slider-present max-slider" style="max-height:none;">
    <div class="swiper-wrapper">
      <?php
      $slideshow_list = $this->Slideshow_model->get_list('home');
      $slot_amount = $slideshow_list->num_rows();
      foreach($slideshow_list->result() as $slideshow){
        ?>
        <div class="swiper-slide">
          <div class="image-container">
            <img src="<?php echo base_url('uploads/'.$slideshow->slideshow_image); ?>" alt="<?php echo $slideshow->slideshow_text; ?>" />
          </div>
        </div>
        <?php
      }
      ?>
    </div>
    <!-- Add Arrows -->
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
  </div>
</div>
