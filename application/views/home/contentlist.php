<div class="container">
  <div class="block-title block-title--top-middle block-title--bottom"><?php echo anchor('content', 'Content'); ?></div>
  <div class="row">
    <?php
    $i = 0;
    foreach($content_list->result() as $content){
      $this->load->view('content/list/thumb', array(
        'content' => $content
      ));
      $i++;
      if($i%2 == 0){
        echo '<div style="display:block;clear:both;"></div>';
      }
    }
    ?>
	</div>
</div>
