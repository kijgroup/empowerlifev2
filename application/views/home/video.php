<?php
$youtube_iframe = $this->Config_model->get_data('youtube_iframe');
if($youtube_iframe != ''){
?>
<div class="container">
  <div class="devider-brand"></div>
  <div class="video-container bottom-space--md">
      <iframe src="<?php echo $youtube_iframe; ?>" frameborder="0"></iframe>
  </div>
</div>
<?php
}
