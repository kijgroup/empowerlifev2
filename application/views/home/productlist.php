<div class="container">
  <div class="block-title block-title--top-middle block-title--bottom"><?php echo anchor('product', 'Product'); ?></div>
  <div class="row product-wrapper">
    <?php
    foreach($product_list->result() as $product){
      $this->load->view('product/list/thumb', array('product'=>$product));
    }
    ?>
	</div>
</div>
