<!-- Revolution full width slider -->
<div class="bannercontainer shop-banner">
  <div class="banner">
    <ul>
      <?php
      $slideshow_list = $this->Slideshow_model->get_list('home');
      $slot_amount = $slideshow_list->num_rows();
      foreach($slideshow_list->result() as $slideshow){
        $image_tag = '<img src="'.base_url('uploads/'.$slideshow->slideshow_image).'" alt="'.$slideshow->slideshow_text.'">';
        ?>
        <!-- Slide -->
        <li data-transition="fade" data-slotamount="1" data-text="<?php echo $slideshow->slideshow_text; ?>" <?php echo ($slideshow->slideshow_url != '')?'data-link="'.$slideshow->slideshow_url.'"':''; ?>>
          <?php
          echo $image_tag;
          ?>
        </li>
        <!-- end slide -->
        <?php
      }
      ?>
    </ul>
  </div>
</div>
