<div class="container">
  <div class="block-title block-title--top-middle block-title--bottom"><?php echo anchor('product', 'Product'); ?></div>
  <div class="row product-wrapper">
    <?php
    foreach($category_list->result() as $category){
      $this->load->view('product/list/category_thumb', array('category'=>$category));
    }
    ?>
	</div>
</div>