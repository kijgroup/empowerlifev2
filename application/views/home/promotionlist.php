<div class="container">
  <div class="block-title block-title--top-middle block-title--bottom"><?php echo anchor('promotion', 'Promotion'); ?></div>
  <div class="row product-wrapper">
    <?php
    foreach($promotion_list->result() as $promotion){
      $this->load->view('promotion/list/thumb', array('product'=>$promotion));
    }
    ?>
	</div>
</div>
