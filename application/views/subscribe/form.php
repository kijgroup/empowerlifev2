<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>การจัดการ</span></li>
    <li><span><?php echo anchor('subscribe', 'ผู้ติดตามข่าวสาร'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>

<?php echo form_open('subscribe/form_post/'.$subscribe_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'subscribe-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลผู้ติดตามข่าวสาร</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="txtUsername" class="col-md-3 control-label">E-Mail <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="subscribe_email" id="subscribe_email" placeholder="" maxlength="20" value="<?php echo ($subscribe_id != 0)?$subscribe->subscribe_email:''; ?>"required>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-offset-3 col-md-6 xs-margin-bottom">
        <input type="checkbox" name="subscribe_status" value="1" <?php echo ($subscribe_id == 0 || $subscribe->subscribe_status == 'show')?'checked="checked"':''; ?> />
        enable
      </label>
    </div>
  </div>
  <div class="panel-footer">
    <div class="form-group">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
