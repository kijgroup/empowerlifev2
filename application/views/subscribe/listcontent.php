<?php
if($total_transaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $total_page = ceil($total_transaction/$per_page);
  $pager_data = array();
  $pager_data['total_transaction'] = $total_transaction;
  $pager_data['search_val'] = $search_val;
  $pager_data['start_date'] = $start_date;
  $pager_data['end_date'] = $end_date;
  $pager_data['per_page'] = $per_page;
  $pager_data['page'] = $page;
  $pager_data['total_page'] = $total_page;
  $pager_data['action'] = site_url('subscribe/filter');
?>
  <div class="form-group">
    <?php $this->load->view('subscribe/listpager', $pager_data); ?>
  </div>
  <div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>อีเมล</th>
        <th class="text-center">สถานะ</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($subscribe_list->result() as $subscribe){
        $detail_url = 'subscribe/detail/'.$subscribe->subscribe_id;
        ?>
        <tr>
          <td>
            <?php echo anchor($detail_url, $subscribe->subscribe_email); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, $subscribe->subscribe_status); ?>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
  </div>
  <div class="form-group">
    <?php $this->load->view('subscribe/listpager', $pager_data); ?>
  </div>
<?php
}
