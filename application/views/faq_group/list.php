<header class="page-header">
<h2>หมวดคำถาม</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>FAQ</span></li>
    <li><span>หมวดคำถาม</span></li>
  </ol>
</div>
</header>
<div class="search-content">
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('faq_group/form', '<i class="fa fa-plus"></i> เพิ่มหมวดคำถาม', array('class'=>'btn btn-success')); ?>
    </div>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
            <tr>
              <th>หมวดคำถาม (th)</th>
              <th>หมวดคำถาม (en)</th>
              <th>สถานะ</th>
              <th></th>
            </tr>
        </thead>
        <tbody>
          <?php
          foreach($faq_group_list->result() as $faq_group){
            $form_url = 'faq_group/form/'.$faq_group->faq_group_id;
            $delete_url = 'faq_group/delete/'.$faq_group->faq_group_id;
            ?>
            <tr>
              <td><?php echo anchor($form_url, $faq_group->faq_group_th); ?></td>
              <td><?php echo anchor($form_url, $faq_group->faq_group_en); ?></td>
              <td>
                <?php echo ($faq_group->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ไม่แสดง</span>'; ?>
              </td>
              <td>
                <?php echo anchor($delete_url, '<i class="fa fa-trash"></i> ลบข้อมูล', array('class'=>'btn btn-sm btn-danger')); ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <br />
  </div>
</div>
