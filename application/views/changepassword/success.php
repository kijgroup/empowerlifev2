<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $this->lang->line('nav_change_password'); ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $this->lang->line('nav_change_password'); ?></li>
    </ol>
    <div class="devider devider--bottom-md"></div>
  </div>
</section>
<div class="container" id="message-contact">
  <div class="col-sm-3">
    <?php $this->load->view('membership/sidemenu'); ?>
  </div>
  <div class="col-sm-9">
    <div class="text-center">
      <h2><?php echo $this->lang->line('changepassword_success'); ?></h2>
      <div><?php echo $this->lang->line('changepassword_success_login'); ?></div>
    </div>
  </div>
</div>
