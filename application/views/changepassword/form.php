<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $this->lang->line('nav_change_password'); ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $this->lang->line('nav_change_password'); ?></li>
    </ol>
    <div class="devider devider--bottom-md"></div>
  </div>
</section>
<div class="container" id="message-contact">
  <div class="col-sm-3">
    <?php $this->load->view('membership/sidemenu'); ?>
  </div>
  <div class="col-sm-9">
    <?php if($page_status == 'false'){ ?>
    <div class="alert alert-danger">
      <span class="alert-market">
        <i class="fa fa-ban"></i>
      </span>
      <span id="alert_login_msg"><?php echo $this->lang->line('changepassword_error_msg'); ?></span>
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
    </div>
    <?php } ?>
    <?php echo form_open('changepassword/form_post', array('role' => 'form', 'class' => 'form-horizontal contact' ,'id'=>'changepasswordform')); ?>
      <div class="form-group">
        <label class="col-md-3 control-label"><?php echo $this->lang->line('changepassword_email_login'); ?></label>
        <div class="col-md-6">
          <input type="tetxt" class="contact__field" value="<?php echo $user->email; ?>"  readonly />
        </div>
      </div><!-- end col -->
      <div class="form-group">
        <label for="login_password" class="col-md-3 control-label"><?php echo $this->lang->line('changepassword_new_password'); ?></label>
        <div class="col-md-6">
          <input type="password" class="contact__field" name="login_password" id="login_password" minlength="8" required />
        </div>
      </div><!-- end col -->
      <div class="form-group">
        <label for="login_password_confirm" class="col-md-3 control-label"><?php echo $this->lang->line('changepassword_confirm_new_password'); ?></label>
        <div class="col-md-6">
          <input type="password" class="contact__field" name="login_password_confirm" id="login_password_confirm" minlength="8" required />
        </div>
      </div><!-- end col -->
      <button class="btn btn--decorated btn-warning" type="submit"><?php echo $this->lang->line('changepassword_change_password'); ?></button>
    <?php echo form_close(); ?>
  </div>
</div>
