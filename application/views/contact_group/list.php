<header class="page-header">
<h2>หมวดหัวข้อคำถาม</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>ติดต่อเจ้าหน้าที่</span></li>
    <li><span>หมวดหัวข้อคำถาม</span></li>
  </ol>
</div>
</header>
<div class="search-content">
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('contact_group/form', '<i class="fa fa-plus"></i> เพิ่มหมวดหัวข้อคำถาม', array('class'=>'btn btn-success')); ?>
    </div>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
            <tr>
              <th>หมวดหัวข้อคำถาม (th)</th>
              <th>หมวดหัวข้อคำถาม (en)</th>
              <th>สถานะ</th>
              <th></th>
            </tr>
        </thead>
        <tbody>
          <?php
          foreach($contact_group_list->result() as $contact_group){
            $form_url = 'contact_group/form/'.$contact_group->contact_group_id;
            $delete_url = 'contact_group/delete/'.$contact_group->contact_group_id;
            ?>
            <tr>
              <td><?php echo anchor($form_url, $contact_group->contact_group_th); ?></td>
              <td><?php echo anchor($form_url, $contact_group->contact_group_en); ?></td>
              <td>
                <?php echo ($contact_group->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ไม่แสดง</span>'; ?>
              </td>
              <td>
                <?php echo anchor($delete_url, '<i class="fa fa-trash"></i> ลบข้อมูล', array('class'=>'btn btn-sm btn-danger')); ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <br />
  </div>
</div>
