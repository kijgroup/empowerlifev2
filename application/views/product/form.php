<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>สินค้า</span></li>
    <li><span><?php echo anchor('product','สินค้า'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open_multipart('product/form_post/'.$product_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'product-form')); ?>
<div class="panel panel-default">
  <div class="panel-heading">ข้อมูล</div>
  <div class="panel-body">
    <div class="form-group">
      <label for="category_id" class="col-md-3 control-label">ประเภทหมวดหมู่ <span class="required">*</span></label>
      <div class="col-md-6">
        <select class="form-control" name="category_id" id="category_id">
          <?php
          $category_list = $this->Category_model->get_list(false);
          foreach($category_list->result() as $category){
            $selected = ($product_id != 0 && $category->category_id == $product->category_id)?'selected="selected"':'';
            echo '<option value="'.$category->category_id.'" '.$selected.'>'.$category->name_th.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label for="name_th" class="col-md-3 control-label">ชื่อสินค้า (th) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="name_th" id="name_th" value="<?php echo ($product_id != 0)?$product->name_th:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="name_en" class="col-md-3 control-label">ชื่อสินค้า (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="name_en" id="name_en" value="<?php echo ($product_id != 0)?$product->name_en:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="slug" class="col-md-3 control-label">slug <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="slug" id="slug" value="<?php echo ($product_id != 0)?$product->slug:''; ?>" placeholder="ชื่อที่แสดงบน url ** โปรดระบุเฉพาะภาษาอังกฤษ" required>
      </div>
    </div>
    <div class="form-group">
      <label for="thumb_image" class="col-md-3 control-label">รูปสินค้า <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        if($product_id != 0 && $product->thumb_image != ''){
          echo '<img src="'.base_url('uploads/'.$product->thumb_image).'" class="img-responsive" style="max-width:300px;" required>';
        }
        ?>
        <input type="file" class="form-control" name="thumb_image" id="thumb_image">
        <span class="help-block">400X400px</span>
      </div>
    </div>
    <div class="form-group">
      <label for="price" class="col-md-3 control-label">ราคา <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="price" id="price" value="<?php echo ($product_id != 0)?$product->price:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="price_before_discount" class="col-md-3 control-label">ราคาก่อนหักส่วนลด <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="price_before_discount" id="price_before_discount" value="<?php echo ($product_id != 0)?$product->price_before_discount:'0'; ?>" required>        
        <span class="help-block">ใส่ 0 ถ้าไม่ต้องการให้แสดงในหน้าเวบไซต์</span>
      </div>
    </div>
    <div class="form-group">
      <label for="price_discount_rate" class="col-md-3 control-label">อัตราการลดราคาจากราคาเต็ม <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="price_discount_rate" id="price_discount_rate" value="<?php echo ($product_id != 0)?$product->price_discount_rate:'0'; ?>" required>
        <span class="help-block">ใส่ 0 ถ้าไม่ต้องการให้แสดงในหน้าเวบไซต์</span>
      </div>
    </div>
    <div class="form-group">
      <label for="detail_th" class="col-md-3 control-label">ข้อมูลสินค้า (th) <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="summernote" name="detail_th" id="detail_th" rows="3" data-upload="<?php echo site_url('product/upload_image'); ?>"><?php echo ($product_id != 0)?$product->detail_th:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="detail_en" class="col-md-3 control-label">ข้อมูลสินค้า (en) <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="summernote" name="detail_en" id="detail_en" rows="3" data-upload="<?php echo site_url('product/upload_image'); ?>"><?php echo ($product_id != 0)?$product->detail_en:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ</label>
      <div class="col-md-6">
        <?php
        $enable_status_list = $this->Enable_status_model->get_list();
        foreach($enable_status_list as $enable_status){
          $checked = (($product_id != 0 && $product->enable_status == $enable_status['status_code']) || ($product_id == 0 && $enable_status['status_code'] == 'show'))?'checked':'';
          ?>
          <label class="radio-inline">
            <input type="radio" name="enable_status" id="enable_status_show" value="<?php echo $enable_status['status_code'] ?>" <?php echo $checked; ?>>
            <?php echo $this->Enable_status_model->get_label($enable_status['status_code']); ?>
          </label>
          <?php
        }
        ?>
      </div>
    </div>
    <div class="form-group">
      <label for="sort_priority" class="col-md-3 control-label">แสดงลำดับที่</label>
      <div class="col-md-6">
        <select name="sort_priority" id="sort_priority" class="form-control">
          <?php
          $max_priority = $this->Product_model->get_max_priority('product');
          $max_priority += ($product_id == 0)?1:0;
          for($i = 1; $i<=$max_priority; $i++){
            $checked = ($product_id != 0 && $product->sort_priority == $i)?'selected="selected"':'';
            echo '<option value="'.$i.'" '.$checked.'>'.$i.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สินค้าใกล้เคียง</label>
      <div class="col-md-9">
        <select name="product_relate[]" id="product_relate" class="form-control multi-select" multiple="multiple">
          <?php
          $product_list = $this->Product_model->get_list();
          $relate_list = $this->Product_relate_model->get_array($product_id);
          foreach($product_list->result() as $product_data){
            $selected = (in_array($product_data->product_id, $relate_list))?'selected="selected"':'';
            echo '<option value="'.$product_data->product_id.'" '.$selected.'>'.$product_data->name_th.'</option>';
          }
          ?>
        </select>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php echo form_close(); ?>
