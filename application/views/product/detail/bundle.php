<?php
$all_bundle_list = $this->Product_bundle_model->get_all_bundle_list($product_id);
if($all_bundle_list->num_rows() > 0){
  ?>
  <h2 class="block-title block-title--top-large-s block-title--bottom">Products Bundle</h2>
  <?php
  foreach($all_bundle_list->result() as $bundle_row){
    ?>
    <div class="row product-wrapper block-title--bottom">
      <div class="col-md-9">
        <?php
        $bundle_list = $this->Product_bundle_model->get_list($bundle_row->product_id);
        $count = 0;
        foreach($bundle_list->result() as $bundle){
          $product_bundle = $this->Product_model->get_data($bundle->bundle_id);
          $thumb_image = '';
          if($product_bundle->thumb_image != ''){
            $thumb_image = base_url('uploads/'.$product_bundle->thumb_image);
          }else{
            $thumb_image = 'http://placehold.it/600x600';
          }
          $main_id = $bundle->product_id;
          if($count > 0){
            echo '<div style="width:20px;height:100px;line-height:100px;display:block;float:left;text-align:center;">+</div>';
          }
          $count++;
          echo '<div style="width:100px;display:block;float:left;text-align:center;">';
          echo '<img src="'.$thumb_image.'" alt="" class="img-responsive" />';
          echo '<div>';
          echo $product_bundle->name_th.'('.$bundle->qty.')';
          echo '</div>';
          echo '</div>';
        }
        ?>
      </div>
      <div class="col-md-3">
      <?php
      $main_product = $this->Product_model->get_data($main_id);
      echo '<h5 style="margin-top:0;">'.$main_product->name_th.'</h5>';
      echo ' ราคา '.number_format($main_product->price) .' บาท';
      echo form_open('cart/add_cart/'.$main_id, array('method'=>'get'));
      echo '<input type="hidden" name="quantity" value="1" />';
      echo '<button type="submit" class="btn btn-warning btn--decorated product__btn">Add to Cart</button>';
      echo form_close();
      ?>
      </div>
    </div>
    <?php
  }
}
