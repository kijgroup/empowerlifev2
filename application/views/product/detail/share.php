<?php
$url = urlencode(site_url('product/detail/'.$product->product_id.'/'.$product->slug));
$title = urlencode(($this->config->item('language_abbr') == 'th')?$product->name_th: $product->name_en);
$twitter_status = urlencode($title.' — empowerlife '.$url);
?>
<p class="sub-header devider--top-large mobile--devider">Share:</p>
<div class="share share--small">
  <a class="share__item share__item--facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>" target="_blank"><i class="fa fa-facebook"></i>Share</a>
  <a class="share__item share__item--twitter" href="https://twitter.com/intent/tweet?source=<?php echo $url; ?>&text=<?php echo $twitter_status; ?>" target="_blank"><i class="fa fa-twitter"></i>Tweet</a>
  <a class="share__item share__item--gplus" href="https://plus.google.com/share?url=<?php echo $url; ?>" target="_blank"><i class="fa fa-google-plus"></i> Share</a>
  <a class="share__item share__item--linkedin" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>&title=<?php echo $title; ?>" target="_blank"><i class="fa fa-linkedin"></i>Share</a>
</div>
