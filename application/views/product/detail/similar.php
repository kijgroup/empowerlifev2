<h2 class="block-title block-title--top-large-s block-title--bottom">Similar Products</h2>
<div class="row product-wrapper">
  <?php
  $relate_list = $this->Product_relate_model->get_list($product->product_id);
  foreach($relate_list->result() as $relate){
    $relate_product = $this->Product_model->get_data($relate->relate_id);
    if($relate_product){
      if($relate_product->product_type == 'product'){
        $this->load->view('product/list/thumb', array('product'=>$relate_product, 'col_width' => 3));
      }else{
        $this->load->view('promotion/list/thumb', array('product'=>$relate_product, 'col_width' => 3));
      }
    }
  }
  ?>
</div>
