<?php
if($this->config->item('language_abbr') == 'th'){
  $product_name = $product->name_th;
  $product_detail = $product->detail_th;
}else{
  $product_name = $product->name_en;
  $product_detail = $product->detail_en;
}
?>
<div class="product product--single product--down">
  <div class="row">
    <div class="col-sm-5">
      <?php $this->load->view('product/detail/image_list'); ?>
    </div><!-- end col -->
  	<div class="col-sm-7">
  		<div class="product__decribe">
        <?php
        $price_class = '';
        if($product->price_before_discount > $product->price){
          $price_class = 'text-red';
          echo '<p class="product__price-discount" style="display:inline-block;">';
          if($product->price_discount_rate > 0){
            echo '-'.$product->price_discount_rate.'%';
          }
          ?>
          <span class="slash-red"><?php echo number_format($product->price_before_discount); ?></span></p><br />
          <?php
        }
        ?>
        <p class="product__price <?php echo $price_class; ?>"><?php echo number_format($product->price); ?>.-</p>
  			<h3 class="product__title"><?php echo $product_name; ?></h3>
  			<!-- Quantity select -->
  			<p class="sub-header"><?php echo $this->lang->line('promotion_quantity'); ?></p>
        <?php echo form_open('cart/add_cart/'.$product->product_id,array('method'=>'get')); ?>
          <div class="quantity-choose">
            <div class="quantity buttons_added">
              <div class="qtyminus-wrap">
                <input type="button" value="-" class="qtyminus quantity__elem" data-field="quantity">
              </div>
              <input type="text" name="quantity" id="quantity" value="1" class="qty quantity__elem">
              <div class="qtyplus-wrap">
                <input type="button" value="+" class="qtyplus quantity__elem" data-field="quantity">
              </div>
            </div>
          </div>
          <button type="submit" class="btn btn-info btn--decorated product__btn"><?php echo $this->lang->line('promotion_add_to_cart'); ?></button>
        <?php echo form_close(); ?>
      </div>
      <?php $this->load->view('product/detail/share'); ?>
    </div><!-- end col -->
  </div><!-- end row -->
</div>
<h2 class="block-title block-title--top-large-s block-title--bottom"><?php echo $this->lang->line('product_detail'); ?></h2>
<div>
  <p class="product__info"><?php echo $product_detail; ?></p>
</div>
<?php $this->load->view('product/detail/bundle', array('product_id'=>$product->product_id)); ?>
<?php $this->load->view('product/detail/similar'); ?>
