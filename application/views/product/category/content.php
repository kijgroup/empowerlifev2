<?php
if($this->config->item('language_abbr') == 'th'){
 $category_name = $category->name_th;
 $category_content = $category->content_th;
 $btn_label = 'สั่งซื้อ';
}else{
  $category_name = $category->name_en;
  $category_content = $category->content_en;
  $btn_label = 'order';
}
?>
<div class="container">
  <?php if($category->icon_image != ''){ ?>
  <img src="<?php echo base_url('uploads/'.$category->icon_image); ?>" alt="<?php echo $category_name; ?>" style="height:40px;margin-top:10px;">
  <?php } ?>
  <div class="pull-right">
    <a href="#product-list" class="btn btn--decorated btn-info" style="border-radius: 5px;padding:10px 15px;font-family:psl_kanda_probold;font-size:16px; margin:13px 0 8px 0;"><?php echo $btn_label; ?></a>
  </div>
</div>
<?php
echo '<style>';
echo $category->style;
echo '</style>';
echo $category_content;
?>