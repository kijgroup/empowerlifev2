<section class="container" id="product-list" style="padding-top:100px;">
  <div class="row product-wrapper text-center">
    <?php
    $total_product = count($product_list);
    $mod_3 = $total_product%3;
    $mod_2 = $total_product%2;
    $mod_3_inx = $mod_2_inx = -1;
    if($mod_3 != 0){
      $mod_3_inx = $total_product - $mod_3;
      $mod_3_class = (3 - $mod_3)*2;
    }
    if($mod_2 != 0){
      $mod_2_inx = $total_product - 1;
      $mod_2_class = 3;
    }
    $inx = 0;
    foreach($product_list as $product){
      $class_offset = '';
      if($inx == $mod_3_inx){
        $class_offset .= 'col-md-offset-'.$mod_3_class;
      }elseif($mod_3_inx != 0 && $inx > $mod_3_inx){
        $class_offset .= 'col-md-offset-0';
      }
      if($inx == $mod_2_inx){
        $class_offset .= ' col-sm-offset-'.$mod_2_class;
      }
      $inx++;
      $this->load->view('product/list/thumb', array('product'=>$product, 'class_offset' => $class_offset));
    }
    ?>
  </div>
</section>