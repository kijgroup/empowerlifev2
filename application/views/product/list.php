<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $this->lang->line('nav_product'); ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $this->lang->line('nav_product'); ?></li>
    </ol>
    <div class="devider devider--bottom-md"></div>
  </div>
</section>
<section class="container">
  <h3 class="not-visible">Main container</h3>
  <div class="row">
    <div class="col-sm-3">
      <?php $this->load->view('product/list/sidemenu'); ?>
    </div>
    <div class="col-sm-9">
      <?php $this->load->view('product/list/content'); ?>
    </div>
  </div>
</section>
