<div class="shop-view">
  <span class="shop-view__results"><?php echo $total_product; ?> results</span>
</div>
<?php echo form_open('product/index/'.$category_slug, array('class'=>'select select--thin select--size pull-right', 'name'=>'select', 'id'=>'sorting')); ?>
  <select class="select-box pull-right" id="sortby" name="sortby" tabindex="0">
    <option value="default" <?php echo ($sort_by == 'default')?'selected="selected"':''; ?>>default sorting</option>
    <option value="popular" <?php echo ($sort_by == 'popular')?'selected="selected"':''; ?>>by popularity</option>
    <option value="arrival" <?php echo ($sort_by == 'arrival')?'selected="selected"':''; ?>>by arrival</option>
    <option value="low" <?php echo ($sort_by == 'low')?'selected="selected"':''; ?>>by price: low to high</option>
    <option value="high" <?php echo ($sort_by == 'high')?'selected="selected"':''; ?>>by price: high to low</option>
  </select>
<?php echo form_close(); ?>
<div class="row product-wrapper">
  <?php
  foreach($product_list->result() as $product){
    $this->load->view('product/list/thumb', array('product'=>$product));
  }
  ?>
</div>
<?php
$this->load->view('product/list/pager');
