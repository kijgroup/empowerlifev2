<div class="sidebar">
  <h2 class="heading heading--section heading--first">Categories</h2>
  <ul class="list list--marker">
    <?php
    echo '<li class="list__item">'.anchor('product', 'ทั้งหมด',array('class'=>'list__link')).'</li>';
    $category_list = $this->Category_model->get_list();
    foreach($category_list->result() as $category){
      echo '<li class="list__item">'.anchor('product/index/'.$category->slug, $this->Category_model->return_name_lang($category, $this->config->item('language_abbr')). ' ('.$this->Product_model->count_in_category($category->category_id).')',array('class'=>'list__link')).'</li>';
    }
    ?>
  </ul>
</div>
