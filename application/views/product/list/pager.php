<div>
  <form class="form-inline">
    แสดง
    <select name="ddlPerPage" class="ddlPerPage form-control"
      data-search_val="<?php echo $search_val; ?>"
      data-category_id="<?php echo $category_id; ?>"
      data-enable_status="<?php echo $enable_status; ?>"
      data-action="<?php echo $action; ?>"
    >
      <?php
      $arr_per_page = array(10,20,50,100,200);
      foreach($arr_per_page as $per_page_val){
        $selected = ($per_page_val == $per_page)?'selected="selected"':'';
        echo '<option val="'.$per_page_val.'" '.$selected.'>'.$per_page_val.'</option>';
      }
      ?>
    </select>
    จาก <span class="int"><?php echo $total_transaction; ?></span> รายการ
    <div class="pull-right">หน้า
      <select name="ddlPager" class="ddlPager form-control"
      data-search_val="<?php echo $search_val; ?>"
      data-category_id="<?php echo $category_id; ?>"
      data-enable_status="<?php echo $enable_status; ?>"
      data-per_page="<?php echo $per_page; ?>"
      data-action="<?php echo $action; ?>"
      >
        <?php
        for($i = 1; $i <= $total_page; $i++){
          $selected = ($i == $page)?'selected="selected"':'';
          echo '<option val="'.$i.'" '.$selected.'>'.$i.'</option>';
        }
        ?>
      </select>
      / <span class="int"><?php echo $total_page; ?></span>
    </div>
  </form>
</div>
