<?php
if($total_transaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $total_page = ceil($total_transaction/$per_page);
  $pager_data = array(
    'total_transaction' => $total_transaction,
    'search_val' => $search_val,
    'category_id' => $category_id,
    'enable_status' => $enable_status,
    'per_page' => $per_page,
    'page' => $page,
    'total_page' => $total_page,
    'action' => site_url('product/filter')
  );
?>
<div class="form-group">
    <?php $this->load->view('product/list/pager', $pager_data); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th width="120">รูปสินค้า</th>
        <th>ข้อมูลสินค้า</th>
        <th>ราคา</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($product_list->result() as $product){
        $detail_url = 'product/detail/'.$product->product_id;
        ?>
        <tr>
          <td>
            <?php
            if($product->thumb_image != ''){
              $str_img = '<img src="'.base_url('uploads/'.$product->thumb_image).'" style="width:120px;" />';
              echo anchor('product/detail/'.$product->product_id, $str_img);
            }
            ?>
          </td>
          <td>
            <div><b>TH:</b> <?php echo anchor('product/detail/'.$product->product_id, $product->name_th); ?></div>
            <div><b>EN:</b> <?php echo anchor('product/detail/'.$product->product_id, $product->name_en); ?></div>
            <div><b>ประเภทสินค้า:</b> <?php echo anchor('product/detail/'.$product->product_id, $this->Category_model->get_name($product->category_id)); ?></div>
            <div><?php echo $this->Enable_status_model->get_label($product->enable_status); ?></div>
            <small><?php echo $product->create_date.' <b>by</b> '.$this->Admin_model->get_login_name_by_id($product->create_by); ?></small>
          </td>
          <td>
            <div><?php echo $product->price; ?> บาท</div>
            <?php
            if($product->price_before_discount > $product->price){
              echo '<small>';
              echo '<span style="text-decoration: line-through;">'.$product->price_before_discount.' บาท</span> ';
              if($product->price_discount_rate > 0){
                echo '(ลด '.$product->price_discount_rate.' %)';
              }
              echo '</small>';
            }
            ?>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
  <?php $this->load->view('product/list/pager', $pager_data); ?>
</div>
<?php
}
