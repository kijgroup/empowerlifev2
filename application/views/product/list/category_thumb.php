<?php
$detail_url= site_url('product/category/'.$category->slug);
$thumb_image = '';
if($category->thumb_image != ''){
  $thumb_image = base_url('uploads/'.$category->thumb_image);
}else{
  $thumb_image = 'http://placehold.it/600x600';
}
$col_width = (isset($col_width))?$col_width:4;
?>
<div class="col-sm-6 col-md-<?php echo $col_width; ?>">
  <!-- Product preview -->
  <div class="product product--round product--flow" style="position:relative; padding:0; padding-bottom:25px;">
    <a href="<?php echo $detail_url; ?>" class="product__photo">
      <img src="<?php echo $thumb_image; ?>" alt="">
    </a>
    <a href="<?php echo $detail_url; ?>" style="padding-top:15px;border-top:1px solid #dfe6e7;display:block;">
      <span class="product__title" style="margin-top:0;margin-bottom:0;font-weight:bold;font-size:24px;">
        <?php echo $this->Category_model->return_name_lang($category, $this->config->item('language_abbr')); ?>
      </span>
      <p style="font-size:17px;color:#696559;font-family:psl_kanda_proregular;"><?php echo $category->desc_th; ?></p>
      <div class="bar-add-cart" href="#" style="margin-bottom:-25px;">
        <i class="fa fa-shopping-basket"></i> สั่งซื้อสินค้า
      </div>
    </a>
  </div>
  <!-- end category preview -->
</div>
