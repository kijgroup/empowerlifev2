<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $this->lang->line('nav_product'); ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $this->lang->line('nav_product'); ?></li>
    </ol>
    <div class="devider devider--bottom-md"></div>
  </div>
</section>
<section class="container">
  <h3 class="not-visible">Product</h3>
  <?php $this->load->view('product/list/category_list'); ?>
</section>
