<style>
</style>
<?php echo form_open('profile/form_post', array('role' => 'form', 'class' => 'form-horizontal contact' ,'id'=>'profileform')); ?>
  <div class="form-group">
    <label for="member_firstname" class="control-label col-md-3"><?php echo $this->lang->line('profile_form_member_first_name'); ?></label>
    <div class="col-md-6">
      <input type="text" class="contact__field" name="member_firstname" id="member_firstname" placeholder="" value="<?php echo $member->member_firstname ?>" />
    </div>
  </div><!-- end col -->
  <div class="form-group">
    <label for="member_lastname" class="control-label col-md-3"><?php echo $this->lang->line('profile_form_member_last_name'); ?></label>
    <div class="col-md-6">
      <input type="text" class="contact__field" name="member_lastname" id="member_lastname" placeholder="" value="<?php echo $member->member_lastname ?>" />
    </div>
  </div><!-- end col -->
  <div class="form-group">
    <label for="member_gender" class="control-label col-md-3"><?php echo $this->lang->line('profile_form_member_gender'); ?></label>
    <div class="col-md-6">
      <select class="contact__field" name="member_gender" id="member_gender">
        <option value="default" selected="selected"><?php echo $this->lang->line('profile_form_member_gender'); ?></option>
        <option value="female" <?php echo ($member->member_gender != 'male')?'selected="selected"':''; ?>>
        <?php echo $this->lang->line('profile_form_member_female'); ?></option>
        <option value="male" <?php echo ($member->member_gender == 'male')?'selected="selected"':''; ?>>
        <?php echo $this->lang->line('profile_form_member_male'); ?></option>
      </select>
    </div>
  </div><!-- end col -->
  <div class="form-group">
    <label for="member_birthdate" class="control-label col-md-3"><?php echo $this->lang->line('profile_form_member_birthday'); ?></label>
    <div class="col-md-6">
      <input type="text" class="contact__field" name="member_birthdate" id="member_birthdate" placeholder="" value="<?php echo $member->member_birthdate ?>" />
    </div>
  </div><!-- end col -->
  <div class="form-group">
    <label for="member_phone" class="control-label col-md-3"><?php echo $this->lang->line('profile_form_member_phone'); ?></label>
    <div class="col-md-6">
      <input type="text" class="contact__field" name="member_phone" id="member_phone" placeholder="" value="<?php echo $member->member_phone ?>" />
    </div>
  </div><!-- end col -->
  <div class="form-group">
    <label for="member_mobile" class="control-label col-md-3"><?php echo $this->lang->line('profile_form_member_mobile'); ?></label>
    <div class="col-md-6">
      <input type="text" class="contact__field" name="member_mobile" id="member_mobile" placeholder="" value="<?php echo $member->member_mobile ?>" />
    </div>
  </div><!-- end col -->
  <div class="form-group">
    <label for="member_address" class="control-label col-md-3"><?php echo $this->lang->line('profile_form_member_address'); ?></label>
    <div class="col-md-6">
      <input type="text" class="contact__field" name="member_address" id="member_address" placeholder="" value="<?php echo $member->member_address ?>" />
    </div>
  </div><!-- end col -->
  <div class="form-group">
    <label for="member_district" class="control-label col-md-3"><?php echo $this->lang->line('profile_form_member_district'); ?></label>
    <div class="col-md-6">
      <input type="text" class="contact__field" name="member_district" id="member_district" placeholder="" value="<?php echo $member->member_district ?>" />
    </div>
  </div><!-- end col -->
  <div class="form-group">
    <label for="member_amphur" class="control-label col-md-3"><?php echo $this->lang->line('profile_form_member_amphur'); ?></label>
    <div class="col-md-6">
      <input type="text" class="contact__field" name="member_amphur" id="member_amphur" placeholder="" value="<?php echo $member->member_amphur ?>" />
    </div>
  </div><!-- end col -->
  <div class="form-group">
    <label for="member_province" class="control-label col-md-3"><?php echo $this->lang->line('profile_form_member_province'); ?></label>
    <div class="col-md-6">
      <input type="text" class="contact__field" name="member_province" id="member_province" placeholder="" value="<?php echo $member->member_province ?>" />
    </div>
  </div><!-- end col -->
  <div class="form-group">
    <label for="member_postcode" class="control-label col-md-3"><?php echo $this->lang->line('profile_form_member_post_code'); ?></label>
    <div class="col-md-6">
      <input type="text" class="contact__field" name="member_postcode" id="member_postcode" placeholder="" value="<?php echo $member->member_postcode ?>" />
    </div>
  </div><!-- end col -->
  <button class="btn btn--decorated btn-warning" type="submit"><?php echo $this->lang->line('profile_form_member_edit'); ?></button>
<?php echo form_close(); ?>
