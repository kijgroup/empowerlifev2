<section class="container">
  <h2 class="block-title block-title--bottom"><?php echo $this->lang->line('nav_register'); ?></h2>
  <div id="alert_register" style="display:none;">
    <div class="alert alert-danger text-center" id="alert_register_msg"></div>
  </div>
  <div class="login">
    <?php echo form_open('register/form_post',array('class'=>'contact', 'id'=>'register-form')); ?>
    <input class="contact__field" name="email" id="email" type="email" placeholder="<?php echo $this->lang->line('register_email'); ?>*" required>
      <input class="contact__field" name="login_password" id="login_password" type="password" placeholder="<?php echo $this->lang->line('register_password'); ?>*" required>
      <input class="contact__field" name="login_password_confirm" id="login_password_confirm" type="password" placeholder="<?php echo $this->lang->line('register_confirm_password'); ?>*" required>
      <input class="contact__field" name="member_firstname" id="member_firstname" type="text" placeholder="<?php echo $this->lang->line('register_first_name'); ?>*" required>
      <input class="contact__field" name="member_lastname" id="member_lastname" type="text" placeholder="<?php echo $this->lang->line('register_last_name'); ?>*" required>
      <select class="contact__field" name="member_gender" id="member_gender">
        <option value="default" selected="selected"><?php echo $this->lang->line('register_gender'); ?></option>
        <option value="female"><?php echo $this->lang->line('register_gender_female'); ?></option>
        <option value="male"><?php echo $this->lang->line('register_gender_male'); ?></option>
      </select>
      <style>
      .birthdate_label{
        text-align:left;
        padding-bottom:3px;
      }
      .birthdate_group{
        text-align:left;
      }
      .birthdate_group>.contact__field{
        width:32%;
        padding: 9px 15px;
      }
      </style>
      <div class="birthdate_label"><?php echo $this->lang->line('register_birthday'); ?></div>
      <div class="birthdate_group">
        <select class="contact__field" name="birthdate_day" id="birthdate_day">
          <?php
          for($i = 1; $i<=31; $i++){
            echo '<option value="'.$i.'">'.$i.'</option>';
          }
          ?>
        </select>
        <select class="contact__field" name="birthdate_month" id="birthdate_month">
          <?php
          $month_name_list_th = array(
            'ม.ค.',
            'ก.พ.',
            'มี.ค.',
            'เม.ย.',
            'พ.ค.',
            'มิ.ย.',
            'ก.ค.',
            'ส.ค.',
            'ก.ย.',
            'ต.ค.',
            'พ.ย.',
            'ธ.ค.',
            'May',
          );
          $month_name_list_en = array(
            'Jan.',
            'Feb.',
            'Mar.',
            'Apr.',
            'May',
            'June',
            'July',
            'Aug.',
            'Sept.',
            'Oct.',
            'Nov.',
            'Dec.',
          );
          $month_name_list = ($this->Masterpage_service->get_lang_full() == 'thai')?$month_name_list_th:$month_name_list_en;
          for($i=0; $i<12; $i++){
            $month_value = $i+1;
            echo '<option value="'.$month_value.'">'.$month_name_list[$i].'</option>';
          }
          ?>
        </select>
        <select class="contact__field" name="birthdate_year" id="birthdate_year">
          <?php
          $start_year = Date('Y');
          for($i = 0; $i<100; $i++){
            $year_value = $start_year - $i;
            $year_name = ($this->Masterpage_service->get_lang_full() == 'thai')?$year_value+543:$year_value;
            echo '<option value="'.$year_value.'">'.$year_name.'</option>';
          }
          ?>
        </select>
      </div>
      <input class="contact__field" name="member_phone" id="member_phone" type="text" placeholder="<?php echo $this->lang->line('register_phone'); ?>">
      <input class="contact__field" name="member_mobile" id="member_mobile" type="text" placeholder="<?php echo $this->lang->line('register_mobile'); ?>">
      <textarea class="contact__field contact__area" name="member_address" id="member_address" placeholder="<?php echo $this->lang->line('register_address'); ?>"></textarea>
      <input class="contact__field" name="member_district" id="member_district" placeholder="<?php echo $this->lang->line('register_district'); ?>">
      <input class="contact__field" name="member_amphur" id="member_amphur" placeholder="<?php echo $this->lang->line('register_amphur'); ?>">
      <input class="contact__field" name="member_province" id="member_province" placeholder="<?php echo $this->lang->line('register_province'); ?>">
      <input class="contact__field" name="member_postcode" id="member_postcode" type="text" placeholder="<?php echo $this->lang->line('register_post_code'); ?>">
      <div class="checkbox">
        <input type="checkbox" name="old_member" id="old_member" value="true" />
        <label for="old_member">เคยมีประวัติการสั่งซื้อกับ Empower Life</label>
      </div>
      <br />
      <br />
      <div class="g-recaptcha" data-sitekey="6Ld19lMUAAAAANjaXRfa7e-G1sCe-VRr8LuE4L9p"></div>
      <br />
      <div>
        การคลิก<b><?php echo $this->lang->line('register_create_account'); ?></b>หมายความว่าคุณยอมรับ<?php echo anchor('privacy', 'นโยบายความเป็นส่วนตัว', array('target' => '_blank', 'style'=>'color:#428bca')); ?>ของ Empower Life
      </div>
      <button class="btn btn--decorated btn-warning login__btn" type="submit"><?php echo $this->lang->line('register_create_account'); ?></button>
    <?php echo form_close(); ?>
  </div>
  <div class="btn-wrapper">
    <h3 class="heading-helper heading-helper--bottom"><?php echo $this->lang->line('register_social'); ?></h3>
    <?php echo anchor('hauth/login/Facebook','<i class="fa fa-facebook"></i>Facebook',array('class' =>'btn btn--decorated btn--facebook')); ?>
    <?php echo anchor('hauth/login/Google','<i class="fa fa-google-plus"></i>Google',array('class' =>'btn btn--decorated btn--google')); ?>
  </div>
</section><!-- end container -->
