<section id="login_bg">
<div  class="container">
<div class="row">
	<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
		<div id="login">
			<p class="text-center">
				<img src="<?php echo base_url('assets/img/logo.png'); ?>" alt="">
			</p>
			<hr>
			<div style="text-align: center;">
				<h4><?php echo $this->lang->line('register_success'); ?></h4>
        <div id="pass-info" class="clearfix"></div>
        <div><?php echo $this->lang->line('register_success_detail'); ?></div>
    </div>
		</div>
	</div>
</div>
</div>
</section><!-- End register -->
