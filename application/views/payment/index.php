<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $this->lang->line('nav_payment'); ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $this->lang->line('nav_payment'); ?></li>
    </ol>
    <div class="devider devider--bottom-md"></div>
  </div>
  <div class="container contact">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <?php echo form_open_multipart('payment/form_post', array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'payment_form')); ?>
        <div class="row">
          <div class="col-sm-6">
            <input class="contact__field" name="order_code" id="order_code" type="text" placeholder="<?php echo $this->lang->line('payment_order_code'); ?>">
          </div><!-- end col -->
        </div>
        <div class="row">
          <div class="col-sm-6">
            <input class="contact__field" name="bank_payment_name" id="bank_payment_name" type="text" placeholder="<?php echo $this->lang->line('payment_bank_payment_name'); ?>" required="required">
          </div><!-- end col -->
          <div class="col-sm-6">
            <input class="contact__field" name="bank_payment_phone" id="bank_payment_phone" type="text" placeholder="<?php echo $this->lang->line('payment_bank_payment_phone'); ?>" required="required">
          </div><!-- end col -->
        </div>
        <div class="row">
          <div class="col-sm-12 text-left" style="padding-bottom:10px;">
            <label class="control-label" style="padding-left: 16px;display: block;font-weight: bold;"><?php echo $this->lang->line('payment_bank_payment'); ?></label>
            <div class="radio" style="margin-top:10px;">
              <table style="width:100%;">
                <?php
                $bank_list = $this->Bank_model->get_list();
                $index = 0;
                foreach($bank_list->result() as $bank){
                  $index++;
                  ?>
                  <tr>
                    <td width="20" valign="middle" style="padding-left:20px;"><input id="bank_<?php echo $bank->bank_id; ?>" type="radio" name="bank_id" value="<?php echo $bank->bank_id; ?>" <?php echo ($index == 1)?'required="required"':''; ?>><label for="bank_<?php echo $bank->bank_id; ?>" style="margin-right:0;">&nbsp;</label></td>
                    <td width="100" style="padding:5px 10px;"><label for="bank_<?php echo $bank->bank_id; ?>" style="padding-left:0;"><img src="<?php echo base_url('uploads/'.$bank->bank_name_icon); ?>" /></label></td>
                    <td>
                      <label for="bank_<?php echo $bank->bank_id; ?>" style="padding-left:0;">
                        <div style="font-size:20px;"><?php echo ($this->config->item('language_abbr') == 'th')?$bank->bank_name_th:$bank->bank_name_en; ?></div>
                        <div><?php echo ($this->config->item('language_abbr') == 'th')?$bank->bank_info_th:$bank->bank_info_en; ?></div>
                      </label>
                    </td>
                  </tr>
                  <?php
                }
                ?>
              </table>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-6">
            <input class="contact__field" name="bank_payment_amount" id="bank_payment_amount" type="text" placeholder="<?php echo $this->lang->line('payment_bank_payment_amount'); ?>" required="required">
          </div><!-- end col -->
        </div>
        <div class="row">
          <div class="col-sm-6">
            <input class="contact__field" name="bank_payment_date" id="bank_payment_date" type="text" placeholder="<?php echo $this->lang->line('payment_bank_payment_date'); ?>" required="required">
          </div><!-- end col -->
          <div class="col-sm-6">
            <input class="contact__field" name="bank_payment_time" id="bank_payment_time" type="text" placeholder="<?php echo $this->lang->line('payment_bank_payment_time'); ?>" required="required">
          </div><!-- end col -->
        </div>
        <div class="row">
          <div class="col-sm-6 text-left">
            <label class="control-label" style="padding-left: 16px;padding-bottom: 5px;display: block;font-weight: bold;"><?php echo $this->lang->line('payment_bank_payment_image'); ?></label>
            <input class="contact__field" name="bank_payment_image" type="file" placeholder="<?php echo $this->lang->line('payment_bank_payment_image'); ?>" required="required">
          </div><!-- end col -->
        </div>
        <div class="row">
          <div class="col-md-12">
            <button class="btn btn--decorated btn-info" type="submit">Submit</button>
          </div>
        </div>
        <?php echo form_close(); ?>
      </div>
    </div>
  </div>
</section>
