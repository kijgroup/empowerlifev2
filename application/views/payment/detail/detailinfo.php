<?php echo form_open('#', array('role' => 'form', 'class' => 'form-horizontal')); ?>
<div class="panel panel-default">
  <div class="panel-body">
      <div class="form-group">
          <label class="col-sm-4 col-print-6 control-label">รหัสการแจ้งชำระเงิน</label>
          <div class="col-sm-8 col-print-6">
              <p class="form-control-static"><?php echo $payment->order_code; ?></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 col-print-6 control-label">ชื่อผู้ชำระเงิน</label>
          <div class="col-sm-8 col-print-6">
              <p class="form-control-static"><?php echo $payment->bank_payment_name; ?></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 col-print-6 control-label">เบอร์โทรศัพท์</label>
          <div class="col-sm-8 col-print-6">
              <p class="form-control-static"><?php echo $payment->bank_payment_phone; ?></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 col-print-6 control-label">สถานะ</label>
          <div class="col-sm-8 col-print-6">
              <p class="form-control-static"><?php echo $payment->bank_payment_status; ?></span></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 col-print-3 control-label">ช่องทางการชำระเงิน</label>
          <div class="col-sm-8 col-print-9">
              <p class="form-control-static"><?php echo $this->Bank_model->get_name($payment->bank_id); ?></span></p>
          </div>
      </div>
      <div class="form-group">
          <label for="txtBranch" class="col-sm-4 col-print-3 control-label">มูลค่าการสั่งซื้อ</label>
          <div class="col-sm-8 col-print-3 xs-margin-bottom">
              <p class="form-control-static"><?php echo number_format($payment->bank_payment_amount, 2); ?> บาท</p>
          </div>
      </div>
      <div class="form-group">
          <label for="txtBranch" class="col-sm-4 col-print-3 control-label">แจ้งวันที่</label>
          <div class="col-sm-8 col-print-3 xs-margin-bottom">
              <p class="form-control-static"><?php echo $payment->bank_payment_date; ?></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 col-print-3 control-label">ไฟล์แนบ</label>
          <div class="col-sm-8 col-print-3">
              <p class="form-control-static"><?php echo ($payment->bank_payment_image != '')?anchor(base_url('uploads/'.$payment->bank_payment_image),'Open file', array('target'=>'_blank')):'ไม่มีไฟล์แนบ'; ?></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 col-print-3 control-label">ปรับปรุงวันที่</label>
          <div class="col-sm-8 col-print-3 xs-margin-bottom">
              <p class="form-control-static"><?php echo $payment->update_date; ?></p>
          </div>
      </div>
      <div class="form-group">
          <label class="col-sm-4 col-print-3 control-label">โดย</label>
          <div class="col-sm-8 col-print-3">
              <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($payment->update_by); ?></p>
          </div>
      </div>
  </div>
</div>
<?php echo form_close(); ?>
