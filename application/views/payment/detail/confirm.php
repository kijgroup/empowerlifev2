<div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="confirmModalLabel">ยืนยันการแจ้งชำระเงิน</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ยืนยัน" เพื่อยืนยันการแจ้งชำระเงิน
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ปิด</button>
        <?php echo anchor('payment/confirmpayment/'.$payment->bank_payment_id, '<i class="glyphicon glyphicon-pencil"></i> ยืนยัน', array('class'=>'btn btn-success')); ?>
      </div>
    </div>
  </div>
</div>
