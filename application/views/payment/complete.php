<section class="page-indecator">
  <div class="container">
  <h2 class="heading"><?php echo $this->lang->line('payment_complete'); ?></h2>
  <!-- Breadcrumb pattern -->
  <ol class="breadcrumb">
    <li><a href="index.html"><?php echo $this->lang->line('nav_home'); ?></a></li>
    <li class="active"><?php echo $this->lang->line('nav_payment'); ?></li>
    <li class="active"><?php echo $this->lang->line('payment_complete_done'); ?></li>
  </ol>
  <!-- Default one color devider -->
  <div class="devider devider--bottom-md"></div>
  </div>
</section>
<section class="container">
  <div class="promo promo--border promo-present">
    <h3 class="promo__heading"><?php echo $this->lang->line('payment_complete'); ?></h3>
    <p><?php echo $this->lang->line('payment_complete_msg'); ?></p>
  </div>
</section>
