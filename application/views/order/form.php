<header class="page-header">
  <h2><?php echo $title; ?></h2>
  <div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
      <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
      <li><span><?php echo anchor('order','เพิ่มรายการสั่งซื้อ'); ?></span></li>
      <li><span><?php echo $title; ?></span></li>
    </ol>
  </div>
</header>
<div>
  <?php
  $member_type_list = $this->Member_type_model->get_list();
  foreach($member_type_list->result() as $member_type){
    echo '<div id="member_type_'.$member_type->member_type_code.'" data-min_amount="'.$member_type->min_amount.'" data-discount="'.$member_type->discount.'"></div>';
  }
  ?>
</div>
<input type="hidden" id="hid_order_id" value="<?php echo $order_id; ?>" />
<input type="hidden" id="hid_member_id" value="<?php echo $member_id; ?>" />
<?php echo form_open('order/form_post/'.$order_id, array('class'=>'form-horizontal','id' => 'order-form'));
$this->load->view('order/form/content_main');
$this->load->view('order/form/order_item_list');
?>
<div class="row">
  <div class="col-md-6">
    <?php $this->load->view('order/form/discount_point'); ?>
  </div>
  <div class="col-md-6">
    <?php $this->load->view('order/form/sum_price'); ?>
  </div>
</div>
<div class="row">
  <div class="col-md-6">
    <?php $this->load->view('order/form/shipping'); ?>
  </div>
  <div class="col-md-6">
    <?php $this->load->view('order/form/billing'); ?>
  </div>
</div>
<div class="panel">
  <div class="panel-body">
    <div class="form-group">
      <label for="remark" class="col-md-3 control-label">หมายเหตุ</label>
      <div class="col-md-6">
        <textarea class="form-control" name="remark" id="remark"><?php
        echo ($order_id != 0)?$order->remark:''; ?></textarea>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php
echo form_close();
$this->load->view('order/form/order_item_template');
$this->load->view('address/popup', array(
  'popup_name' => 'select_address_tax',
  'province_field' => 'tax_province',
  'amphur_field' => 'tax_amphur',
  'district_field' => 'tax_district',
  'postcode_field' => 'tax_post_code',
));
$this->load->view('address/popup', array(
  'popup_name' => 'select_address_shipping',
  'province_field' => 'shipping_province',
  'amphur_field' => 'shipping_amphur',
  'district_field' => 'shipping_district',
  'postcode_field' => 'shipping_post_code',
));
