
<div class="search-control-wrapper">
  <?php echo form_open('order/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'searchForm')); ?>
  <div class="form-group pb-sm pb-sm">
    <input type="text" class="form-control" name="search_val" id="search_val" placeholder="ข้อความที่ต้องการค้นหา" />
  </div>
  <div class="form-group pb-sm">
    <select class="form-control" name="payment_type" id="payment_type">
      <option value="all">การชำระเงินทั้งหมด</option>
      <?php
      $payment_type_list = $this->Payment_type_model->get_list();
      foreach($payment_type_list as $payment_type){
        echo '<option value="'.$payment_type['status_code'].'">'.$payment_type['status_name'].'</option>';
      }
      ?>
    </select>
  </div>
  <div class="form-group pb-sm">
    <select class="form-control" name="order_channel_id" id="order_channel_id">
      <option value="all">ช่องทางทั้งหมด</option>
      <?php
      $order_channel_list = $this->Order_channel_model->get_list();
      foreach($order_channel_list->result() as $order_channel){
        $checked = ($order_status_page == 'web' && $order_channel->order_channel_id == 1)?' selected="selected"':'';
        echo '<option value="'.$order_channel->order_channel_id.'"'.$checked.'>'.$order_channel->order_channel_name.'</option>';
      }
      ?>
    </select>
  </div>
  <div class="form-group pb-sm">
    <select class="form-control" name="send_time_status" id="send_time_status">
      <option value="all">เวลาส่งทั้งหมด</option>
      <?php
      $send_time_status_list = $this->Send_time_status_model->get_list();
      foreach($send_time_status_list as $send_time_status){
        $send_time_status_name = ($send_time_status['status_name'] == '')?'ไม่ได้ส่งเช้า':$send_time_status['status_name'];
        echo '<option value="'.$send_time_status['status_code'].'">'.$send_time_status_name.'</option>';
      }
      ?>
    </select>
  </div>
  <div class="form-group pb-sm">
    <select class="form-control" name="create_by" id="create_by">
      <option value="all">บันทึกโดยเจ้าหน้าที่ทั้งหมด</option>
      <?php
      $admin_list = $this->Admin_model->get_list();
      foreach($admin_list->result() as $admin){
        echo '<option value="'.$admin->admin_id.'">'.$admin->admin_name.'</option>';
      }
      ?>
    </select>
  </div>
  <div class="form-group pb-sm">
    <div class="input-group">
      <span class="input-group-addon">ช่วงเวลา</span>
      <input type="text" class="form-control form-control-date" name="start_date" id="start_date" placeholder="วันที่เริ่มต้น">
      <span class="input-group-addon">-</span>
      <input type="text" class="form-control form-control-date" name="end_date" id="end_date" placeholder="วันที่สิ้นสุด">
    </div>
  </div>
  <?php if($order_status_page == ''){ ?>
  <div class="form-group pb-sm">
    <select class="form-control" name="order_status" id="order_status">
      <option value="all" selected="selected">สถานะทั้งหมด</option>
      <?php
      $order_status_list = $this->Order_status_model->get_list();
      foreach($order_status_list as $order_status){
        echo '<option value="'.$order_status['status_code'].'">'.$order_status['status_name'].'</option>';
      }
      ?>
    </select>
  </div>
  <?php }else{
    echo '<input type="hidden" name="order_status" id="order_status" value="'.$order_status_page.'" />';
  } ?>
  <div class="form-group pb-sm">
    <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> ค้นหา</button>
  </div>
  <?php echo form_close(); ?>
</div>