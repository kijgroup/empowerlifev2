<header class="page-header">
<h2>รายการสั่งซื้อ</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span>รายการสั่งซื้อ</span></li>
    </ol>
</div>
</header>
<div class="search-content">
    <?php $this->load->view('order/list/search'); ?>
    <div class="tab-content pt-lg">
        <div class="pb-lg">
            <?php echo anchor('order/form/0', '<i class="fa fa-plus"></i> เพิ่มรายการสั่งซื้อ', array('class'=>'btn btn-success')); ?>
        </div>
        <div id="result">
        </div>
        <br />
    </div>
</div>
<?php
$this->load->view('order/list/pdf_popup.php');
$this->load->view('order/list/unprint_popup.php');