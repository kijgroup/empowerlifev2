<header class="page-header">
  <h2><?php echo $title; ?></h2>
  <div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
      <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
      <li><span><?php echo anchor('order','รายการสั่งซื้อ'); ?></span></li>
      <li><span><?php echo $title; ?></span></li>
    </ol>
  </div>
</header>
<div class="pb-sm">
  <div class="btn-group hide-print">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
      การกระทำ <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
      <li><?php echo anchor('order', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
      <li><?php echo anchor('order/form/', '<i class="glyphicon glyphicon-plus"></i> สร้างรายการสั่งซื้อใหม่'); ?></li>
      <li><?php echo anchor('order/form/'.$order->order_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
      <li><?php echo anchor('order/update_unprint/'.$order->order_id, '<i class="glyphicon glyphicon-pencil"></i> ปรับเป็นยังไม่พิมพ์'); ?></li>
      <li class="divider"></li>
      <?php
      $order_status_list = $this->Order_status_model->get_list();
      foreach($order_status_list as $order_status){
        if($order->order_status != $order_status['status_code']){
          echo '<li>'.anchor('order/update_status/'.$order->order_id.'/'.$order_status['status_code'], '<i class="glyphicon glyphicon-send"></i> ปรับสถานะ'.$order_status['status_name']).'</li>';
        }
      }
      ?>
      <li class="divider"></li>
      <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
    </ul>
  </div>
  <a href="#" class="btn btn-success" data-toggle="modal" data-target="#pdfModal"><i class="glyphicon glyphicon-download"></i> Download PDF</a>
</div>
<div class="form-horizontal">
  <?php
  $this->load->view('order/detail/content_main');
  $this->load->view('order/detail/order_item_list');
  ?>
  <div class="col-sm-6">
    <?php $this->load->view('order/detail/discount_point'); ?>
  </div>
  <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-6">จำนวนสินค้า</label>
            <div class="col-sm-3">
              <p class="text-right"><?php echo number_format($order->total_item, 0); ?></p>
            </div>
            <label class="col-sm-2">ชิ้น</label>
          </div>
          <div class="form-group">
            <label class="col-sm-6">รวมราคา</label>
            <div class="col-sm-3">
              <p class="text-right"><?php echo number_format($order->item_price, 2); ?></p>
            </div>
            <label class="col-sm-2">บาท</label>
          </div>
          <div class="form-group">
            <label class="col-sm-6">แต้มแลกส่วนลด(<?php echo number_format($order->discount_point_amount, 0); ?> แต้ม)</label>
            <div class="col-sm-3">
              <p class="text-right"><?php echo number_format($order->discount_by_point_amount, 2); ?></p>
            </div>
            <label class="col-sm-2">บาท</label>
          </div>
          <div class="form-group">
            <label class="col-sm-6">ส่วนลด</label>
            <div class="col-sm-3">
              <p class="text-right"><?php echo number_format($order->discount_price, 2); ?></p>
            </div>
            <label class="col-sm-2">บาท</label>
          </div>
          <div class="form-group">
            <label class="col-sm-6">ค่าส่ง</label>
            <div class="col-sm-3">
              <p class="text-right"><?php echo number_format($order->shipping_price, 2); ?></p>
            </div>
            <label class="col-sm-2">บาท</label>
          </div>
          <div class="form-group">
            <label class="col-sm-6">ราคาที่ต้องชำระ</label>
            <div class="col-sm-3">
              <p class="text-right"><?php echo number_format($order->grand_total, 2); ?></p>
            </div>
            <label class="col-sm-2">บาท</label>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">ข้อมูลผู้จัดส่งสินค้า</div>
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-3 control-label">ชื่อผู้รับ</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->shipping_name; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">ที่อยู่</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->shipping_address; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">เขต/ตำบล</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->shipping_district; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">แขวง/อำเภอ</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->shipping_amphur; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">จังหวัด</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->shipping_province; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">ไปรษณีย์</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->shipping_post_code; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">โทรศัพท์</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->shipping_mobile; ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">ข้อมูลออกใบกำกับภาษี</div>
        <div class="panel-body">
          <div class="form-group">
            <label class="col-sm-3 control-label">ชื่อ</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->tax_name; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">ที่อยู่</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->tax_address; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">เขต/ตำบล</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->tax_district; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">แขวง/อำเภอ</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->tax_amphur; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">จังหวัด</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->tax_province; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">ไปรษณีย์</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->tax_post_code; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">Tax id</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->tax_code; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">สาขา</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->tax_branch; ?></p>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label">โทรศัพท์</label>
            <div class="col-sm-9">
              <p class="form-control-static"><?php echo $order->tax_mobile; ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

    <div class="panel panel-default">
      <div class="panel-heading">หมายเหตุ</div>
      <div class="panel-body">
        <p class="form-control-static"><?php echo $order->remark; ?></p>
      </div>
    </div>
</div>
<?php
$this->load->view('order/detail/delete_popup');
$this->load->view('order/detail/pdf_popup');