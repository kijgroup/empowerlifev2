<table style="display:none;">
  <tbody id="table_order_item">
    <tr class="row_item">
      <td>
        <select class="form-control product_id" name="product_id[]">
          <option value="0" disabled selected>โปรดระบุ</option>
          <?php
          $product_list = $this->Product_model->get_list();
          foreach($product_list->result() as $product){
            echo '<option value="'.$product->product_id.'" data-price="'.$product->price.'" >'.$product->name_th.'</option>';
          }
          ?>
        </select>
      </td>
      <td><input type="text" class="form-control text-right qty" name="qty[]" value="0" /></td>
      <td><input type="text" class="form-control text-right price_per_item" name="price_per_item[]" value="0" /></td>
      <td class="text-right price_total">0</td>
      <td class="text-center"><button type="button" class="btn btn-danger btn-remove-product"><i class="fa fa-trash"></i> Remove</button></td>
    </tr>
  </tbody>
</table>
