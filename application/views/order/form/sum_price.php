<div class="panel panel-default">
  <div class="panel-body">
    <div class="form-group">
      <label class="col-sm-6 control-label">จำนวนสินค้า</label>
      <div class="col-sm-6">
        <div class="input-group">
          <input type="text" class="form-control text-right" name="total_item" id="total_item" value="<?php echo ($order_id != 0)?$order->total_item:'0'; ?> " readonly>
          <span class="input-group-addon">ชิ้น</span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-6 control-label">รวมราคา</label>
      <div class="col-sm-6">
        <div class="input-group">
          <input type="text" class="form-control text-right" name="item_price" id="item_price" value="<?php echo ($order_id !=0)? $order->item_price:'0'; ?>" readonly>
          <span class="input-group-addon">บาท</span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-6 control-label">ส่วนลดจากการแลกคะแนน</label>
      <div class="col-sm-6">
        <div class="input-group">
          <input type="text" class="form-control text-right" name="discount_by_point_amount" id="discount_by_point_amount" value="<?php echo ($order_id !=0)? $order->discount_by_point_amount:'0'; ?>" readonly>
          <span class="input-group-addon">บาท</span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-6 control-label">ส่วนลด (<span id="calc_discount_amount"></span>)</label>
      <div class="col-sm-6">
        <div class="input-group">
          <input type="text" class="form-control text-right" name="discount_price" id="discount_price" value="<?php echo ($order_id !=0)? $order->discount_price:'0'; ?>">
          <span class="input-group-addon">บาท</span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-6 control-label">ค่าส่ง</label>
      <div class="col-sm-6">
        <div class="input-group">
          <input type="text" class="form-control text-right" name="shipping_price" id="shipping_price" value="<?php echo ($order_id !=0)? $order->shipping_price:'0'; ?>">
          <span class="input-group-addon">บาท</span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-6 control-label">ราคาที่ต้องชำระ <span class="required">*</span></label>
      <div class="col-sm-6">
        <div class="input-group">
          <input type="text" class="form-control text-right" name="grand_total" id="grand_total" value="<?php echo ($order_id !=0)? $order->grand_total:'0'; ?>" readonly>
          <span class="input-group-addon">บาท</span>
        </div>
      </div>
    </div>
  </div>
</div>
