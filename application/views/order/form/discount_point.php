<div class="panel panel-default">
  <div class="panel-heading">คะแนนแลกส่วนลด</div>
  <div class="panel-body">
    <?php
    $discount_point_list = $this->Discount_point_model->get_list();
    foreach($discount_point_list->result() as $discount_point){
      $input_id = 'discount_point_'.$discount_point->discount_point_id;
      $qty = $this->Order_discount_point_model->get_qty($order_id, $discount_point->discount_point_id);
      ?>
      <div class="form-group">
        <label for="<?php echo $input_id; ?>" class="col-sm-6 control-label"><?php echo $discount_point->use_point.' คะแนน แลก '.$discount_point->discount_amount; ?> บาท</label>
        <div class="col-sm-6">
          <div class="input-group">
            <input type="text" class="form-control text-right discount_point" name="<?php echo $input_id; ?>" id="<?php echo $input_id; ?>" data-use_point="<?php echo $discount_point->use_point; ?>" data-discount_amount="<?php echo $discount_point->discount_amount; ?>" value="<?php echo $qty; ?>" />
            <span class="input-group-addon">ครั้ง</span>
          </div>
        </div>
      </div>
      <?php
    }
    ?>
    <div class="form-group">
      <label class="col-sm-6 control-label">รวมคะแนนที่ใช้</label>
      <div class="col-sm-6">
        <div class="input-group">
          <input type="text" class="form-control text-right" id="discount_point_amount" name="discount_point_amount" value="<?php echo ($order_id != 0)?$order->discount_point_amount:0; ?>" readonly />
          <span class="input-group-addon">คะแนน</span>
        </div>
      </div>
    </div>
  </div>
</div>