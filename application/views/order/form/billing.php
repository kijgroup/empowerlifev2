<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลใบกำกับภาษี</div>
  <div class="panel-body">
    <div class="form-group">
      <label class="col-sm-4 control-label">ต้องการ TAX</label>
      <div class="col-sm-8">
        <select class="form-control" name="is_tax" id="is_tax">
          <option value="true">ต้องการ</option>
          <option value="false">ไม่ต้องการ</option>
        </select>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">ชื่อ-นามสกุล</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="tax_name" id="tax_name" value="<?php echo ($order_id !=0)? $order->tax_name:''; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">เลขบัตรประชาชน</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="tax_code" id="tax_code" value="<?php echo ($order_id !=0)? $order->tax_code:''; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">ที่อยู่</label>
      <div class="col-sm-8">
        <textarea type="text" class="form-control" name="tax_address" id="tax_address"><?php echo ($order_id !=0)? $order->tax_address:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">แขวง/ตำบล</label>
      <div class="col-sm-8">
        <div class="input-group">
          <input type="text" class="form-control" name="tax_district" id="tax_district" value="<?php echo ($order_id !=0)? $order->tax_district:''; ?>">
          <span class="input-group-btn">
            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#select_address_tax"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">เขต/อำเภอ</label>
      <div class="col-sm-8">
        <div class="input-group">
          <input type="text" class="form-control" name="tax_amphur" id="tax_amphur" value="<?php echo ($order_id !=0)? $order->tax_amphur:''; ?>">
          <span class="input-group-btn">
            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#select_address_tax"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">จังหวัด</label>
      <div class="col-sm-8">
        <div class="input-group">
          <input type="text" class="form-control" name="tax_province" id="tax_province" value="<?php echo ($order_id !=0)? $order->tax_province:''; ?>">
          <span class="input-group-btn">
            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#select_address_tax"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">รหัสไปรษณีย์</label>
      <div class="col-sm-8">
        <div class="input-group">
          <input type="text" class="form-control" name="tax_post_code" id="tax_post_code" value="<?php echo ($order_id !=0)? $order->tax_post_code:''; ?>">
          <span class="input-group-btn">
            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#select_address_tax"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">สาขา</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="tax_branch" id="tax_branch" value="<?php echo ($order_id !=0)? $order->tax_branch:''; ?>">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-4 control-label">โทรศัพท์</label>
      <div class="col-sm-8">
        <input type="text" class="form-control" name="tax_mobile" id="tax_mobile" value="<?php echo ($order_id != 0)?$order->tax_mobile:''; ?>">
      </div>
    </div>
  </div>
</div>
