<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลการสั่งซื้อ</div>
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">รหัสสั่งซื้อ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $order->order_code; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">print</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Order_print_status_model->get_label($order->is_print); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สมาชิก</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo anchor('member/detail/'.$order->member_id, $this->Member_model->get_name($order->member_id)); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">วันที่สั่งซื้อ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $order->create_date; ?> โดย <?php echo $this->Admin_model->get_login_name_by_id($order->create_by); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">แก้ไขล่าสุด</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $order->update_date; ?> โดย <?php echo $this->Admin_model->get_login_name_by_id($order->update_by); ?></p>
      </div>
    </div>
  </div>
</div>
<div class="panel">
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">ช่องทางการสั่งซื้อ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Order_channel_model->get_name($order->order_channel_id); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">Source</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Order_sub_channel_model->get_name($order->order_sub_channel_id); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">รายละเอียด Source</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $order->order_channel_text; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">Show</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Contact_channel_model->get_name($order->contact_channel_id); ?></p>
      </div>
    </div>
  </div>
</div>
<div class="panel panel-default">
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">วิธีการชำระเงิน</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Payment_type_model->get_label($order->payment_type); ?></p>
      </div>
    </div>
    <?php if($order->payment_type == 'post'){ ?>
    <div class="form-group">
      <label class="col-md-3 control-label">ชื่อสาขาไปรษณีย์</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $order->postoffice; ?></p>
      </div>
    </div>
    <?php } ?>
    <div class="form-group">
      <label class="col-md-3 control-label">วิธีการจัดส่ง</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Shipping_type_model->get_name($order->shipping_type_id); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ธนาคารที่ชำระเงิน</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php
        if($order->bank_id != 0){
          $bank = $this->Bank_model->get_data($order->bank_id);
          echo $bank->bank_name_th.'<p>'.$bank->bank_info_th.'</p>';
        }
        ?></p>
      </div>
    </div>
  </div>
</div>
<div class="panel">
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะการส่ง</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Order_status_model->get_label($order->order_status); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ตัด Stock</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Stock_model->get_name($order->stock_id); ?></p>
      </div>
    </div>
  </div>
</div>
<div class="panel">
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">ส่งเช้า</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Send_time_status_model->get_name($order->send_time_status); ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะการโอนเงิน</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $this->Payment_status_model->get_name($order->payment_status); ?></p>
      </div>
    </div>
  </div>
</div>