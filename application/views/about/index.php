<?php $this->load->view('about/slideshow'); ?>
<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $this->lang->line('nav_about'); ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $this->lang->line('nav_about'); ?></li>
    </ol>
    <div class="devider devider--bottom-xs"></div>
  </div>
</section>
<section class="container">
  <?php echo $about->page_content_th; ?>
</section>
