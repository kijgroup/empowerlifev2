<!-- Revolution full width slider -->
<div class="bannercontainer shop-banner">
  <div class="banner">
    <ul>
      <?php
      foreach($slideshow_list->result() as $slideshow){
        ?>
        <!-- Slide -->
        <li data-transition="fade" data-slotamount="3" data-text="<?php echo $slideshow->slideshow_text; ?>">
          <img src="<?php echo base_url('uploads/'.$slideshow->slideshow_image); ?>" alt="<?php echo $slideshow->slideshow_text; ?>">
        </li>
        <!-- end slide -->
        <?php
      }
      ?>
    </ul>
  </div>
</div>
