<header class="page-header">
<h2>สมาชิกเวบไซต์</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>สมาชิกเวบไซต์</span></li>
  </ol>
</div>
</header>
<div class="search-content">
    <div class="search-control-wrapper">
        <?php echo form_open('member/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'searchForm')); ?>
        <div class="form-group">
            <input type="text" class="form-control" name="txtSearchVal" id="txtSearchVal" placeholder="ข้อความที่ต้องการค้นหา" />
        </div>
        <div class="form-group">
          <select class="form-control" name="member_type_code" id="member_type_code">
            <option value="all" selected="selected">ทุกประเภทสมาชิก</option>
            <?php
            foreach($member_type_list->result() as $member_type){
              echo '<option value="'.$member_type->member_type_code.'">'.$member_type->member_type_name.'</option>';
            }
            ?>
          </select>
        </div>
        <div class="form-group">
          <select class="form-control" name="ddlEnableStatus" id="ddlEnableStatus">
            <option value="all" selected="selected">สถานะทั้งหมด</option>
            <option value="active">Active</option>
            <option value="inactive">Inactive</option>
          </select>
        </div>
        <div class="form-group">
          <select class="form-control" name="ddlSort" id="ddlSort">
            <option value="create_date">วันที่สร้าง</option>
            <option value="member_code">รหัสสมาชิก</option>
            <option value="member_firstname">ชื่อสมาชิก</option>
          </select>
        </div>
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> ค้นหา</button>
        <?php echo form_close(); ?>
    </div>
    <div class="tab-content pt-lg">
      <div class="pb-lg">
        <?php echo anchor('member/form', '<i class="fa fa-plus"></i> เพิ่มสมาชิก', array('class'=>'btn btn-success')); ?>
        <?php echo anchor('member/download_excel', '<i class="fa fa-download"></i> Export Excel', array('class'=>'btn btn-primary', 'target' => '_blank')); ?>
      </div>
      <div id="result">
      </div>
      <br />
    </div>
</div>
