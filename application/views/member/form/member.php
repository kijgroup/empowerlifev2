<div class="panel panel-default">
  <div class="panel-heading">ข้อมูลสมาชิก</div>
  <div class="panel-body">
    <?php if($member_id != 0){ ?>
    <div class="form-group">
      <label for="member_code" class="col-md-3 control-label">รหัสสมาชิก</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $member->member_code; ?></p>
      </div>
    </div>
    <?php } ?>
    <div class="form-group">
      <label class="col-md-3 control-label">ประเภทสมาชิก <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        $member_type_list = $this->Member_type_model->get_list();
        foreach($member_type_list->result_array() as $member_type){
          $checked = (($member_id != 0 && $member->member_type == $member_type['member_type_code']) OR ($member_id == 0 && $member_type['member_type_code'] == 'normal'))?'checked':'';
          echo '
          <div class="radio">
            <label>
              <input type="radio" name="member_type" id="member_type_'.$member_type['member_type_code'].'" value="'.$member_type['member_type_code'].'" '.$checked.'>
              '.$this->Member_type_model->get_label($member_type['member_type_code']).'
            </label>
          </div>';
        }
        ?>
      </div>
    </div>
    <div class="form-group">
      <label for="buy_total" class="col-md-3 control-label">ยอดซื้อสะสม <span class="required">*</span></label>
      <div class="col-md-6">
        <div class="input-group">
          <input type="text" class="form-control" name="buy_total" id="buy_total" value="<?php echo ($member_id != 0)?$member->buy_total:'0'; ?>" required>
          <span class="input-group-addon">บาท</span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="expire_date" class="col-md-3 control-label">วันที่หมดอายุ vip <span class="required">*</span></label>
      <div class="col-md-6">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          <input type="text" class="form-control form-control-date" name="expire_date" id="expire_date" value="<?php echo ($member_id != 0  && $member->member_type != 'normal')?$member->expire_date:date('Y-m-d', strtotime(" + 1 year")); ?>" required>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="member_point" class="col-md-3 control-label">แต้มสะสม <span class="required">*</span></label>
      <div class="col-md-6">
        <div class="input-group">
          <input type="text" class="form-control" name="member_point" id="member_point" value="<?php echo ($member_id != 0)?$member->member_point:'0'; ?>" required>
        <span class="input-group-addon">แต้ม</span>
      </div>
      </div>
    </div>
    <div class="form-group">
      <label for="member_firstname" class="col-md-3 control-label">ชื่อ <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="member_firstname" id="member_firstname" value="<?php echo ($member_id != 0)?$member->member_firstname:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="member_lastname" class="col-md-3 control-label">นามสกุล <span class="required">*</span></label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="member_lastname" id="member_lastname" value="<?php echo ($member_id != 0)?$member->member_lastname:''; ?>" required>
      </div>
    </div>
    <div class="form-group">
      <label for="member_gender" class="col-md-3 control-label">เพศ</label>
      <div class="col-md-6">
        <?php
        $gender_list = $this->Member_gender_model->get_list();
        foreach($gender_list as $gender){
          $checked = (($member_id != 0 && $member->member_gender == $gender['status_code']) OR ($member_id == 0 && $gender['status_code'] == 'undefined'))?'checked':'';
          echo '
          <div class="radio">
            <label>
              <input type="radio" name="member_gender" id="member_gender_'.$gender['status_code'].'" value="'.$gender['status_code'].'" '.$checked.'>
              '.$this->Member_gender_model->get_label($gender['status_code']).'
            </label>
          </div>';
        }
        ?>
      </div>
    </div>
    <div class="form-group">
      <label for="member_birthdate" class="col-md-3 control-label">วันเกิด</label>
      <div class="col-md-6">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
          <input type="text" class="form-control form-control-date" name="member_birthdate" id="member_birthdate" value="<?php echo ($member_id != 0)?$member->member_birthdate:''; ?>">
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="member_address" class="col-md-3 control-label">ที่อยู่ <span class="required">*</span></label>
      <div class="col-md-6">
        <textarea class="form-control" name="member_address" id="member_address"><?php echo ($member_id != 0)?$member->member_address:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label for="member_address" class="col-md-3 control-label">ตำบล/แขวง</label>
      <div class="col-md-6">
        <div class="input-group">
          <input type="text" class="form-control" name="member_district" id="member_district" value="<?php echo ($member_id != 0)?$member->member_district:''; ?>">
          <span class="input-group-btn">
            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#select_address"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="member_address" class="col-md-3 control-label">เขต/อำเภอ</label>
      <div class="col-md-6">
        <div class="input-group">
          <input type="text" class="form-control" name="member_amphur" id="member_amphur" value="<?php echo ($member_id != 0)?$member->member_amphur:''; ?>">
          <span class="input-group-btn">
            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#select_address"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="member_address" class="col-md-3 control-label">จังหวัด<span class="required">*</span></label>
      <div class="col-md-6">
        <div class="input-group">
          <input type="text" class="form-control" name="member_province" id="member_province" value="<?php echo ($member_id != 0)?$member->member_province:''; ?>">
          <span class="input-group-btn">
            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#select_address"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="member_postcode" class="col-md-3 control-label">รหัสไปรษณีย์ <span class="required">*</span></label>
      <div class="col-md-6">
        <div class="input-group">
          <input type="text" class="form-control" name="member_postcode" id="member_postcode" value="<?php echo ($member_id != 0)?$member->member_postcode:''; ?>">
          <span class="input-group-btn">
            <a href="#" class="btn btn-info" data-toggle="modal" data-target="#select_address"><i class="fa fa-search"></i></a>
          </span>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label for="member_phone" class="col-md-3 control-label">เบอร์โทรศัพท์</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="member_phone" id="member_phone" value="<?php echo ($member_id != 0)?$member->member_phone:''; ?>">
      </div>
    </div>
    <div class="form-group">
      <label for="member_mobile" class="col-md-3 control-label">เบอร์มือถือ</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="member_mobile" id="member_mobile" value="<?php echo ($member_id != 0)?$member->member_mobile:''; ?>">
      </div>
    </div>
    <div class="form-group">
      <label for="member_email" class="col-md-3 control-label">อีเมล</label>
      <div class="col-md-6">
        <input type="text" class="form-control" name="member_email" id="member_email" value="<?php echo ($member_id != 0)?$member->member_email:''; ?>">
      </div>
    </div>
  </div>
</div>
