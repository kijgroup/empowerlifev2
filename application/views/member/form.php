<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span><?php echo anchor('member','สมาชิกเวบไซต์'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<?php echo form_open('member/form_post/'.$member_id, array('role' => 'form', 'class' => 'form-horizontal' , 'id' => 'member-form'));
$this->load->view('member/form/member');
$this->load->view('member/form/user');
?>
<div class="panel panel-default">
  <div class="panel-heading">รายละเอียดเพิ่มเติม</div>
  <div class="panel-body">
    <div class="form-group">
      <label class="col-md-3 control-label">หมายเหตุ</label>
      <div class="col-md-6">
        <textarea class="form-control" name="note" id="note"><?php echo ($member_id != 0)?$member->note:''; ?></textarea>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ <span class="required">*</span></label>
      <div class="col-md-6">
        <?php
        $enable_status_list = $this->Member_enable_status_model->get_list();
        foreach($enable_status_list as $enable_status){
          $checked = (($member_id != 0 && $member->enable_status == $enable_status['status_code']) OR ($member_id == 0 && $enable_status['status_code'] == 'active'))?'checked':'';
          echo '
          <div class="radio">
            <label>
              <input type="radio" name="enable_status" id="enable_status_'.$enable_status['status_code'].'" value="'.$enable_status['status_code'].'" '.$checked.'>
              '.$this->Member_enable_status_model->get_label($enable_status['status_code']).'
            </label>
          </div>';
        }
        ?>
      </div>
    </div>
  </div>
  <div class="panel-footer">
    <div class="row">
      <div class="col-md-offset-3 col-md-6">
        <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-save"></i> บันทึก</button>
        <a class="btn btn-warning" href="#" onClick="window.history.go(-1); return false;"><i class="glyphicon glyphicon-remove"></i> ยกเลิก</a>
      </div>
    </div>
  </div>
</div>
<?php
echo form_close();
$this->load->view('address/popup', array(
  'popup_name' => 'select_address',
  'province_field' => 'member_province',
  'amphur_field' => 'member_amphur',
  'district_field' => 'member_district',
  'postcode_field' => 'member_postcode',
));

