<!-- Preview slider main area -->
<div id="slideshow"  class="slideshow slidershow-large" style="border:none;"></div>
<!-- Preview slider variant area -->
<div id="thumbs" class="thumbs--full" style="display: block;">
  <ul class="thumbs">
    <?php
    foreach($product_image_list->result() as $product_image){
      ?>
      <li>
        <a class="thumb" href="<?php echo base_url('uploads/'.$product_image->main_image); ?>">
          <div class="img-container">
            <img alt="" src="<?php echo base_url('uploads/'.$product_image->thumb_image); ?>" />
          </div>
        </a>
      </li>
      <?php
    }
    ?>
  </ul>
  <div class="bottom pagination"></div>
</div>
<!-- emd preview slider -->
