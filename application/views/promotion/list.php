<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $this->lang->line('nav_promotion'); ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $this->lang->line('nav_promotion'); ?></li>
    </ol>
    <div class="devider devider--bottom-sm"></div>
  </div>
</section>
<section class="container">
  <h3 class="not-visible">Main container</h3>
  
  <div class="row">
    <div class="col-sm-3">
      <?php $this->load->view('promotion/list/sidemenu'); ?>
    </div>
    <div class="col-sm-9">
      <?php $this->load->view('promotion/list/content'); ?>
    </div>
  </div>
</section>
