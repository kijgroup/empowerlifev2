<div class="shop-view">
  <span class="shop-view__results"><?php echo $total_product; ?> results</span>
</div>
<div class="row product-wrapper">
  <?php
  foreach($product_list->result() as $product){
    $this->load->view('promotion/list/thumb', array('product'=>$product));
  }
  ?>
</div>
<?php
$this->load->view('promotion/list/pager');
