<?php
$detail_url= site_url('promotion/detail/'.$product->product_id.'/'.$product->slug);
$thumb_image = '';
if($product->main_image != ''){
  $thumb_image = base_url('uploads/'.$product->thumb_image);
}else{
  $thumb_image = 'http://placehold.it/600x600';
}
$col_width = (isset($col_width))?$col_width:4;
?>
<div class="col-sm-6 col-md-<?php echo $col_width; ?>">
  <!-- Product preview -->
  <div class="product product--round product--flow" style="position:relative;">
    <a href="<?php echo $detail_url; ?>" class="product__photo">
      <img src="<?php echo $thumb_image; ?>" alt="">
    </a>
    <a class="bar-add-cart" href="<?php echo site_url('cart/add_cart/'.$product->product_id.'/1/list'); ?>">
      <i class="fa fa-shopping-basket"></i> ใส่ตะกร้า
    </a>
    <a href="<?php echo $detail_url; ?>" style="padding-top:15px;border-top:1px solid #dfe6e7;display:block;">
      <span class="product__title" style="margin-top:0;"><?php echo $this->Product_model->return_name_lang($product, $this->config->item('language_abbr')); ?></span>
      <?php
      $price_class = '';
      if($product->price_before_discount > $product->price){
        $price_class = 'text-red';
        echo '<p class="product__price-discount">';
        if($product->price_discount_rate > 0){
          echo '-'.$product->price_discount_rate.'%';
        }
        ?>
        <span class="slash-red"><?php echo number_format($product->price_before_discount); ?></span></p>
        <?php
      }
      ?>
      <p class="product__price <?php echo $price_class; ?>"><?php echo number_format($product->price); ?>.-</p>
    </a>
    <?php if($product->is_expire == 'true'){ ?>
      <div style="position: absolute;bottom: 0;left: 0;right: 0;margin-bottom: -10px;">
        <p style="text-align:center;color:red;"><?php echo $this->lang->line('expire_in') ?> <span class="promotion_countdown" data-expire="<?php echo $product->expire_date; ?>"></span></p>
      </div>
      <?php } ?>
  </div>
  <!-- end product preview -->
</div>
