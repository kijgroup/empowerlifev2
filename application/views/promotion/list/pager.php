<!-- Rectangle shapes -->
<?php
$link_url = 'promotion/index/'.$category_code.'/?page=';
$current_page = $page;
$total_page = ($total_product > 0)?ceil($total_product/$per_page):0;
$prev_page = $current_page - 1;
$next_page = $current_page + 1;
$start_page = $current_page - 2;
$start_page = ($start_page > 1)?$start_page:1;
$end_page = $current_page + 2;
$end_page = ($end_page < $total_page)?$end_page:$total_page;
?>
<div class="pagination pagination--rect devider--top-large">
  <?php
  if($prev_page > 0){
    echo anchor($link_url.$prev_page, '<i class="fa fa-angle-left"></i>', array('class' => 'pagination__prev'));
  }
  ?>
  <div class="pagination__block">
    <?php
    for($page_index = $start_page; $page_index <= $end_page; $page_index++){
      $class = ($page_index == $current_page)?'active-page':'';
      $class = (abs($page_index - $current_page) == 1)?'mobile-small':$class;
      $class = (abs($page_index - $current_page) == 2)?'mobile-large':$class;
      echo anchor($link_url.$page_index, $page_index, array('class'=>'pagination__item '.$class));
      echo ' ';
    }
    ?>
  </div>
  <?php
  if($next_page <= $total_page){
    echo anchor($link_url.$next_page, '<i class="fa fa-angle-right"></i>', array('class' => 'pagination__next'));
  }
  ?>
</div>
