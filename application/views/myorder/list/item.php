<?php
$order_status_col = '';
switch($order->order_status){
  case 'open':
  case 'confirm':
    $order_status_col = '<td class="table__wait"><i class="fa fa-spinner"></i> Pending</td>';
    break;
  case 'complete':
    $order_status_col = '<td class="table__done"><i class="fa fa-check"></i> Complete</td>';
    break;
  case 'cancel':
    $order_status_col = '<td class="table__error"><i class="fa fa fa-times"></i> Cancel</td>';
    break;
}
$detail_id_name = 'collapse'.$order->order_id;
?>
<tr class="toggle_anchor" data-target="#<?php echo $detail_id_name; ?>" style="cursor:pointer">
  <td><?php echo $order->order_code; ?></td>
  <td><?php echo $this->Datetime_service->display_datetime($order->create_date); ?></td>
  <td class="text-right"><?php echo number_format($order->grand_total, 2); ?></td>
  <?php echo $order_status_col; ?>
  <td>
    <?php echo anchor('myorder/detail/'.$order->order_code, $this->lang->line('myorder_item_detail'), array('class'=>'btn btn-primary btn-sm')); ?>
  </td>
</tr>
<tr>
  <td colspan="5" style="padding:0;">
    <div id="<?php echo $detail_id_name; ?>" style="display:none; padding:10px;background:#dfe6e7;">
      <table class="table" style="margin:0;">
        <thead style="background:#dfe6e7;border:none;">
          <tr>
            <th style="color:#737c85;width:10px;">#</th>
            <th style="color:#737c85;"><?php echo $this->lang->line('myorder_order_item_product'); ?></th>
            <th style="color:#737c85;"><?php echo $this->lang->line('myorder_order_item_unit'); ?></th>
            <th style="color:#737c85;"><?php echo $this->lang->line('myorder_order_item_total'); ?></th>
          </tr>
        </thead>
        <tbody>
          <?php
          $order_item_list = $this->Order_item_model->get_list($order->order_id);
          $index = 1;
          foreach($order_item_list->result() as $order_item){
            $product = $this->Product_model->get_data($order_item->product_id);
            $thumb_image = '';
            if($product->thumb_image != ''){
              $thumb_image = base_url('uploads/'.$product->thumb_image);
            }else{
              $thumb_image = 'http://placehold.it/600x600';
            }
            ?>
            <tr>
              <td><?php echo $index++; ?></td>
              <td class="table__item">
                <img src="<?php echo $thumb_image; ?>" alt="" style="max-width:50px;" class="product__thumb" />
                <p class="product__shortname"><?php echo $this->Product_model->return_name_lang($product, $this->config->item('language_abbr')); ?></p>
              </td>
              <td class="text-right"><?php echo number_format($order_item->qty); ?></td>
              <td class="text-right"><?php echo number_format($order_item->price_total); ?> ฿</td>
            </tr>
            <?php
          }
          ?>
          <tr>
            <td></td>
            <td class="checkout__result" colspan="2"><?php
              echo 'ใช้ '.$order->discount_point_amount.' แต้มแลก '.$order->discount_by_point_amount.' บาท';
            ?></td>
            <td class="text-right">- <?php echo number_format($order->discount_by_point_amount); ?> ฿</td>
          </tr>
          <tr>
            <td></td>
            <td class="checkout__result" colspan="2"><?php echo $this->lang->line('myorder_order_item_discount'); ?></td>
            <td class="text-right">- <?php echo number_format($order->discount_price); ?> ฿</td>
          </tr>
          <tr>
            <td></td>
            <td class="checkout__result" colspan="2">ค่าส่งสินค้า</td>
            <td class="text-right"><?php echo number_format($order->shipping_price); ?> ฿</td>
          </tr>
          <tr>
            <td></td>
            <td class="checkout__result" colspan="2">รวม</td>
            <td class="text-right checkout__total"><?php echo number_format($order->grand_total); ?> ฿</td>
          </tr>
        </tbody>
      </table>
    </div>
  </td>
</tr>
