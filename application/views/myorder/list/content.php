<div class="table-responsive">
  <table class="table table-bordered table--wide table-present">
    <colgroup class="col-sm-width"></colgroup>
    <colgroup class="col-sm-width"></colgroup>
    <colgroup class="col-sm-width"></colgroup>
    <colgroup class="col-sm-width"></colgroup>
    <colgroup class="col-sm-width"></colgroup>
    <colgroup class="col-sm-width"></colgroup>
    <thead>
      <tr>
        <th><?php echo $this->lang->line('myorder_content_order_id'); ?></th>
        <th><?php echo $this->lang->line('myorder_content_date'); ?></th>
        <th><?php echo $this->lang->line('myorder_content_total'); ?></th>
        <th><?php echo $this->lang->line('myorder_content_status'); ?></th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($order_list->result() as $order){
        $this->load->view('myorder/list/item', array('order' => $order));
      }
      ?>
    </tbody>
  </table>
</div>
