<?php
$order_item_list = $this->Order_item_model->get_list($order->order_id);
?>
<div class="table-responsive">
  <table class="table table-bordered table--wide table-present">
    <colgroup class="col-third"></colgroup>
    <colgroup class="col-thin"></colgroup>
    <colgroup class="col-thin"></colgroup>
    <colgroup class="col-thin"></colgroup>
    <thead>
      <tr>
        <th><?php echo $this->lang->line('myorder_order_item_product'); ?></th>
        <th><?php echo $this->lang->line('myorder_order_item_price'); ?></th>
        <th><?php echo $this->lang->line('myorder_order_item_unit'); ?></th>
        <th><?php echo $this->lang->line('myorder_order_item_total'); ?></th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($order_item_list->result() as $order_item){
        $product = $this->Product_model->get_data($order_item->product_id);
        $thumb_image = '';
        if($product->thumb_image != ''){
          $thumb_image = base_url('uploads/'.$product->thumb_image);
        }else{
          $thumb_image = 'http://placehold.it/600x600';
        }
        ?>
        <tr>
          <td class="table__item">
            <img src="<?php echo $thumb_image; ?>" alt="" style="max-width:50px;" class="product__thumb" />
            <p class="product__shortname"><?php echo $this->Product_model->return_name_lang($product, $this->config->item('language_abbr')); ?></p>
          </td>
          <td class="text-right"><?php echo number_format($order_item->price_per_item); ?> ฿</td>
          <td class="text-right"><?php echo number_format($order_item->qty); ?></td>
          <td class="text-right"><?php echo number_format($order_item->price_total); ?> ฿</td>
        </tr>
        <?php
      }
      ?>
      <tr>
        <td class="checkout__result" colspan="3"><?php
          echo 'ใช้ '.$order->discount_point_amount.' แต้มแลก '.$order->discount_by_point_amount.' บาท';
        ?></td>
        <td class="text-right">- <?php echo number_format($order->discount_by_point_amount); ?> ฿</td>
      </tr>
      <tr>
        <td class="checkout__result" colspan="3"><?php echo $this->lang->line('myorder_order_item_discount'); ?></td>
        <td class="text-right">- <?php echo number_format($order->discount_price); ?> ฿</td>
      </tr>
      <tr>
        <td class="checkout__result" colspan="3">ค่าส่งสินค้า</td>
        <td class="text-right"><?php echo number_format($order->shipping_price); ?> ฿</td>
      </tr>
    </tbody>
  </table>
  <table class="table-info">
    <colgroup class="col-section-sm"></colgroup>
    <colgroup class="col-section-lg"></colgroup>
    <tbody>
      <tr>
        <td class="checkout__result"><?php echo $this->lang->line('myorder_order_item_grand_total'); ?></td>
        <td class="checkout__total"><?php echo number_format($order->grand_total); ?> ฿</td>
      </tr>
    </tbody>
  </table>
</div>
