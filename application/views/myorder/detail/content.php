<div>
  <?php
  if($order->order_status == 'open'){
    echo anchor('payment?order_code='.$order->order_code,'แจ้งชำระเงิน', array('class'=>'btn btn-info btn--decorated'));
  }
  ?>
  <table class="table table--vertical table--space">
    <colgroup class="col-width-one"></colgroup>
    <colgroup class="col-width-two"></colgroup>
    <tbody>
      <tr>
        <td class="vertical-heading"><?php echo $this->lang->line('myorder_content_order_id'); ?></td>
        <td><?php echo $order->order_code; ?></td>
      </tr>
      <tr>
        <td class="vertical-heading"><?php echo $this->lang->line('myorder_content_status'); ?></td>
        <td><?php echo $order->order_status; ?></td>
      </tr>
      <tr>
        <td class="vertical-heading"><?php echo $this->lang->line('myorder_content_order_date'); ?></td>
        <td><?php echo $this->Datetime_service->display_datetime($order->create_date, $this->config->item('language_abbr')); ?></td>
      </tr>
    </tbody>
  </table>
</div>
<?php
$this->load->view('myorder/detail/order_item_list');
$this->load->view('myorder/detail/shipping_address');
$this->load->view('myorder/detail/tax_address');
?>
