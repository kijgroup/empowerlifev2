<?php
if($order->tax_code != ''){
  ?>
  <div>
    <h5><?php echo $this->lang->line('myorder_tax'); ?></h5>
    <table class="table table--vertical table--space">
      <colgroup class="col-width-one"></colgroup>
      <colgroup class="col-width-two"></colgroup>
      <tbody>
        <tr>
          <td class="vertical-heading"><?php echo $this->lang->line('myorder_tax_name'); ?></td>
          <td><?php echo $order->tax_name; ?></td>
        </tr>
        <tr>
          <td class="vertical-heading"><?php echo $this->lang->line('myorder_tax_address'); ?></td>
          <td><?php echo $order->tax_address; ?></td>
        </tr>
        <tr>
          <td class="vertical-heading"><?php echo $this->lang->line('myorder_tax_district'); ?></td>
          <td><?php echo $order->tax_district; ?></td>
        </tr>
        <tr>
          <td class="vertical-heading"><?php echo $this->lang->line('myorder_tax_amphur'); ?></td>
          <td><?php echo $order->tax_amphur; ?></td>
        </tr>
        <tr>
          <td class="vertical-heading"><?php echo $this->lang->line('myorder_tax_province'); ?></td>
          <td><?php echo $order->tax_province; ?></td>
        </tr>
        <tr>
          <td class="vertical-heading"><?php echo $this->lang->line('myorder_tax_post_code'); ?></td>
          <td><?php echo $order->tax_post_code; ?></td>
        </tr>
        <tr>
          <td class="vertical-heading"><?php echo $this->lang->line('myorder_tax_tax_id'); ?></td>
          <td><?php echo $order->tax_code; ?></td>
        </tr>
        <tr>
          <td class="vertical-heading"><?php echo $this->lang->line('myorder_tax_branch'); ?></td>
          <td><?php echo $order->tax_branch; ?></td>
        </tr>
        <tr>
          <td class="vertical-heading"><?php echo $this->lang->line('myorder_tax_mobile'); ?></td>
          <td><?php echo $order->tax_mobile; ?></td>
        </tr>
      </tbody>
    </table>
  </div>
  <?php
}
