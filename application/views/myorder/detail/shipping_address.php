<div>
  <h5><?php echo $this->lang->line('myorder_shipping'); ?></h5>
  <table class="table table--vertical table--space">
    <colgroup class="col-width-one"></colgroup>
    <colgroup class="col-width-two"></colgroup>
    <tbody>
      <tr>
        <td class="vertical-heading"><?php echo $this->lang->line('myorder_shipping_name'); ?></td>
        <td><?php echo $order->shipping_name; ?></td>
      </tr>
      <tr>
        <td class="vertical-heading"><?php echo $this->lang->line('myorder_shipping_moblie'); ?></td>
        <td><?php echo $order->shipping_mobile; ?></td>
      </tr>
      <tr>
        <td class="vertical-heading"><?php echo $this->lang->line('myorder_shipping_address'); ?></td>
        <td><?php echo $order->shipping_address; ?></td>
      </tr>
      <tr>
        <td class="vertical-heading"><?php echo $this->lang->line('myorder_shipping_district'); ?></td>
        <td><?php echo $order->shipping_district; ?></td>
      </tr>
      <tr>
        <td class="vertical-heading"><?php echo $this->lang->line('myorder_shipping_amphur'); ?></td>
        <td><?php echo $order->shipping_amphur; ?></td>
      </tr>
      <tr>
        <td class="vertical-heading"><?php echo $this->lang->line('myorder_shipping_province'); ?></td>
        <td><?php echo $order->shipping_province; ?></td>
      </tr><tr>
        <td class="vertical-heading"><?php echo $this->lang->line('myorder_shipping_post_code'); ?></td>
        <td><?php echo $order->shipping_post_code; ?></td>
      </tr>
    </tbody>
  </table>
</div>
