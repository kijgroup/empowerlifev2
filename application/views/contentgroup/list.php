<header class="page-header">
<h2>ประเภทบทความ</h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>บทความ</span></li>
    <li><span>ประเภทบทความ</span></li>
  </ol>
</div>
</header>
<div class="search-content">
  <div class="tab-content pt-lg">
    <div class="pb-lg">
      <?php echo anchor('contentgroup/form', '<i class="fa fa-plus"></i> เพิ่มประเภทบทความ', array('class'=>'btn btn-success')); ?>
    </div>
    <div class="table-responsive">
      <table class="table table-striped">
        <thead>
            <tr>
              <th>ชื่อประเภท (th)</th>
              <th>ชื่อประเภท (en)</th>
              <th>slug</th>
              <th class="text-center">บทความ</th>
              <th>สถานะ</th>
              <th></th>
            </tr>
        </thead>
        <tbody>
          <?php
          foreach($content_group_list->result() as $content_group){
            $form_url = 'contentgroup/form/'.$content_group->content_group_id;
            $delete_url = 'contentgroup/delete/'.$content_group->content_group_id;
            ?>
            <tr>
              <td><?php echo anchor($form_url, $content_group->name_th); ?></td>
              <td><?php echo anchor($form_url, $content_group->name_en); ?></td>
              <td><?php echo $content_group->slug; ?></td>
              <td class="text-center"><?php echo $this->Content_group_model->count_content($content_group->content_group_id); ?></td>
              <td>
                <?php echo ($content_group->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ไม่แสดง</span>'; ?>
              </td>
              <td>
                <?php echo anchor($delete_url, '<i class="fa fa-trash"></i> ลบข้อมูล', array('class'=>'btn btn-sm btn-danger')); ?>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
    <br />
  </div>
</div>
