<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>ของรางวัล</th>
        <th class="text-right" style="width:100px;">จำนวน</th>
        <th class="text-right" style="width:150px;">แต้ม</th>
        <th class="text-right" style="width:150px;">รวมแต้ม</th>
        <th style="width:120px;"></th>
      </tr>
    </thead>
    <tbody id="reward_item_list">
      <?php
      if($reward_transaction_item_list){
        foreach($reward_transaction_item_list->result() as $reward_transaction_item){
          $this->load->view('reward_transaction/form/item', array('reward_transaction_item' => $reward_transaction_item));
        }
      }
      ?>
    </tbody>
  </table>
</div>
<div class="pb-sm">
  <button type="button" class="btn btn-primary" id="btn-plus"><i class="fa fa-plus"></i> เพิ่มรายการ</button>
</div>
