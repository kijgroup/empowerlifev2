<tr class="row_item">
  <td>
    <select class="form-control reward_id" name="reward_id[]">
      <?php
      $reward_list = $this->Reward_model->get_list();
      foreach($reward_list->result() as $reward){
        $selected = ($reward_transaction_item->reward_id == $reward->reward_id)?'selected="selected"':'';
        echo '<option value="'.$reward->reward_id.'" '.$selected.' data-point="'.$reward->reward_point.'">'.$reward->name_th.'</option>';
      }
      ?>
    </select>
  </td>
  <td><input type="text" class="form-control text-right qty" name="qty[]" value="<?php echo $reward_transaction_item->qty; ?>" /></td>
  <td><input type="text" class="form-control text-right reward_point" name="reward_point[]" value="<?php echo $reward_transaction_item->reward_point; ?>" /></td>
  <td class="text-right total_reward_point"><?php echo number_format($reward_transaction_item->total_reward_point, 0); ?></td>
  <td class="text-center"><button type="button" class="btn btn-danger btn-remove-reward"><i class="fa fa-trash"></i> Remove</button></td>
</tr>
