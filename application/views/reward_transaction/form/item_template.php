<table style="display:none;">
  <tbody id="table_reward_item">
    <tr class="row_item">
      <td>
        <select class="form-control reward_id" name="reward_id[]">
          <option value="0" disabled selected>โปรดระบุ</option>
          <?php
          $reward_list = $this->Reward_model->get_list();
          foreach($reward_list->result() as $reward){
            echo '<option value="'.$reward->reward_id.'" data-point="'.$reward->reward_point.'" >'.$reward->name_th.'</option>';
          }
          ?>
        </select>
      </td>
      <td><input type="text" class="form-control text-right qty" name="qty[]" value="0" /></td>
      <td><input type="text" class="form-control text-right reward_point" name="reward_point[]" value="0" /></td>
      <td class="text-right total_reward_point">0</td>
      <td class="text-center"><button type="button" class="btn btn-danger btn-remove-reward"><i class="fa fa-trash"></i> Remove</button></td>
    </tr>
  </tbody>
</table>
