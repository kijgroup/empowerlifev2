<?php
if($total_transaction == 0){
    echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
    $total_page = ceil($total_transaction/$per_page);
    $pager_data = array();
    $pager_data['total_transaction'] = $total_transaction;
    $pager_data['search_val'] = $search_val;
    $pager_data['reward_transaction_status'] = $reward_transaction_status;
    $pager_data['per_page'] = $per_page;
    $pager_data['page'] = $page;
    $pager_data['total_page'] = $total_page;
    $pager_data['action'] = site_url('reward_transaction/filter');
?>
<div class="pb-lg">
  <button type="button" class="btn btn-primary" id="btn-download"><i class="glyphicon glyphicon-download"></i> ดาวน์โหลด PDF</button>
</div>
<div class="form-group">
  <?php $this->load->view('reward_transaction/list/pager', $pager_data); ?>
</div>
<?php echo form_open('reward_transaction/pdf_list', array('id'=>'print-form', 'target'=>'_blank')); ?>
<div class="table-responsive">
<table class="table table-striped table-bordered table-hover">
  <thead>
    <tr>
      <th width="50"><input type="checkbox" class="chk_all" onClick="selectall(this)"></th>
      <th>วันที่ส่งข้อมูล</th>
      <th>ชื่อผู้แลกของรางวัล</th>
      <th>ที่อยู่จัดส่งสินค้า</th>
      <th>แต้มแลก</th>
      <th>สถานะ</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach($reward_transaction_list->result() as $reward_transaction){
      $detail_url = 'reward_transaction/detail/'.$reward_transaction->reward_transaction_id;
      $member = $this->Member_model->get_data($reward_transaction->member_id);
      $member_name = $member->member_firstname.' '.$member->member_lastname.'('.$member->member_code.')';
      ?>
      <tr>
        <td><input type="checkbox" name="reward_transaction_id[]" value="<?php echo $reward_transaction->reward_transaction_id; ?>"></td>
        <td>
          <?php echo anchor($detail_url, $reward_transaction->create_date); ?>
        </td>
        <td>
          <?php echo anchor($detail_url, $member_name); ?>
        </td>
        <td>
          <div>ผู้รับสินค้า : <strong><?php echo $reward_transaction->shipping_name; ?></strong></div>
          <div>เบอร์โทร : <strong><?php echo $reward_transaction->shipping_mobile; ?></strong></div>
          <div><?php echo $reward_transaction->shipping_address.' '.$reward_transaction->shipping_district.' '.$reward_transaction->shipping_province.' '.$reward_transaction->shipping_post_code; ?></div>
        </td>
        <td>
          <?php echo anchor($detail_url, number_format($reward_transaction->total_reward_point, 0)); ?>
        </td>
        <td>
          <?php echo $this->Reward_transaction_status_model->get_label($reward_transaction->reward_transaction_status); ?>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>
</div>
<?php echo form_close(); ?>
<div class="form-group">
  <?php $this->load->view('reward_transaction/list/pager', $pager_data); ?>
</div>
<?php
}
