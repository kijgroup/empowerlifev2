<div class="search-control-wrapper">
  <?php echo form_open('reward_transaction/filter', array('role' => 'form', 'class' => 'form-inline', 'id' => 'searchForm')); ?>
  <div class="form-group">
    <input type="text" class="form-control" name="search_val" id="search_val" placeholder="ข้อความที่ต้องการค้นหา" />
  </div>
  <div class="form-group">
    <select class="form-control" name="reward_transaction_status" id="reward_transaction_status">
      <option value="all" selected="selected">สถานะทั้งหมด</option>
      <?php
      $status_list = $this->Reward_transaction_status_model->get_list();
      foreach($status_list as $status){
        echo '<option value="'.$status['status_code'].'">'.$status['status_name'].'</option>';
      }
      ?>
    </select>
  </div>
  <button class="btn btn-primary" type="submit"><i class="glyphicon glyphicon-filter"></i> ค้นหา</button>
  <?php echo form_close(); ?>
</div>
