<header class="page-header">
  <h2><?php echo $title; ?></h2>
  <div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
      <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
      <li><span><?php echo anchor('reward_transaction','รายการแลกของรางวัล'); ?></span></li>
      <li><span><?php echo $title; ?></span></li>
    </ol>
  </div>
</header>
<div class="pb-sm">
  <div class="btn-group hide-print">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
      การกระทำ <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" role="menu">
      <li><?php echo anchor('reward_transaction', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
      <li><?php echo anchor('reward_transaction/form/', '<i class="glyphicon glyphicon-plus"></i> สร้างรายการแลกของรางวัลใหม่'); ?></li>
      <li><?php echo anchor('reward_transaction/form/'.$reward_transaction->reward_transaction_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
      <li class="divider"></li>
      <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
    </ul>
  </div>
  <?php
  echo anchor('reward_transaction/print_pdf/'.$reward_transaction_id, '<i class="glyphicon glyphicon-download"></i> Download PDF', array('class' => 'btn btn-success', 'target'=>'_blank'));
  ?>
</div>
<div class="form-horizontal">
  <div class="row">
    <div class="col-lg-6">
      <?php $this->load->view('reward_transaction/detail/main_info'); ?>
    </div>
    <div class="col-lg-6">
      <?php $this->load->view('reward_transaction/detail/shipping_info'); ?>
    </div>
  </div>
  <?php
  $this->load->view('reward_transaction/detail/item_list');
  ?>
</div>
<?php
$this->load->view('reward_transaction/detail/delete_popup');
