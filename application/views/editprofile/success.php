<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<section class="body-error error-inside">
    <div class="center-error">
        <div class="main-error mb-xlg">
            <h2 class="error-code text-dark text-center text-weight-semibold m-none">Updated</h2>
            <p class="error-explanation text-center">ได้ทำการแก้ไขข้อมูลส่วนตัวของคุณเรียบร้อยแล้ว</p>
        </div>
    </div>
</section>