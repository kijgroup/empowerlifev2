<!doctype html>
<html>
  <head>
    <!-- Basic Page Needs -->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <meta http-equiv="description" name="description" content="<?php echo $this->Config_model->get_data('meta_description'); ?>">
    <meta http-equiv="keywords" name="keywords" content="<?php echo $this->Config_model->get_data('meta_keyword'); ?>">
    <meta name="author" content="Empowerlife.CO.,LTD">

    <!-- Mobile Specific Metas-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="telephone=no" name="format-detection">
    <?php
    if(isset($og_url)){
      echo '<meta property="og:url" content="'.$og_url.'" />';
    }
    if(isset($og_title)){
      echo '<meta property="og:title" content="'.$og_title.'" />';
    }
    if(isset($og_image)){
      echo '<meta property="og:image" content="'.$og_image.'" />';
    }
    if(isset($og_description)){
      echo '<meta property="og:description" content="'.$og_description.'" />';
    }
    ?>
    <meta property="og:type" content="website" />
    <link rel="shortcut icon" href="<?php echo base_url('favicon.ico'); ?>" type="image/x-icon"/>
    <!-- Fonts -->
    <link href="<?php echo base_url('assets/fonts/stylesheet.css?v=2'); ?>" rel="stylesheet">
    <!-- Open Sans -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400italic,600,700italic,400,700,800italic' rel='stylesheet' type='text/css'>
    <!-- VarelaRound -->
    <link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <!-- Icon Font - Font Awesome -->
    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Stylesheets -->
    <!-- External -->
    <!-- Slideshow -->
    <link href="<?php echo base_url('assets/external/jquery.bxslider/jquery.bxslider.css'); ?>" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url('assets/external/owl.carousel.2/assets/owl.carousel.css'); ?>" rel="stylesheet" type="text/css">
    <!-- Mobile menu -->
    <link href="<?php echo base_url('assets/external/z-nav/z-nav.css'); ?>" rel="stylesheet">
    <!-- Revolution slider -->
    <link href="<?php echo base_url('assets/external/rs-plugin/css/settings.css'); ?>" rel="stylesheet"  media="screen" />
    <!-- Touch slider - Swiper -->
    <link href="<?php echo base_url('assets/external/swiper/idangerous.swiper.css'); ?>" rel="stylesheet" />
    <!-- mCustomScrollbar -->
    <link href="<?php echo base_url('assets/external/mCustomScrollbar/jquery.mCustomScrollbar.css'); ?>" rel="stylesheet" />
    <!-- Magnific popup - responsive popup plugin -->
    <link href="<?php echo base_url('assets/external/magnific-popup/magnific-popup.css'); ?>" rel="stylesheet" />
    <!-- Popup -->
    <link href="<?php echo base_url('assets/external/fancybox/source/jquery.fancybox.css'); ?>" rel="stylesheet" type="text/css">
    <!-- Custom -->
    <?php $this->Masterpage_service->print_css(); ?>
    <link href="<?php echo base_url('assets/css/style.css?v=8'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/layout/layout.css?v=6'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/css/preloader.css'); ?>" rel="stylesheet" />
    <!-- Basic JavaScript-->
    <!-- Modernizr -->
    <script src="<?php echo base_url('assets/external/modernizr/modernizr.custom.js?v=1'); ?>"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>
    <![endif]-->

    <!--[if lte IE 9]>
      <link href="<?php echo base_url('assets/css/ie9.css'); ?>" rel="stylesheet" />
    <![endif]-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <!--script async src="https://www.googletagmanager.com/gtag/js?id=UA-37254122-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-37254122-1');
    </script-->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-110204815-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-110204815-1');
    </script>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-T6TPMHJ');</script>
    <!-- End Google Tag Manager -->
  </head>
  <body>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T6TPMHJ"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <input type="hidden" id="base_url" value="<?php echo base_url(); ?>" />
    <input type="hidden" id="site_url" value="<?php echo site_url(); ?>" />
    <input type="hidden" id="lang" value="<?php echo $this->config->item('language_abbr'); ?>" />
    <div class="wrapper" id="top">
      <?php $this->load->view('masterpage/header'); ?>
      <?php echo $content; ?>
      <?php $this->load->view('masterpage/footer'); ?>
    </div>
    <div class="animationload">
      <div class="preloader" id="loading">
        <div class="loading-dot"></div>
      </div>
    </div>
    <!-- JavaScript-->
    <!-- External-->
    <!-- jQuery 1.10.1-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script>
    window.jQuery || document.write('<script src="<?php echo base_url('assets/external/jquery/jquery-1.10.1.min.js'); ?>"><\/script>')
    </script>
    <!-- Bootstrap 3-->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- Mobile menu -->
    <script src="<?php echo base_url('assets/external/z-nav/jquery.mobile.menu.js'); ?>"></script>
    <!-- Touch slider - Swiper -->
    <script src="<?php echo base_url('assets/external/swiper/idangerous.swiper.js?v=1'); ?>"></script>
    <!-- Scroll to piugin -->
    <script src="<?php echo base_url('assets/external/scrollto/jquery.scrollTo.min.js?v=2'); ?>"></script>
    <!-- mCustomScrollbar -->
    <script src="<?php echo base_url('assets/external/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js'); ?>"></script>
    <!-- Magnific popup - responsive popup plugin -->
    <script src="<?php echo base_url('assets/external/magnific-popup/jquery.magnific-popup.min.js'); ?>"></script>
    <!-- Slideshow -->
    <script src="<?php echo base_url('assets/external/jquery.bxslider/jquery.bxslider.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/external/jquery.bxslider/plugins/jquery.easing.1.3.js'); ?>"></script>
    <script src="<?php echo base_url('assets/external/jquery.bxslider/plugins/jquery.fitvids.js'); ?>"></script>
    <script src="<?php echo base_url('assets/external/owl.carousel.2/owl.carousel.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/external/jquery.browser.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/external/fancybox/source/jquery.fancybox.js'); ?>"></script>
    <script src="<?php echo base_url('assets/external/noty/packaged/jquery.noty.packaged.min.js'); ?>" type="text/javascript"></script>
    <!-- Custom -->
    <script src="<?php echo base_url('assets/js/custom.js?v=2'); ?>"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="<?php echo base_url('assets/js/main.js?v=1'); ?>"></script>
    <script>
      $(document).ready(function() {
        cart();
				shopPopup();
        preloader();
      });
    </script>
    <!-- Facebook Pixel Code -->
    <!--script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '117210105551341'); 
    fbq('track', 'PageView');
    fbq('track', 'Contact');
    </script>
    <noscript>
    <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=117210105551341&ev=PageView
    &noscript=1"/>
    </noscript-->
    <!-- End Facebook Pixel Code -->
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window,document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '385949901868378'); 
    fbq('track', 'PageView');
    </script>
    <noscript>
    <img height="1" width="1" 
    src="https://www.facebook.com/tr?id=385949901868378&ev=PageView
    &noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    <?php $this->Masterpage_service->print_js(); ?>
  </body>
</html>
