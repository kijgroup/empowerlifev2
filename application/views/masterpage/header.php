<!-- Header section -->
<header class="header header--shop header--light">
  <div class="header-fixed">
    <div class="header-line header-line--shop waypoint" data-animate-down="header-up" data-animate-up="header-down">
      <div class="container">
        <!-- Contact information about company -->
        <address class="contact-info pull-right">
          <?php
          $lang_url = $this->Masterpage_service->change_lange_url();
          echo anchor($lang_url,$this->lang->line('switch_lang'), array('class'=>'contact-info__item', 'style'=>'padding-right:0; padding-left:10px;'));
          ?>
        </address>
        <!-- end contact information -->
        <ol class="breadcrumb breadcrumb--nav pull-right">
          <?php
          if($this->Login_service->get_login_status()){
            echo '<li>'.anchor('membership',$this->session->userdata('member_name')).'</li>';
            echo '<li>'.anchor('login/logout',$this->lang->line('nav_logout')).'</li>';
          }else{
            echo '<li>'.anchor('register',$this->lang->line('nav_register')).'</li>';
            echo '<li>'.anchor('login',$this->lang->line('nav_login')).'</li>';
          }
          ?>
        </ol>
      </div>
      <!-- end container -->
    </div>
    <div class="fixed-top header-down">
      <div class="container relative-element">
        <!--  Logo  -->
        <a class="logo logo--shop" href="<?php echo site_url(); ?>">
          <img src="<?php echo base_url('assets/images/logo-full.png?v=2'); ?>" alt="Empowerlife">
        </a>
        <!-- End Logo -->
        <!-- Navigation section -->
        <nav class="z-nav">
          <!-- Toggle for menu mobile view -->
          <a href="#" class="z-nav__toggle">
            <span class="menu-icon"></span>
            <span class="menu-text">navigation</span>
            <div class="menu-head"></div>
          </a>
          <ul class="z-nav__list z-nav--shop">
            <li class="z-nav__item"><?php echo anchor('', $this->lang->line('nav_home'), array('class'=>'z-nav__link'.(($nav == 'home')?' z-nav__link--active':''))); ?></li>
            <li class="z-nav__item hidden-sm hidden-md hidden-lg"><?php echo anchor('membership',$this->lang->line('nav_membership'), array('class'=>'z-nav__link')); ?></li>
            <li class="z-nav__item"><?php echo anchor('product', $this->lang->line('nav_product'), array('class'=>'z-nav__link'.(($nav == 'product')?' z-nav__link--active':''))); ?></li>
            <?php if($this->Product_model->is_remain_promotion()){ ?>
              <li class="z-nav__item"><?php echo anchor('promotion', $this->lang->line('nav_promotion'), array('class'=>'z-nav__link'.(($nav == 'promotion')?' z-nav__link--active':''))); ?></li>
            <?php } ?>
            <li class="z-nav__item"><?php echo anchor('payment', $this->lang->line('nav_payment'), array('class'=>'z-nav__link'.(($nav == 'payment')?' z-nav__link--active':''))); ?></li>
            <li class="z-nav__item"><?php echo anchor('content', $this->lang->line('nav_content'), array('class'=>'z-nav__link'.(($nav == 'content')?' z-nav__link--active':''))); ?></li>
            <li class="z-nav__item"><?php echo anchor('about', $this->lang->line('nav_about'), array('class'=>'z-nav__link'.(($nav == 'about')?' z-nav__link--active':''))); ?></li>
            <li class="z-nav__item"><?php echo anchor('contact', $this->lang->line('nav_contact'), array('class'=>'z-nav__link'.(($nav == 'contact')?' z-nav__link--active':''))); ?></li>
          </ul>
          <!-- end list menu item -->
        </nav>
        <!-- end navigation section -->
        <?php $this->load->view('masterpage/cartheader'); ?>
      </div>
      <!-- end container -->
    </div>
  </div>
</header>
<!-- end header section -->
