<!-- Footer space -->
<div class="bottom-space--md"></div>
<!-- Footer section -->
<footer class="footer footer--info footer--light">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div id="nav-social">
          <div class="social-list">
            <a href="https://www.facebook.com/zeoil" target="_blank" class="social-ico social-facebook"></a>
            <a href="http://line.me/ti/p/%40zeoil" target="_blank" class="social-ico social-line"></a>
            <a href="https://www.youtube.com/channel/UCLxp2nyF56lcTYgnXhtkAOg?src_vid=TL9RSRdFKeg&amp;feature=iv&amp;annotation_id=channel%3A547dc8bc-0000-23d7-969f-001a113b78a6" target="_blank" class="social-ico social-youtube"></a>
            <a href="https://instagram.com/zeoilgold/" target="_blank" class="social-ico social-instagram"></a>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- Latest post -->
      <div class="col-sm-3">
        <h3 class="heading-info"><?php echo $this->lang->line('footer_know_us'); ?></h3>
        <ul class="footer__list">
          <li><?php echo anchor('about', $this->lang->line('footer_know_us_about_us')); ?></li>
          <li><?php echo anchor('verify', $this->lang->line('footer_know_us_verify')); ?></li>
          <li><?php echo anchor('oem', $this->lang->line('footer_know_us_oem')); ?></li>
          <li><?php echo anchor('content/index/sharecare', $this->lang->line('footer_know_us_profit')); ?></li>
          <li><?php echo anchor('joinus', $this->lang->line('footer_know_us_careers')); ?></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h3 class="heading-info"><?php echo $this->lang->line('footer_need_help'); ?></h3>
        <ul class="footer__list">
          <li><?php echo anchor('faq', $this->lang->line('footer_need_help_faq')); ?></li>
          <li><?php echo anchor('contact', $this->lang->line('footer_need_help_contact')); ?></li>
          <li><?php echo anchor('send_policy', $this->lang->line('footer_need_help_send_policy')); ?></li>
          <li><?php echo anchor('return_policy', $this->lang->line('footer_need_help_return_policy')); ?></li>
          <li><?php echo anchor('quick_order',  $this->lang->line('footer_need_help_quick_order')); ?></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h3 class="heading-info"><?php echo $this->lang->line('footer_my_account'); ?></h3>
        <ul class="footer__list">
          <?php if($this->Login_service->get_login_status()){ ?>
          <li><?php echo anchor('membership', $this->lang->line('footer_my_account_membership')); ?></li>
          <?php }else{ ?>
          <li><?php echo anchor('login', $this->lang->line('footer_my_account_login')); ?></li>
          <?php } ?>
          <li><?php echo anchor('myorder', $this->lang->line('footer_my_account_myorder')); ?></li>
          <li><?php echo anchor('unsubscribe', $this->lang->line('footer_my_account_unsubscribe')); ?></li>
          <li><?php echo anchor('reward', $this->lang->line('footer_my_account_reward')); ?></li>
        </ul>
      </div>
      <div class="col-sm-3">
        <h3 class="heading-info" style="font-size:15px;"><?php echo $this->lang->line('footer_subscribe'); ?></h3>
        <div id="mc_embed_signup">
          <?php echo form_open('subscribe/form_post', array('class'=>'validate form form--name', 'id'=>'subscribe_form', 'data-successmsg'=>'<strong>เสร็จสิ้น</strong><br />ขอบคุณที่ติดตามข่าวสารกับ Empowerlife!')); ?>
            <div id="mc_embed_signup_scroll">
              <input type="email" value="" name="subscribe_email" class="subscribe_email form__input" id="mce-EMAIL" placeholder="<?php echo $this->lang->line('footer_subscribe_enter_email'); ?>" required>
              <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
              <div style="position: absolute; left: -5000px;"><input type="text" name="b_21c10d3734fc03bebe9a827ab_ea1d7bc533" tabindex="-1" value=""></div>
              <div class="clear"><button type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button form__submit"><span class="fa fa-arrow-circle-right"></span></button></div>
            </div>
          <?php echo form_close(); ?>
        </div>
        <div style="padding-top:20px;">
          <a href="javascript:void(0);" onclick="open_popup('https://www.trustmarkthai.com/callbackData/popup.php?data=abe41-38-5-7df023b29829d1f9e59d314093b874b82281520f');"><img height="37px" title="Registered" src="https://www.trustmarkthai.com/trust_banners/bns_registered.gif"></a><img height="37px" title="DHL" src="<?php echo base_url('assets/images/dhl-logo.png'); ?>">
        </div>
      </div>

      <!-- end social links -->
    </div><!-- end row -->
    <div class="copy" style="text-align:center;">
      <div style="padding-bottom:5px;">© 2012 Empowerlife Co.,Ltd. All rights reserved.</div>
      <div style="padding-bottom:5px;">หมายเหตุ * บริษัท เอ็มพาวเวอร์ไลฟ์ จำกัด เป็นบริษัทที่จำหน่ายผลิตภัณฑ์ Ze-Oil ซึ่งไม่มีการโฆษณาสรรพคุณของผลิตภัณฑ์ตามที่ อย. กำหนด<br>หากมีการโฆษณาสรรพคุณทางสื่อต่างๆ ทางบริษัทไม่ถือว่าเป็นการโฆษณาของบริษัท</div>
      <div>Designed by <a href="https://www.sakidlocode.com" target="_blank">SakidloCode</a></div>
    </div>

  </div><!-- end container -->
</footer>
<div class="modal modal-subscribe" id="modal-subscribe" tabindex="-1" role="dialog" aria-labelledby="modal_subscribe_label" style="display:none;">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modal_subscribe_label">สำเร็จ!</h4>
      </div>
      <div class="modal-body">
        <div class="modal-icon">
          <i class="fa fa-check"></i>
        </div>
        ขอบคุณที่รับติดตามข่าวสารจากทาง Empowerlife
      </div>
    </div>
  </div>
</div>
<!-- end footer section -->
<div class="top-scroll"><i class="fa fa-angle-up"></i></div>
<script type="text/javascript">
  function open_popup(a){
    window.open(a, 'DBD_certificate', 'width=700,height=720,scrollbars=no,location=no,resizable=no');
  }
</script>
