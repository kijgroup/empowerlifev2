<?php
$cart_product = $this->Product_model->get_data($product_id);
if($cart_product){
  $product_url = 'product/detail/'.$cart_product->product_id.'/'.$cart_product->slug;
  $product_name = $this->Product_model->return_name_lang($cart_product, $this->config->item('language_abbr'));
  $product_image_url = base_url('uploads/'.$cart_product->thumb_image);
  $product_image = '<img alt="" src="'.$product_image_url.'" />';
  $delete_cart_item_url = 'cart/delete_item/'.$product_id;
  ?>
  <!-- Shopping cart item -->
  <div class="cart__item">
    <?php echo anchor($product_url, $product_image, array('class'=>'cart__item-image')); ?>
    <?php echo anchor($product_url, $product_name, array('class'=>'cart__item-title')); ?>
    <p class="cart__item-price"><?php echo number_format($cart_product->price, 2); ?> ฿</p>
    <p class="cart__item-bonus">Quantity: <?php echo $quantity; ?></p>
    <?php echo anchor($delete_cart_item_url, '<i class="fa fa-times"></i>', array('class'=>'cart__close')); ?>
  </div>
  <!-- end Shopping cart item -->
  <?php
}
