<?php
$cart_items = $this->Cart_model->get_list();
$total_items = count($cart_items);
$items_price = $this->Cart_model->get_items_price();
?>
<style>
@media screen and (min-width: 769px) {
  .cart__list{
    max-height: 460px;
    overflow-y: auto;
  }
}
</style>
<!-- Shopping cart section -->
<div class='cart'>
  <!-- Toggle for menu mobile view -->
  <a href="#" class="cart__toggle">
      <span class="fa cart__toggle--icon"><img src="<?php echo base_url('assets\images\cart_icon.png'); ?>" alt="basket" style="" /></span>
      <div class="cart__head">
        <span class="cart__head__icon"><img src="<?php echo base_url('assets\images\cart_icon.png'); ?>" alt="basket" /></span>
          <span class="cart__text"><?php echo $this->lang->line('nav_cart'); ?> (<?php echo $total_items; ?>) - <span class="cart-highlight"><?php echo $items_price; ?> ฿</span></span>
      </div>
  </a>
  <!-- Shopping cart indecator -->
  <div class="cart__link">
      <i class="fa cart__icon"><img src="<?php echo base_url('assets\images\cart_icon.png'); ?>" alt="basket" style="width:19px;" /></i>
      <span class="cart__indecator"><?php echo $this->lang->line('nav_cart'); ?> (<?php echo $total_items; ?>) - <span class="cart-highlight"><?php echo $items_price; ?> ฿</span></span>
      <i class="fa fa-angle-down icon__more"></i>
  </div>
  <?php if($total_items > 0){ ?>
  <!-- Shopping cart list -->
  <div class='cart__list'>
    <?php
    foreach($cart_items as $cart_item){
      $this->load->view('masterpage/cartheader_item', $cart_item);
    }
    ?>
    <div class="checkout-wrap">
      <?php echo anchor('checkout', 'Check Out', array('class' => 'btn btn-info btn--decorated')); ?>
    </div>
  </div>
  <!-- end cart list-->
  <?php } ?>
</div>
<!-- end shopping cart -->
