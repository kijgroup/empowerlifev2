<div class="row">
  <div class="col-md-12">
    <h3 class="text-center"><?php echo $this->lang->line('contact_form_contact_us'); ?></h3>
  </div>
  <p><?php echo $this->lang->line('contact_form_complete_form'); ?></p>
  <?php if($show_success == 'complete'){ ?>
  <div class="promo promo-present">
    <h3 class="promo__heading"><?php echo $this->lang->line('contact_form_received_msg'); ?></h3>
    <p><?php echo $this->lang->line('contact_form_contact_information'); ?></p>
  </div>
  <?php } ?>
</div>
<div class="container contact" id="message-contact">
  <div class="row">
    <?php echo form_open('contact/form_post', array('role' => 'form', 'class' => 'form-horizontal' ,'id'=>'contactform')); ?>
    <div class="row">
      <div class="col-sm-6">
        <select class="contact__field" name="contact_group_id" id="contact_group_id">
          <?php
          $contact_group_list = $this->Contact_group_model->get_list(false);
          foreach($contact_group_list->result() as $contact_group){
            echo '<option value="'.$contact_group->contact_group_id.'" >'.$contact_group->contact_group_th.'</option>';
          }
          ?>
        </select>
      </div><!-- end col -->
    </div>
    <div class="row">
      <div class="col-sm-6">
        <textarea class="contact__field" name="contact_detail" placeholder="<?php echo $this->lang->line('contact_form_contact_message'); ?>" rows="5" required></textarea>
      </div><!-- end col -->
    </div>
    <div class="row">
      <div class="col-sm-6">
        <input class="contact__field" name="contact_name" type="text" placeholder="<?php echo $this->lang->line('contact_form_contact_name'); ?>" required>
      </div><!-- end col -->
    </div>
    <div class="row">
      <div class="col-sm-6">
        <input class="contact__field" name="contact_email" type="email" placeholder="<?php echo $this->lang->line('contact_form_contact_email'); ?>" required>
      </div><!-- end col -->
    </div>
    <div class="row">
      <div class="col-sm-6">
        <input class="contact__field" name="contact_phone" type="text" placeholder="<?php echo $this->lang->line('contact_form_contact_phone'); ?>" required>
      </div><!-- end col -->
    </div>
    <div class="g-recaptcha" data-sitekey="6Ld19lMUAAAAANjaXRfa7e-G1sCe-VRr8LuE4L9p"></div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <button type="submit" class="btn btn-primary" id="submit-contact"><?php echo $this->lang->line('contact_form_send_message'); ?></button>
  </div>
</div>
<?php echo form_close(); ?>
