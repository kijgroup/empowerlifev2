<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
    <ol class="breadcrumbs">
        <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
        <li><span><?php echo anchor('contact','ติดต่อเจ้าหน้าที่'); ?></span></li>
        <li><span><?php echo $title; ?></span></li>
    </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('contact', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
    <div class="panel-body form-horizontal">
      <div class="form-group">
          <label class="col-sm-3 control-label">หัวข้อคำถาม</label>
          <div class="col-sm-9">
              <p class="form-control-static"><?php echo $this->Contact_group_model->get_name($contact->contact_group_id); ?></p>
          </div>
      </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">ชื่อ</label>
            <div class="col-sm-9">
                <p class="form-control-static"><?php echo $contact->contact_name; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">อีเมล</label>
            <div class="col-sm-9">
                <p class="form-control-static"><?php echo $contact->contact_email; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">เบอร์โทรศัพท์</label>
            <div class="col-sm-9">
                <p class="form-control-static"><?php echo $contact->contact_phone; ?></p>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">รายละเอียด</label>
            <div class="col-sm-9">
                <p class="form-control-static"><?php echo $contact->contact_detail; ?></p>
            </div>
        </div>
        <div class="row form-group">
            <label class="col-sm-3 col-print-3 control-label">สร้างวันที่</label>
            <div class="col-sm-3 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $contact->create_date; ?></p>
            </div>
        </div>
        <?php if($contact->update_by > 0){ ?>
        <div class="row form-group">
            <label class="col-sm-3 col-print-3 control-label">ปรับปรุงวันที่</label>
            <div class="col-sm-3 col-print-3 xs-margin-bottom">
                <p class="form-control-static"><?php echo $contact->update_date; ?></p>
            </div>
            <label class="col-sm-3 col-print-3 control-label">โดย</label>
            <div class="col-sm-3 col-print-3">
                <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($contact->update_by); ?></p>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="deleteModalLabel">ลบข้อมูล</h4>
      </div>
      <div class="modal-body">
        กรุณากดปุ่ม "ลบข้อมูล" เพื่อยืนยันการลบข้อมูลนี้ออกจากระบบ
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ยกเลิก</button>
        <?php echo anchor('contact/delete/'.$contact->contact_id, '<i class="glyphicon glyphicon-trash"></i> ลบข้อมูล', array('class'=>'btn btn-danger')); ?>
      </div>
    </div>
  </div>
</div>
