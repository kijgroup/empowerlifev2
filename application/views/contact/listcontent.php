<?php
if($total_transaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $total_page = ceil($total_transaction/$per_page);
  $pager_data = array();
  $pager_data['total_transaction'] = $total_transaction;
  $pager_data['search_val'] = $search_val;
  $pager_data['start_date'] = $start_date;
  $pager_data['end_date'] = $end_date;
  $pager_data['per_page'] = $per_page;
  $pager_data['page'] = $page;
  $pager_data['total_page'] = $total_page;
  $pager_data['action'] = site_url('contact/filter');
?>
  <div class="form-group">
    <?php $this->load->view('contact/listpager', $pager_data); ?>
  </div>
  <div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th>เวลาที่ติดต่อ</th>
        <th>ชื่อ</th>
        <th>อีเมล</th>
        <th>เบอร์โทร</th>
        <th class="text-center">สถานะ</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($contact_list->result() as $contact){
        $detail_url = 'contact/detail/'.$contact->contact_id;
        ?>
        <tr>
          <td>
            <?php echo $contact->create_date; ?>
          </td>
          <td>
            <?php echo anchor($detail_url, $contact->contact_name); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, ($contact->contact_email != '')?$contact->contact_email:'-'); ?>
          </td>
          <td>
            <?php echo anchor($detail_url, ($contact->contact_phone != '')?$contact->contact_phone:'-'); ?>
          </td>
          <td class="text-center">
            <?php echo ($contact->is_read == 'read')?'<span class="label label-success">อ่านแล้ว</span>':'<span class="label label-warning">ยังไม่ได้อ่าน</span>'; ?>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
  </div>
  <div class="form-group">
    <?php $this->load->view('contact/listpager', $pager_data); ?>
  </div>
<?php
}
