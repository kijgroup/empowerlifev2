<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $this->lang->line('nav_contact'); ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $this->lang->line('nav_contact'); ?></li>
    </ol>
    <div class="devider devider--bottom-md"></div>
  </div>
</section>
<section class="container">
  <div class="content text-center">
    <div class="row">
      <div class="col-lg-6">
        <div class="visible-lg" style="padding-top:100px;"></div>
        <a id="map-popup" href="<?php echo base_url('assets/images/contact/EMPL_map.jpg'); ?>" ><img src="<?php echo base_url('assets/images/contact/EMPL_map.jpg'); ?>" style="max-width:100%;" /></a>
      </div>
      <div class="col-lg-6">
        <?php $this->load->view('contact/index/form'); ?>
      </div>
    </div>
  </div>
  <hr/>
  <style>
    .map>iframe{
      width:100%;
      height:350px;
      border:0;
    }
  </style>
  <div class="map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2740.7764806957757!2d100.47800337460926!3d13.715829154800783!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e2985dd1c389f7%3A0xad15fefecf4b6ca!2z4Lia4Lij4Li04Lip4Lix4LiXIOC5gOC4reC5h-C4oeC4nuC4suC4p-C5gOC4p-C4reC4o-C5jOC5hOC4peC4n-C5jCDguIjguLPguIHguLHguJQ!5e0!3m2!1sth!2s!4v1444118090346" style="width:100%;height:350px;" frameborder="0" allowfullscreen></iframe>
    <img src="<?php echo base_url('assets/images/shadow-center.jpg'); ?>" class="img-responsive" alt="shadow" />
  </div>
  <div style="text-align:center;">
    <?php echo $contact_content->page_content_th; ?>
  </div>
</section>