<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $this->lang->line('nav_quick_order'); ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $this->lang->line('nav_quick_order'); ?></li>
    </ol>
    <div class="devider devider--bottom-md"></div>
  </div>
</section>
<style>
.text-red{
  color:red;
}
</style>
<section class="container">
  <div class="container contact">
    <div class="row">
      <?php echo form_open('quick_order/form_post', array('role' => 'form', 'class' => 'form-horizontal' ,'id'=>'quick_orderform')); ?>
      <div class="table-responsive">
        <table class="table table-bordered table--wide table--product">
          <colgroup class="col-third">
          <colgroup class="col-thin">
          <colgroup class="col-thin">
          <colgroup class="col-thin">
          <thead>
            <tr>
              <th><?php echo $this->lang->line('quick_order_product'); ?></th>
              <th class="text-right" style="width:120px;"><?php echo $this->lang->line('quick_order_price'); ?></th>
              <th class="text-right" style="width:120px;max-width:120px;"><?php echo $this->lang->line('quick_order_qty'); ?></th>
              <th class="text-right" style="width:120px;"><?php echo $this->lang->line('quick_order_total'); ?></th>
            </tr>
          </thead>
          <tbody>
            <?php
            $product_list = $this->Product_model->get_all_list();
            foreach($product_list->result() as $product){
              $product_name = $this->Product_model->return_name_lang($product, $this->config->item('language_abbr'));
              $product_image_url = base_url('uploads/'.$product->thumb_image);
              $product_image = '<img alt="" src="'.$product_image_url.'" class="product__thumb" style="width:50px;" />';
              ?>
              <tr>
                <td class="table__item text-left">
                  <div>
                    <?php
                    echo $product_image;
                    echo '<p class="product__shortname">'.$product_name.'</p>';
                    ?>
                  </div>
                </td>
                <td class="text-center">
                <?php
                $price_class = '';
                if($product->price_before_discount > $product->price){
                  $price_class = 'text-red';
                  echo '<small class="product__price-discount" style="display:inline-block;">';
                  if($product->price_discount_rate > 0){
                    echo '-'.$product->price_discount_rate.'%';
                  }
                  ?>
                  <span class="slash-red"><?php echo number_format($product->price_before_discount); ?></span></small>
                  <?php
                  echo '<br />';
                }
                echo '<div class="'.$price_class.'">';
                echo number_format($product->price);
                echo '</div>';
                ?>
                </td>
                <td><input class="contact__field text-right product_qty" name="qty_<?php echo $product->product_id; ?>" id="qty_<?php echo $product->product_id; ?>" data-product_id="<?php echo $product->product_id; ?>" data-price="<?php echo $product->price; ?>" type="number" value="0"></td>
                <td class="text-right"><span id="total_<?php echo $product->product_id ?>">0</span></td>
              </tr>
              <?php
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <button type="submit" class="btn btn-info btn--decorated" id="submit-contact"><?php echo $this->lang->line('quick_order_button'); ?></button>
    </div>
  </div>
<?php echo form_close(); ?>
</section>
