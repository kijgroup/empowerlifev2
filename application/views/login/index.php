<section class="container">
  <h2 class="block-title block-title--bottom"><?php echo $this->lang->line('content_login'); ?></h2>
  <div class="login">
    <div class="alert alert-danger" id="alert_login" style="display:none;">
      <span class="alert-market">
        <i class="fa fa-ban"></i>
      </span>
      <span id="alert_login_msg"></span>
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
    </div>
    <?php echo form_open('login/form_post',array('class'=>'contact', 'id'=>'login-form')); ?>
      <input class="contact__field" id="member_email" name="member_email" type="email" placeholder="<?php echo $this->lang->line('content_login_email'); ?>">
      <input class="contact__field" id="member_password" name="member_password" type="password" placeholder="<?php echo $this->lang->line('content_login_password'); ?>">
      <div class="checkbox">
        <input id="check" type="checkbox" name="check" value="check" checked="checked">
        <label for="check"><?php echo $this->lang->line('content_login_remember_me'); ?></label>
      </div>
      <?php echo anchor('forgotpassword', $this->lang->line('content_login_forgot_password'), array('class'=> 'login__callback')); ?>
      <button class="btn btn--decorated btn-warning login__btn" type="submit"><?php echo $this->lang->line('content_login'); ?></button>
    <?php echo form_close(); ?>
  </div>
  <div class="btn-wrapper">
    <h3 class="heading-helper heading-helper--bottom"><?php echo $this->lang->line('content_login_social'); ?></h3>
    <?php echo anchor('hauth/login/Facebook','<i class="fa fa-facebook"></i>Facebook',array('class' =>'btn btn--decorated btn--facebook')); ?>
    <?php echo anchor('hauth/login/Google','<i class="fa fa-google-plus"></i>Google',array('class' =>'btn btn--decorated btn--google')); ?>
  </div>
</section><!-- end container -->
