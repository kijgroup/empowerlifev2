<section id="login_bg">
<div  class="container">
<div class="row">
	<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
		<div id="login">
			<p class="text-center">
				<img src="<?php echo base_url('assets/img/logo.png'); ?>" alt="">
			</p>
			<hr>
			<?php echo form_open('signin/form_post'); ?>
				<div class="form-group">
					<input type="text" class=" form-control" name="txtLoginName" placeholder="Email">
					<span class="input-icon"><i class=" icon-user"></i></span>
				</div>
				<div class="form-group" style="margin-bottom:5px;">
					<input type="password" class=" form-control" name="txtPassword" id="txtPassword" placeholder="Password" style="margin-bottom:5px;">
					<span class="input-icon"><i class="icon-lock"></i></span>
				</div>
				<p class="small">
					<?php echo anchor('forgotpassword', 'Forgot Password?'); ?>
				</p>
				<button type="submit" class="button_fullwidth">Log in</button>
				<?php echo anchor('register','Register', array('class'=>'button_fullwidth-2')); ?>
			<?php echo form_close(); ?>
		</div>
	</div>
</div>
</div>
</section> <!-- End login -->
