<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $this->lang->line('nav_content'); ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $this->lang->line('nav_content'); ?></li>
    </ol>
    <div class="devider devider--bottom-sm"></div>
  </div>
</section>
<section class="container">
  <h3 class="not-visible">Main container</h3>
  <div>
    <div class="shop-view">
      <span class="shop-view__results"><?php echo $total_content; ?> <?php echo $this->lang->line('content_results'); ?></span>
    </div>
    <?php echo form_open('content', array('class'=>'select select--thin select--size pull-right text-right', 'name'=>'select', 'method'=>'get')); ?>
      <div style="width:55px;display:block;float:left;padding-right:5px;"><?php echo $this->lang->line('content_type'); ?></div>
      <div style="width:200px;display:block;float:left;">
        <select class="select-box" id="content_group" name="content_group" tabindex="0">
          <option value="all"><?php echo $this->lang->line('content_type_all'); ?></option>
          <?php
          $content_group_list = $this->Content_group_model->get_list();
          foreach($content_group_list->result() as $content_group_data){
            $content_group_name = $content_group_data->name_th;
            $selected = ($content_group_slug == $content_group_data->slug)?'selected="selected"':'';
            echo '<option value="'.$content_group_data->slug.'" '.$selected.'>'.$content_group_name.'</option>';
          }
          ?>
        </select>
      </div>
      <div style="display:block;clear:both;"></div>
    <?php echo form_close(); ?>
  </div>
  <div class="row">
    <?php
    $i = 0;
    foreach($content_list->result() as $content){
      $this->load->view('content/list/thumb', array(
        'content' => $content
      ));
      $i++;
      if($i%2 == 0){
        echo '<div style="display:block;clear:both;"></div>';
      }
    }
    ?>
    <div class="clearfix"></div>
  </div> <!-- end row -->
  <?php
  $this->load->view('content/list/pager');
  ?>
</section>
