<div class="col-sm-4">
  <article>
    <?php
    if($this->config->item('language_abbr') == 'th'){
      $subject = $content->subject_th;
    }else{
      $subject = $content->subject_en;
    }
    $content_url = 'content/detail/'.$content->content_id.'/'.urlencode($subject);
    $content_container = $this->load->view('content/detail/thumb_content', array('content'=>$content), true);
    echo anchor($content_url, $content_container, array('class'=> 'post post--preview-link', 'style' => 'min-height:auto;'));
    ?>
  </article>
</div>
