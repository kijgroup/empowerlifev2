<?php
$content_image_list = $this->Content_image_model->get_list($content_id);
if($content_image_list->num_rows() > 0){
  ?>
  <ul class="bxslider">
    <?php
    $content_image_list = $this->Content_image_model->get_list($content_id);
    foreach($content_image_list->result() as $content_image){
      echo '<li><img src="'.base_url('uploads/'.$content_image->main_image).'" alt="" /></li>';
    }
    ?>
  </ul>
  <div id="bx-pager">
    <?php
    $counter = 0;
    foreach($content_image_list->result() as $content_image){
      echo '<a data-slide-index="'.$counter.'"><img src="'.base_url('uploads/'.$content_image->thumb_image).'" alt=""></a>';
      $counter ++;
    }
    ?>
  </div>
  <?php
}