<?php
$url = urlencode(site_url('content/detail/'.$content_id));
$title = urlencode(($this->config->item('language_abbr') == 'th')?$content->subject_th: $content->subject_en);
$twitter_status = urlencode($title.' — empowerlife '.$url);
?>
<div class="post__interaction bottom-universal">
  <p class="sub-header">Share This Post:</p>
  <div class="share share--large share-present">
    <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $url; ?>" class="share__item share__item--facebook" title="Share on Facebook" target="_blank"><i class="fa fa-facebook"></i>Facebook</a>
    <a href="https://twitter.com/intent/tweet?source=<?php echo $url; ?>&text=<?php echo $twitter_status; ?>" class="share__item share__item--twitter" title="Tweet" target="_blank"><i class="fa fa-twitter"></i>Twitter</a>
    <a href="https://plus.google.com/share?url=<?php echo $url; ?>" class="share__item share__item--gplus" title="Share on Google+" target="_blank"><i class="fa fa-google-plus"></i>Google+</a>
    <a href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url; ?>&title=<?php echo $title; ?>" class="share__item share__item--linkedin" title="Share on LinkedIn" target="_blank"><i class="fa fa-linkedin"></i>Linkedin</a>
  </div>
</div>
