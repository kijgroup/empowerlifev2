relate
<?php
if(count($relate_content_list) > 0){
  ?>
  <h2><?php echo $this->lang->line('content_relate'); ?></h2>
  <div class="row">
    <?php
    foreach($relate_content_list as $relate_content){
      $this->load->view('content/detail/thumb', array('content' => $relate_content));
    }
    ?>
  </div>
  <?php
}