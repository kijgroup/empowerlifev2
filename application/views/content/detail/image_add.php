<!-- Modal -->
<div class="modal fade" id="addImageModal" tabindex="-1" role="dialog" aria-labelledby="addImageModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="addImageModalLabel">เพิ่มรูปภาพ</h4>
      </div>
      <div class="modal-body">
        <?php
        echo form_open('content/image_add/'.$content_id, array('class'=> 'dropzone dz-square', 'id'=>'dropzone'));
        echo form_close();
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal">ปิด</button>
      </div>
    </div>
  </div>
</div>
