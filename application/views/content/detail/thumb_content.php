<?php
if($this->config->item('language_abbr') == 'th'){
  $subject = $content->subject_th;
  $short_content = $content->short_content_th;
}else{
  $subject = $content->subject_en;
  $short_content = $content->short_content_en;
}
$content_url = 'content/detail/'.$content->content_id.'/'.urlencode($subject);
$content_group = $this->Content_group_model->get_name($content->content_group_id, $this->config->item('language_abbr'));
$image_url = base_url('uploads/'.$content->thumb_image);
?>
<div class="row">
  <div class="one-column col-xs-4">
    <div class="image-container image-container--max image-container--fading">
      <img src="<?php echo $image_url; ?>" alt="">
    </div>
  </div><!-- end col -->
  <div class="one-column col-xs-8">
    <h3 class="post__heading" style="margin:0px;font-size:15px;"><?php echo $subject; ?></h3>
  </div><!-- end col -->
</div><!-- end row -->
