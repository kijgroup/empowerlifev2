<?php
if($this->config->item('language_abbr') == 'th'){
  $subject = $content->subject_th;
  $short_content = $content->short_content_th;
}else{
  $subject = $content->subject_en;
  $short_content = $content->short_content_en;
}
$content_url = 'content/detail/'.$content->content_id.'/'.urlencode($subject);
$content_group = $this->Content_group_model->get_name($content->content_group_id, $this->config->item('language_abbr'));
$image_url = base_url('uploads/'.$content->thumb_image);
?>
<div class="row">
  <div class="one-column col-xs-6 col-sm-5 col-md-7 col-lg-6">
    <div class="image-container image-container--max image-container--fading">
      <img src="<?php echo $image_url; ?>" alt="">
    </div>
  </div><!-- end col -->
  <div class="one-column col-xs-6 col-sm-7 col-md-5 col-lg-6">
    <h3 class="post__heading"><?php echo $subject; ?></h3>
    <p class="post__text"><?php echo $short_content; ?></p>
    <p class="post__author"><?php echo $content_group; ?></p>
  </div><!-- end col -->
</div><!-- end row -->
