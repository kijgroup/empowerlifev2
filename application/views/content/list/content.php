<?php
if($total_transaction == 0){
  echo '<div class="alert alert-warning" role="alert">ไม่พบรายการตามที่ท่านค้นหา</div>';
}else{
  $total_page = ceil($total_transaction/$per_page);
  $pager_data = array(
    'total_transaction' => $total_transaction,
    'search_val' => $search_val,
    'content_group_id' => $content_group_id,
    'enable_status' => $enable_status,
    'per_page' => $per_page,
    'page' => $page,
    'total_page' => $total_page,
    'action' => site_url('content/filter')
  );
?>
<div class="form-group">
    <?php $this->load->view('content/list/pager', $pager_data); ?>
</div>
<div class="table-responsive">
  <table class="table table-striped table-bordered table-hover">
    <thead>
      <tr>
        <th width="120">รูปบทความ</th>
        <th>บทความ</th>
      </tr>
    </thead>
    <tbody>
      <?php
      foreach($content_list->result() as $content){
        $detail_url = 'content/detail/'.$content->content_id;
        ?>
        <tr>
          <td>
            <?php
            if($content->thumb_image != ''){
              $str_img = '<img src="'.base_url('uploads/'.$content->thumb_image).'" style="width:120px;" />';
              echo anchor('content/detail/'.$content->content_id, $str_img);
            }
            ?>
          </td>
          <td>
            <div><b>TH:</b> <?php echo anchor('content/detail/'.$content->content_id, $content->subject_th); ?></div>
            <div><b>EN:</b> <?php echo anchor('content/detail/'.$content->content_id, $content->subject_en); ?></div>
            <div><b>ประเภทบทความ:</b> <?php echo anchor('content/detail/'.$content->content_id, $this->Content_group_model->get_name($content->content_group_id)); ?></div>
            <div><?php echo ($content->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ซ่อน</span>'; ?></div>
            <small><?php echo $content->create_date.' <b>by</b> 
            '.$this->Admin_model->get_login_name_by_id($content->create_by); ?></small>
            <div><?php
            $tag_list = explode(",", $content->tags);
            foreach($tag_list as $tag){
              echo ' <span class="label label-info">'.$tag.'</span> ';
            }
            ?></div>
          </td>
        </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<div class="form-group">
  <?php $this->load->view('content/list/pager', $pager_data); ?>
</div>
<?php
}
