<?php
if($this->config->item('language_abbr') == 'th'){
 $subject = $content->subject_th;
 $content_detail = $content->content_th;
}else{
  $subject = $content->subject_en;
  $content_detail = $content->content_en;
}
?>
<section class="page-indecator">
  <div class="container">
    <h2 class="heading"><?php echo $this->lang->line('nav_content'); ?></h2>
    <ol class="breadcrumb">
      <li><?php echo anchor('', $this->lang->line('nav_home')); ?></li>
      <li class="active"><?php echo $this->lang->line('nav_content'); ?></li>
    </ol>
    <div class="devider"></div>
  </div>
</section>
<style>
  .post{
    color: #000;
  }
  .post p{
    color: #000;
  }
</style>
<section class="container">
  <div class="post">
    <h1 class="post-heading"><?php echo $subject; ?></h1>
    <?php echo str_replace('></iframe>',' allowfullscreen></iframe>',$content_detail); ?>
  </div>
  <?php
  $this->load->view('content/detail/image_list');
  $this->load->view('content/detail/relate_content');
  $this->load->view('content/detail/social');
  ?>
</section>
