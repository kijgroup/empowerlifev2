<header class="page-header">
<h2><?php echo $title; ?></h2>
<div class="right-wrapper pull-right" style="padding-right:20px;">
  <ol class="breadcrumbs">
    <li><?php echo anchor('dashboard','<i class="fa fa-home"></i>'); ?></li>
    <li><span>ของขาย</span></li>
    <li><span><?php echo anchor('reward','ของรางวัล'); ?></span></li>
    <li><span><?php echo $title; ?></span></li>
  </ol>
</div>
</header>
<div class="btn-group hide-print" style="padding-bottom:10px;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    การกระทำ <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li><?php echo anchor('reward', '<i class="glyphicon glyphicon-arrow-left"></i> กลับไปหน้ารายการ'); ?></li>
    <li><?php echo anchor('reward/form/', '<i class="glyphicon glyphicon-plus"></i> เพิ่มของรางวัลใหม่'); ?></li>
    <li><?php echo anchor('reward/form/'.$reward->reward_id, '<i class="glyphicon glyphicon-pencil"></i> แก้ไขข้อมูล'); ?></li>
    <li class="divider"></li>
    <li><a href="#" data-toggle="modal" data-target="#deleteModal"><i class="glyphicon glyphicon-trash"></i> ลบข้อมูล</a></li>
  </ul>
</div>
<div class="panel panel-default">
  <div class="panel-body form-horizontal">
    <div class="form-group">
      <label class="col-md-3 control-label">ชื่อของรางวัล (th)</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $reward->name_th; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ชื่อของรางวัล (en)</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo $reward->name_en; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">รูปของรางวัล</label>
      <div class="col-md-3">
        <p class="form-control-static"><?php echo ($reward->thumb_image != '')?'<img src="'.base_url('uploads/'.$reward->thumb_image).'" class="img-responsive" />':''; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ข้อมูลของรางวัล (th)</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward->detail_th; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">ข้อมูลของรางวัล (en)</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward->detail_en; ?></p>
      </div>
    </div>
    <div class="form-group">
      <label class="col-md-3 control-label">แต้มแลกของ</label>
      <div class="col-md-6">
        <p class="form-control-static"><?php echo $reward->reward_point; ?></p>
      </div>
    </div>

    <div class="form-group">
      <label class="col-md-3 control-label">สถานะ</label>
      <div class="col-md-9">
        <p class="form-control-static"><?php echo ($reward->enable_status == 'show')?'<span class="label label-success">แสดง</span>':'<span class="label label-danger">ซ่อน</span>'; ?></p>
      </div>
    </div>
    <div class="row form-group">
      <label class="col-md-3 col-print-3 control-label">สร้างวันที่</label>
      <div class="col-md-3 col-print-3 xs-margin-bottom">
        <p class="form-control-static"><?php echo $reward->create_date; ?></p>
      </div>
      <label class="col-md-3 col-print-3 control-label">โดย</label>
      <div class="col-md-3 col-print-3">
        <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($reward->create_by); ?></p>
      </div>
    </div>
    <?php if($reward->update_by > 0){ ?>
      <div class="row form-group">
        <label class="col-md-3 col-print-3 control-label">ปรับปรุงวันที่</label>
        <div class="col-md-3 col-print-3 xs-margin-bottom">
          <p class="form-control-static"><?php echo $reward->update_date; ?></p>
        </div>
        <label class="col-md-3 col-print-3 control-label">โดย</label>
        <div class="col-md-3 col-print-3">
          <p class="form-control-static"><?php echo $this->Admin_model->get_login_name_by_id($reward->update_by); ?></p>
        </div>
      </div>
    <?php } ?>
  </div>
</div>
<?php
$this->load->view('reward/detail/delete_popup');
