<?php
$main_image = '';
$thumb_image = '';
if($reward->main_image != ''){
  $main_image = base_url('uploads/'.$reward->main_image);
  $thumb_image = base_url('uploads/'.$reward->thumb_image);
}else{
  $main_image = 'http://placehold.it/800x600';
  $thumb_image = 'http://placehold.it/600x600';
}
$col_width = (isset($col_width))?$col_width:4;
?>
<div class="col-sm-6 col-md-<?php echo $col_width; ?>">
  <!-- Product preview -->
  <div class="product product--round product--flow">
    <div class="product__photo">
      <img src="<?php echo $thumb_image; ?>" alt="">
    </div>
    <span class="product__title" style="margin-top:10px;"><?php echo $this->Reward_model->get_name_by_obj($reward, $this->config->item('language_abbr')); ?></span>
    <p class="product__price"><?php echo number_format($reward->reward_point); ?> P</p>
    <?php
    if($this->Login_service->get_login_status()){
      $field_name = 'qty_'.$reward->reward_id;
      ?>
    <div>
      <p class="sub-header devider--middle mobile--devider" style="margin-top:10px;margin-bottom:0;"><?php echo $this->lang->line('reward_shipping_point_qty'); ?></p>
      <div class='quantity-choose quantity-choose--sep'>
        <div class="quantity buttons_added">
          <div class="qtyminus-wrap" style="text-align:left;">
            <input type='button' value='-' class='qtyminus quantity__elem' data-field='<?php echo $field_name; ?>' />
          </div>
          <input type='text' id="<?php echo $field_name; ?>" name='<?php echo $field_name; ?>' value='0' data-point="<?php echo $reward->reward_point; ?>"
          data-reward_name="<?php echo $this->Reward_model->get_name_by_obj($reward, $this->config->item('language_abbr')); ?>"
           class='qty-reward qty quantity__elem' />
          <div class="qtyplus-wrap" style="text-align:left;">
            <input type='button' value='+' class='qtyplus quantity__elem' data-field='<?php echo $field_name; ?>' />
          </div>
        </div>
      </div>
    </div>
    <?php } ?>
  </div>
  <!-- end product preview -->
</div>
