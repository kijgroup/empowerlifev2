<h3 class="sub-header sub-header--shop">
<?php echo $this->lang->line('reward_point'); ?>
<br />
ประจำวันที่ <?php echo $this->Datetime_service->display_date(date('Y-m-d'), 'th'); ?>
</h3>
<table class="table--short table--cut" style="margin-bottom:30px;">
  <colgroup></colgroup>
  <colgroup></colgroup>
  <colgroup></colgroup>
  <thead>
    <tr style="font-size:16px;border-bottom:1px solid #999;">
      <th class="checkout__result">สินค้า</th>
      <th class="checkout__result" style="padding:0 10px;">จำนวน</th>
      <th class="checkout__result text-right" style="min-width:60px">คะแนน</th>
    </tr>
  <thead>
  <tbody id="reward_item_list">
  </tbody>
</table>
<hr />
<table class="table--short table--cut">
  <colgroup class="col-half"></colgroup>
  <colgroup class="col-half"></colgroup>
  <tbody>
    <tr>
      <td style="padding-left:0;"><?php echo $this->lang->line('reward_point_customer'); ?></td>
      <input type="hidden" id="member_point" value="<?php echo $member->member_point; ?>" />
      <td class="checkout__result text-right"><?php echo $member->member_point; ?> P</td>
    </tr>
    <tr>
      <td style="padding-left:0;"><?php echo $this->lang->line('reward_use_point'); ?></td>
      <td class="checkout__result text-right"><span id="use_point">0</span> P</td>
    </tr>
    <tr>
      <td style="padding-left:0;"><?php echo $this->lang->line('reward_point_remain'); ?></td>
      <td class="checkout__result text-right"><span id="remain_point"><?php echo $member->member_point; ?></span> P</td>
    </tr>
  </tbody>
</table>
<div class="checkout__sum checkout__sum--large"><?php echo $this->lang->line('reward_point_qty'); ?> <span id="reward_qty">0</span> ชิ้น</div>
