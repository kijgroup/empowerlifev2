<?php
if($this->Login_service->get_login_status()){
  echo form_open('reward/form_post', array('class'=>'contact', 'id'=>'reward-form'));
  echo '<div class="alert alert-danger" id="form-error" style="display:none;">
    <span class="alert-market">
      <i class="fa fa-ban"></i>
    </span>
    <span id="form-error-msg"></span>
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times"></i></button>
  </div>';
}
?>
<div class="row product-wrapper">
  <?php
  foreach($reward_list->result() as $reward){
    $this->load->view('reward/list/thumb', array('reward'=>$reward));
  }
  ?>
</div>
<?php
if($this->Login_service->get_login_status()){
  $this->load->view('reward/list/form.php');
  echo form_close();
}
