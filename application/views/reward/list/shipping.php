<div class="contact contact--choose">
  <h3 class="sub-header sub-header--shop sub-header--side"><?php echo $this->lang->line('reward_address'); ?></h3>
  <input class="contact__field" name="shipping_name" id="shipping_name" type="text" placeholder="<?php echo $this->lang->line('reward_shipping_name'); ?>" value="<?php echo $member->member_firstname.' '.$member->member_lastname; ?>">
  <input class="contact__field" name="shipping_mobile" id="shipping_mobile" type="text" placeholder="<?php echo $this->lang->line('reward_shipping_mobile'); ?>" value="<?php echo $member->member_mobile; ?>">
  <textarea class="contact__field contact__area" name="shipping_address" id="shipping_address" placeholder="<?php echo $this->lang->line('reward_shipping_address'); ?>" style="min-height:80px;"><?php echo $member->member_address; ?></textarea>
  <div class="row">
    <div class="col-md-6">
      <input class="contact__field" name="shipping_district" id="shipping_district" type="text" placeholder="<?php echo $this->lang->line('reward_shipping_district'); ?>" value="<?php echo $member->member_district; ?>">
    </div>
    <div class="col-md-6">
      <input class="contact__field" name="shipping_amphur" id="shipping_amphur" type="text" placeholder="<?php echo $this->lang->line('reward_shipping_amphur'); ?>" value="<?php echo $member->member_amphur; ?>">
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <input class="contact__field" name="shipping_province" id="shipping_province" type="text" placeholder="<?php echo $this->lang->line('reward_shipping_province'); ?>" value="<?php echo $member->member_province; ?>">
    </div>
    <div class="col-md-6">
      <input class="contact__field" name="shipping_post_code" id="shipping_post_code" type="text" placeholder="<?php echo $this->lang->line('reward_shipping_post_code'); ?>" value="<?php echo $member->member_postcode; ?>">
    </div>
  </div>
  <textarea class="contact__field contact__area" name="note" id="note" placeholder="<?php echo $this->lang->line('reward_shipping_note'); ?>" style="min-height:80px;"></textarea>
  <button type="submit" class="btn btn-info btn--decorated check__btn"><?php echo $this->lang->line('reward_shipping_point'); ?></button>
</div>
