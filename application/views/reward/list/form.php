<div style="display:block;clear:both;"></div>
<h2 class="block-title block-title--top-s"><?php echo $this->lang->line('reward_list'); ?></h2>
<div class="row" style="text-align:left;">
  <div class="col-md-6">
    <?php $this->load->view('reward/list/point'); ?>
  </div>
  <div class="col-md-6 separate-block">
    <?php $this->load->view('reward/list/shipping'); ?>
  </div>
</div>
