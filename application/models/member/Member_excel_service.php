<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Member_excel_service extends CI_Model{
  public function export_list(){
    ini_set('max_execution_time', 300);
    date_default_timezone_set('Asia/Bangkok');
    include(FCPATH."PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
    $today = date('Y-m-d');
    $objPHPExcel = new PHPExcel();
    $objPHPExcel->getProperties()->setCreator("Narathip Harijiratiwong")
    							 ->setLastModifiedBy("Narathip Harijiratiwong")
    							 ->setTitle("Member ".$today)
    							 ->setSubject("Member ".$today)
    							 ->setDescription("Member.")
    							 ->setKeywords("Empowerlife")
                   ->setCategory("Empowerlife Member");
    // Add some data
    $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', '#')
                ->setCellValue('B1', 'Member Code')
                ->setCellValue('C1', 'Member Type')
                ->setCellValue('D1', 'Expire Date')
                ->setCellValue('E1', 'Buy Total')
                ->setCellValue('F1', 'Member Point')
                ->setCellValue('G1', 'Email Login')
                ->setCellValue('H1', 'Register Date')
                ->setCellValue('I1', 'Email')
                ->setCellValue('J1', 'Firstname')
                ->setCellValue('K1', 'Lastname')
                ->setCellValue('L1', 'Phone Number')
                ->setCellValue('M1', 'Mobile')
                ->setCellValue('N1', 'Birthday')
                ->setCellValue('O1', 'Gender')
                ->setCellValue('P1', 'Address')
                ->setCellValue('Q1', 'District')
                ->setCellValue('R1', 'Amphur')
                ->setCellValue('S1', 'Province')
                ->setCellValue('T1', 'Postcode')
                ->setCellValue('U1', 'Note')
                ;
    $sheet_count = 2;
    $member_list = $this->get_list();
    $counter = 1;
    foreach($member_list->result() as $member){
      $objPHPExcel->setActiveSheetIndex(0)
                  ->setCellValue('A'.$sheet_count, $counter)
                  ->setCellValue('B'.$sheet_count, $member->member_code)
                  ->setCellValue('C'.$sheet_count, $member->member_type)
                  ->setCellValue('D'.$sheet_count, $member->expire_date)
                  ->setCellValue('E'.$sheet_count, $member->buy_total)
                  ->setCellValue('F'.$sheet_count, $member->member_point)
                  ->setCellValue('G'.$sheet_count, $member->email)
                  ->setCellValue('H'.$sheet_count, $member->create_date)
                  ->setCellValue('I'.$sheet_count, $member->member_email)
                  ->setCellValue('J'.$sheet_count, $member->member_firstname)
                  ->setCellValue('K'.$sheet_count, $member->member_lastname)
                  ->setCellValue('L'.$sheet_count, "'".$member->member_phone)
                  ->setCellValue('M'.$sheet_count, "'".$member->member_mobile)
                  ->setCellValue('N'.$sheet_count, $member->member_birthdate)
                  ->setCellValue('O'.$sheet_count, $member->member_gender)
                  ->setCellValue('P'.$sheet_count, $member->member_address)
                  ->setCellValue('Q'.$sheet_count, $member->member_district)
                  ->setCellValue('R'.$sheet_count, $member->member_amphur)
                  ->setCellValue('S'.$sheet_count, $member->member_province)
                  ->setCellValue('T'.$sheet_count, $member->member_postcode)
                  ->setCellValue('U'.$sheet_count, $member->note)
                  ;
      $sheet_count ++;
    }
    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('Emportlife Member');
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    // Redirect output to a client’s web browser (Excel5)
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="member_'.date('Ymd').'.xls"');
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
    exit;
  }
  private function get_list(){
    $this->db->select('tbl_member.*');
    $this->db->select('tbl_user.email');
    $this->db->join('tbl_user', 'tbl_user.member_id = tbl_member.member_id', 'left');
    $this->db->where('tbl_member.is_delete', 'active');
    return $this->db->get('tbl_member');
  }
}