<?php
class Member_type_model extends CI_Model{
  public function get_data($member_type_code){
    $this->db->where('member_type_code', $member_type_code);
    return $this->query_data();
  }
  public function get_name($member_type_code){
    $member_type = $this->get_data($member_type_code);
    if(!$member_type){
      return '-';
    }
    return $member_type->member_type_name;
  }
  public function get_next_step($member_type_code){
    $current_member_type = $this->get_data($member_type_code);
    if(!$current_member_type || $current_member_type->step == 4){
      return false;
    }
    return $this->get_data_by_step($current_member_type->step + 1);
  }
  public function get_data_by_step($step){
    $this->db->where('step', $step);
    return $this->query_data();
  }
  private function query_data(){
    $query = $this->db->get('tbl_member_type');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
}
