<?php
class Member_filter_service extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function get_data_content($search_val, $member_type_code, $enable_status, $sort, $page = 1, $per_page = 20) {
    $data = array();
    $data['search_val'] = $search_val;
    $data['member_type_code'] = $member_type_code;
    $data['enable_status'] = $enable_status;
    $data['sort'] = $sort;
    $data['page'] = $page;
    $data['per_page'] = $per_page;
    $data['member_list'] = $this->get_list($search_val, $member_type_code, $enable_status, $sort, $page, $per_page);
    $data['total_transaction'] = $this->get_count($search_val, $member_type_code, $enable_status, $sort);
    return $data;
  }
  private function get_count($search_val, $member_type_code, $enable_status, $sort){
    $this->db->select('tbl_member.member_id');
    $this->where_transaction($search_val, $member_type_code, $enable_status);
    $query = $this->db->get('tbl_member');
    return $query->num_rows();
  }
  private function get_list($search_val, $member_type_code, $enable_status, $sort, $page, $per_page){
    $this->db->select('tbl_member.*');
    $this->where_transaction($search_val, $member_type_code, $enable_status);
    if($sort == 'member_firstname'){
      $this->db->order_by($sort, 'ASC');
    }else{
      $this->db->order_by($sort, 'DESC');
    }
    $offset = ($page - 1) * $per_page;
    return $this->db->get('tbl_member', $per_page, $offset);
  }
  private function where_transaction($search_val, $member_type_code, $enable_status){
    if($search_val != ''){
      $str_where = "(member_firstname like '%$search_val%' ";
      $str_where .= "OR member_lastname like '%$search_val%' ";
      $str_where .= "OR member_email like '%$search_val%' ";
      $str_where .= "OR member_mobile like '%$search_val%' ";
      $str_where .= "OR member_phone like '%$search_val%' ";
      $str_where .= "OR member_code like '%$search_val%' ";
      $str_where .= ')';
      $this->db->where($str_where, NULL,FALSE);
    }
    if($member_type_code != 'all'){
      $this->db->where('tbl_member.member_type', $member_type_code);
    }
    if($enable_status != 'all'){
      $this->db->where('tbl_member.enable_status', $enable_status);
    }
    $this->db->where('is_delete', 'active');
  }
}
