<?php
class Forgot_password_service extends CI_Model {
  var $NO_USERNAME = 'require_email';
  var $NOT_FOUND = 'email_not_found';
  var $INVALID_ACCOUNT = 'invalid_account';

  function __construct() {
    parent::__construct();
    $this->load->model(array(
      'member/User_model',
      'Encode_service',
    ));
  }

  public function forgot_email($email) {
    return ($email !== 'null') ? $email : '';
  }

  public function get_error_message($error) {
    $error_message = '';
    if ($error != '') {
      $arr_message = array(
        $this->NO_USERNAME => 'Please enter the email address that you use to login.',
        $this->NOT_FOUND => 'No user was found with that email address.',
        $this->INVALID_ACCOUNT => 'This account has been disabled, please contact administrator.'
      );
      $error_message = (isset($arr_message[$error])) ? $arr_message[$error] : $error;
    }
    return $error_message;
  }

  public function forgot_password($email) {
    $this->check_email_input($email);
    $member = $this->check_email($email);
    $this->check_member_allow($member);
    $this->send_change_password_email($member);
    return $member->member_id;
  }

  public function get_login_status() {
    if ($this->session->userdata('member_logged_in')) {
      return true;
    }
    return false;
  }

  private function check_email_input($email) {
    if (!$email) {
      redirect('forgotpassword/index/' . $this->NO_USERNAME);
    }
  }
  private function check_email($email) {
    $this->db->select('tbl_user.*');
    $this->db->like('LOWER(email)', strtolower($email));
    $query = $this->db->get('tbl_user');
    if ($query->num_rows() === 0) {
      redirect('forgotpassword/index/' . $this->NOT_FOUND);
    }
    $user = $query->row();
    $this->db->where('member_id', $user->member_id);
    $member = $this->db->get('tbl_member')->row();
    return $member;
  }
  private function check_member_allow($member) {
    if ($member->enable_status != 'active') {
      redirect('forgotpassword/index/' . $this->INVALID_ACCOUNT);
    }
  }
  private function send_change_password_email($member){
    $hash_code = $this->Encode_service->encode_number($member->member_id);
    $this->db->where('member_id', $member->member_id);
    $user = $this->db->get('tbl_user')->row();
    $link_url = 'forgotpassword/changepassword/'.$hash_code;
    $this->load->model('Sendmail_service');
    $content = $this->load->view('forgotpassword/forgotpasswordemail', array(
      'member' => $member,
      'link_url' => $link_url
    ), true);
    $subject = 'Reset Password';
    $this->Sendmail_service->send_mail($user->email, $subject, $content);
  }
}
