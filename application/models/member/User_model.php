<?php
class User_model extends CI_Model{
  var $login_key = '';
  public function get_data($user_id){
    $this->db->where('user_id', $user_id);
    return $this->query_data();
  }
  public function get_data_by_email($email){
    $this->db->where('email', $email);
    return $this->query_data();
  }
  public function get_login($email, $login_password){
    $this->db->where('email', $email);
    $this->db->where('login_password', $this->encrypt_password($login_password));
    return $this->query_data();
  }
  public function login_token($email, $token){
    $this->db->where('email', $email);
    $this->db->where('login_password', $token);
    return $this->query_data();
  }
  private function query_data(){
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_user');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function insert($data){
    $this->db->set('member_id', $data['member_id']);
    $this->db->set('email', $data['email']);
    $this->db->set('login_password', $this->encrypt_password($data['login_password']));
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_user');
    $user_id = $this->db->insert_id();
    return $user_id;
  }
  public function update_social($user_id, $provider, $provider_id, $provider_url, $avata_image){
    if($provider == 'Facebook'){
      $this->db->set('facebook_id', $provider_id);
      $this->db->set('facebook_url', $provider_url);
    }elseif($provider == 'Google'){
      $this->db->set('google_id', $provider_id);
      $this->db->set('google_url', $provider_url);
    }
    $this->db->set('avata_image', $avata_image);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('user_id', $user_id);
    $this->db->update('tbl_user');
  }
  public function update_fields($user_id, $fields){
    foreach($fields as $column => $value){
      $this->db->set($column, $value);
    }
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('user_id', $user_id);
    $this->db->update('tbl_user');
  }
  public function set_last_login($user_id){
    $this->db->set('last_login', 'NOW()', false);
    $this->db->where('user_id', $user_id);
    $this->db->update('tbl_user');
  }
  public function change_password($user_id, $login_password){
    $this->db->set('login_password', $this->encrypt_password($login_password));
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('user_id', $user_id);
    $this->db->update('tbl_user');
  }
  public function encrypt_password($login_password){
    return md5($this->login_key.$login_password);
  }
}
