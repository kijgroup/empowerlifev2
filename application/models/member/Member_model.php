<?php
class Member_model extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function get_data($member_id){
    $this->db->where('member_id', $member_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_member');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
  public function get_data_by_email($member_email){
    $this->db->where('member_email', $member_email);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_member');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
  public function insert($data){
    $this->db->set('member_email', $data['member_email']);
    $this->db->set('member_firstname', $data['member_firstname']);
    $this->db->set('member_lastname', $data['member_lastname']);
    $this->db->set('member_birthdate', $data['member_birthdate']);
    $this->db->set('member_gender', $data['member_gender']);
    $this->db->set('member_phone', $data['member_phone']);
    $this->db->set('member_mobile', $data['member_mobile']);
    $this->db->set('member_address', $data['member_address']);
    $this->db->set('member_district', $data['member_district']);
    $this->db->set('member_amphur', $data['member_amphur']);
    $this->db->set('member_province', $data['member_province']);
    $this->db->set('member_postcode', $data['member_postcode']);
    $this->db->set('enable_status', 'active');
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_member');
    $member_id = $this->db->insert_id();
    return $member_id;
  }
  public function update($member_id, $data){
    $this->db->set('member_firstname', $data['member_firstname']);
    $this->db->set('member_lastname', $data['member_lastname']);
    $this->db->set('member_birthdate', $data['member_birthdate']);
    $this->db->set('member_phone', $data['member_phone']);
    $this->db->set('member_mobile', $data['member_mobile']);
    $this->db->set('member_address', $data['member_address']);
    $this->db->set('member_district', $data['member_district']);
    $this->db->set('member_amphur', $data['member_amphur']);
    $this->db->set('member_province', $data['member_province']);
    $this->db->set('member_postcode', $data['member_postcode']);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('member_id', $member_id);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_member');
  }
  public function update_user_id($member_id, $user_id){
    $this->db->set('user_id', $user_id);
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
  }
  public function update_reward_point($member_id, $member_point){
    $this->db->set('member_point', $member_point);
    $this->db->where('member_id', $member_id);
    $this->db->update('tbl_member');
  }
}
