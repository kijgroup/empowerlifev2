<?php
class Enable_status_model extends CI_Model{
  var $status_list = array(
    'show' => array(
      'status_code' => 'show',
      'status_name' => 'แสดง',
      'label_class' => 'label label-success'
    ),
    'hide' => array(
      'status_code' => 'hide',
      'status_name' => 'ซ่อน',
      'label_class' => 'label label-danger'
    ),
    'admin' => array(
      'status_code' => 'admin',
      'status_name' => 'เฉพาะเจ้าหน้าที่',
      'label_class' => 'label label-warning'
    ),
  );
  public function get_list(){
    return $this->status_list;
  }
  public function get_data($status_code){
    if(!isset($this->status_list[$status_code])){
      return false;
    }
    return $this->status_list[$status_code];
  }
  public function get_name($status_code){
    $status = $this->get_data($status_code);
    return $status['status_name'];
  }
  public function get_label($status_code){
    $status = $this->get_data($status_code);
    if(!isset($status['status_name'])){
      return '-';
    }
    return '<span class="'.$status['label_class'].'">'.$status['status_name'].'</span>';
  }
}
