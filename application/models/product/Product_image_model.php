<?php
class Product_image_model extends CI_Model{
  public function get_list($product_id){
    $this->db->where('product_id', $product_id);
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_product_image');
  }
}
