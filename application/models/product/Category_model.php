<?php
class Category_model extends CI_Model{
  public function get_list(){
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_category');
  }
  public function get_data($category_id){
    $this->db->where('category_id', $category_id);
    return $this->query_get_data();
  }
  public function get_name($category_id, $lang){
    return $this->return_name_lang($this->get_data($category_id), $lang);
  }
  public function get_data_by_slug($slug){
    $this->db->where('slug', $slug);
    return $this->query_get_data();
  }
  public function get_name_by_slug($slug, $lang){
    return $this->return_name_lang($this->get_data_by_slug($slug), $lang);
  }
  private function query_get_data(){
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_category');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function return_name_lang($category, $lang){
    if(!$category){
      return '-';
    }
    if($lang == 'th'){
      return $category->name_th;
    }else{
      return $category->name_en;
    }
  }
}
