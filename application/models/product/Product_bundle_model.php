<?php
class Product_bundle_model extends CI_Model{
  public function get_list($product_id){
    $this->db->where('product_id', $product_id);
    return $this->db->get('tbl_product_bundle');
  }
  public function get_bundle_list($bundle_id){
    $query = $this->get_all_bundle_list($bundle_id);
    if($query->num_rows() == 0){
      return false;
    }
    $product_id = $query->row()->product_id;
    return $this->get_list($product_id);
  }
  public function get_all_bundle_list($bundle_id){
    $this->db->select('tbl_product_bundle.*');
    $this->db->where('bundle_id', $bundle_id);
    $this->db->join('tbl_product', 'tbl_product.product_id = tbl_product_bundle.product_id');
    $this->db->where('tbl_product.enable_status', 'show');
    $this->db->where('tbl_product.is_delete', 'active');
    return $this->db->get('tbl_product_bundle');

  }
}
