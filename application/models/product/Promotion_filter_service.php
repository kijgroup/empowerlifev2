<?php
class Promotion_filter_service extends CI_Model{
  var $per_page = 10;
  public function get_data_content($category_code,$page){
    $total_product = $this->count_list($category_code);
    $total_page = ceil($total_product/$this->per_page);
    $ret_val = array(
      'category_code' => $category_code,
      'page' => $page,
      'per_page' => $this->per_page,
      'product_list' => $this->get_list($category_code, $page),
      'total_product' => $total_product,
      'total_page' => $total_page
    );
    return $ret_val;
  }
  private function get_list($category_code, $page){
    $this->where_transaction($category_code);
    $offset = ($page - 1) * $this->per_page;
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_product', $this->per_page, $offset);
  }
  private function count_list($category_code){
    $this->where_transaction($category_code);
    $this->db->select('product_id');
    $query = $this->db->get('tbl_product');
    return $query->num_rows();
  }
  private function where_transaction($category_code){
    if($category_code !== 'all'){
      $this->load->model('product/Category_model');
      $this->db->select('category_id');
      $category = $this->Category_model->get_data_by_slug($category_code);
      $this->db->select('product_id');
      $this->db->where('category_id', $category->category_id);
      $this->db->where('product_type', 'product');
      $this->db->where('is_delete', 'active');
      $product_list = $this->db->get('tbl_product');
      $product_id_list = array();
      foreach($product_list->result() as $product){
        $product_id_list[] = $product->product_id;
      }
      $promotion_id_list = array();
      if(count($product_id_list)>0){
        $this->db->distinct();
        $this->db->select('product_id');
        $this->db->where_in('bundle_id', $product_id_list);
        $bundle_list = $this->db->get('tbl_product_bundle');
        foreach($bundle_list->result() as $bundle){
          $promotion_id_list[] = $bundle->product_id;
        }
      }
      if(count($promotion_id_list) > 0){
        $this->db->where_in('product_id', $promotion_id_list);
      }else{
        $this->db->where('product_id', 0);
      }
    }
    $this->db->where("(is_expire = 'false' OR expire_date >= NOW())");
    $this->db->where('product_type', 'promotion');
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
  }
}
