<?php
class Product_model extends CI_Model{
  public function is_remain_promotion(){
    $this->db->where("(is_expire = 'false' OR expire_date >= NOW())");
    $this->db->where('product_type', 'promotion');
    $this->db->where('is_delete', 'active');
    $this->db->where('enable_status', 'show');
    $query = $this->db->get('tbl_product');
    return ($query->num_rows() > 0);
  }
  public function get_home_product_list(){
    $this->db->where('product_type', 'product');
    $this->db->where('is_delete', 'active');
    $this->db->where('enable_status', 'show');
    $this->db->order_by('sort_priority', 'ASC');
    $this->db->limit(6);
    return $this->db->get('tbl_product');
  }
  public function get_home_promotion_list(){
    $this->db->where("(is_expire = 'false' OR expire_date >= NOW())");
    $this->db->where('product_type', 'promotion');
    $this->db->where('is_delete', 'active');
    $this->db->where('enable_status', 'show');
    $this->db->order_by('sort_priority', 'ASC');
    $this->db->limit(6);
    return $this->db->get('tbl_product');
  }
  public function count_in_category($category_id){
    $this->db->select('product_id');
    $this->db->where('category_id', $category_id);
    $this->db->where('product_type', 'product');
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_product');
    return $query->num_rows();
  }
  public function count_promotion_in_category($category_id){
    $this->db->select('product_id');
    $this->db->where('category_id', $category_id);
    $this->db->where('product_type', 'product');
    $this->db->where('is_delete', 'active');
    $product_list = $this->db->get('tbl_product');
    $product_id_list = array();
    foreach($product_list->result() as $product){
      $product_id_list[] = $product->product_id;
    }
    $promotion_id_list = array();
    if(count($product_id_list)>0){
      $this->db->distinct();
      $this->db->select('product_id');
      $this->db->where_in('bundle_id', $product_id_list);
      $bundle_list = $this->db->get('tbl_product_bundle');
      foreach($bundle_list->result() as $bundle){
        $promotion_id_list[] = $bundle->product_id;
      }
    }
    if(count($promotion_id_list)===0){
      return 0;
    }
    $this->db->select('product_id');
    $this->db->where("(is_expire = 'false' OR expire_date >= NOW())");
    $this->db->where('product_type', 'promotion');
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $this->db->where_in('product_id', $promotion_id_list);
    $query = $this->db->get('tbl_product');
    return $query->num_rows();
  }
  public function get_in_category($category_id){
    $this->db->where('category_id', $category_id);
    $this->db->where('product_type', 'product');
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_product');
  }
  public function get_all_in_category($category_id){
    $product_id_list = array();
    $this->db->where('category_id', $category_id);
    $this->db->where('product_type', 'product');
    $this->db->where('is_delete', 'active');
    $product_list = $this->db->get('tbl_product');
    foreach($product_list->result() as $product){
      $product_id_list[] = $product->product_id;
    }
    $promotion_id_list = array();
    if(count($product_id_list)>0){
      $this->db->distinct();
      $this->db->select('product_id');
      $this->db->where_in('bundle_id', $product_id_list);
      $bundle_list = $this->db->get('tbl_product_bundle');
      foreach($bundle_list->result() as $bundle){
        $promotion_id_list[] = $bundle->product_id;
      }
    }
    $product_list = $this->get_in_category($category_id);
    if(count($promotion_id_list)===0){
      return $product_list->result();
    }
    $this->db->where("(is_expire = 'false' OR expire_date >= NOW())");
    $this->db->where('product_type', 'promotion');
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $this->db->where_in('product_id', $promotion_id_list);
    $this->db->order_by('sort_priority', 'ASC');
    $promotion_list = $this->db->get('tbl_product');
    return array_merge($promotion_list->result(), $product_list->result());
  }
  public function get_all_list(){
    $this->db->where("(is_expire = 'false' OR expire_date >= NOW())");
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $this->db->order_by('product_type', 'asc');
    return $this->db->get('tbl_product');
  }
  public function get_data($product_id){
    $this->db->where('product_id', $product_id);
    return $this->query_get_data();
  }
  public function get_name($product_id, $lang){
    return $this->return_name_lang($this->get_data($product_id), $lang);
  }
  public function get_data_by_slug($slug){
    $this->db->where('slug', $slug);
    return $this->query_get_data();
  }
  public function get_name_by_slug($slug, $lang){
    return $this->return_name_lang($this->get_data_by_slug($slug), $lang);
  }
  private function query_get_data(){
    $query = $this->db->get('tbl_product');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function return_name_lang($product, $lang){
    if(!$product){
      return '-';
    }
    if($lang == 'th'){
      return $product->name_th;
    }else{
      return $product->name_en;
    }
  }
}
