<?php
class Product_filter_service extends CI_Model{
  var $per_page = 10;
  public function get_data_content($category_slug, $sort_by, $page){
    $this->load->model('product/Category_model');
    $category = $this->Category_model->get_data_by_slug($category_slug);
    $category_id = ($category)?$category->category_id:0;
    $ret_val = array(
      'category_slug' => $category_slug,
      'sort_by' => $sort_by,
      'category' => $category,
      'page' => $page,
      'per_page' => $this->per_page,
      'product_list' => $this->get_list($category_id, $sort_by, $page),
      'total_product' => $this->count_list($category_id),
      'total_page' => 1
    );
    $ret_val['total_page'] = ($ret_val['total_product'] == 0)?1:ceil($ret_val['total_product']/$this->per_page);
    return $ret_val;
  }
  private function get_list($category_id, $sort_by, $page){
    $this->where_transaction($category_id);
    $offset = ($page - 1) * $this->per_page;
    $this->order($sort_by);
    return $this->db->get('tbl_product', $this->per_page, $offset);
  }
  private function order($sort_by){
    switch ($sort_by) {
      case 'default':
        //$field_name = ($this->config->item('language_abbr')=='th')?'name_th':'name_en';
        $this->db->order_by('sort_priority', 'ASC');
        break;
      case 'popular':
        $this->db->order_by('sort_priority', 'ASC');
        break;
      case 'arrival':
        $this->db->order_by('create_date', 'DESC');
        break;
      case 'low':
        $this->db->order_by('price', 'ASC');
        break;
      case 'high':
        $this->db->order_by('price', 'DESC');
        break;
      default:
        break;
    }
  }
  private function count_list($category_id){
    $this->where_transaction($category_id);
    $query = $this->db->get('tbl_product');
    return $query->num_rows();
  }
  private function where_transaction($category_id){
    if($category_id != 0){
      $this->db->where('category_id', $category_id);
    }
    $this->db->where('product_type', 'product');
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
  }
}
