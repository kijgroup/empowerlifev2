<?php
class Bank_model extends CI_Model{
  public function get_data($bank_id){
    $this->db->where('bank_id', $bank_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_bank');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_list(){
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_bank');
  }
}
