<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Thailand_model extends CI_Model{
  public function get_province_list(){
    return $this->db->get('province');
  }
  public function get_amphur_list($province_id){
    $this->db->where('PROVINCE_ID', $province_id);
    return $this->db->get('amphur');
  }
  public function get_district_list($amphur_id){
    $this->db->where('AMPHUR_ID', $amphur_id);
    return $this->db->get('district');
  }
  public function get_postcode($district_id){
    $this->db->where('district_id', $district_id);
    $query = $this->db->get('zipcode');
    if($query->num_rows() == 0){
      return '';
    }
    return $query->row()->zipcode;
  }
}