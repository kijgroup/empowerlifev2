<?php
class Subscribe_model extends CI_Model{
  public function insert($subscribe_email){
    $subscribe = $this->get_data($subscribe_email);
    if($subscribe){
      $this->enable($subscribe->subscribe_id);
    }else{
      $this->db->set('subscribe_email', $subscribe_email);
      $this->db->set('subscribe_status', 'enable');
      $this->db->set('create_date', 'NOW()', false);
      $this->db->set('create_by', 0);
      $this->db->insert('tbl_subscribe');
    }
  }
  public function cancel($subscribe_id){
    $this->update_status($subscribe_id, 'disable');
  }
  public function enable($subscribe_id){
    $this->update_status($subscribe_id, 'enable');
  }
  private function update_status($subscribe_id, $subscribe_status){
    $this->db->set('subscribe_status', $subscribe_status);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('subscribe_id',$subscribe_id);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_subscribe');
  }
  private function get_data($subscribe_email){
    $this->db->where('subscribe_email', $subscribe_email);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_subscribe');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
}
