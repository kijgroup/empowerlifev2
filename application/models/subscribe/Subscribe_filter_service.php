<?php
class Subscribe_filter_service extends CI_Model {

  function __construct() {
    parent::__construct();
  }

  public function get_data_content($search_val, $start_date, $end_date, $page = 1, $per_page = 20) {
    $data = array();
    $data['search_val'] = $search_val;
    $data['start_date'] = $start_date;
    $data['end_date'] = $end_date;
    $data['page'] = $page;
    $data['per_page'] = $per_page;
    $data['subscribe_list'] = $this->get_list($search_val, $start_date, $end_date, $page, $per_page);
    $data['total_transaction'] = $this->get_count($search_val, $start_date, $end_date);
    return $data;
  }
  private function get_count($search_val, $start_date, $end_date){
    $this->db->select('tbl_subscribe.subscribe_id');
    $this->where_transaction($search_val, $start_date, $end_date);
    $query = $this->db->get('tbl_subscribe');
    return $query->num_rows();
  }
  private function get_list($search_val, $start_date, $end_date, $page, $per_page){
    $this->db->select('tbl_subscribe.*');
    $this->where_transaction($search_val, $start_date, $end_date);
    $offset = ($page - 1) * $per_page;
    return $this->db->get('tbl_subscribe', $per_page, $offset);
  }
  private function where_transaction($search_val, $start_date, $end_date){
    if($search_val != ''){
      $str_where = "(subscribe_email like '%$search_val%' ";
      $str_where .= ')';
      $this->db->where($str_where, NULL,FALSE);
    }
    if($start_date != ''){
      $this->db->where('create_date >=', $start_date.' 00:00:00');
    }
    if($end_date != ''){
      $this->db->where('create_date <=', $end_date.' 23:59:59');
    }
    $this->db->where('is_delete', 'active');
  }
}
