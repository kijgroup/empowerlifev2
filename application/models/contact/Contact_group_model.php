<?php
class Contact_group_model extends CI_Model{
  public function get_list(){
  $this->db->where('enable_status', 'show');
  $this->db->where('is_delete', 'active');
  $this->db->order_by('sort_priority', 'ASC');
  return $this->db->get('tbl_contact_group');
  }
  public function get_data($contact_group_id){
    $this->db->where('contact_group_id', $contact_group_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_contact_group');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function return_name_lang($contact_group, $lang){
    if(!$contact_group){
      return '-';
    }
    if($lang == 'th'){
      return $contact_group->contact_group_th;
    }else{
      return $contact_group->contact_group_en;
    }
  }
}
