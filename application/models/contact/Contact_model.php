<?php
class Contact_model extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function insert($data){
    $member_id = 0;
    if($this->session->userdata('member_logged_in')){
      $member_id = $this->session->userdata('member_id');
    }
    $this->db->set('contact_group_id', $data['contact_group_id']);
    $this->db->set('contact_name', $data['contact_name']);
    $this->db->set('contact_email', $data['contact_email']);
    $this->db->set('contact_phone', $data['contact_phone']);
    $this->db->set('contact_detail', $data['contact_detail']);
    $this->db->set('member_id', $member_id);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->insert('tbl_contact');
  }
}
