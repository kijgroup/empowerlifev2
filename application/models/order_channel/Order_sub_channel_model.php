<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order_sub_channel_model extends MY_Model{
  public $table_name = 'tbl_order_sub_channel';
  public $field_key = 'order_sub_channel_id';
  public $field_name = 'order_sub_channel_name';
  public $field_search_val_list = array('tbl_order_sub_channel');
  public $is_sort_priority = true;
  public $structure = array(
    array(
      'field_name' => 'Source',
      'field_key' => 'order_sub_channel_name',
      'field_type' => 'text',
      'placeholder' => 'โปรดระบุ *',
      'required' => true
    ),
  );
}
