<?php

Class Order_channel_model extends CI_Model{

  public function get_list(){
    $this->db->where('is_delete','active');
    $this->db->order_by('sort_priority','ASC');
    return $this->db->get('tbl_order_channel');
  }

  public function get_data($order_channel_id){
    $this->db->where('order_channel_id', $order_channel_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_order_channel');
    if($query->num_rows()==0){
      return false;
    }
    return $query->row();
  }

  public function get_name($order_channel_id){
    $order_channel = $this->get_data($order_channel_id);
    if(!$order_channel){
      return 'ไม่ระบุ';
    }
    return $order_channel->order_channel_name;
  }

  public function get_max_priority(){
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_order_channel');
    return $query->num_rows();
  }

  public function insert($data){
    $this->move_up_priority($data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_order_channel');
    $order_channel_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('order_channel',$order_channel_id,'insert','insert new order_channel ', $data, $this->db->last_query());
    return $order_channel_id;
  }

  public function update($order_channel_id, $data){
    $order_channel = $this->get_data($order_channel_id);
    if($order_channel){
      $this->move_down_priority($order_channel->sort_priority);
      $this->move_up_priority($data['sort_priority']);
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('order_channel_id',$order_channel_id);
      $this->db->update('tbl_order_channel');
      $this->Log_action_model->insert_log('order_channel', $order_channel_id,'update','update order_channel', $data, $this->db->last_query());
    }
  }

  public function delete($order_channel_id){
    $order_channel = $this->get_data($order_channel_id);
    if($order_channel){
      $this->move_down_priority($order_channel->sort_priority);
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('order_channel_id', $order_channel_id);
      $this->db->update('tbl_order_channel');
      $this->Log_action_model->insert_log('order_channel', $order_channel_id, 'delete','detele order_channel', array(), $this->db->last_query());
    }
  }

  private function set_to_db($data){
    $this->db->set('order_channel_name', $data['order_channel_name']);
    $this->db->set('sort_priority', $data['sort_priority']);
  }

  private function move_up_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_order_channel');
  }

  private function move_down_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority -1', false);
    $this->db->where('sort_priority >', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_order_channel');
  }

}
