<?php
class Log_action_model extends CI_Model {
    function __construct() {
        parent::__construct();
    }
    public function get_data($log_action_id){
        $this->db->where('log_action_id', $log_action_id);
        $query = $this->db->get('tbl_log_action');
        if($query->num_rows() > 0){
            return $query->row();
        }
        return false;
    }
    public function insert_log($page, $page_id, $action_type, $action_name, $data, $last_query){
        $data = array();
        $data['log_action_page'] = $page;
        $data['page_id'] = $page_id;
        $data['log_action_type'] = $action_type;
        $data['log_action_name'] = $action_name;
        $data['log_action_detail'] = $this->concat_log_data($data);
        $data['log_action_query'] = $last_query;
        $this->insert($data);
    }
    public function insert($data){
        $this->db->set('admin_id', $this->session->userdata('admin_id'));
        $this->db->set('log_action_page', $data['log_action_page']);
        $this->db->set('page_id', $data['page_id']);
        $this->db->set('log_action_type', $data['log_action_type']);
        $this->db->set('log_action_name', $data['log_action_name']);
        $this->db->set('log_action_detail', $data['log_action_detail']);
        $this->db->set('log_action_query', $data['log_action_query']);
        $this->db->set('create_date', 'NOW()', false);
        $this->db->insert('tbl_log_action');
    }
    public function concat_log_data($data){
        $str = '';
        foreach($data as $key=>$value){
            if($key != 'login_password'){
                $str .= '<br />'.$key.' : '.$value;
            }
        }
        return $str;
    }
}
