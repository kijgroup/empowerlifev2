<?php
class Login_service extends CI_Model {
  var $NO_EMAIL = 'require_username';
  var $NO_PASSWORD = 'require_password';
  var $NO_USER_FOUND = 'user_and_password_miss_match';
  var $USERTYPE_INVALID = 'unenable_user';
  var $response_status = true;
  var $response_msg = '';

  function __construct() {
    parent::__construct();
    $this->load->model(array(
      'member/Member_model',
      'member/User_model'
    ));
  }

  public function must_login() {
    if (!$this->get_login_status()) {
      $this->input->set_cookie(array(
        'name' => 'after_login_url',
        'value' => base_url(uri_string()),
        'expire' => 600,
        'path'   => '/'
      ));
      redirect('login');
    }
  }
  public function must_not_login() {
    if ($this->get_login_status()) {
      redirect('home');
    }
  }

  public function logout() {
    $this->session->sess_destroy();
  }

  public function get_error_message($error) {
    $error_message = '';
    if ($error != '') {
      $arr_message[$this->NO_EMAIL] = 'กรุณาระบุอีเมล';
      $arr_message[$this->NO_PASSWORD] = 'กรุณาระบุรหัสผ่าน';
      $arr_message[$this->NO_USER_FOUND] = 'กรุณาตรวจสอบชื่อผู้ใช้ และรหัสผ่านอีกครั้ง';
      $arr_message[$this->USERTYPE_INVALID] = 'บัญชีนี้ไม่สามารถใช้ในระบบนี้ได้';
      $error_message = (isset($arr_message[$error])) ? $arr_message[$error] : $error;
    }
    return $error_message;
  }

  public function login($email, $login_password) {
    $this->check_email_input($email);
    if($this->response_status){
      $this->check_password_input($login_password);
    }
    if($this->response_status){
      $member = $this->check_member_login($email, $login_password);
    }
    if($this->response_status){
      $this->check_member_allow($member);
    }
    if($this->response_status){
      $this->create_session($member);
    }
    return array(
      'resp_status' => $this->response_status,
      'resp_msg' => $this->get_error_message($this->response_msg)
    );
  }

  public function get_login_status() {
    if ($this->session->userdata('member_logged_in')) {
      return true;
    }
    $email = get_cookie('member_email');
    if(isset($member_email)){
      $token = get_cookie('token');
      $user = $this->User_model->get_token($email);
      if($user && $user->enable_status == 'active'){
        $member = $this->Member_model->get_data($user->member_id);
        $this->create_session($member);
        return true;
      }
    }
    return false;
  }

  private function check_email_input($email) {
    if (!$email) {
      throw new Exception($this->NO_EMAIL);
    }
  }

  private function check_password_input($login_password) {
    if (!$login_password) {
      throw new Exception($this->NO_PASSWORD);
    }
  }

  private function check_member_login($email, $login_password) {
    $user = $this->User_model->get_login($email, $login_password);
    if (!$user) {
      throw new Exception($this->NO_USER_FOUND);
      return false;
    }
    $member = $this->Member_model->get_data($user->member_id);
    return $member;
  }
  private function check_member_allow($member) {
    if ($member->enable_status != 'active') {
      throw new Exception($this->USERTYPE_INVALID);
    }
  }
  public function create_session($member) {
    $userdata = array();
    $userdata['member_id'] = $member->member_id;
    $userdata['member_type'] = $member->member_type;
    $userdata['member_email'] = $member->member_email;
    $userdata['member_name'] = $member->member_firstname;
    $userdata['member_logged_in'] = TRUE;
    $this->session->set_userdata($userdata);
    $this->Cart_model->update_price();
  }

}
