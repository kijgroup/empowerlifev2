<?php
class Bank_payment_model extends CI_Model{
  public function insert($data){
    $member_id = ($this->session->userdata('member_id') !== null)?$this->session->userdata('member_id'):0;
    $this->db->set('payment_id', 0);
    $this->db->set('bank_id', $data['bank_id']);
    $this->db->set('order_id', $data['order_id']);
    $this->db->set('member_id', $member_id);
    $this->db->set('bank_payment_name', $data['bank_payment_name']);
    $this->db->set('bank_payment_date', $data['bank_payment_date']);
    $this->db->set('bank_payment_time', $data['bank_payment_time']);
    $this->db->set('bank_payment_amount', $data['bank_payment_amount']);
    $this->db->set('bank_payment_phone', $data['bank_payment_phone']);
    $this->db->set('order_code', $data['order_code']);
    $this->db->set('bank_payment_image', $data['bank_payment_image']);
    $this->db->set('bank_payment_status', 'checking');
    $this->db->set('bank_payment_note', '');
    $this->db->set('is_delete', 'active');
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_bank_payment');
    return $this->db->insert_id();
  }
}
