<?php
class Masterpage_service extends CI_Model {
  var $css_list;
  var $js_list;
  var $data;
  function __construct() {
    parent::__construct();
    $this->css_list = array();
    $this->js_list = array();
    $this->data = array();
  }
  public function get_lang_full(){
    $curr_lang = $this->config->item('language_abbr');
    return ($curr_lang == 'th')?'thai':'english';
  }
  public function change_lange_url(){
    $curr_lang = $this->config->item('language_abbr');
    $lang = ($curr_lang == 'en')?'th':'en';
    $url = base_url($lang).uri_string();
    return ($_SERVER['QUERY_STRING']) ? $url.'?'.$_SERVER['QUERY_STRING'] : $url;
  }
  public function display($content, $title = '', $nav = ''){
    $data = $this->data;
    $data['content'] = $content;
    $data['title'] = $title;
    $data['nav'] = $nav;
    $this->load->view('masterpage/masterpage', $data);
  }
  public function set_data($key, $value){
    $this->data[$key] = $value;
  }
  public function add_css($css_file){
    $this->css_list[] = base_url($css_file);
  }
  public function add_js($js_file){
    $this->js_list[] = base_url($js_file);
  }
  public function add_outer_css($css_file){
    $this->css_list[] = $css_file;
  }
  public function add_outer_js($js_file){
    $this->js_list[] = $js_file;
  }
  public function print_css(){
    return $this->load->view('masterpage/cssloader', array('css_list'=>$this->css_list), FALSE);
  }
  public function print_js(){
    return $this->load->view('masterpage/jsloader', array('js_list'=>$this->js_list), FALSE);
  }
}
