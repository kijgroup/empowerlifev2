<?php
class Discount_point_model extends CI_Model{
  public function get_list(){
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'asc');
    return $this->db->get('tbl_discount_point');
  }
  public function get_data($discount_point_id){
    $this->db->where('discount_point_id', $discount_point_id);
    $query = $this->db->get('tbl_discount_point');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
}
