<?php
class Order_model extends CI_Model{
  public function get_list(){
    $this->db->where('member_id', $this->session->userdata('member_id'));
    $this->db->where('is_delete', 'active');
    return $this->db->get('tbl_order');
  }
  public function get_data($order_id){
    $this->db->where('order_id', $order_id);
    return $this->query_get_data();
  }
  public function get_data_by_code($order_code){
    $this->db->where('order_code', $order_code);
    return $this->query_get_data();
  }
  private function query_get_data(){
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_order');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function insert($data){
    $this->set_next_code();
    $this->set_to_db($data);
    $this->db->set('member_id', $this->session->userdata('member_id'));
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_order');
    $order_id = $this->db->insert_id();
    return $order_id;
  }
  public function update($order_id, $data){
    $this->set_to_db($data);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('order_id', $order_id);
    $this->db->update('tbl_order');
  }
  public function delete($order_id){
    $this->db->set('is_delete', 'delete');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('order_id', $order_id);
    $this->db->update('tbl_order');
  }
  public function update_price($order_id, $data){
    $this->db->set('total_item', $data['total_item']);
    $this->db->set('item_price', $data['item_price']);
    $this->db->set('discount_price', $data['discount_price']);
    $this->db->set('shipping_price', $data['shipping_price']);
    $this->db->set('grand_total', $data['grand_total']);
    $this->db->where('order_id', $order_id);
    $this->db->update('tbl_order');
  }
  private function set_to_db($data){
    $this->db->set('discount_point_amount', $data['discount_point_amount']);
    $this->db->set('discount_by_point_amount', $data['discount_by_point_amount']);
    $this->db->set('shipping_name', $data['shipping_name']);
    $this->db->set('shipping_address', $data['shipping_address']);
    $this->db->set('shipping_post_code', $data['shipping_post_code']);
    $this->db->set('shipping_district', $data['shipping_district']);
    $this->db->set('shipping_amphur', $data['shipping_amphur']);
    $this->db->set('shipping_province', $data['shipping_province']);
    $this->db->set('shipping_mobile', $data['shipping_mobile']);
    $this->db->set('is_tax', $data['is_tax']);
    $this->db->set('tax_name', $data['tax_name']);
    $this->db->set('tax_address', $data['tax_address']);
    $this->db->set('tax_post_code', $data['tax_post_code']);
    $this->db->set('tax_district', $data['tax_district']);
    $this->db->set('tax_amphur', $data['tax_amphur']);
    $this->db->set('tax_province', $data['tax_province']);
    $this->db->set('tax_mobile', $data['tax_mobile']);
    $this->db->set('tax_code', $data['tax_code']);
    $this->db->set('tax_branch', $data['tax_branch']);
    $this->db->set('payment_type', $data['payment_type']);
    $this->db->set('postoffice', $data['postoffice']);
  }
  public function update_status($order_id, $order_status){
    $this->db->set('order_status', $order_status);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('order_id', $order_id);
    $this->db->update('tbl_order');
  }
  public function set_next_code(){
    $code_month = $this->get_order_code_month();
    $code_number = $this->get_next_running_number($code_month);
    $order_code = 'OD'.$code_month.str_pad($code_number, 4, "0", STR_PAD_LEFT);
    $this->db->set('order_code_month', $code_month);
    $this->db->set('order_code_running', $code_number);
    $this->db->set('order_code', $order_code);
    return $order_code;
  }
  public function get_next_running_number($code_month){
    $ret_val = 0;
    $this->db->select('MAX(order_code_running) AS max_number');
    $this->db->where('order_code_month', $code_month);
    $query = $this->db->get('tbl_order');
    if($query->num_rows() > 0){
      $ret_val = $query->row()->max_number;
    }
    $ret_val ++;
    return $ret_val;
  }
  private function get_order_code_month(){
    $ret_val = str_pad(((date('y')+543)%100),2,"0", STR_PAD_LEFT);
    $ret_val .= date('m');
    return $ret_val;
  }
}
