<?php
class Payment_status_model extends CI_Model{
  var $status_list = array(
    'unpaid' => array(
      'status_code' => 'unpaid',
      'status_name' => '',
      'label_class' => 'label label-warning'
    ),
    'paid' => array(
      'status_code' => 'paid',
      'status_name' => 'ชำระเงินแล้ว',
      'label_class' => 'label label-success'
    )
  );
  public function get_list(){
    return $this->status_list;
  }
  public function get_data($status_code){
    if(!isset($this->status_list[$status_code])){
      return false;
    }
    return $this->status_list[$status_code];
  }
  public function get_name($status_code){
    $status = $this->get_data($status_code);
    return $status['status_name'];
  }
  public function get_label($status_code){
    $status = $this->get_data($status_code);
    if(!isset($status['status_name'])){
      return '-';
    }
    return '<span class="'.$status['label_class'].'">'.$status['status_name'].'</span>';
  }
}
