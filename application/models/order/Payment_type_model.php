<?php
class Payment_type_model extends CI_Model{
  var $status_list = array(
    'bank' => array(
      'status_code' => 'bank',
      'status_name' => 'โอนเงิน จัดส่งโดย ไปรษณีย์ EMS',
      'label_class' => 'label label-primary'
    ),
    'post' => array(
      'status_code' => 'post',
      'status_name' => 'ชำระเงินและรับสินค้าที่ไปรษณีย์',
      'label_class' => 'label label-warning'
    ),
    'home' => array(
      'status_code' => 'home',
      'status_name' => 'จัดส่งและชำระเงินที่หน้าบ้านโดย ขนส่ง DHL',
      'label_class' => 'label label-danger'
    ),
    'cash' => array(
      'status_code' => 'cash',
      'status_name' => 'เงินสด',
      'label_class' => 'label label-success'
    ),
    'creditcard' => array(
      'status_code' => 'creditcard',
      'status_name' => 'Credit Card',
      'label_class' => 'label label-success'
    ),
    'debitcard' => array(
      'status_code' => 'debitcard',
      'status_name' => 'Debit Card',
      'label_class' => 'label label-success'
    ),
  );
  public function get_list(){
    return $this->status_list;
  }
  public function get_data($status_code){
    if(!isset($this->status_list[$status_code])){
      return false;
    }
    return $this->status_list[$status_code];
  }
  public function get_name($status_code){
    $status = $this->get_data($status_code);
    return $status['status_name'];
  }
  public function get_label($status_code){
    $status = $this->get_data($status_code);
    if(!isset($status['status_name'])){
      return '-';
    }
    return '<span class="'.$status['label_class'].'">'.$status['status_name'].'</span>';
  }
}
