<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Order_discount_point_model extends CI_Model{
  public function insert($data){
    foreach($data as $field_name => $value){
      $this->db->set($field_name, $value);
    }
    $this->db->insert('tbl_order_discount_point');
  }
}