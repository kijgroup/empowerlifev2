<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Contact_channel_model extends MY_Model{
  public $table_name = 'tbl_contact_channel';
  public $field_key = 'contact_channel_id';
  public $field_name = 'contact_channel_name';
  public $field_search_val_list = array('contact_channel_name');
  public $is_sort_priority = true;
  public $structure = array(
    array(
      'field_name' => 'ช่องทางการติดต่อ',
      'field_key' => 'contact_channel_name',
      'field_type' => 'text',
      'placeholder' => 'โปรดระบุ *',
      'required' => true
    ),
  );
}
