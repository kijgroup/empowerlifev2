<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Shipping_type_model extends MY_Model{
  public $table_name = 'tbl_shipping_type';
  public $field_key = 'shipping_type_id';
  public $field_name = 'shipping_type_name';
  public $field_search_val_list = array('shipping_type_name');
  public $is_sort_priority = true;
  public $structure = array(
    array(
      'field_name' => 'ช่องทางการจัดส่ง',
      'field_key' => 'shipping_type_name',
      'field_type' => 'text',
      'placeholder' => 'โปรดระบุ *',
      'required' => true
    ),
  );
}
