<?php
class Send_time_status_model extends CI_Model{
  var $status_list = array(
    '' => array(
      'status_code' => '',
      'status_name' => '',
      'label_class' => ''
    ),
    'morning' => array(
      'status_code' => 'morning',
      'status_name' => 'ส่งเช้า',
      'label_class' => 'label label-primary'
    )
  );
  public function get_list(){
    return $this->status_list;
  }
  public function get_data($status_code){
    if(!isset($this->status_list[$status_code])){
      return false;
    }
    return $this->status_list[$status_code];
  }
  public function get_name($status_code){
    $status = $this->get_data($status_code);
    return $status['status_name'];
  }
  public function get_label($status_code){
    $status = $this->get_data($status_code);
    if(!isset($status['status_name'])){
      return '-';
    }
    return '<span class="'.$status['label_class'].'">'.$status['status_name'].'</span>';
  }
}
