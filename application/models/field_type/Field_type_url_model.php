<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field_type_url_model extends CI_Model{
  protected $default_config = array(
    'field_name' => 'ไม่ระบุ',
    'field_key' => '',
    'placeholder' => '',
    'value' => '',
    'required' => false,
    'help_text' => ''
  );
  public function display_form($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    $this->load->view('field_type/form/text', $field_config);
  }
  public function display_detail($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    $this->load->view('field_type/detail/url', $field_config);
  }
  public function display_value($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    return $field_config['value'];
  }
}
