<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field_type_password_model extends CI_Model{
  protected $default_config = array(
    'field_name' => 'ไม่ระบุ',
    'field_key' => '',
    'placeholder' => 'โปรดระบุรหัสผ่าน',
    'value' => '',
    'required' => true,
    'help_text' => ''
  );
  public function display_form($my_config){
    if($my_config['value'] != ''){
      $my_config['required'] = false;
    }
    $field_config = array_merge($this->default_config, $my_config);
    $this->load->view('field_type/form/password', $field_config);
  }
  public function display_detail($my_config){
  }
  public function display_value($my_config){
  }
}
