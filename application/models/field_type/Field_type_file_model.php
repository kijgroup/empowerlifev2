<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field_type_file_model extends CI_Model{
  protected $default_config = array(
    'field_name' => 'ไม่ระบุ',
    'field_key' => '',
    'placeholder' => '',
    'value' => '',
    'required' => false,
    'help_text' => 'ขนาดไฟล์ไม่เกิน 2 MB'
  );
  public function display_form($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    $field_config['required'] = ($field_config['value'] != '')?false:$field_config['required'];
    $this->load->view('field_type/form/file', $field_config);
  }
  public function display_detail($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    $this->load->view('field_type/detail/file', $field_config);
  }
  public function display_value($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    return $field_config['value'];
  }
}
