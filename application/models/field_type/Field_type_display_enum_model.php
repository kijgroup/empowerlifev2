<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field_type_display_enum_model extends CI_Model{
  protected $default_config = array(
    'field_name' => 'ไม่ระบุ',
    'value' => '',
    'source_data' => array(
      'active' => array(
        'source_key' => 'active',
        'source_name' => 'ปกติ',
        'source_label' => 'label label-success'
      ),
      'inactive' => array(
        'source_key' => 'inactive',
        'source_name' => 'ไม่ปกติ',
        'source_label' => 'label label-danger'
      ),
    ),
    'source_model' => '',
    'help_text' => ''
  );
  public function display_form($my_config){
    if($my_config['value'] != ''){
			$field_config = array_merge($this->default_config, $my_config);
			$field_config['value'] = $this->get_display_value($field_config);
			$this->load->view('field_type/detail/text', $field_config);
		}
  }
  public function display_detail($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    $field_config['value'] = $this->get_display_value($field_config);
    $this->load->view('field_type/detail/text', $field_config);
  }
  private function get_display_value($field_config){
		$selected_data = $field_config['source_data'][$field_config['value']];
		if(!isset($selected_data)){
			return '-';
		}
		return '<span class="'.$selected_data['source_label'].'">'.$selected_data['source_name'].'</span>';
  }
  public function display_value($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    return $this->get_display_value($field_config);
  }
}
