<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Field_type_date_model extends CI_Model{
  protected $default_config = array(
    'field_name' => 'ไม่ระบุ',
    'field_key' => '',
    'placeholder' => '',
    'value' => '',
    'required' => false,
    'help_text' => ''
  );
  public function display_form($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    $this->load->view('field_type/form/date', $field_config);
  }
  public function display_detail($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    $this->load->view('field_type/detail/date', $field_config);
  }
  public function display_value($my_config){
    $field_config = array_merge($this->default_config, $my_config);
    return $this->Datetime_service->display_date($field_config['value']);
  }
}
