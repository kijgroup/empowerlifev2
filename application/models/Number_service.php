<?php

class Number_service extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function format_int($number){
  	return number_format($number);
  }
  public function format_float($number){
    return number_format($number, 2, '.', ',');
  }
}
