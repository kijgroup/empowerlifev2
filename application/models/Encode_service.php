<?php
class Encode_service extends CI_Model{
  var $PAD_STRING_LENGTH = 20;
  public function encode_number($string){
    return base64_encode(gzdeflate(str_pad($string, $this->PAD_STRING_LENGTH, "0", STR_PAD_LEFT ), 9));
  }
  public function decode_number($compressed){
    return @gzinflate(base64_decode($compressed), $this->PAD_STRING_LENGTH);
  }
}
