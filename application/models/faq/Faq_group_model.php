<?php
class Faq_group_model extends CI_Model{
  public function get_list(){
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_faq_group');
  }
  public function get_first_id(){
    $this->db->select('faq_group_id');
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    $query = $this->db->get('tbl_faq_group', 1, 0);
    if($query->num_rows() == 0){
      return 0;
    }
    return $query->row()->faq_group_id;
  }
  public function get_data($faq_group_id){
    $this->db->where('faq_group_id', $faq_group_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_faq_group');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function return_name_lang($faq_group, $lang){
    if(!$faq_group){
      return '-';
    }
    if($lang == 'th'){
      return $faq_group->faq_group_th;
    }else{
      return $faq_group->faq_group_en;
    }
  }
}
