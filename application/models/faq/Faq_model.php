<?php
class Faq_model extends CI_Model{
  public function get_list($faq_group_id){
    $this->db->where('faq_group_id', $faq_group_id);
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_faq');
  }
}
