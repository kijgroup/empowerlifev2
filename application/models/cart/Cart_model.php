<?php
class Cart_model extends CI_Model{
  public $min_free_shipping = 360;
  public $shipping_cost = 50;
  public function get_list(){
    if(!$this->session->has_userdata('cart_items')){
      return array();
    }
    return $this->session->userdata('cart_items');
  }
  public function add_item($product_id, $quantity){
    if($product_id > 0 && $quantity >= 0){
      if($this->session->has_userdata('cart_items')){
        $cart_items = $this->session->userdata('cart_items');
        $is_update = false;
        foreach($cart_items as $key => $cart_item){
          if($cart_item['product_id'] == $product_id){
            if($quantity > 0){
              $cart_items[$key]['quantity'] = $quantity;
            }else{
              array_splice($cart_items, $key, 1);
            }
            $is_update = true;
            break;
          }
        }
        if(!$is_update){
          $cart_items[] = array(
            'product_id' => $product_id,
            'quantity' => $quantity
          );
        }
      }else{
        $cart_items = array();
        $cart_items[] = array(
          'product_id' => $product_id,
          'quantity' => $quantity
        );
      }
      $this->session->set_userdata('cart_items', $cart_items);
      $this->update_price();
    }
  }
  public function delete_item($product_id){
    if($product_id > 0){
      if($this->session->has_userdata('cart_items')){
        $cart_items = $this->session->userdata('cart_items');
        foreach($cart_items as $key => $cart_item){
          if($cart_item['product_id'] == $product_id){
            array_splice($cart_items, $key, 1);
            break;
          }
        }
        $this->session->set_userdata('cart_items', $cart_items);
        $this->update_price();
      }
    }
  }
  public function clear_cart(){
    $this->session->set_userdata('cart_items', array());
    $this->update_price();
  }
  public function update_price(){
    $this->load->model(array('product/Product_model','member/Member_type_model'));
    $total_items_price = 0;
    $cart_items = $this->session->userdata('cart_items');
    if(!isset($cart_items)){
      return false;
    }
    foreach($cart_items as $key => $cart_item){
      $product = $this->Product_model->get_data($cart_item['product_id']);
      $total_items_price += $product->price * $cart_item['quantity'];
    }
    $discount_rate = 0;
    if($this->Login_service->get_login_status()){
      $member = $this->Member_model->get_data($this->session->userdata('member_id'));
      $member_type = $this->Member_type_model->get_data($member->member_type);
      if($total_items_price >= $member_type->min_amount){
        $discount_rate = $member_type->discount;
      }
    }
    $discount_amount = $total_items_price * $discount_rate / 100;
    $shipping_price = ($total_items_price < $this->min_free_shipping)?$this->shipping_cost:0;
    $cart_price = $total_items_price - $discount_amount + $shipping_price;
    $this->session->set_userdata('items_price', $total_items_price);
    $this->session->set_userdata('discount_rate', $discount_rate);
    $this->session->set_userdata('discount', $discount_amount);
    $this->session->set_userdata('shipping_price', $shipping_price);
    $this->session->set_userdata('cart_price', $cart_price);
  }
  public function get_items_price(){
    if($this->session->has_userdata('items_price')){
      return $this->session->userdata('items_price');
    }
    return 0;
  }
  public function get_discount_rate(){
    if($this->session->has_userdata('discount_rate')){
      return $this->session->userdata('discount_rate');
    }
    return 0;
  }
  public function get_discount(){
    if($this->session->has_userdata('discount')){
      return $this->session->userdata('discount');
    }
    return 0;
  }
  public function get_price(){
    if($this->session->has_userdata('cart_price')){
      return $this->session->userdata('cart_price');
    }
    return 0;
  }
  public function get_shipping_amount(){
    if($this->session->has_userdata('shipping_price')){
      return $this->session->userdata('shipping_price');
    }
    return 0;
  }
}
