<?php

class Datetime_service extends CI_Model {

  function __construct() {
    parent::__construct();
  }
  public function display_time($str_time){
    list($hour,$min,$sec) = explode(':', $str_time);
    return $hour.':'.$min;
  }
  public function display_date($str_date, $lang = 'th'){
    list($year,$month,$day) = explode('-', $str_date);
    return $day.' '.$this->get_month_name($month, $lang).' '.$year;
  }
  public function display_datetime($str_datetime, $lang = 'th'){
    if($str_datetime == ''){
      return '';
    }
    list($str_date, $str_time) = explode(' ', $str_datetime);
    list($year,$month,$day) = explode('-', $str_date);
    list($hour,$min,$sec) = explode(':', $str_time);
    return '<i class="icon-calendar"></i> '.$day.' '.$this->get_month_name($month, $lang).' '.$year.' <i class=" icon-clock"></i> '.$hour.':'.$min;
  }
  public function get_month_name($month_id, $lang){
    $month_id = intval($month_id);
    $month_th = array('','ม.ค.','ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.');
    $month_en = array('','JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC');
    return ($lang == 'th')?$month_th[$month_id]:$month_en[$month_id];
  }
  public function getDateDiff($start_datetime, $end_datetime, $lang = 'th'){
    $start_date = strtotime($start_datetime);
    $end_date = strtotime($end_datetime);
    $diff_date = $end_date - $start_date;
    $diff_date = ceil($diff_date/(60*60*24));
    return $diff_date.' '.(($lang=='th')?'วัน':(($diff_date > 1)?'Days':'Day'));
  }
}
