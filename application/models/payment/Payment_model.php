<?php
class Payment_model extends CI_Model{
  public function get_data($bank_payment_id){
    $this->db->where('bank_payment_id', $bank_payment_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_bank_payment');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function insert($data){
    $this->move_up_priority($data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_bank_payment');
    $payment_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('payment',$payment_id,'insert', 'insert new payment ', $data, $this->db->last_query());
    return $payment_id;
  }
  public function update($bank_payment_id, $data){
    $payment = $this->get_data($bank_payment_id);
    if($payment){
      $this->move_down_priority($payment->sort_priority);
      $this->move_up_priority($data['sort_priority']);
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('bank_payment_id', $bank_payment_id);
      $this->db->update('tbl_bank_payment');
      $this->Log_action_model->insert_log('payment', $bank_payment_id,'update', 'update payment', $data, $this->db->last_query());
    }
  }
  public function delete($bank_payment_id){
    $payment = $this->get_data($bank_payment_id);
    if($payment){
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('bank_payment_id', $bank_payment_id);
      $this->db->update('tbl_bank_payment');
      $this->Log_action_model->insert_log('payment', $bank_payment_id,'delete', 'delete payment', array(), $this->db->last_query());
    }
  }
  public function update_status($bank_payment_id, $bank_payment_status){
    $this->db->set('bank_payment_status', $bank_payment_status);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('payment_id', $payment_id);
    $this->db->update('tbl_bank_payment');
  }
}
