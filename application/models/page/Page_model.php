<?php
class Page_model extends CI_Model{
  public function get_data($page_code){
    $this->db->where('page_code', $page_code);
    $query = $this->db->get('tbl_page');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
}
