<?php
class Upload_file_model extends CI_Model {
  var $upload_path = './uploads/';
  function __construct() {
    parent::__construct();
  }
  public function upload($prefix, $file_name = 'file'){
    $ret_val = array();
    $this->load->library('upload');
    $this->upload->initialize(array(
      'file_name' => $this->gen_image_name($prefix),
      'upload_path' => $this->upload_path,
      'allowed_types' => 'csv|pdf|xls|xlsx|ppt|pptx|gtar|gz|gzip|zip|rar|txt|rtx|rtf|doc|docx|word|xl|7zip|gif|jpg|png|jpeg',
      'max_size' => '1000000'
    ));
    if (!$this->upload->do_upload($file_name)){
      $ret_val = array('success'=>false, 'message'=>$this->upload->display_errors(), 'file_name'=>'');
    }else{
      $upload_data = $this->upload->data();
      $ret_val = array(
        'success' => true,
        'message' => 'success',
        'file_name' => $upload_data['file_name']
      );
    }
    return $ret_val;
  }
  private function gen_image_name($prefix){
      $date = date('Y/m/d H:i:s');
      $replace = array(":"," ","/");
      return $prefix.'_'.str_ireplace($replace, "", $date);
  }
  public function delete_file($file_name){
    $this->load->helper('file');
    $path = FCPATH.'uploads/'.$file_name;
    if (file_exists($path)) {
      unlink($path) or die('failed deleting: ' . $path);
    }
  }
}
