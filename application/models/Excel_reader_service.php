<?php
class Excel_reader_service extends CI_Model{
  public function read_first_sheet($file_name){
    include(FCPATH."PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
    $objPHPExcel = PHPExcel_IOFactory::load(FCPATH.$file_name);
    return $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
  }
  public function get_xlsx_obj($file_name){
    include(FCPATH."PHPExcel-1.8/Classes/PHPExcel/IOFactory.php");
    $objPHPExcel = PHPExcel_IOFactory::load(FCPATH.$file_name);
    return $objPHPExcel;
  }
}
