<?php
class Content_filter_service extends CI_Model{
  var $per_page = 10;
  public function get_data_content($content_group_slug, $page){
    $ret_val = array(
      'content_group_slug' => $content_group_slug,
      'page' => $page,
      'per_page' => $this->per_page,
      'content_list' => $this->get_list($content_group_slug, $page),
      'total_content' => $this->count_list($content_group_slug),
      'total_page' => 1
    );
    $ret_val['total_page'] = ($ret_val['total_content'] == 0)?1:ceil($ret_val['total_content']/$this->per_page);
    return $ret_val;
  }
  private function get_list($content_group_slug, $page){
    $this->where_transaction($content_group_slug);
    $this->db->order_by('sort_priority', 'ASC');
    $offset = ($page - 1) * $this->per_page;
    return $this->db->get('tbl_content', $this->per_page, $offset);
  }
  private function count_list($content_group_slug){
    $this->where_transaction($content_group_slug);
    $query = $this->db->get('tbl_content');
    return $query->num_rows();
  }
  private function where_transaction($content_group_slug){
    if($content_group_slug != 'all'){
      $this->load->model('content/Content_group_model');
      $content_group = $this->Content_group_model->get_data_by_slug($content_group_slug);
      $content_group_id = $content_group->content_group_id;
      $this->db->where('content_group_id', $content_group_id);
    }
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
  }
}
