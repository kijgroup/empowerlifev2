<?php
class Content_group_model extends CI_Model{
  public function get_data($content_group_id){
    $this->db->where('content_group_id', $content_group_id);
    return $this->query_get_data();
  }
  public function get_data_by_slug($slug){
    $this->db->where('slug', $slug);
    return $this->query_get_data();
  }
  public function get_name($content_group_id, $lang){
    $content_group = $this->get_data($content_group_id);
    if(!$content_group){
      return '-';
    }
    if($lang == 'th'){
      return $content_group->name_th;
    }else{
      return $content_group->name_en;
    }
  }
  public function get_list(){
    $this->db->where('is_delete', 'active');
    $this->db->where('enable_status', 'show');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_content_group');
  }
  private function query_get_data(){
    $query = $this->db->get('tbl_content_group');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
}
