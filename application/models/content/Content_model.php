<?php
class Content_model extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function get_data($content_id){
    $this->db->where('content_id', $content_id);
    $this->db->where('is_delete', 'active');
    $this->db->where('enable_status', 'show');
    $query = $this->db->get('tbl_content');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_data_by_slug($slug){
    $this->db->where('slug', $slug);
    $this->db->where('is_delete', 'active');
    $this->db->where('enable_status', 'show');
    $query = $this->db->get('tbl_content');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_relate_content($tags, $content_group_id, $content_id){
    $relate_content = array();
    $content_list = $this->get_by_tags($tags, $content_id);
    $ignore_id = array($content_id);
    if($content_list){
      foreach($content_list->result() as $content){
        $relate_content[] = $content;
        $ignore_id[] = $content->content_id;
      }
    }
    if(count($relate_content) < 3){
      $get_amount = 3 - count($relate_content);
      $content_list = $this->get_by_content_group($content_group_id, $ignore_id, $get_amount);
      foreach($content_list->result() as $content){
        $relate_content[] = $content;
      }
    }
    return $relate_content;
  }
  private function get_by_tags($tags, $content_id){
    if($tags == ''){
      return false;
    }
    $tag_list = explode(",", $tags);
    $str_where = '(';
    foreach($tag_list as $index => $tag){
      $tag = $this->db->escape_str($tag);
      $str_where .= ($index > 0)?' OR ':'';
      $str_where .= " tags like '%$tag%' ";
    }
    $str_where .= ')';
    $this->db->where($str_where);
    $this->db->where('content_id !=', $content_id);
    $this->db->where('is_delete', 'active');
    $this->db->where('enable_status', 'show');
    $this->db->order_by('rand()');
    $this->db->limit(3);
    return $this->db->get('tbl_content');
  }
  private function get_by_content_group($content_group_id, $content_id_list, $get_amount){
    $this->db->where('content_group_id', $content_group_id);
    $this->db->where_not_in('content_id', $content_id_list);
    $this->db->where('is_delete', 'active');
    $this->db->where('enable_status', 'show');
    $this->db->order_by('rand()');
    $this->db->limit($get_amount);
    return $this->db->get('tbl_content');
  }
  public function get_home_list(){
    $this->db->where('is_delete', 'active');
    $this->db->where('enable_status', 'show');
    $this->db->order_by('content_id', 'DESC');
    $this->db->limit(4);
    return $this->db->get('tbl_content');
  }
}
