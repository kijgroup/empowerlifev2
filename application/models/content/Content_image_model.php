<?php
class Content_image_model extends CI_Model{
  public function get_list($content_id){
    $this->db->where('content_id', $content_id);
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_content_image');
  }
}
