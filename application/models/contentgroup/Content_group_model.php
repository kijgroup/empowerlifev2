<?php
class Content_group_model extends CI_Model{
  public function get_list($all_status = true){
    if(!$all_status){
      $this->db->where('enable_status', 'show');
    }
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_content_group');
  }
  public function count_content($content_group_id){
    $this->db->where('content_group_id', $content_group_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_content');
    return $query->num_rows();
  }
  public function get_data($content_group_id){
    $this->db->where('content_group_id', $content_group_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_content_group');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_name($content_group_id){
    $content_group = $this->get_data($content_group_id);
    if(!$content_group){
      return 'ไม่ระบุ';
    }
    return $content_group->name_th;
  }
  public function valid_slug($current_id, $slug){
    $this->db->where('content_group_id !=', $current_id);
    $this->db->where('slug', trim($slug));
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_content_group');
    return ($query->num_rows() == 0);
  }
  public function get_max_priority(){
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_content_group');
    return $query->num_rows();
  }
  public function insert($data){
    $this->move_up_priority($data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_content_group');
    $content_group_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('content_group',$content_group_id,'insert', 'insert new content group ', $data, $this->db->last_query());
    return $content_group_id;
  }
  public function update($content_group_id, $data){
    $content_group = $this->get_data($content_group_id);
    if($content_group){
      $this->move_down_priority($content_group->sort_priority);
      $this->move_up_priority($data['sort_priority']);
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('content_group_id', $content_group_id);
      $this->db->update('tbl_content_group');
      $this->Log_action_model->insert_log('content_group', $content_group_id,'update', 'update content group', $data, $this->db->last_query());
    }
  }
  public function delete($content_group_id){
    $content_group = $this->get_data($content_group_id);
    if($content_group){
      $this->move_down_priority($content_group->sort_priority);
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('content_group_id', $content_group_id);
      $this->db->update('tbl_content_group');
      $this->Log_action_model->insert_log('content_group', $content_group_id,'delete', 'delete content group', array(), $this->db->last_query());
    }
  }
  private function set_to_db($data){
    $this->db->set('name_th', $data['name_th']);
    $this->db->set('name_en', $data['name_en']);
    $this->db->set('slug', trim($data['slug']));
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('sort_priority', $data['sort_priority']);
  }
  private function move_up_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_content_group');
  }
  private function move_down_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority - 1', false);
    $this->db->where('sort_priority >', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_content_group');
  }
}
