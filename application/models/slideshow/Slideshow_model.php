<?php
class Slideshow_model extends CI_Model{
  public function get_list($page){
    $this->db->where('page', $page);
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $this->db->order_by('sort_priority', 'ASC');
    return $this->db->get('tbl_slideshow');
  }
}
