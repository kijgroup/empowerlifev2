<?php

Class Category_model extends CI_Model{

  public function get_list($all_status = true){
    if(!$all_status){
      $this->db->where('enable_status', 'show');
    }
    $this->db->where('is_delete','active');
    $this->db->order_by('sort_priority','ASC');
    return $this->db->get('tbl_category');
  }

  public function count_product($category_id){
    $this->db->where('category_id', $category_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_product');
    return $query->num_rows();
  }

  public function get_data($category_id){
    $this->db->where('category_id', $category_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_category');
    if($query->num_rows()==0){
      return false;
    }
    return $query->row();
  }

  public function get_name($category_id){
    $category = $this->get_data($category_id);
    if(!$category){
      return 'ไม่ระบุ';
    }
    return $category->name_th;
  }

  public function valid_slug($current_id, $slug){
    $this->db->where('category_id !=', $current_id);
    $this->db->where('slug', trim($slug));
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_category');
    return ($query->num_rows() == 0);
  }

  public function get_max_priority(){
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_category');
    return $query->num_rows();
  }

  public function insert($data){
    $this->move_up_priority($data['sort_priority']);
    $this->set_to_db($data);
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_category');
    $category_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('category',$category_id,'insert','insert new category ', $data, $this->db->last_query());
    return $category_id;
  }

  public function update($category_id, $data){
    $category = $this->get_data($category_id);
    if($category){
      $this->move_down_priority($category->sort_priority);
      $this->move_up_priority($data['sort_priority']);
      $this->set_to_db($data);
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('category_id',$category_id);
      $this->db->update('tbl_category');
      $this->Log_action_model->insert_log('category', $category_id,'update','update category', $data, $this->db->last_query());
    }
  }

  public function delete($category_id){
    $category = $this->get_data($category_id);
    if($category){
      $this->move_down_priority($category->sort_priority);
      $this->db->set('is_delete', 'delete');
      $this->db->set('update_date', 'NOW()', false);
      $this->db->set('update_by', $this->session->userdata('admin_id'));
      $this->db->where('category_id', $category_id);
      $this->db->update('tbl_category');
      $this->Log_action_model->insert_log('category', $category_id, 'delete','detele category', array(), $this->db->last_query());
    }
  }

  private function set_to_db($data){
    $this->db->set('name_th', $data['name_th']);
    $this->db->set('name_en', $data['name_en']);
    $this->db->set('slug', trim($data['slug']));
    $this->db->set('desc_th', $data['desc_th']);
    $this->db->set('desc_en', $data['desc_en']);
    $this->db->set('content_th', $data['content_th']);
    $this->db->set('content_en', $data['content_en']);
    $this->db->set('style', $data['style']);
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('sort_priority', $data['sort_priority']);
    if($data['thumb_image'] != ''){
      $this->db->set('thumb_image', $data['thumb_image']);
    }
    if($data['icon_image'] != ''){
      $this->db->set('icon_image', $data['icon_image']);
    }
  }

  private function move_up_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority + 1', false);
    $this->db->where('sort_priority >=', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_category');
  }

  private function move_down_priority($sort_priority){
    $this->db->set('sort_priority', 'sort_priority -1', false);
    $this->db->where('sort_priority >', $sort_priority);
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_category');
  }

}
