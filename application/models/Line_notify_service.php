<?php
class Line_notify_service extends CI_Model{
  var $line_url = '';
  var $line_token = '';
  public function __construct(){
    parent::__construct();
    $this->line_url = $this->config->item('line_url');
    $this->line_token = $this->config->item('line_token');
  }
  public function send_message($message){
    $post_field = 'message='.$message;
    return $this->send_to_line($post_field);
  }
  private function send_with_image($message, $image_thumbnail, $image_fill_size){
    $post_field = 'message='.$message.'&imageThumbnail='.$image_thumbnail.'&imageFullsize='.$image_full_size;
    return $this->send_to_line($post_field);
  }
  private function send_to_line($post_field){
    $ret_val = '';
    $ch_one = curl_init();
    curl_setopt( $ch_one, CURLOPT_URL, $this->line_url);
    // SSL USE
    curl_setopt( $ch_one, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt( $ch_one, CURLOPT_SSL_VERIFYPEER, 0);
    //POST
    curl_setopt( $ch_one, CURLOPT_POST, 1);
    // Message
    curl_setopt( $ch_one, CURLOPT_POSTFIELDS, $post_field);
    // follow redirects
    curl_setopt( $ch_one, CURLOPT_FOLLOWLOCATION, 1);
    //ADD header array
    $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$this->line_token );
    curl_setopt($ch_one, CURLOPT_HTTPHEADER, $headers);
    //RETURN
    curl_setopt( $ch_one, CURLOPT_RETURNTRANSFER, 1);
    $result = curl_exec( $ch_one );
    //Check error
    if(curl_error($ch_one)){
      $ret_val = 'error:' . curl_error($ch_one);
    }else{
      $result_ = json_decode($result, true);
      $ret_val = "status : ".$result_['status'];
      $ret_val .= "message : ". $result_['message'];
    }
    curl_close( $ch_one );
    return $ret_val;
  }
}
