<?php
class Config_model extends CI_Model {
  function __construct() {
    parent::__construct();
  }
  public function get_data($code){
    $this->db->select('config_value');
    $this->db->where('config_code', $code);
    $query = $this->db->get('tbl_config');
    if($query->num_rows() > 0){
      return $query->row()->config_value;
    }
    return '';
  }
}
