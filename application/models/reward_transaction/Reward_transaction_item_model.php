<?php
class Reward_transaction_item_model extends CI_Model{
  public function get_list($reward_transaction_id){
    $this->db->where('reward_transaction_id', $reward_transaction_id);
    return $this->db->get('tbl_reward_transaction_item');
  }
  public function return_name_lang($reward_transaction_item, $lang){
    if(!$reward_transaction_item){
      return '-';
    }
    if($lang == 'th'){
      return $reward_transaction_item->reward_name_th;
    }else{
      return $reward_transaction_item->reward_name_en;
    }
  }
  public function insert($data){
    $field_list = array('reward_transaction_id', 'reward_id', 'reward_name_en', 'reward_name_th', 'reward_point', 'qty', 'total_reward_point');
    foreach($field_list as $field_name){
      $this->db->set($field_name, $data[$field_name]);
    }
    $this->db->insert('tbl_reward_transaction_item');
  }
}
