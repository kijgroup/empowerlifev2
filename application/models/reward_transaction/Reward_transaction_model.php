<?php
class Reward_transaction_model extends CI_Model{
  public function get_data($reward_transaction_id){
    $this->db->where('reward_transaction_id', $reward_transaction_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_reward_transaction');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_list($member_id){
    $this->db->where('member_id', $member_id);
    $this->db->where('is_delete', 'active');
    $this->db->order_by('reward_transaction_id', 'DESC');
    return $this->db->get('tbl_reward_transaction');
  }
  public function insert($data){
    $field_list = array('member_id', 'shipping_name', 'shipping_mobile', 'shipping_address', 'shipping_district', 'shipping_amphur', 'shipping_province', 'shipping_post_code', 'note');
    foreach($field_list as $field_name){
      $this->db->set($field_name, $data[$field_name]);
    }
    $this->db->set('create_date', 'NOW()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_reward_transaction');
    $reward_transaction_id = $this->db->insert_id();
    return $reward_transaction_id;
  }
  public function update_point($reward_transaction_id, $total_reward_point){
    $this->db->set('total_reward_point', $total_reward_point);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('reward_transaction_id', $reward_transaction_id);
    $this->db->update('tbl_reward_transaction');
  }
}
