<?php
class Reward_transaction_status_model extends CI_Model{
  var $status_list = array(
    'checking' => array(
      'status_code' => 'checking',
      'status_name' => 'รอส่งของ',
      'label_class' => 'label label-warning'
    ),
    'done' => array(
      'status_code' => 'done',
      'status_name' => 'เสร็จสิ้น',
      'label_class' => 'label label-success'
    ),
  );
  public function get_list(){
    return $this->status_list;
  }
  public function get_data($status_code){
    return $this->status_list[$status_code];
  }
  public function get_name($status_code){
    $status = $this->get_data($status_code);
    return $status['status_name'];
  }
  public function get_label($status_code){
    $status = $this->get_data($status_code);
    return '<span class="'.$status['label_class'].'">'.$status['status_name'].'</span>';
  }
}
