<?php
class Reward_transaction_filter_service extends CI_Model{
  public function get_data_content($search_val, $reward_transaction_status, $page = 1, $per_page = 20) {
    $data = array();
    $data['search_val'] = $search_val;
    $data['reward_transaction_status'] = $reward_transaction_status;
    $data['page'] = $page;
    $data['per_page'] = $per_page;
    $data['reward_transaction_list'] = $this->get_list($search_val, $reward_transaction_status, $page, $per_page);
    $data['total_transaction'] = $this->get_count($search_val, $reward_transaction_status);
    return $data;
  }
  private function get_count($search_val, $reward_transaction_status){
    $this->db->select('tbl_reward_transaction.reward_transaction_id');
    $this->where_transaction($search_val, $reward_transaction_status);
    $query = $this->db->get('tbl_reward_transaction');
    return $query->num_rows();
  }
  private function get_list($search_val, $reward_transaction_status, $page, $per_page){
    $this->db->select('tbl_reward_transaction.*');
    $this->where_transaction($search_val, $reward_transaction_status);
    $this->db->order_by('tbl_reward_transaction.reward_transaction_id', 'DESC');
    $offset = ($page - 1) * $per_page;
    return $this->db->get('tbl_reward_transaction', $per_page, $offset);
  }
  private function where_transaction($search_val, $reward_transaction_status){
    if($search_val != ''){
      $str_where = "(shipping_name like '%$search_val%' ";
      $str_where .= "OR shipping_mobile like '%$search_val%' ";
      $str_where .= "OR shipping_address like '%$search_val%' ";
      $str_where .= "OR shipping_district like '%$search_val%' ";
      $str_where .= "OR shipping_province like '%$search_val%' ";
      $str_where .= "OR shipping_post_code like '%$search_val%' ";
      $str_where .= "OR note like '%$search_val%' ";
      $str_where .= ')';
      $this->db->where($str_where, NULL,FALSE);
    }
    if($reward_transaction_status != 'all'){
      $this->db->where('tbl_reward_transaction.reward_transaction_status', $reward_transaction_status);
    }
    $this->db->where('is_delete', 'active');
  }
}
