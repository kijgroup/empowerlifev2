<?php

class PDF_service extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    public function create_pdf_file($html, $stylesheet, $header, $footer, $folder, $file_name){
        $mpdf = $this->setup_pdf($html, $stylesheet, $header, $footer);
        $fullPath = $folder.$file_name;
        $mpdf->Output($fullPath,'F');
        return $fullPath;
    }
    public function view_pdf_file($html, $stylesheet, $header, $footer, $file_name){
        $mpdf = $this->setup_pdf($html, $stylesheet, $header, $footer);
        $mpdf->Output($file_name,'I');
    }
    private function setup_pdf($html, $stylesheet, $header, $footer){
        include(FCPATH."/vendor/autoload.php");        
        $defaultFontConfig = (new Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'] + [
            'THSarabunNew' => [
                'R' => 'THSarabunNew.ttf',
                'I' => 'THSarabunNew-Italic.ttf',
                'B' => 'THSarabunNew-Bold.ttf',
                'useOTL' => 0xFF,
            ]
        ];
        $mpdf=new \Mpdf\Mpdf([
            'fontdata' => $fontData,
            'default_font' => 'THSarabunNew'
        ]);
        $mpdf->autoScriptToLang = true;
        $mpdf->baseScript = 1;
        $mpdf->autoLangToFont = true;
        $mpdf->SetHeader($header);
        $mpdf->SetFooter($footer);
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->list_indent_first_level = 0;
        $mpdf->WriteHTML($stylesheet,1);
        $mpdf->WriteHTML($html,2);
        return $mpdf;
    }
}
