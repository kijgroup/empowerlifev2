<?php
class Reward_model extends CI_Model{
  public function get_list(){
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $this->db->order_by('reward_id', 'DESC');
    return $this->db->get('tbl_reward');
  }
  public function get_data($reward_id){
    $this->db->where('reward_id', $reward_id);
    $this->db->where('enable_status', 'show');
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_reward');
    if($query->num_rows() == 0){
      return false;
    }
    return $query->row();
  }
  public function get_name($reward_id, $lang){
    if($lang == 'th'){
      return $reward->name_th;
    }else{
      return $reward->name_en;
    }
  }
  public function get_name_by_obj($reward, $lang){
    if(!$reward){
      return '-';
    }
    if($lang == 'th'){
      return $reward->name_th;
    }else{
      return $reward->name_en;
    }
  }
  public function get_detail_by_obj($reward, $lang){
    if(!$reward){
      return '-';
    }
    if($lang == 'th'){
      return $reward->detail_th;
    }else{
      return $reward->detail_en;
    }
  }
}
