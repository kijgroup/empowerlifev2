<?php
class Content extends CI_Controller{
  public function __construct(){
    parent::__construct();
      $this->lang->load('content', $this->Masterpage_service->get_lang_full());
    $this->load->model(array(
      'content/Content_group_model',
      'content/Content_model',
      'content/Content_filter_service',
      'content/Content_image_model'
    ));
  }
  public function index($content_group_slug = 'all'){
    $page = $this->input->get('page');
    $page = ($page == '')?1:$page;
    $data_content = $this->Content_filter_service->get_data_content($content_group_slug, $page);
    $content = $this->load->view('content/list', $data_content, true);
    $this->Masterpage_service->add_css('assets/external/bootstrap-select/bootstrap-select.css');
    $this->Masterpage_service->add_js('assets/external/bootstrap-select/bootstrap-select.js');
    $this->Masterpage_service->add_js('assets/js/page/content/list.js?v=1');
    $this->Masterpage_service->display($content, 'Empowerlife', 'content');
  }
  public function detail($content_id = 0){
    $content_data = $this->Content_model->get_data($content_id);
    $relate_content_list = $this->Content_model->get_relate_content($content_data->tags, $content_data->content_group_id, $content_id);
    $content = $this->load->view('content/detail', array(
      'content_id' => $content_id,
      'content' => $content_data, 
      'relate_content_list' => $relate_content_list,
    ), true);
    $this->Masterpage_service->set_data('og_url', site_url('content/detail/'.$content_id));
    $this->Masterpage_service->set_data('og_title', $content_data->subject_th);
    $this->Masterpage_service->set_data('og_image', base_url('uploads/'.$content_data->content_image));
    $this->Masterpage_service->set_data('og_description', $content_data->short_content_th);
    $this->Masterpage_service->add_js('assets/js/page/content/detail.js');
    $this->Masterpage_service->display($content, $content_data->subject_th.' - Empowerlife', 'content');
  }
}
