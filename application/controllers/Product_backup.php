<?php
class Product extends CI_Controller{
  function __construct(){
    parent::__construct();
      $this->lang->load('promotion', $this->Masterpage_service->get_lang_full());
    $this->load->model(array('product/Product_model', 'product/Product_image_model','product/Product_filter_service', 'product/Category_model', 'product/Product_relate_model', 'product/Product_bundle_model'));
  }
  public function index($category_slug = 'all', $sort_by = 'default'){
    $page = $this->input->get('page');
    $page = ($page < 1)?1:$page;
    $content = $this->load->view('product/list',
      $this->Product_filter_service->get_data_content(
        $category_slug,
        $sort_by,
        $page
      ), true);
    $this->Masterpage_service->add_css('assets/external/magnific-popup/magnific-popup.css');
    $this->Masterpage_service->add_js('assets/external/magnific-popup/jquery.magnific-popup.min.js');
  $this->Masterpage_service->add_css('assets/external/bootstrap-select/bootstrap-select.css');
  $this->Masterpage_service->add_js('assets/external/bootstrap-select/bootstrap-select.js');
    $this->Masterpage_service->add_js('assets/js/page/product/index.js?v=1');
    $this->Masterpage_service->display($content, 'Empowerlife', 'product');
  }
  public function detail($product_id, $slug){
    $product = $this->Product_model->get_data($product_id);
    $content = $this->load->view('product/detail', array(
      'product_id' => $product_id,
      'slug' => $slug,
      'product' => $product,
      'product_image_list' => $this->Product_image_model->get_list($product->product_id)
    ), true);
    $this->Masterpage_service->add_css('assets/external/magnific-popup/magnific-popup.css');
    $this->Masterpage_service->add_js('assets/external/magnific-popup/jquery.magnific-popup.min.js');
    $this->Masterpage_service->add_js('assets/external/galleriffic/jquery.galleriffic.js?v=1');
    $this->Masterpage_service->add_js('assets/external/raty/jquery.raty.js');
    $this->Masterpage_service->add_js('assets/js/page/product/detail.js');
    $this->Masterpage_service->display($content, 'Empowerlife', 'product');
  }
}
