<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->lang->load('profile', $this->Masterpage_service->get_lang_full());
    $this->lang->load('membership', $this->Masterpage_service->get_lang_full());
  }
  public function index(){
    $this->Login_service->must_login();
    $data = array();
    $data['member'] = $this->Member_model->get_data($this->session->userdata('member_id'));
    $content = $this->load->view('profile/form', $data, true);
    $this->Masterpage_service->display($content, 'Profile', 'profile');
  }
  public function form_post(){
    $this->Login_service->must_login();
    $data = array();
    $data['member_firstname'] = $this->input->post('member_firstname');
    $data['member_lastname'] = $this->input->post('member_lastname');
    $data['member_email'] = $this->input->post('member_email');
    $data['member_birthdate'] = $this->input->post('member_birthdate');
    $data['member_gender'] = $this->input->post('member_gender');
    $data['member_phone'] = $this->input->post('member_phone');
    $data['member_mobile'] = $this->input->post('member_mobile');
    $data['member_address'] = $this->input->post('member_address');
    $data['member_district'] = $this->input->post('member_district');
    $data['member_amphur'] = $this->input->post('member_amphur');
    $data['member_province'] = $this->input->post('member_province');
    $data['member_postcode'] = $this->input->post('member_postcode');
    $this->Member_model->update($this->session->userdata('member_id'), $data);
    redirect('profile/success');
  }
  public function success(){
    $this->Login_service->must_login();
    $data = array();
    $content = $this->load->view('profile/success', $data, true);
    $this->Masterpage_service->display($content, 'profile', 'profile');
  }
}
