<?php
class Reward extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->lang->load('membership', $this->Masterpage_service->get_lang_full());
    $this->lang->load('reward', $this->Masterpage_service->get_lang_full());
    $this->load->model(array(
      'reward/Reward_model',
      'reward_transaction/Reward_transaction_model',
      'reward_transaction/Reward_transaction_item_model',
      'Line_notify_service',
    ));
  }
  public function index($response = ""){
    $member = false;
    if($this->Login_service->get_login_status()){
      $member = $this->Member_model->get_data($this->session->userdata('member_id'));
    }
    $content = $this->load->view('reward/list', array(
      'reward_list' => $this->Reward_model->get_list(),
      'member' => $member,
      'response' => $response
    ), true);
    $this->Masterpage_service->add_js('assets/js/page/reward/index.js?v=1');
    $this->Masterpage_service->display($content, 'Empowerlife', 'reward');
  }
  public function form_post(){
    $this->Login_service->must_login();
    $reward_list = $this->Reward_model->get_list();
    $use_reward_point = 0;
    $arr_reward_transaction_item = array();
    foreach($reward_list->result() as $reward){
      $qty = $this->input->post('qty_'.$reward->reward_id);
      if($qty == 0){
        continue;
      }
      $total_reward_point = $reward->reward_point * $qty;
      $use_reward_point += $total_reward_point;
      $arr_reward_transaction_item[] = array(
        'reward_id' => $reward->reward_id,
        'reward_name_en' => $reward->name_en,
        'reward_name_th' => $reward->name_th,
        'reward_point' => $reward->reward_point,
        'qty' => $qty,
        'total_reward_point' => $total_reward_point
      );
    }
    $member = $this->Member_model->get_data($this->session->userdata('member_id'));
    $remain_reward_point = $member->member_point - $use_reward_point;
    if($use_reward_point > 0 && $remain_reward_point >= 0){
      $reward_transaction_field_list = array('shipping_name', 'shipping_mobile', 'shipping_address', 'shipping_district', 'shipping_amphur', 'shipping_province', 'shipping_post_code', 'note');
      $reward_transaction_data = array('member_id'=> $this->session->userdata('member_id'));
      foreach($reward_transaction_field_list as $reward_transaction_field){
        $reward_transaction_data[$reward_transaction_field] = $this->input->post($reward_transaction_field);
      }
      $reward_transaction_id = $this->Reward_transaction_model->insert($reward_transaction_data);
      $this->Reward_transaction_model->update_point($reward_transaction_id, $use_reward_point);
      $this->Member_model->update_reward_point($member->member_id, $remain_reward_point);
      foreach($arr_reward_transaction_item as $reward_transaction_item){
        $reward_transaction_item['reward_transaction_id'] = $reward_transaction_id;
        $this->Reward_transaction_item_model->insert($reward_transaction_item);
      }
      $message = "\nมีการแลกของรางวัล\n\nReward transaction id: ".$reward_transaction_id."\nโดยสมาชิก: ".$member->member_firstname.' '.$member->member_lastname."\n แต้มที่ใช้: ".$use_reward_point."\n รายละเอียดเพิ่มเติม: \n".$reward_transaction_data['note'];
      $this->Line_notify_service->send_message($message);
      redirect('reward/complete');
    }else{
      redirect('reward/index/error');
    }
  }
  public function complete(){
    $this->Login_service->must_login();
    $content = $this->load->view('reward/complete', array(), true);
    $this->Masterpage_service->display($content, 'Empowerlife', 'reward');
  }
}
