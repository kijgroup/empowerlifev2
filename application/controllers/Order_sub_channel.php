<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_sub_channel extends MY_Controller{
  public $page_name = 'Source';
  public $controller = 'Order_sub_channel';
  public $nav = 'order_sub_channel';
  public $parent_nav_name = 'การจัดการ';
  public $list_display = array('order_sub_channel_name');
  public $display_form = array(
    array(
      'panel_name' => 'ข้อมูล Source',
      'item_list' => array(
        'order_sub_channel_name',
        'sort_priority',
        'create_date',
        'update_date'
      ),
    )
  );
  public $breadcrumb_list = array(
    array(
      'url' => '',
      'name' => 'การจัดการ',
    ),
    array(
      'url' => 'order_sub_channel_name',
      'name' => 'Source',
    )
  );
  public function __construct(){
    parent::__construct();
    $this->load->model('order_channel/Order_sub_channel_model');
    $this->Main_model = $this->Order_sub_channel_model;
    parent::prepare_data();
  }
}
