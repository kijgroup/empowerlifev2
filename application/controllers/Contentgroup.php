<?php
class Contentgroup extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->Login_service->must_login();
    $this->load->model(array('contentgroup/Content_group_model'));
  }
  public function index(){
    $content = $this->load->view('contentgroup/list', array(
      'content_group_list' => $this->Content_group_model->get_list()
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/contentgroup/list.js');
    $this->Masterpage_service->display($content, 'ประเภทบทความ', 'contentgroup');
  }
  public function form($content_group_id = 0){
    $this->Login_service->must_login();
    $title = (($content_group_id == 0)?'เพิ่ม':'แก้ไข').'ประเภทบทความ';
    $content_group = $this->Content_group_model->get_data($content_group_id);
    $content = $this->load->view('contentgroup/form', array(
      'content_group_id' => $content_group_id,
      'content_group' => $content_group,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/contentgroup/form.js');
    $this->Masterpage_service->display($content, $title, 'contentgroup');
  }
  public function form_post($content_group_id){
    $data = array(
      'name_th' => $this->input->post('name_th'),
      'name_en' => $this->input->post('name_en'),
      'slug' => $this->input->post('slug'),
      'enable_status' => $this->input->post('enable_status'),
      'sort_priority' => $this->input->post('sort_priority')
    );
    if($content_group_id == 0){
      $content_group_id = $this->Content_group_model->insert($data);
    }else{
      $this->Content_group_model->update($content_group_id, $data);
    }
    redirect('contentgroup');
  }
  public function delete($content_group_id){
    $this->Content_group_model->delete($content_group_id);
    redirect('contentgroup/');
  }
}
