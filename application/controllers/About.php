<?php
class About extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model(array('slideshow/Slideshow_model', 'page/Page_model'));
  }
  public function index(){
    $about = $this->Page_model->get_data('about');
    $slideshow_list = $this->Slideshow_model->get_list('about');
    $content = $this->load->view('about/index', array(
      'about' => $about,
      'slideshow_list' => $slideshow_list
    ), true);
    $this->Masterpage_service->add_js('assets/external/rs-plugin/js/jquery.themepunch.plugins.min.js');
    $this->Masterpage_service->add_js('assets/external/rs-plugin/js/jquery.themepunch.revolution.min.js');
    $this->Masterpage_service->add_js('assets/js/page/about.js');
    $this->Masterpage_service->display($content, 'Empowerlife', 'about');
  }
}
