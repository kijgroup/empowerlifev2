<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_channel extends CI_Controller {
  function __construct(){
    parent::__construct();
    $this->Login_service->must_login();
    $this->load->model(array('order_channel/Order_channel_model'));
  }
  public function index(){
    $content = $this->load->view('order_channel/list', array(
      'order_channel_list' => $this->Order_channel_model->get_list()
    ), TRUE);
    $this->Masterpage_service->display($content, 'Order_channel', 'order_channel');
  }
  public function form($order_channel_id = 0){
    $this->Login_service->must_login();
    $title = (($order_channel_id ==0)?'เพิ่ม':'แก้ไข').'ข้อมูลแต้มแลกส่วนลด';
    $order_channel = $this->Order_channel_model->get_data($order_channel_id);
    $content = $this->load->view('order_channel/form', array(
      'order_channel_id' => $order_channel_id,
      'order_channel' => $order_channel,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/order_channel/form.js');
    $this->Masterpage_service->display($content, 'ช่องทางการสั่งซื้อ', 'order_channel');
  }
  public function form_post($order_channel_id){
    $data = array(
      'order_channel_name' => $this->input->post('order_channel_name'),
      'sort_priority' => $this->input->post('sort_priority'),
    );
    if($order_channel_id == 0){
      $order_channel_id = $this->Order_channel_model->insert($data);
    }else{
      $this->Order_channel_model->update($order_channel_id, $data);
    }
    redirect('order_channel');
  }

  public function delete($order_channel_id){
    $this->Order_channel_model->delete($order_channel_id);
    redirect('order_channel/');
  }
}
