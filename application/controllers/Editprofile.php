<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editprofile extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('admin/Admin_model'));
    }
    public function index() {
        $this->Login_service->must_login();
        $data_content = array();
        $this->session->userdata('admin_id');
        $data_content['admin'] = $this->Admin_model->get_data($this->session->userdata('admin_id'));
        $title = $data_content['title'] = 'แก้ไขข้อมูลส่วนตัว';
        $content = $this->load->view('editprofile/index', $data_content, TRUE);
        $this->Masterpage_service->display($content, $title, 'editprofile');
    }

    public function form_post() {
        $this->Login_service->must_login();
        $data['admin_name'] = $this->input->post('admin_name');
        $this->Admin_model->edit_profile($data);
        $this->Login_service->update_session();
        redirect('editprofile/success');
    }

    public function success() {
        $this->Login_service->must_login();
        $data_content = array();
        $title = $data_content['title'] = 'แก้ไขข้อมูลส่วนตัว';
        $content = $this->load->view('editprofile/success', $data_content, TRUE);
        $this->Masterpage_service->display($content, $title, 'editprofile');
    }
}
