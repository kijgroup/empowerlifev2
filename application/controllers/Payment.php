<?php
class Payment extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->lang->load('payment', $this->Masterpage_service->get_lang_full());
    $this->load->model(array(
      'bank_payment/Bank_payment_model',
      'bank/Bank_model',
      'Line_notify_service'
    ));
  }
  public function index(){
    $content = $this->load->view('payment/index', array(), true);
    $this->Masterpage_service->add_js('assets/external/jquery.maskedinput.min.js');
    $this->Masterpage_service->add_js('assets/js/page/payment.js');
    $this->Masterpage_service->display($content, 'Empowerlife', 'payment');
  }
  public function form_post(){
    $this->load->model('Upload_image_model');
    $order_id = 0;
    $member_id = 0;
    $bank_payment_image = '';
    $image_data = $this->Upload_image_model->upload_single_image('payment', 'bank_payment_image');
    if($image_data['success']){
      $bank_payment_image = $image_data['image_file'];
    }
    $data = array(
      'bank_id' => $this->input->post('bank_id'),
      'order_id' => $order_id,
      'member_id' => $member_id,
      'order_code' => $this->input->post('order_code'),
      'bank_payment_name' => $this->input->post('bank_payment_name'),
      'bank_payment_phone' => $this->input->post('bank_payment_phone'),
      'bank_payment_date' => $this->input->post('bank_payment_date'),
      'bank_payment_time' => $this->input->post('bank_payment_time'),
      'bank_payment_amount' => $this->input->post('bank_payment_amount'),
      'bank_payment_image' => $bank_payment_image
    );
    $bank_payment_id = $this->Bank_payment_model->insert($data);
    $bank = $this->Bank_model->get_data($data['bank_id']);
    $message = "\nมีการแจ้งชำระเงิน\n\nPayment id: ".$bank_payment_id."\nหมายเลข Order: ".$data['order_code']."\n ผู้ชำระ: ".$data['bank_payment_name']."\n โทรศัพท์มือถือ: ".$data['bank_payment_phone']."\n ธนาคาร: ".$bank->bank_name_th."\n".$bank->bank_info_th."\n จำนวนเงิน: ".$data['bank_payment_amount']."\n วันที่ชำระ: ".$data['bank_payment_date']."\n เวลา: ".$data['bank_payment_time'];
    $this->Line_notify_service->send_message($message);
    redirect('payment/complete');
  }
  public function complete(){
    $content = $this->load->view('payment/complete', array(), true);
    $this->Masterpage_service->display($content, 'Empowerlife', 'payment');
  }
}
