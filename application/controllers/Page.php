<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model(array('page/Page_model'));
    }
    public function index($page_code = ''){
        $this->Login_service->must_login();
        $dataContent = array();
        $page = $this->Page_model->get_data($page_code);
        $dataContent['page_code'] = $page_code;
        $title = (($page)?$page->page_name_th:'Page not found');
        $dataContent['page'] = $page;
        $dataContent['title'] = $title;
        $content = $this->load->view('page/form', $dataContent, TRUE);
        $this->Masterpage_service->add_css('assets_admin/vendor/summernote/summernote.css');
        $this->Masterpage_service->add_js('assets_admin/vendor/summernote/summernote.min.js');
        $this->Masterpage_service->add_js('assets_admin/js/page/form.js');
        $this->Masterpage_service->display($content, $title, $page_code);
    }

    public function form_post($page_code){
        $this->Login_service->must_login();
        $data = array();
        $data['page_name_th'] = $this->input->post('page_name_th');
        $data['page_name_en'] = $this->input->post('page_name_en');
        $data['page_content_th'] = $this->input->post('page_content_th');
        $data['page_content_en'] = $this->input->post('page_content_en');
        $this->Page_model->update($page_code, $data);
        redirect('page/index/'.$page_code);
    }
    public function upload_image(){
      $content_image = '';
      $this->load->model('Upload_image_model');
      $image_data = $this->Upload_image_model->upload_single_image('page_detail', 'file');
      if($image_data['success']){
        $content_image = $image_data['image_file'];
      }
      echo base_url('uploads/'.$content_image);
    }
}
