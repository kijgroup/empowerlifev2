<?php
class Faq extends CI_Controller{
  public function index($faq_group_id = 0){
    $this->load->model(array('faq/Faq_group_model', 'faq/Faq_model'));
    if($faq_group_id == 0){
      $faq_group_id = $this->Faq_group_model->get_first_id();
    }
    $faq_list = $this->Faq_model->get_list($faq_group_id);
    $content = $this->load->view('faq/index', array(
      'faq_group_id' => $faq_group_id,
      'faq_list' => $faq_list
    ), true);
    $this->Masterpage_service->add_js('assets/external/rs-plugin/js/jquery.themepunch.plugins.min.js');
    $this->Masterpage_service->add_js('assets/external/rs-plugin/js/jquery.themepunch.revolution.min.js');
    $this->Masterpage_service->add_js('assets/js/page/faq.js');
    $this->Masterpage_service->display($content, 'Empowerlife', 'faq');
  }
}
