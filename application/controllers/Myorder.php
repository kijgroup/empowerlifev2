<?php
class Myorder extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->lang->load('myorder', $this->Masterpage_service->get_lang_full());
    $this->lang->load('membership', $this->Masterpage_service->get_lang_full());
    $this->load->model(array(
      'order/Order_model',
      'order/Order_item_model'
    ));
    $this->Login_service->must_login();
  }
  public function index(){
    $content = $this->load->view('myorder/list', array(
      'order_list' => $this->Order_model->get_list()
    ), true);
    $this->Masterpage_service->add_js('assets/js/page/myorder.js');
    $this->Masterpage_service->display($content, 'Empowerlife', 'membership');
  }
  public function detail($order_code){
    $order = $this->Order_model->get_data_by_code($order_code);
    $content = $this->load->view('myorder/detail', array(
      'order_code' => $order_code,
      'order' => $order
    ), true);
    $this->Masterpage_service->display($content, 'Empowerlife', 'membership');
  }
}
