<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Member extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array
    ('member/Member_model',
    'member/Member_type_model',
    'member/Member_enable_status_model', 'member/Member_gender_model',
    'order/Order_model',
    'order/Order_status_model',
    'reward_transaction/Reward_transaction_model',
    'reward_transaction/Reward_transaction_status_model',
    'member/User_model'));
  }
  public function index() {
    $this->Login_service->must_login();
    $content = $this->load->view('member/list', array(
      'member_type_list' => $this->Member_type_model->get_list()
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/member/list.js?v=2');
    $this->Masterpage_service->display($content, 'สมาชิกเวบไซต์', 'member');
  }
  public function filter(){
    $this->Login_service->must_login('js');
    $search_val = $this->input->post('searchVal');
    $member_type_code = $this->input->post('member_type_code');
    $enable_status = $this->input->post('enableStatus');
    $sort = $this->input->post('sort');
    $page = $this->input->post('page');
    $per_page = $this->input->post('perPage');
    $this->load->model('member/Member_filter_service');
    $data = $this->Member_filter_service->get_data_content(
      $search_val,
      $member_type_code,
      $enable_status,
      $sort,
      $page,
      $per_page);
    $this->load->view('member/list/content', $data);
  }
  public function detail($member_id){
    $this->Login_service->must_login();
    $member = $this->Member_model->get_data($member_id);
    $order_list = $this->Order_model->get_list_by_member($member_id);
    $reward_transaction_list = $this->Reward_transaction_model->get_list_by_member($member_id);
    $title = 'รายละเอียดสมาชิก';
    $data_content = array();
    $data_content['member_id'] = $member_id;
    $data_content['member'] = $member;
    $data_content['order_list'] = $order_list;
    $data_content['reward_transaction_list'] = $reward_transaction_list;
    $data_content['title'] = $title;
    $content = $this->load->view('member/detail', $data_content, TRUE);
    $this->Masterpage_service->add_js('assets_admin/js/member/detail.js?v=1');
    $this->Masterpage_service->display($content, $title, 'member');
  }
  public function form($member_id = 0){
    $this->Login_service->must_login();
    $title = (($member_id == 0)?'เพิ่ม':'แก้ไข').'สมาชิกเวบไซต์';
    $content = $this->load->view('member/form', array(
      'member_id' => $member_id,
      'member' => $this->Member_model->get_data($member_id),
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_css('assets_admin/vendor/select2/css/select2.css');
    $this->Masterpage_service->add_css('assets_admin/vendor/select2-bootstrap-theme/select2-bootstrap.css');
    $this->Masterpage_service->add_js('assets_admin/vendor/select2/js/select2.js');
    $this->Masterpage_service->add_js('assets_admin/js/address/popup.js');
    $this->Masterpage_service->add_js('assets_admin/js/member/form.js');
    $this->Masterpage_service->display($content, $title, 'member');
  }

  public function form_post($member_id){
    $this->Login_service->must_login();
    $member_field_list = array('member_type', 'expire_date', 'buy_total', 'member_point', 'member_firstname', 'member_lastname', 'member_gender', 'member_birthdate', 'member_email', 'member_phone', 'member_mobile', 'member_address', 'member_postcode', 'enable_status', 'member_district', 'member_province', 'member_amphur');
    $member_data = array();
    foreach($member_field_list as $member_field_name){
      $member_data[$member_field_name] = $this->input->post($member_field_name);
    }
    if($member_id == 0){
      $member_id = $this->Member_model->insert($member_data);
      $has_user = $this->input->post('has_user');
      if($has_user == 'true'){
        $user_data = array(
          'member_id' => $member_id,
          'email' => $this->input->post('email'),
          'login_password' => $this->input->post('login_password'),
          'enable_status' => $this->input->post('enable_status'),
        );
        $data['login_password'] = $this->input->post('login_password');
        $user_id = $this->User_model->insert($user_data);
        $this->Member_model->set_user($member_id, $user_id);
      }
    }else{
      $member = $this->Member_model->get_data($member_id);
      if($member){
        $this->Member_model->update($member_id, $member_data);
        $has_user = $this->input->post('has_user');
        if($member->user_id == 0){
          if($has_user == 'true'){
            $user_data = array(
              'member_id' => $member_id,
              'email' => $this->input->post('email'),
              'login_password' => $this->input->post('login_password'),
              'enable_status' => $this->input->post('enable_status'),
            );
            $data['login_password'] = $this->input->post('login_password');
            $user_id = $this->User_model->insert($user_data);
            $this->Member_model->set_user($member_id, $user_id);
          }
        }else{
          if($has_user == 'true'){
            $this->User_model->update($member->user_id, array(
              'email' => $this->input->post('email'),
              'enable_status' => $this->input->post('enable_status'),
            ));
          }else{
            $this->User_model->delete($member->user_id);
            $this->Member_model->set_user($member_id, 0);
          }
        }
      }
    }
    redirect('member/detail/'.$member_id);
  }
  public function delete($member_id){
    $this->Login_service->must_login();
    $member = $this->Member_model->get_data($member_id);
    if($member){
      $this->Member_model->delete($member_id);
      $this->User_model->delete($member->user_id);
    }
    redirect('member/');
  }
  public function update_field($field_name, $member_id){
    $this->Login_service->must_login();
    $input_value = $this->input->post('input_value');
    $this->Member_model->update_field($member_id, $field_name, $input_value);
    redirect('member/detail/'.$member_id);
  }
  public function form_change_password($member_id){
    $this->Login_service->must_login();
    $member = $this->Member_model->get_data($member_id);
    if($member){
      $this->User_model->update_password($member->user_id, $this->input->post('txtPassword'));
    }
    redirect('member/detail/'.$member_id);
  }
  public function create_member_code($member_id){
    $this->Login_service->must_login();
    $this->Member_model->create_member_code($member_id);
    redirect('member/detail/'.$member_id);
  }
  public function download_excel(){
    $this->Login_service->must_login();
    $this->load->model(array('member/Member_excel_service'));
    $this->Member_excel_service->export_list();
  }
}
