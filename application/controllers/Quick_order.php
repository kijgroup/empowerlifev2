<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Quick_order extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->lang->load('quick_order', $this->Masterpage_service->get_lang_full());
  }
  public function index(){
    $data = array();
    $content = $this->load->view('quick_order/index', array(), true);
    $this->Masterpage_service->add_js('assets/js/page/quick_order.js');
    $this->Masterpage_service->display($content, 'สั่งซื้อด่วน', 'quick_order');
  }
  public function form_post(){
    $this->Cart_model->clear_cart();
    $product_list = $this->Product_model->get_all_list();
    foreach($product_list->result() as $product){
      $qty = $this->input->post('qty_'.$product->product_id);
      if($qty > 0){
        $this->Cart_model->add_item($product->product_id, $qty);
      }
    }
    redirect('checkout/');
  }
}
