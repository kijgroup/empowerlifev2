<?php
class Cart extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model(array('cart/Cart_model'));
  }
  public function index(){
    $cart_items = $this->Cart_model->get_list();
    var_dump($cart_items);
  }
  public function add_cart($product_id, $quantity = 1, $redirect_page = 'product'){
    $get_quantity = (int) $this->input->get('quantity');
    $quantity = ($get_quantity > 0)?$get_quantity:$quantity;
    $product = $this->Product_model->get_data($product_id);
    if($product){
      $this->Cart_model->add_item($product_id, $quantity);
      if($redirect_page == 'product'){
        redirect('product/detail/'.$product->product_id.'/'.$product->slug);
      }
    }
    $this->redirect_prev_page();
  }
  public function delete_item($product_id){
    $this->Cart_model->delete_item($product_id);
    $this->redirect_prev_page();
  }
  public function clear_cart(){
    $this->Cart_model->clear_cart($product_id);
    $this->redirect_prev_page();
  }
  private function redirect_prev_page(){
    if(isset($_SERVER['HTTP_REFERER'])){
      header('Location: ' . $_SERVER['HTTP_REFERER']);
    }else{
      redirect('home');
    }
  }
}
