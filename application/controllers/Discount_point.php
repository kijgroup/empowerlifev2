<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Discount_point extends CI_Controller {
  function __construct(){
    parent::__construct();
    $this->Login_service->must_login();
    $this->load->model(array('discount_point/Discount_point_model'));
  }
  public function index(){
    $content = $this->load->view('discount_point/list', array(
      'discount_point_list' => $this->Discount_point_model->get_list()
    ), TRUE);
    $this->Masterpage_service->display($content, 'Discount_point', 'discount_point');
  }
  public function form($discount_point_id = 0){
    $this->Login_service->must_login();
    $title = (($discount_point_id ==0)?'เพิ่ม':'แก้ไข').'ข้อมูลแต้มแลกส่วนลด';
    $discount_point = $this->Discount_point_model->get_data($discount_point_id);
    $content = $this->load->view('discount_point/form', array(
      'discount_point_id' => $discount_point_id,
      'discount_point' => $discount_point,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/discount_point/form.js');
    $this->Masterpage_service->display($content, 'แต้มแลกส่วนลด', 'discount_point');
  }

  public function form_post($discount_point_id){
    $data = array(
      'use_point' => $this->input->post('use_point'),
      'discount_amount' => $this->input->post('discount_amount'),
      'sort_priority' => $this->input->post('sort_priority'),
      'enable_status' => $this->input->post('enable_status'),
    );
    if($discount_point_id == 0){
      $discount_point_id = $this->Discount_point_model->insert($data);
    }else{
      $this->Discount_point_model->update($discount_point_id, $data);
    }
    redirect('discount_point');
  }

  public function delete($discount_point_id){
    $this->Discount_point_model->delete($discount_point_id);
    redirect('discount_point/');
  }
}
