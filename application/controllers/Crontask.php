<?php
class Crontask extends CI_Controller{
  public function index(){
    $this->check_outdate_promotion();
  }
  private function check_outdate_promotion(){
    $this->db->set('enable_status', 'hide');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', 0);
    $this->db->where('product_type', 'promotion');
    $this->db->where('is_expire', 'true');
    $this->db->where('expire_date >=', 'NOW()');
    $this->db->where('is_delete', 'active');
    $this->db->update('tbl_product');
  }
}
