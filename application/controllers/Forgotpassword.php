<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Forgotpassword extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->load->model(array(
      'member/Member_model',
      'member/Forgot_password_service',
      'member/Forgot_password_model',
      'Encode_service',
    ));
    $this->lang->load('forgotpassword', $this->Masterpage_service->get_lang_full());
  }
  public function index($error_message = ''){
    $content = $this->load->view('forgotpassword/form', array(
      'error_message' => $error_message,
    ), true);
    $this->Masterpage_service->display($content, 'Forgot Password', 'forgotpassword');
  }
  public function form_post(){
    $member_email = $this->input->post('member_email');
    $this->session->set_flashdata('member_email', $member_email);
    $this->Forgot_password_service->forgot_password($member_email);
    redirect('forgotpassword/complete');
  }
  public function complete(){
    $content = $this->load->view('forgotpassword/complete', array(), true);
    $this->Masterpage_service->display($content, 'Forgot Password Complete', 'forgotpassword');
  }
  public function changepassword($hash_code){
    $str_decode = $this->Encode_service->decode_number($hash_code);
    if($str_decode == ''){
      $data = array();
      $content = $this->load->view('forgotpassword/errorhash', $data, true);
    }else{
      $member_id = intval($str_decode);
      $member = $this->Member_model->get_data($member_id);
      $data = array(
        'hash_code' => $hash_code,
        'member' => $member
      );
      $content = $this->load->view('forgotpassword/changepassword', $data, true);
    }
    $this->Masterpage_service->display($content, 'Forgot Password', 'forgotpassword');
  }
  public function changepassword_post($hash_code){
    $str_decode = $this->Encode_service->decode_number($hash_code);
    if($str_decode == ''){
      $data = array();
      $content = $this->load->view('forgotpassword/errorhash', $data, true);
      $this->Masterpage_service->display($content, 'Forgot Password', 'forgotpassword');
      exit(0);
    }
    $member_id = intval($str_decode);
    $member = $this->Member_model->get_data($member_id);
    $password = trim($this->input->post('password'));
    $confirm_password = trim($this->input->post('confirm_password'));
    if($password != $confirm_password || $password == ''){
      redirect('forgotpassword/changepassword/'.$hash_code.'/error');
    }
    $this->db->where('member_id', $member_id);
    $user = $this->db->get('tbl_user');
    $this->User_model->change_password($user->user_id, $password);
    $this->Login_service->create_session($member);
    redirect('home');
  }
}
