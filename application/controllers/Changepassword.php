<?php
class Changepassword extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->lang->load('changepassword', $this->Masterpage_service->get_lang_full());
    $this->lang->load('membership', $this->Masterpage_service->get_lang_full());
  }
  public function index($page_status = 'true'){
    $this->Login_service->must_login();
    $member = $this->Member_model->get_data($this->session->userdata('member_id'));
    $content = $this->load->view('changepassword/form', array(
      'member' => $member,
      'user' => $this->User_model->get_data($member->user_id),
      'page_status' => $page_status
    ), true);
    $this->Masterpage_service->display($content, 'Changepassword', 'changepassword');
  }
  public function form_post(){
    $this->Login_service->must_login();
    $login_password = $this->input->post('login_password');
    $login_password_confirm = $this->input->post('login_password_confirm');
    if($login_password != $login_password_confirm){
      redirect('changepassword/index/false');
    }
    $member = $this->Member_model->get_data($this->session->userdata('member_id'));
    $this->User_model->change_password($member->user_id, $login_password);
    redirect('changepassword/success');
  }
  public function success(){
    $this->Login_service->must_login();
    $content = $this->load->view('changepassword/success', array(), true);
    $this->Masterpage_service->display($content, 'changepassword', 'changepassword');
  }
}
