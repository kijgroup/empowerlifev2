<?php
class Oem extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model(array('slideshow/Slideshow_model','page/Page_model'));
  }
  public function index(){
    $page_data = $this->Page_model->get_data('oem');
    $slideshow_list = $this->Slideshow_model->get_list('oem');
    $content = $this->load->view('page/index', array(
      'page' => $page_data,
      'slideshow_list' => $slideshow_list
    ), true);
    $this->Masterpage_service->add_js('assets/external/rs-plugin/js/jquery.themepunch.plugins.min.js');
    $this->Masterpage_service->add_js('assets/external/rs-plugin/js/jquery.themepunch.revolution.min.js');
    $this->Masterpage_service->add_js('assets/js/page/oem.js');
    $this->Masterpage_service->display($content, 'Empowerlife', 'oem');
  }
}
