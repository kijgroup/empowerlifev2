<?php
class Checkout extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->lang->load('checkout', $this->Masterpage_service->get_lang_full());
    $this->load->model(array(
      'order/Order_model',
      'order/Order_item_model',
      'member/Member_model',
      'member/Member_type_model',
      'bank/Bank_model',
      'order/Discount_point_model',
      'order/Order_discount_point_model',
      'Line_notify_service',
    ));
    $this->Login_service->must_login();
  }
  public function index(){
    $member = $this->Member_model->get_data($this->session->userdata('member_id'));
    $member_type = $this->Member_type_model->get_data($member->member_type);
    $content = $this->load->view('checkout/index', array(
      'member' => $member,
      'member_type' => $member_type,
    ), true);
    $this->Masterpage_service->add_js('assets/js/page/checkout/index.js?v=4');
    $this->Masterpage_service->add_js('assets/external/waypoint/waypoints.min.js');
    $this->Masterpage_service->display($content, 'Empowerlife', 'checkout');
  }
  public function form_post(){
    $is_tax = $this->input->post('is_tax');
    if(!isset($is_tax) || $is_tax != 'true'){
      $is_tax = 'false';
    }
    $payment_type = $this->input->post('payment_type');
    list($order_discount_point_list, $discount_point_amount, $discount_by_point_amount) = $this->calc_discount_by_point();
    var_dump($order_discount_point_list);
    var_dump($discount_point_amount);
    var_dump($discount_by_point_amount);
    $member = $this->Member_model->get_data($this->session->userdata('member_id'));
    if($member->member_point < $discount_point_amount){
      redirect('membership');
    }
    $order_data = array(
      'discount_point_amount' => $discount_point_amount,
      'discount_by_point_amount' => $discount_by_point_amount,
      'shipping_name' => trim($this->input->post('shipping_name')),
      'shipping_mobile' => trim($this->input->post('shipping_mobile')),
      'shipping_address' => trim($this->input->post('shipping_address')),
      'shipping_district' => trim($this->input->post('shipping_district')),
      'shipping_amphur' => trim($this->input->post('shipping_amphur')),
      'shipping_province' => trim($this->input->post('shipping_province')),
      'shipping_post_code' => trim($this->input->post('shipping_post_code')),
      'is_tax' => $is_tax,
      'tax_name' => trim($this->input->post('tax_name')),
      'tax_mobile' => trim($this->input->post('tax_mobile')),
      'tax_address' => trim($this->input->post('tax_address')),
      'tax_district' => trim($this->input->post('tax_district')),
      'tax_amphur' => trim($this->input->post('tax_amphur')),
      'tax_province' => trim($this->input->post('tax_province')),
      'tax_post_code' => trim($this->input->post('tax_post_code')),
      'tax_code' => trim($this->input->post('tax_code')),
      'tax_branch' => trim($this->input->post('tax_branch')),
      'payment_type' => trim($payment_type),
      'postoffice' => trim($this->input->post('postoffice'))
    );
    $same_address = $this->input->post('same_address');
    if($is_tax == 'true' && isset($same_address) && $same_address == 'true'){
      $order_data['tax_mobile'] = $order_data['shipping_mobile'];
      $order_data['tax_address'] = $order_data['shipping_address'];
      $order_data['tax_district'] = $order_data['shipping_district'];
      $order_data['tax_amphur'] = $order_data['shipping_amphur'];
      $order_data['tax_province'] = $order_data['shipping_province'];
      $order_data['tax_post_code'] = $order_data['shipping_post_code'];
    }
    $order_id = $this->Order_model->insert($order_data);
    if(in_array($payment_type, array('post', 'home'))){
      $this->Order_model->update_status($order_id, 'confirm');
    }
    $cart_items = $this->Cart_model->get_list();
    $total_items = 0;
    $items_price = 0;
    foreach($cart_items as $cart_item){
      $product = $this->Product_model->get_data($cart_item['product_id']);
      if($product){
        $product_price_total = $product->price * $cart_item['quantity'];
        $this->Order_item_model->insert(array(
          'order_id' => $order_id,
          'product_id' => $product->product_id,
          'price' => $product->price,
          'qty' => $cart_item['quantity'],
          'price_total' => $product_price_total
        ));
        $total_items += $cart_item['quantity'];
        $items_price += $product_price_total;
      }
    }
    $member_type = $this->Member_type_model->get_data($member->member_type);
    $discount_rate = 0;
    if($items_price >= $member_type->min_amount){
      $discount_rate = $member_type->discount;
    }
    $discount_price = ($items_price - $discount_by_point_amount) * $discount_rate / 100;
    $shipping_price = $this->Cart_model->get_shipping_amount();
    $grand_total = $items_price - $discount_price - $discount_by_point_amount + $shipping_price;
    $this->Order_model->update_price($order_id, array(
      'total_item' => $total_items,
      'item_price' => $items_price,
      'discount_price' => $discount_price,
      'shipping_price' => $shipping_price,
      'grand_total' => $grand_total
    ));
    $this->insert_discount_point($order_id, $order_discount_point_list);
    $this->Member_model->update_reward_point($member->member_id, $member->member_point - $discount_point_amount);
    $order = $this->Order_model->get_data($order_id);
    $this->Cart_model->clear_cart();
    $payment_type_name = '';
    if($payment_type == 'bank'){
      $payment_type_name = 'โอนเงิน';
    }elseif($payment_type == 'post'){
      $payment_type_name = 'ชำระเงินและรับสินค้าที่ไปรษณีย์ (พกง)';
    }elseif($payment_type == 'home'){
      $payment_type_name = 'จัดส่งและชำระเงินที่หน้าบ้าน (DHL)';
    }
    $message = "\nมีการสั่งซื้อสินค้า\n\nORDER CODE: ".$order->order_code."\nโดยสมาชิก: ".$member->member_firstname.' '.$member->member_lastname."\n ยอดที่ต้องชำระ: ".number_format($grand_total)." บาท\n ช่องทางการชำระเงิน :".$payment_type_name;
    $this->Line_notify_service->send_message($message);
    redirect('checkout/complete/'.$order->order_code);
  }
  public function complete($order_code){
    $content = $this->load->view('checkout/complete', array(
      'order_code' => $order_code
    ), true);
    $this->Masterpage_service->display($content, 'Empowerlife', 'checkout');
  }
  private function calc_discount_by_point(){
    $order_discount_point_list = array();
    $discount_point_amount = 0;
    $discount_by_point_amount = 0;
    $discount_point_list = $this->Discount_point_model->get_list();
    foreach($discount_point_list->result() as $discount_point){
      $qty = $this->input->post('discount_point_'.$discount_point->discount_point_id);
      if($qty > 0){
        $total_use_point = $qty * $discount_point->use_point;
        $total_discount_amount = $qty * $discount_point->discount_amount;
        $order_discount_point_list[] = array(
          'discount_point_id' => $discount_point->discount_point_id,
          'qty' => $qty,
          'use_point' => $discount_point->use_point,
          'discount_amount' => $discount_point->discount_amount,
          'total_use_point' => $total_use_point,
          'total_discount_amount' => $total_discount_amount
        );
        $discount_point_amount += $total_use_point;
        $discount_by_point_amount += $total_discount_amount;
      }
    }
    return array(
      $order_discount_point_list,
      $discount_point_amount,
      $discount_by_point_amount
    );
  }
  private function insert_discount_point($order_id, $order_discount_point_list){
    foreach($order_discount_point_list as $order_discount_point){
      var_dump($order_discount_point);
      //$order_discount_point['order_id'] = $order_id;
      $insert_data = array_merge(array('order_id' => $order_id), $order_discount_point);
      $this->Order_discount_point_model->insert($insert_data);
    }
  }
}
