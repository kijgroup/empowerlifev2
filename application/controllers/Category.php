<?php
Class Category extends CI_Controller{

  function __construct(){
    parent::__construct();
    $this->Login_service->must_login();
    $this->load->model(array('category/Category_model'));
  }

  public function index(){
    $content = $this->load->view('category/list', array(
      'category_list' => $this->Category_model->get_list()
    ), TRUE);
    $this->Masterpage_service->add_js('/assets_admin/js/category/list.js');
    $this->Masterpage_service->display($content, 'หมวดหมู่สินค้า', 'category');
  }

  public function form($category_id = 0){
    $this->Login_service->must_login();
    $title = (($category_id ==0)?'เพิ่ม':'แก้ไข').'หมวดสินค้า';
    $category = $this->Category_model->get_data($category_id);
    $content = $this->load->view('category/form', array(
      'category_id' => $category_id,
      'category' => $category,
      'title' => $title
    ), TRUE);
    $this->Masterpage_service->add_css('/assets_admin/vendor/codemirror/lib/codemirror.css');
    $this->Masterpage_service->add_css('/assets_admin/vendor/codemirror/theme/monokai.css"');
    $this->Masterpage_service->add_js('/assets_admin/vendor/codemirror/lib/codemirror.js');
    $this->Masterpage_service->add_js('/assets_admin/vendor/codemirror/addon/selection/active-line.js');
    $this->Masterpage_service->add_js('/assets_admin/vendor/codemirror/addon/edit/matchbrackets.js');
    $this->Masterpage_service->add_js('/assets_admin/vendor/codemirror/mode/javascript/javascript.js');
    $this->Masterpage_service->add_js('/assets_admin/vendor/codemirror/mode/xml/xml.js');
    $this->Masterpage_service->add_js('/assets_admin/vendor/codemirror/mode/htmlmixed/htmlmixed.js');
    $this->Masterpage_service->add_js('/assets_admin/vendor/codemirror/mode/css/css.js');
    $this->Masterpage_service->add_js('/assets_admin/js/category/form.js');
    $this->Masterpage_service->display($content, 'หมวดหมู่สินค้า', 'category');
  }

  public function form_post($category_id){
    $data = array(
      'name_th' => $this->input->post('name_th'),
      'name_en' => $this->input->post('name_en'),
      'slug' => $this->input->post('slug'),
      'thumb_image' => '',
      'icon_image' => '',
      'desc_th' => $this->input->post('desc_th'),
      'desc_en' => $this->input->post('desc_en'),
      'content_th' => $this->input->post('content_th'),
      'content_en' => $this->input->post('content_en'),
      'style' => $this->input->post('style'),
      'enable_status' => $this->input->post('enable_status'),
      'sort_priority' => $this->input->post('sort_priority')
    );
    $this->load->model(array('Upload_image_model'));
    $thumb_data = $this->Upload_image_model->upload_single_image('thumb_image','thumb_image');
    if($thumb_data['success']){
      $data['thumb_image'] = $thumb_data['image_file'];
    }
    $icon_data = $this->Upload_image_model->upload_single_image('icon_image','icon_image');
    if($icon_data['success']){
      $data['icon_image'] = $icon_data['image_file'];
    }
    if($category_id == 0){
      $category_id = $this->Category_model->insert($data);
    }else{
      $this->Category_model->update($category_id, $data);
    }
    redirect('category/form/'.$category_id);
  }

  public function delete($category_id){
    $this->Category_model->delete($category_id);
    redirect('category/');
  }


}
