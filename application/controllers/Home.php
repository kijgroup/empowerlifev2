<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
  public function index(){
    $this->load->model(array(
      'product/Product_model',
      'product/Category_model',
      'content/Content_group_model',
      'content/Content_model',
      'slideshow/Slideshow_model'
    ));
    $content_list = $this->Content_model->get_home_list();
    $category_list = $this->Category_model->get_list();
    //$product_list = $this->Product_model->get_home_product_list();
    $promotion_list = $this->Product_model->get_home_promotion_list();
    $content = $this->load->view('home/index', array(
      'content_list' => $content_list,
      'category_list' => $category_list,
      'promotion_list' => $promotion_list
    ), true);
    $this->Masterpage_service->add_js('assets/js/countdown.js');
    $this->Masterpage_service->add_js('assets/external/rs-plugin/js/jquery.themepunch.plugins.min.js');
    $this->Masterpage_service->add_js('assets/external/rs-plugin/js/jquery.themepunch.revolution.min.js');
    $this->Masterpage_service->add_js('assets/external/swiper/idangerous.swiper.js');
    $this->Masterpage_service->add_js('assets/js/page/home.js?v=2');
    $this->Masterpage_service->display($content, 'Empowerlife', 'home');
  }
  public function popup(){
    $obj = new StdClass();
		$lang = $this->config->item('language_abbr');
    $obj->upload_file = base_url('uploads/'.$this->Config_model->get_data(($lang == 'th')?'popup_image_th':'popup_image_en'));
    $obj->redirect_url = $this->Config_model->get_data('popup_url');
    $obj->popup_status = $this->Config_model->get_data('popup_status');
    header('Content-type: text/javascript');
    echo json_encode($obj);
  }
}
