<?php
class Login extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->lang->load('login', $this->Masterpage_service->get_lang_full());
  }
  public function index(){
    $content = $this->load->view('login/index', array(), true);
    $this->Masterpage_service->add_js('assets/js/page/login/index.js');
    $this->Masterpage_service->add_js('assets/js/facebooklogin.js');
    $this->Masterpage_service->display($content, 'Empowerlife', 'login');
  }
  public function form_post(){
    try{
      $email = $this->input->post('email');
      $login_password = $this->input->post('login_password');
      $this->Login_service->login($email, $login_password);
      $check = $this->input->post('check');
      if(isset($check)){
        $expire = 865000;//time()+865000;
        $this->input->set_cookie(array(
          'name' => 'email',
          'value' => $email,
          'expire' => $expire,
          'path'   => '/'
        ));
        $this->input->set_cookie(array(
          'name' => 'token',
          'value' => md5($login_password),
          'expire' => $expire,
          'path'   => '/'
        ));
      }
      $resp_msg = (get_cookie('after_login_url'))?get_cookie('after_login_url'):base_url();
      header('Content-Type: application/json;charset=utf-8');
      echo json_encode(array(
        'resp_status' => true,
        'resp_msg' => $resp_msg
      ));
    }catch(Exception $e){
      header('Content-Type: application/json;charset=utf-8');
      echo json_encode(array(
        'resp_status' => false,
        'resp_code' => $e->getMessage(),
        'resp_msg' => $this->Login_service->get_error_message($e->getMessage())
      ));
    }
  }
  public function logout(){
    $this->session->sess_destroy();
    delete_cookie('email');
    delete_cookie('token');
    $this->load->library('HybridAuthLib');
    $this->hybridauthlib->logoutAllProviders();
    redirect('home');
  }
}
