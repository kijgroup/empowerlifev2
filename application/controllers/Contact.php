<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->lang->load('contact', $this->Masterpage_service->get_lang_full());
    $this->load->model(array(
      'contact/Contact_group_model',
      'contact/Contact_model',
      'page/Page_model',
      'Line_notify_service'
    ));
  }
  public function index($show_success = ''){
    $contact_content = $this->Page_model->get_data('contact');
    $content = $this->load->view('contact/index', array(
      'show_success' => $show_success,
      'contact_content' => $contact_content
    ), true);
    $this->Masterpage_service->add_js('assets/js/page/contact.js?v=1');
    $this->Masterpage_service->display($content, 'Contact', 'contact');
  }
  public function form_post(){
    
    $response = $this->input->post("g-recaptcha-response");   
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $data = array(
      'secret' => '6Ld19lMUAAAAAPb-YskESSi4wcAaKxsNekXXmAdZ',
      'response' => $_POST["g-recaptcha-response"],
    );
    $query = http_build_query($data);
    $options = array(
      'http' => array (
        'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                    "Content-Length: ".strlen($query)."\r\n".
                    "User-Agent:MyAgent/1.0\r\n",
        'method' => 'POST',
        'content' => $query
      )
    );
    $context  = stream_context_create($options);
    $verify = file_get_contents($url, false, $context);
    $captcha_success=json_decode($verify);
    if(!$captcha_success->success){
      redirect('contact/index/recaptcha');
    }
    $data = array(
      'contact_group_id' => $this->input->post('contact_group_id'),
      'contact_detail' => $this->input->post('contact_detail'),
      'contact_name' => $this->input->post('contact_name'),
      'contact_email' => $this->input->post('contact_email'),
      'contact_phone' => $this->input->post('contact_phone'),
    );
    if(strpos($data['contact_detail'], '<a') !== false || strpos($data['contact_detail'], '<script') !== false){
      redirect('contact/index/complete/tag');
    }
    $contact_id = $this->Contact_model->insert($data);
    $contact_group = $this->Contact_group_model->get_data($data['contact_group_id']);
    $contact_group_name = '';
    if($contact_group){
      $contact_group_name = $contact_group->contact_group_th;
    }
    $message = "\nมีการแจ้งติดต่อเจ้าหน้าที่\n\nจาก: ".$data['contact_name']."\n email: ".$data['contact_email']."\n เบอร์โทรศัพท์: ".$data['contact_phone']."\n หัวข้อ: ".$contact_group->contact_group_th."\n ข้อความ: \n".$data['contact_detail'];
    $this->Line_notify_service->send_message($message);
    redirect('contact/index/complete');
  }

}
