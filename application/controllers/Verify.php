<?php
class Verify extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->load->model(array('page/Page_model'));
  }
  public function index(){
    $page_data = $this->Page_model->get_data('verify');
    $content = $this->load->view('page/index', array(
      'page' => $page_data
    ), true);
    $this->Masterpage_service->display($content, 'Empowerlife', 'verify');
  }
}
