<?php
class Myreward extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->lang->load('membership', $this->Masterpage_service->get_lang_full());
    $this->lang->load('myreward', $this->Masterpage_service->get_lang_full());
    $this->load->model(array(
      'reward_transaction/Reward_transaction_model',
      'reward_transaction/Reward_transaction_item_model',
      'reward/Reward_model',
    ));
    $this->Login_service->must_login();
  }
  public function index(){
    $content = $this->load->view('myreward/list/index', array(
      'reward_transaction_list' => $this->Reward_transaction_model->get_list($this->session->userdata('member_id'))
    ), true);
    $this->Masterpage_service->display($content, 'Empowerlife', 'membership');
  }
  public function detail($reward_transaction_id){
    $reward_transaction = $this->Reward_transaction_model->get_data($reward_transaction_id);
    $content = $this->load->view('myreward/detail/index', array(
      'reward_transaction_id' => $reward_transaction_id,
      'reward_transaction' => $reward_transaction,
      'reward_transaction_item_list' => $this->Reward_transaction_item_model->get_list($reward_transaction_id)
    ), true);
    $this->Masterpage_service->display($content, 'Empowerlife', 'membership');
  }
}
