<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
  function __construct() {
    parent::__construct();
    $this->lang->load('register', $this->Masterpage_service->get_lang_full());
    $this->Login_service->must_not_login();
    $this->load->model(array(
      'member/Member_model',
      'member/User_model',
      'Line_notify_service'
    ));
  }
  public function index($err_msg = ''){
    $content = $this->load->view('register/index', array(), true);
    $this->Masterpage_service->add_js('assets/js/page/register/index.js?v=1');
    if($err_msg != ''){
      var_dump($err_msg);
    }
    $this->Masterpage_service->display($content, 'Empowerlife', 'register');
  }
  public function form_post(){
    //start validate recaptcha
    $response = $this->input->post("g-recaptcha-response");   
    $url = 'https://www.google.com/recaptcha/api/siteverify';
    $data = array(
      'secret' => '6Ld19lMUAAAAAPb-YskESSi4wcAaKxsNekXXmAdZ',
      'response' => $_POST["g-recaptcha-response"],
    );
    $query = http_build_query($data);
    $options = array(
      'http' => array (
        'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                    "Content-Length: ".strlen($query)."\r\n".
                    "User-Agent:MyAgent/1.0\r\n",
        'method' => 'POST',
        'content' => $query
      )
    );
    $context  = stream_context_create($options);
    $verify = file_get_contents($url, false, $context);
    $captcha_success=json_decode($verify);
    if(!$captcha_success->success){
      redirect('register/index/recaptcha');
    }
    //end validate recaptcha
    $this->main_register_flow();
  }
  public function social_post(){
    $this->main_register_flow(true);
  }
  private function main_register_flow($is_social = false){
    $email = $this->input->post('email');
    $login_password = $this->input->post('login_password');
    $login_password_confirm = $this->input->post('login_password_confirm');
    $member_field_list = array('member_firstname', 'member_lastname', 'member_gender', 'member_phone', 'member_mobile', 'member_address', 'member_postcode','member_district','member_amphur','member_province');
    $data = array();
    foreach($member_field_list as $member_field){
      $data[$member_field] = $this->input->post($member_field);
    }
    $old_member = $this->input->post('old_member');
    $old_member = (!isset($old_member) || $old_member != 'true')?'false':'true';
    $data['member_birthdate'] = $this->input->post('birthdate_year').'-'.$this->input->post('birthdate_month').'-'.$this->input->post('birthdate_day');
    $data['member_email'] = $email;
    //check duplicate email
    if($this->User_model->get_data_by_email($email)){
      header('Content-Type: application/json;charset=utf-8');
      echo json_encode(array(
        'resp_status' => false,
        'resp_msg' => 'มีอีเมลนี้แล้วในระบบ'
      ));
      exit(0);
      return;
    }
    if($login_password != $login_password_confirm){
      header('Content-Type: application/json;charset=utf-8');
      echo json_encode(array(
        'resp_status' => false,
        'resp_msg' => 'กรุณากรอกรหัสผ่านให้ตรงกัน'
      ));
      exit(0);
      return;
    }
    $member_id = $this->Member_model->insert($data);
    $user_id = $this->User_model->insert(array(
      'member_id' => $member_id,
      'email' => $email,
      'login_password' => $login_password
    ));
    $this->Member_model->update_user_id($member_id, $user_id);
    $message = "\nมีการสมัครสมาชิกเวบไซต์\n\nโดยสมาชิก: ".$data['member_firstname'].' '.$data['member_lastname']."\n email: ".$email."\n เบอร์โทรศัพท์: ".$data['member_phone']."\n เบอร์มือถือ: ".$data['member_mobile'];
    if($old_member == 'true'){
      $message .= "\nเคยมีประวัติการสั่งซื้อกับ Empower Life";
    }
    $this->Line_notify_service->send_message($message);
    if($is_social){
      $provider = $this->input->post('provider');
      $provider_id = $this->input->post('provider_id');
      $avata_image = $this->input->post('avata_image');
      $this->User_model->update_social(
        $user_id,
        $this->input->post('provider'),
        $this->input->post('provider_id'),
        $this->input->post('provider_url'),
        $this->input->post('avata_image')
      );
    }
    $member = $this->Member_model->get_data($member_id);
    $this->Login_service->create_session($member);
    $resp_msg = (get_cookie('after_login_url'))?get_cookie('after_login_url'):'';
    header('Content-Type: application/json;charset=utf-8');
    echo json_encode(array(
      'resp_status' => true,
      'resp_msg' => $resp_msg
    ));
  }
}
