<?php
class Membership extends CI_Controller{
  public function __construct(){
    parent::__construct();
    $this->lang->load('myorder', $this->Masterpage_service->get_lang_full());
    $this->lang->load('membership', $this->Masterpage_service->get_lang_full());
  }
  public function index(){
    $this->Login_service->must_login();
    $this->load->model(array(
      'member/Member_type_model',
      'order/Order_model',
      'order/Order_item_model',
      'reward_transaction/Reward_transaction_model',
    ));
    $member = $this->Member_model->get_data($this->session->userdata('member_id'));
    $this->db->limit(0,3);
    $order_list = $this->Order_model->get_list();
    $this->db->limit(0,3);
    $reward_transaction_list = $this->Reward_transaction_model->get_list($this->session->userdata('member_id'));
    $content = $this->load->view('membership/index', array(
      'member' => $member,
      'order_list' => $order_list,
      'reward_transaction_list' => $reward_transaction_list
    ), true);
    $this->Masterpage_service->add_js('assets/js/page/myorder.js');
    $this->Masterpage_service->display($content, 'Empowerlife', 'membership');
  }
}
