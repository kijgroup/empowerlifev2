<?php
class Promotion extends CI_Controller{
  function __construct(){
    parent::__construct();
    $this->lang->load('promotion', $this->Masterpage_service->get_lang_full());
    $this->load->model(array('product/Product_model', 'product/Product_image_model','product/Promotion_filter_service', 'product/Category_model', 'product/Product_relate_model', 'product/Product_bundle_model'));
  }
  public function index($category_code = 'all'){
    $page = $this->input->get('page');
    $page = ($page < 1)?1:$page;
    $content = $this->load->view('promotion/list',
      $this->Promotion_filter_service->get_data_content(
        $category_code,
        $page
      ), true);
    $this->Masterpage_service->add_js('assets/js/countdown.js');
    $this->Masterpage_service->add_css('assets/external/magnific-popup/magnific-popup.css');
    $this->Masterpage_service->add_js('assets/external/magnific-popup/jquery.magnific-popup.min.js');
    $this->Masterpage_service->add_js('assets/js/page/promotion/index.js');
    $this->Masterpage_service->display($content, 'Empowerlife', 'promotion');
  }
  public function detail($product_id, $slug = ''){
    $product = $this->Product_model->get_data($product_id);
    if(!$product){
      redirect('promotion');
    }
    $content = $this->load->view('promotion/detail', array(
      'product_id' => $product_id,
      'slug' => $slug,
      'product' => $product,
      'product_image_list' => $this->Product_image_model->get_list($product->product_id)
    ), true);
    $this->Masterpage_service->add_css('assets/external/magnific-popup/magnific-popup.css');
    $this->Masterpage_service->add_js('assets/external/magnific-popup/jquery.magnific-popup.min.js');
    $this->Masterpage_service->add_js('assets/external/galleriffic/jquery.galleriffic.js?v=1');
    $this->Masterpage_service->add_js('assets/external/raty/jquery.raty.js');
    $this->Masterpage_service->add_js('assets/js/page/promotion/detail.js');
    $this->Masterpage_service->display($content, 'Empowerlife', 'promotion');
  }
}
