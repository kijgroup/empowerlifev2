<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Address extends CI_Controller{
  public function __construct(){
    parent::__construct();
  }
  public function index(){
  }
  public function amphur_list($province_id){
    $amphur_list = $this->Thailand_model->get_amphur_list($province_id);
    foreach($amphur_list->result() as $amphur){
      echo '<option value="'.$amphur->AMPHUR_ID.'">'.$amphur->AMPHUR_NAME.'</option>';
    }
  }
  public function district_list($amphur_id){
    $district_list = $this->Thailand_model->get_district_list($amphur_id);
    foreach($district_list->result() as $district){
      echo '<option value="'.$district->DISTRICT_ID.'">'.$district->DISTRICT_NAME.'</option>';
    }
  }
  public function postcode($district_id){
    echo $this->Thailand_model->get_postcode($district_id);
  }
}