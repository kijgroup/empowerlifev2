<?php
$lang['content_forgotpassword'] = 'ลืมรหัสผ่าน';
$lang['content_forgotpassword_email'] = 'อีเมล์';
$lang['content_forgotpassword_password'] = 'รหัสผ่าน';
$lang['content_forgotpassword_login'] = 'จำรหัสผ่านได้?';
$lang['content_login_social'] = 'ลงชื่อเข้าใช้ผ่านบัญชี Social Network:';
