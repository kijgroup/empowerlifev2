<?php
$lang['register_email'] = 'อีเมล์ในการเข้าสู่ระบบ';
$lang['register_password'] = 'รหัสผ่านในการเข้าสู่ระบบ';
$lang['register_confirm_password'] = 'ยืนยันรหัสผ่าน';
$lang['register_first_name'] = 'ชื่อ';
$lang['register_last_name'] = 'นามสกุล';
$lang['register_gender'] = 'เพศ';
$lang['register_gender_female'] = 'หญิง';
$lang['register_gender_male'] = 'ชาย';
$lang['register_birthday'] = 'วันเกิด';
$lang['register_phone'] = 'เบอร์โทรศัพท์';
$lang['register_mobile'] = 'มือถือ';
$lang['register_address'] = 'ที่อยู่';
$lang['register_district'] = 'แขวง/ตำบล';
$lang['register_amphur'] = 'เขต/อำเภอ';
$lang['register_province'] = 'จังหวัด';
$lang['register_post_code'] = 'รหัสไปรษณีย์';
$lang['register_create_account'] = 'สร้างบัญชี';
$lang['register_social'] = 'ล็อคอินผ่านบัญชีโซเชียลมีเดียของฉัน:';

$lang['register_success'] = 'ลงทะเบียนเสร็จสิ้น';
$lang['register_success_detail'] = 'โปรดคลิกลิ้งที่อีเมลเพื่อยืนยันตัวตน';
