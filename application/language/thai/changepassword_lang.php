<?php
$lang['changepassword_error_msg'] = 'โปรดยืนยันรหัสผ่านให้ตรงกัน';
$lang['changepassword_email_login'] = 'อีเมลเข้าสู่ระบบ';
$lang['changepassword_new_password'] = 'รหัสผ่านใหม่';
$lang['changepassword_confirm_new_password'] = 'ยืนยันรหัสผ่านใหม่';
$lang['changepassword_change_password'] = 'เปลี่ยนรหัสผ่าน';

$lang['changepassword_success'] = 'เปลี่ยนรหัสผ่านเรียบร้อย';
$lang['changepassword_success_login'] = 'คุณได้เปลี่ยนรหัสผ่านใหม่เรียบร้อยแล้ว';
