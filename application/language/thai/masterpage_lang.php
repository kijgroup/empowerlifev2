<?php
$lang['switch_lang'] = '<img src="'.base_url().'assets/images/flag_en.png" alt="English Language" />';
$lang['nav_register'] = 'สมัครสมาชิก';
$lang['nav_login'] = 'เข้าสู่ระบบ';
$lang['nav_membership'] = 'บัญชีของฉัน';
$lang['nav_logout'] = 'ออกจากระบบ';

$lang['nav_home'] = 'หน้าแรก';
$lang['nav_product'] = 'ผลิตภัณฑ์';
$lang['nav_promotion'] = 'โปรโมชั่น';
$lang['nav_payment'] = 'แจ้งชำระเงิน';
$lang['nav_content'] = 'บทความ';
$lang['nav_about'] = 'เกี่ยวกับเรา';
$lang['nav_contact'] = 'ติดต่อเรา';
$lang['nav_quick_order'] = 'สั่งซื้อด่วน';
$lang['nav_reward'] = 'แลกของสมนาคุณ';
$lang['nav_profile'] = 'แก้ไขข้อมูลส่วนตัว';
$lang['nav_change_password'] = 'เปลี่ยนรหัสผ่าน';

$lang['nav_cart'] = 'ตะกร้า';

$lang['footer_know_us'] = 'รู้จักเรา';
$lang['footer_know_us_about_us'] = 'เกี่ยวกับเรา Empowerlife';
$lang['footer_know_us_verify'] = 'รับประกันคุณภาพ';
$lang['footer_know_us_oem'] = 'OEM';
$lang['footer_know_us_profit'] = 'Empowerlife คืนกำไรสู่สังคม';
$lang['footer_know_us_careers'] = 'ร่วมงานกับเรา';
$lang['footer_need_help'] = 'ต้องการความช่วยเหลือ';
$lang['footer_need_help_faq'] = 'ศูนย์ช่วยเหลือ / คำถามที่พบบ่อย';
$lang['footer_need_help_contact'] = 'ติดต่อเรา / ข้อเสนอแนะ';
$lang['footer_need_help_send_policy'] = 'นโยบายการส่ง';
$lang['footer_need_help_return_policy'] = 'เงื่อนไขการส่งคืนสินค้า';
$lang['footer_need_help_quick_order'] = 'สั่งซื้อด่วน';
$lang['footer_my_account'] = 'บัญชีของฉัน';
$lang['footer_my_account_login'] = 'ลงชื่อเข้าใช้ / ลงทะเบียน';
$lang['footer_my_account_membership'] = 'ภาพรวมบัญชีของฉัน';
$lang['footer_my_account_myorder'] = 'ข้อมูลการสั่งซื้อ';
$lang['footer_my_account_unsubscribe'] = 'ยกเลิกการติดตามผ่านอีเมล';
$lang['footer_my_account_reward'] = 'แลกของสมนาคุณ';
$lang['footer_subscribe'] = 'ติดตามข่าวสารผ่านอีเมล';
$lang['footer_subscribe_enter_email'] = 'กรอกอีเมล';

$lang['expire_in'] = 'โปรโมชั่นนี้จะสิ้นสุดในอีก';