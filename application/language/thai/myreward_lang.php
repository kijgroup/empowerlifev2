<?php
$lang['myreward_content_status'] = 'สถานะ';
$lang['myreward_content_date'] = 'วันที่แลกของสมนาคุณ';
$lang['myreward_detail'] = 'รายการแลกของสมนาคุณ';

$lang['myreward_detail_reward'] = 'ของสมนาคุณ';
$lang['myreward_detail_reward_point'] = 'คะแนนต่อหน่วย';
$lang['myreward_detail_reward_qty'] = 'จำนวน';
$lang['myreward_detail_reward_total'] = 'รวม(คะแนน)';
$lang['myreward_detail_reward_grand_total'] = 'คะแนนรวม:';

$lang['myreward_list_content_date'] = 'วันที่';
$lang['myreward_list_content_grand_total'] = 'ยอดรวม';
$lang['myreward_list_content_status'] = 'สถานะ';
