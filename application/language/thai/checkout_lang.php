<?php
$lang['checkout_content_next'] = 'ต่อไป';
$lang['checkout_content_order'] = 'สั่งซื้อสินค้า';

$lang['checkout_index_checkout'] = 'รถเข็นของคุณ';
$lang['checkout_index_home'] = 'หน้าแรก';

$lang['checkout_content_step1_address'] = 'ที่อยู่จัดส่งเอกสาร';
$lang['checkout_content_step1_contact'] = 'ข้อมูลติดต่อ';
$lang['checkout_content_step1_address_product'] = 'ที่อยู่จัดส่งสินค้า';
$lang['checkout_content_step1_shipping_name'] = 'ชื่อผู้รับสินค้า';
$lang['checkout_content_step1_shipping_mobile'] = 'เบอร์โทรศัพท์';
$lang['checkout_content_step1_shipping_address'] = 'ที่อยู่';
$lang['checkout_content_step1_shipping_district'] = 'แขวง/ตำบล';
$lang['checkout_content_step1_shipping_amphur'] = 'เขต/อำเภอ';
$lang['checkout_content_step1_shipping_province'] = 'จังหวัด';
$lang['checkout_content_step1_shipping_post_code'] = 'รหัสไปรษณีย์';
$lang['checkout_content_step1_is_tax'] = 'ต้องการใบเสร็จ';
$lang['checkout_content_step1_address_tax'] = 'ที่อยู่รับใบเสร็จ / ใบกำกับภาษี';
$lang['checkout_content_step1_same_address'] = 'ใช้ที่อยู่เดียวกับที่อยู่รับสินค้า';
$lang['checkout_content_step1_tax_name'] = 'ออกใบกำกับภาษีในนาม';
$lang['checkout_content_step1_tax_mobile'] = 'เบอร์โทรศัพท์';
$lang['checkout_content_step1_tax_address'] = 'ที่อยู่';
$lang['checkout_content_step1_tax_province'] = 'จังหวัด';
$lang['checkout_content_step1_tax_post_code'] = 'รหัสไปรษณีย์';
$lang['checkout_content_step1_tax_code'] = 'เลขที่ออกใบกำกับภาษี';
$lang['checkout_content_step1_tax_branch'] = 'สำนักงานใหญ่/เลขที่สาขา';

$lang['checkout_content_step2_payment'] = 'ช่องทางการชำระเงิน';
$lang['checkout_content_step2_payment_bank'] = 'โอนเงิน';
$lang['checkout_content_step2_payment_post'] = 'ชำระเงินและรับสินค้าที่ไปรษณีย์ (พกง)';
$lang['checkout_content_step2_payment_home'] = 'จัดส่งและชำระเงินที่หน้าบ้าน (DHL)';

$lang['checkout_content_step3_order_product'] = 'รายการสั่งซื้อสินค้า';
$lang['checkout_content_step3_product'] = 'สินค้า';
$lang['checkout_content_step3_product_price'] = 'ราคา';
$lang['checkout_content_step3_product_unit'] = 'จำนวน';
$lang['checkout_content_step3_product_total'] = 'ราคารวม';
$lang['checkout_content_step3_discount'] = 'ส่วนลดสมาชิก';
$lang['checkout_content_step3_order_total'] = 'รวม:';

$lang['checkout_content_step3_cart_total'] = 'รวม:';
$lang['checkout_content_step3_cart_subtotal'] = 'ค่าสินค้า:';
$lang['checkout_content_step3_discount_point'] = 'ส่วนลดแต้ม:';
$lang['checkout_content_step3_discount'] = 'ส่วนลดสมาชิก:';
$lang['checkout_content_step3_shipping'] = 'ค่าส่งสินค้า:';

$lang['checkout_content_complete_checkout_complete'] = 'การชำระเสร็จสมบูรณ์';
$lang['checkout_content_complete'] = 'Complete';
$lang['checkout_content_complete_order_number'] = 'หมายเลขสั่งซื้อของท่านคือ ';
$lang['checkout_content_complete_order_status_here'] = 'ตรวจสอบการสั่งซื้อของท่านได้ที่นี่!';
