<?php
$lang['myorder_history'] = 'ประวัติการสั่งซื้อ';
$lang['myorder_content_order_id'] = 'เลขที่ออเดอร์';
$lang['myorder_content_status'] = 'สถานะ';
$lang['myorder_content_order_date'] = 'วันที่สั่งซื้อ';
$lang['myorder_content_date'] = 'วันที่';
$lang['myorder_content_total'] = 'ยอดรวม';
$lang['myorder_content_status'] = 'สถานะ';
$lang['myorder_item_detail'] = 'ดูรายละเอียด';

$lang['myorder_order_item_product'] = 'สินค้า';
$lang['myorder_order_item_price'] = 'ราคาต่อหน่วย';
$lang['myorder_order_item_unit'] = 'จำนวน';
$lang['myorder_order_item_total'] = 'รวม';
$lang['myorder_order_item_discount'] = 'ส่วนลด';
$lang['myorder_order_item_grand_total'] = 'ราคารวม:';

$lang['myorder_shipping'] = 'ที่อยู่จัดส่งสินค้า';
$lang['myorder_shipping_name'] = 'ชื่อผู้รับสินค้า';
$lang['myorder_shipping_moblie'] = 'เบอร์โทรศัพท์';
$lang['myorder_shipping_address'] = 'ทีอยู่';
$lang['myorder_shipping_district'] = 'แขวง/ตำบล';
$lang['myorder_shipping_amphur'] = 'เขต/อำเภอ';
$lang['myorder_shipping_province'] = 'จังหวัด';
$lang['myorder_shipping_post_code'] = 'รหัสไปรษณีย์';

$lang['myorder_tax'] = 'ที่อยู่ในการออกใบกำกับภาษี';
$lang['myorder_tax_name'] = 'ชื่อ';
$lang['myorder_tax_address'] = 'ที่อยู่';
$lang['myorder_tax_district'] = 'แขวง/ตำบล';
$lang['myorder_tax_amphur'] = 'เขต/อำเภอ';
$lang['myorder_tax_province'] = 'จังหวัด';
$lang['myorder_tax_post_code'] = 'ไปรษณีย์';
$lang['myorder_tax_tax_id'] = 'เลขประจำตัวผู้เสียภาษี';
$lang['myorder_tax_branch'] = 'สาขา';
$lang['myorder_tax_mobile'] = 'โทรศัพท์';
