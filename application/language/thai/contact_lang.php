<?php
$lang['contact_form_contact_us'] = 'ติดต่อเจ้าหน้าที่';
$lang['contact_form_complete_form'] = 'เพื่อความรวดเร็วในการทำงาน โปรดกรอกข้อมูลให้ครบถ้วน';
$lang['contact_form_received_msg'] = 'ได้รับข้อความแล้ว';
$lang['contact_form_contact_information'] = 'ทางเราจะติดต่อกลับตามข้อมูลที่ท่านได้ให้ไว้';
$lang['contact_form_contact_message'] = 'ข้อความ *';
$lang['contact_form_contact_name'] = 'ชื่อ-นามสกุล *';
$lang['contact_form_contact_email'] = 'อีเมล์ *';
$lang['contact_form_contact_phone'] = 'เบอร์โทรศัพท์ *';
$lang['contact_form_send_message'] = 'ส่งข้อความ';
$lang['contact_form_successfully'] = 'ข้อความของคุณถูกส่งเรียบร้อยแล้ว';
