<?php
$lang['reward_list'] = 'สรุปรายการแลกของสมนาคุณ';
$lang['reward_point'] = 'รายการสินค้าที่ใช้คะแนนสะสมในการแลก';
$lang['reward_point_customer'] = 'คะแนนที่มี:';
$lang['reward_use_point'] = 'คะแนนที่ใช้';
$lang['reward_point_remain'] = 'คะแนนคงเหลือ';
$lang['reward_point_qty'] = 'แลกของสมนาคุณ';

$lang['reward_address'] = 'ที่อยู่จัดส่งของสมนาคุณ';
$lang['reward_shipping_name'] = 'ชื่อผู้รับสินค้า';
$lang['reward_shipping_mobile'] = 'เบอร์โทรศัพท์';
$lang['reward_shipping_address'] = 'ที่อยู่';
$lang['reward_shipping_district'] = 'เขต/ตำบล';
$lang['reward_shipping_amphur'] = 'แขวง/อำเภอ';
$lang['reward_shipping_province'] = 'จังหวัด';
$lang['reward_shipping_post_code'] = 'รหัสไปรษณีย์';
$lang['reward_shipping_note'] = 'รายละเอียดเพิ่มเติม';
$lang['reward_shipping_point'] = 'แลกของสมนาคุณ';
$lang['reward_shipping_point_qty'] = 'จำนวน:';

$lang['reward_complete'] = 'แลกของสมนาคุณเรียบร้อย';
$lang['reward_complete_detail'] = 'ข้อมูลการแลกของสมนาคุณของท่านได้ถูกส่งแล้ว ทางเราจะรีบจัดส่งสินค้าไปให้';
