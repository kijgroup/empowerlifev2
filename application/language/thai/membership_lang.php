<?php
$lang['membership_my_account'] = 'บัญชีของฉัน';
$lang['membership_my_account_member'] = 'ภาพรวม';
$lang['membership_my_account_myorder'] = 'ประวัติการสั่งซื้อ';
$lang['membership_my_account_reward'] = 'แลกของสมนาคุณ';
$lang['membership_my_account_my_reward'] = 'ประวัติการแลกของสมนาคุณ';
$lang['membership_my_account_profile'] = 'แก้ไขข้อมูลส่วนตัว';
$lang['membership_my_account_change_password'] = 'เปลี่ยนรหัสผ่าน';

$lang['membership_last_order_list'] = 'รายการสั่งซื้อล่าสุด';
$lang['membership_last_reward_list'] = 'รายการแลกของสมนาคุณล่าสุด';
$lang['membership_order_id'] = 'เลขที่ออเดอร์';
$lang['membership_last_order_date'] = 'วันที่';
$lang['membership_last_order_total'] = 'ยอดรวม';
$lang['membership_last_order_status'] = 'สถานะ';
$lang['membership_last_order_detail'] = 'ดูรายละเอียด';

$lang['membership_profile_customer'] = 'ข้อมูลสมาชิก';
$lang['membership_profile_end_date'] = 'วันหมดอายุสมาชิก:';
$lang['membership_profile_name'] = 'ชื่อ:';
$lang['membership_profile_code'] = 'รหัสสมาชิก:';
$lang['membership_profile_level'] = 'ระดับ:';
$lang['membership_profile_buy'] = 'ยอดซื้อสะสม:';
$lang['membership_profile_baht'] = 'บาท';
$lang['membership_profile_score'] = 'คะแนนสะสม:';
$lang['membership_profile_point'] = 'คะแนน';
$lang['membership_profile_buy_present'] = 'ยอดซื้อสะสมปัจจุบัน';
$lang['membership_profile_want_to_buy'] = 'ต้องการยอดซื้อสะสม';
$lang['membership_profile_discount'] = 'ส่วนลด';
