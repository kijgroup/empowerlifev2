<?php
$lang['quick_order_product'] = 'สินค้า';
$lang['quick_order_price'] = 'ราคาต่อหน่วย';
$lang['quick_order_qty'] = 'จำนวน';
$lang['quick_order_total'] = 'ราคารวม';
$lang['quick_order_button'] = 'สั่งซื้อสินค้า';
