<?php
$lang['content_login'] = 'เข้าสู่ระบบ';
$lang['content_login_email'] = 'อีเมล์';
$lang['content_login_password'] = 'รหัสผ่าน';
$lang['content_login_remember_me'] = 'อยู่ในสถานะลงชื่อเข้าใช้';
$lang['content_login_forgot_password'] = 'ลืมรหัสผ่าน?';
$lang['content_login_social'] = 'ลงชื่อเข้าใช้ผ่านบัญชี Social Network:';
