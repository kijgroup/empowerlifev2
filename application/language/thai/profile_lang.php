<?php
$lang['profile_form_member_first_name'] = 'ชื่อ :';
$lang['profile_form_member_last_name'] = 'นามสกุล :';
$lang['profile_form_member_gender'] = 'เพศ :';
$lang['profile_form_member_female'] = 'หญิง';
$lang['profile_form_member_male'] = 'ชาย';
$lang['profile_form_member_birthday'] = 'วันเกิด :';
$lang['profile_form_member_phone'] = 'เบอร์โทรศัพท์ :';
$lang['profile_form_member_mobile'] = 'เบอร์โทรศัพท์มือถือ :';
$lang['profile_form_member_address'] = 'ที่อยู่ :';
$lang['profile_form_member_district'] = 'แขวง/ตำบล :';
$lang['profile_form_member_amphur'] = 'เขต/อำเภอ :';
$lang['profile_form_member_province'] = 'จังหวัด :';
$lang['profile_form_member_post_code'] = 'รหัสไปรษณีย์ :';
$lang['profile_form_member_edit'] = 'แก้ไขข้อมูล';

$lang['profile_sucess_complete'] = 'แก้ไขข้อมูลเรียบร้อย';
$lang['profile_sucess_detail'] = 'ข้อมูลส่วนตัวของท่านได้ถูกปรับปรุงแล้ว';
