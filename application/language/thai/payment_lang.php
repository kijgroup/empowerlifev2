<?php
$lang['payment_order_code'] = 'หมายเลขออเดอร์';
$lang['payment_bank_payment_name'] = 'ชื่อ - นามสกุล';
$lang['payment_bank_payment_phone'] = 'โทรศัพท์มือถือ';
$lang['payment_bank_payment_amount'] = 'จำนวนเงิน';
$lang['payment_bank_payment_date'] = 'วันที่ชำระเงิน';
$lang['payment_bank_payment_time'] = 'เวลาชำระเงิน';
$lang['payment_bank_payment_image'] = 'แนบไฟล์';

$lang['payment_bank_payment'] = 'บัญชีธนาคาร';
$lang['payment_complete'] = 'การชำระเงินเสร็จสิ้น';
$lang['payment_complete_done'] = 'การชำระเงินสำเร็จ';
$lang['payment_complete_msg'] = 'ได้รับการแจ้งชำระเงินแล้ว ทางเราจะตรวจสอบและจัดส่งสินค้าให้ท่านในภายหลัง';
