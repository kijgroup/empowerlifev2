<?php
$lang['profile_form_member_first_name'] = 'First Name :';
$lang['profile_form_member_last_name'] = 'Last Name :';
$lang['profile_form_member_gender'] = 'Gender :';
$lang['profile_form_member_female'] = 'Female';
$lang['profile_form_member_male'] = 'Male';
$lang['profile_form_member_birthday'] = 'Birth Day :';
$lang['profile_form_member_phone'] = 'Phone :';
$lang['profile_form_member_mobile'] = 'Mobile :';
$lang['profile_form_member_address'] = 'Address :';
$lang['profile_form_member_district'] = 'Sub-District :';
$lang['profile_form_member_amphur'] = 'District :';
$lang['profile_form_member_province'] = 'Province :';
$lang['profile_form_member_post_code'] = 'Post Code :';
$lang['profile_form_member_edit'] = 'Edit';

$lang['profile_sucess_complete'] = 'Changes Saved.';
$lang['profile_sucess_detail'] = 'Your profile has been successfully updated.';
