<?php
$lang['switch_lang'] = '<img src="'.base_url().'assets/images/flag_th.png" alt="ภาษาไทย" />';
$lang['nav_register'] = 'Register';
$lang['nav_login'] = 'Login';
$lang['nav_membership'] = 'Account';
$lang['nav_logout'] = 'Logout';

$lang['nav_home'] = 'Home';
$lang['nav_product'] = 'Products';
$lang['nav_promotion'] = 'Promotions';
$lang['nav_payment'] = 'Payment';
$lang['nav_content'] = 'Content';
$lang['nav_about'] = 'About Us';
$lang['nav_contact'] = 'Contact Us';
$lang['nav_quick_order'] = 'Quick Order';
$lang['nav_reward'] = 'Rewards';
$lang['nav_profile'] = 'Profile';
$lang['nav_change_password'] = 'Change Password';

$lang['nav_cart'] = 'Cart';

$lang['footer_know_us'] = 'Get to know us';
$lang['footer_know_us_about_us'] = 'About Empowerlife';
$lang['footer_know_us_verify'] = 'Quality Promise';
$lang['footer_know_us_oem'] = 'OEM';
$lang['footer_know_us_profit'] = 'Empowerlife and CSR';
$lang['footer_know_us_careers'] = 'Careers';
$lang['footer_need_help'] = 'Help';
$lang['footer_need_help_faq'] = 'FAQ';
$lang['footer_need_help_contact'] = 'Contact us / Send us Feedback';
$lang['footer_need_help_send_policy'] = 'Delivery Policy';
$lang['footer_need_help_return_policy'] = 'Return Policy';
$lang['footer_need_help_quick_order'] = 'Quick Order';
$lang['footer_my_account'] = 'Account';
$lang['footer_my_account_login'] = 'Login';
$lang['footer_my_account_membership'] = 'Account Dashboard';
$lang['footer_my_account_myorder'] = 'Your Order';
$lang['footer_my_account_unsubscribe'] = 'Email Unsubscribe';
$lang['footer_my_account_reward'] = 'Reward';
$lang['footer_subscribe'] = 'Sign Up for News & Special Offers';
$lang['footer_subscribe_enter_email'] = 'enter email';

$lang['expire_in'] = 'Expire in:';