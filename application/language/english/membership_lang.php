<?php
$lang['membership_my_account'] = 'My Profile';
$lang['membership_my_account_member'] = 'My Profile';
$lang['membership_my_account_myorder'] = 'Purchase History';
$lang['membership_my_account_reward'] = 'Reward';
$lang['membership_my_account_my_reward'] = 'Redemption History';
$lang['membership_my_account_profile'] = 'Profile';
$lang['membership_my_account_change_password'] = 'Change Password';

$lang['membership_last_order_list'] = 'My Last Order';
$lang['membership_last_reward_list'] = 'My Redemption';
$lang['membership_order_id'] = 'Order ID';
$lang['membership_last_order_date'] = 'Date';
$lang['membership_last_order_total'] = 'Total';
$lang['membership_last_order_status'] = 'Status';
$lang['membership_last_order_detail'] = 'Detail';

$lang['membership_profile_customer'] = 'My Profile';
$lang['membership_profile_end_date'] = 'Expiration Date:';
$lang['membership_profile_name'] = 'Name:';
$lang['membership_profile_code'] = 'Member ID:';
$lang['membership_profile_level'] = 'Level:';
$lang['membership_profile_buy'] = 'Accumulated Sales:';
$lang['membership_profile_baht'] = 'Baht';
$lang['membership_profile_score'] = 'Points:';
$lang['membership_profile_point'] = 'Points';
$lang['membership_profile_buy_present'] = 'Accumulated Sales';
$lang['membership_profile_want_to_buy'] = 'Accumulated Sales to Upgrade to';
$lang['membership_profile_discount'] = 'Discount';
