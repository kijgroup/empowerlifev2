<?php
$lang['myreward_content_status'] = 'Status';
$lang['myreward_content_date'] = 'Date';
$lang['myreward_detail'] = 'Redeem Now';

$lang['myreward_detail_reward'] = 'Reward';
$lang['myreward_detail_reward_point'] = 'Points';
$lang['myreward_detail_reward_qty'] = 'QTY';
$lang['myreward_detail_reward_total'] = 'Total(reward)';
$lang['myreward_detail_reward_grand_total'] = 'Total Points:';

$lang['myreward_list_content_date'] = 'Date';
$lang['myreward_list_content_grand_total'] = 'Total';
$lang['myreward_list_content_status'] = 'Status';
