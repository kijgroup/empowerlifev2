<?php
$lang['contact_form_contact_us'] = 'Contact Us';
$lang['contact_form_complete_form'] = 'To receive the proper response, fill in all of the fields below.';
$lang['contact_form_received_msg'] = 'Thank you for your message.';
$lang['contact_form_contact_information'] = 'We will contact you as soon as possible.';
$lang['contact_form_contact_message'] = 'Message *';
$lang['contact_form_contact_name'] = 'Contact Name *';
$lang['contact_form_contact_email'] = 'E-Mail *';
$lang['contact_form_contact_phone'] = 'Mobile Phone *';
$lang['contact_form_send_message'] = 'Send Message';
$lang['contact_form_successfully'] = 'Your message was sent successfully.';
