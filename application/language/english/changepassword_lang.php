<?php
$lang['changepassword_error_msg'] = 'Password do not match. Please retype.';
$lang['changepassword_email_login'] = 'E-mail';
$lang['changepassword_new_password'] = 'New Password';
$lang['changepassword_confirm_new_password'] = 'Confirm Password';
$lang['changepassword_change_password'] = 'Change Password';

$lang['changepassword_success'] = 'Your password has been successfully updated.';
$lang['changepassword_success_login'] = 'Your password has been successfully updated.';
