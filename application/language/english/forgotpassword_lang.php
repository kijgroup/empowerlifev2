<?php
$lang['content_forgotpassword'] = 'Forgot Password';
$lang['content_forgotpassword_email'] = 'email';
$lang['content_forgotpassword_password'] = 'password';
$lang['content_forgotpassword_login'] = 'Remember password?';
$lang['content_login_social'] = 'You can also sign in via social accounts:';
