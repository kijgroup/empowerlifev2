<?php
$lang['reward_list'] = 'Summary';
$lang['reward_point'] = 'Calculate';
$lang['reward_point_customer'] = 'Total Points Balance:';
$lang['reward_use_point'] = 'Use';
$lang['reward_point_remain'] = 'Balance';
$lang['reward_point_qty'] = 'Redeem Now';

$lang['reward_address'] = 'Delivery Address';
$lang['reward_shipping_name'] = 'Name';
$lang['reward_shipping_mobile'] = 'Mobile';
$lang['reward_shipping_address'] = 'Address';
$lang['reward_shipping_district'] = 'Sub-District';
$lang['reward_shipping_amphur'] = 'District';
$lang['reward_shipping_province'] = 'Province';
$lang['reward_shipping_post_code'] = 'Post Code';
$lang['reward_shipping_note'] = 'Note';
$lang['reward_shipping_point'] = 'Redeem now';
$lang['reward_shipping_point_qty'] = 'Quantity:';

$lang['reward_complete'] = 'Your redemption is successful!';
$lang['reward_complete_detail'] = 'Your order will be delivered soon!';
