<?php
$lang['checkout_content_next'] = 'Next';
$lang['checkout_content_order'] = 'Purchase';

$lang['checkout_index_checkout'] = 'Checkout';
$lang['checkout_index_home'] = 'Home';

$lang['checkout_content_step1_address'] = 'Mailling Address';
$lang['checkout_content_step1_contact'] = 'Contact Information';
$lang['checkout_content_step1_address_product'] = 'Delivery Address';
$lang['checkout_content_step1_shipping_name'] = 'Name';
$lang['checkout_content_step1_shipping_mobile'] = 'Mobile Phone';
$lang['checkout_content_step1_shipping_address'] = 'Address';
$lang['checkout_content_step1_shipping_district'] = 'Sub-District';
$lang['checkout_content_step1_shipping_amphur'] = 'District';
$lang['checkout_content_step1_shipping_province'] = 'Province';
$lang['checkout_content_step1_shipping_post_code'] = 'Post Office';
$lang['checkout_content_step1_is_tax'] = 'Receipt Required';
$lang['checkout_content_step1_address_tax'] = 'Tax Invoice';
$lang['checkout_content_step1_same_address'] = 'Use Contact Information for Address';
$lang['checkout_content_step1_tax_name'] = 'Issue tax invoice on behalf of';
$lang['checkout_content_step1_tax_mobile'] = 'Mobile';
$lang['checkout_content_step1_tax_address'] = 'Address';
$lang['checkout_content_step1_tax_province'] = 'Province';
$lang['checkout_content_step1_tax_post_code'] = 'Post Office';
$lang['checkout_content_step1_tax_code'] = 'TAX ID';
$lang['checkout_content_step1_tax_branch'] = 'Head Office / Branch';

$lang['checkout_content_step2_payment'] = 'Payment Channels';
$lang['checkout_content_step2_payment_bank'] = 'Money Transfer';
$lang['checkout_content_step2_payment_post'] = 'Cash on Picking up your package at the post office';
$lang['checkout_content_step2_payment_home'] = 'Cash on Delivery';

$lang['checkout_content_step3_order_product'] = 'Your Order';
$lang['checkout_content_step3_product'] = 'Product';
$lang['checkout_content_step3_product_price'] = 'Price';
$lang['checkout_content_step3_product_unit'] = 'Unit';
$lang['checkout_content_step3_product_total'] = 'Total Price';
$lang['checkout_content_step3_discount'] = 'Member Discount';
$lang['checkout_content_step3_order_total'] = 'Order total:';

$lang['checkout_content_step3_cart_total'] = 'cart totals:';
$lang['checkout_content_step3_cart_subtotal'] = 'Cart subtotal:';
$lang['checkout_content_step3_discount'] = 'Member Discount:';
$lang['checkout_content_step3_shipping'] = 'Shipping price:';

$lang['checkout_content_complete_checkout_complete'] = 'Checkout Complete';
$lang['checkout_content_complete'] = 'Complete';
$lang['checkout_content_complete_order_number'] = 'Your order number is ';
$lang['checkout_content_complete_order_status_here'] = 'Check your order status here!';
