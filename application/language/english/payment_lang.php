<?php
$lang['payment_order_code'] = 'Order Code';
$lang['payment_bank_payment_name'] = 'Name-Surname';
$lang['payment_bank_payment_phone'] = 'Phone';
$lang['payment_bank_payment_amount'] = 'Amount';
$lang['payment_bank_payment_date'] = 'Date';
$lang['payment_bank_payment_time'] = 'Time';
$lang['payment_bank_payment_image'] = 'Attach File';

$lang['payment_bank_payment'] = 'Bank Account';
$lang['payment_complete'] = 'Payment Complete';
$lang['payment_complete_done'] = 'Complete';
$lang['payment_complete_msg'] = 'We have received your payment. Your order will be delivered soon!';
