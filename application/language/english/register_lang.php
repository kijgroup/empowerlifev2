<?php
$lang['register_email'] = 'E-mail';
$lang['register_password'] = 'Password';
$lang['register_confirm_password'] = 'Confirm Password';
$lang['register_first_name'] = 'First Name';
$lang['register_last_name'] = 'Last Name';
$lang['register_gender'] = 'gender';
$lang['register_gender_female'] = 'Female';
$lang['register_gender_male'] = 'Male';
$lang['register_birthday'] = 'BirthDay';
$lang['register_phone'] = 'Phone';
$lang['register_mobile'] = 'Mobile';
$lang['register_address'] = 'Address';
$lang['register_district'] = 'Sub-District';
$lang['register_amphur'] = 'District';
$lang['register_province'] = 'Province';
$lang['register_post_code'] = 'Post Code';
$lang['register_create_account'] = 'Create an account';
$lang['register_social'] = 'You can also sign in via social accounts:';

$lang['register_success'] = 'Your registration is almost completed.';
$lang['register_success_detail'] = 'please check your email to activate your account.';
