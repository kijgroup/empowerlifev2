<?php
$lang['myorder_history'] = 'Purchase History';
$lang['myorder_content_order_id'] = 'Order ID';
$lang['myorder_content_status'] = 'Status';
$lang['myorder_content_order_date'] = 'Order Date';
$lang['myorder_content_date'] = 'Date';
$lang['myorder_content_total'] = 'Total';
$lang['myorder_content_status'] = 'Status';
$lang['myorder_item_detail'] = 'Detail';

$lang['myorder_order_item_product'] = 'Product';
$lang['myorder_order_item_price'] = 'Price';
$lang['myorder_order_item_unit'] = 'QTY';
$lang['myorder_order_item_total'] = 'Total';
$lang['myorder_order_item_discount'] = 'Discount';
$lang['myorder_order_item_grand_total'] = 'Grand Total:';

$lang['myorder_shipping'] = 'Delivery Address';
$lang['myorder_shipping_name'] = 'Receiver';
$lang['myorder_shipping_moblie'] = 'Moblie';
$lang['myorder_shipping_address'] = 'Address';
$lang['myorder_shipping_district'] = 'Sub-District';
$lang['myorder_shipping_amphur'] = 'District';
$lang['myorder_shipping_province'] = 'Province';
$lang['myorder_shipping_post_code'] = 'Post Code';

$lang['myorder_tax'] = 'Address for Tax Invoice';
$lang['myorder_tax_name'] = 'Company Name';
$lang['myorder_tax_address'] = 'Address';
$lang['myorder_tax_district'] = 'Sub-District';
$lang['myorder_tax_amphur'] = 'District';
$lang['myorder_tax_province'] = 'Province';
$lang['myorder_tax_post_code'] = 'Post Code';
$lang['myorder_tax_tax_id'] = 'TAX ID';
$lang['myorder_tax_branch'] = 'Branch';
$lang['myorder_tax_mobile'] = 'Mobile';
