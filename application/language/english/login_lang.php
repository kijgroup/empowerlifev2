<?php
$lang['content_login'] = 'Login';
$lang['content_login_email'] = 'email';
$lang['content_login_password'] = 'password';
$lang['content_login_remember_me'] = 'remember me';
$lang['content_login_forgot_password'] = 'Forgot password?';
$lang['content_login_social'] = 'You can also sign in via social accounts:';
