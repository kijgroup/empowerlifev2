$(document).ready(function(){
  setup_date_picker();
  setupTextarea();
  init_multiselect();
  init_radio();
  $("#promotion-form").validate({
    highlight: function(label) {
      $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(label) {
      $(label).closest('.form-group').removeClass('has-error');
      label.remove();
    },
    errorPlacement: function( error, element ) {
      var placement = element.closest('.input-group');
      if (!placement.get(0)) {
        placement = element;
      }
      if (error.text() !== '') {
        placement.after(error);
      }
    }
  });
});
function setup_date_picker(){
  $('.form-control-date').datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    todayBtn: true
  });
}
function init_radio(){
  check_expire();
  on_change_expire();
  check_bundle();
  on_change_bundle();
}
function check_expire(){
  $('input[type=radio][name=is_expire]').on('change', on_change_expire);
}
function on_change_expire(){
  var check_expire = $('input[type=radio][name=is_expire]:checked').val();
  if (check_expire == 'true') {
    $('#expire_form').show();
  }
  else if (check_expire == 'false') {
    $('#expire_form').hide();
  }
}
function check_bundle(){
  $('input[type=radio][name=is_bundle]').on('change', on_change_bundle);
}
function on_change_bundle(){
  var check_bundle = $('input[type=radio][name=is_bundle]:checked').val();
  if (check_bundle == 'true') {
    $('#bundle_form').show();
  }
  else if (check_bundle == 'false') {
    $('#bundle_form').hide();
  }
}
function setupTextarea(){
  $('.summernote').summernote({
    height: 400,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['fontname', ['fontname']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['height', ['height']],
      ['table', ['table']],
      ['insert', ['link', 'picture', 'video']],
      ['view', ['fullscreen', 'codeview', 'help']]
    ],
    popover: {
      image: [
        ['custom', ['imageAttributes']],
        ['imagesize', ['imageSize100', 'imageSize50', 'imageSize25']],
        ['float', ['floatLeft', 'floatRight', 'floatNone']],
        ['remove', ['removeMedia']]
      ],
    },
    imageAttributes:{
      icon:'<i class="note-icon-pencil"/>',
      removeEmpty:false, // true = remove attributes | false = leave empty if present
      disableUpload: false // true = don't display Upload Options | Display Upload Options
    },
    fontSizes: ['8', '9', '10', '11', '12','13', '14','15','16','17', '18','20','22', '24','30', '36', '48' , '64'],
    minHeight: null,
    maxHeight: null,
    callbacks: {
      onImageUpload: function(files) {
        url = $(this).data('upload');
        sendFile(files[0], url, $(this));
      }
    }
  });
}
function sendFile(file, url, editor) {
  data = new FormData();
  data.append("file", file);
  $.ajax({
    data: data,
    type: "POST",
    url:url,
    cache: false,
    contentType: false,
    processData: false,
    success: function(url) {
      editor.summernote('insertImage', url);
    }
  });
}
function init_multiselect(){
  $('.multi-select').multiSelect({
    selectableHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='ค้นหา'>",
    selectionHeader: "<input type='text' class='search-input form-control' autocomplete='off' placeholder='ค้นหา'>",
    afterInit: function(ms){
      var that = this,
          $selectableSearch = that.$selectableUl.prev(),
          $selectionSearch = that.$selectionUl.prev(),
          selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
          selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

      that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
      .on('keydown', function(e){
        if (e.which === 40){
          that.$selectableUl.focus();
          return false;
        }
      });

      that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
      .on('keydown', function(e){
        if (e.which == 40){
          that.$selectionUl.focus();
          return false;
        }
      });
    },
    afterSelect: function(){
      this.qs1.cache();
      this.qs2.cache();
    },
    afterDeselect: function(){
      this.qs1.cache();
      this.qs2.cache();
    }
  });
}
