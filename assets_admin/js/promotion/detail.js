$(document).ready(function(){
  add_event_on_modal_close();
  add_sort_event();
});
function add_sort_event(){
  var sort_image_url = $('.sortable').data('sort_url');
  $('.sortable').sortable({
    cursor: "move",
    containment: 'div.sortable',
    start: function(e, ui) {
      ui.placeholder.height(ui.item.height());
    },
    sort: function(e, ui) {
      var top = e.pageY - $('.sortable').offset().top - (ui.helper.outerHeight(true) / 2);
      ui.helper.css({'top' : top + 'px'});
    },
    stop: function(e, ui) {
      $image_id = ui.item.data('image_id');
      $priority = (parseInt($('div[data-image_id="' + $image_id + '"]', $(this)).index()) + 1);
      $.post(sort_image_url, {image_id:$image_id, sort_priority: $priority});
    }
  }).disableSelection();
}
function add_event_on_modal_close(){
  $('#addImageModal').on('hide.bs.modal', function (e) {
    location.reload();
  });
}
