$(document).ready(function() {
    $("#searchForm").submit(function(event) {
        event.preventDefault();
        var action = $(this).attr("action");
        onSearchSubmit(action);
    });
    $('#txtStartDate').datepicker({format: 'yyyy-mm-dd'});
    $('#txtEndDate').datepicker({format: 'yyyy-mm-dd'});
    $("#searchForm").submit();
});
function onSearchSubmit(action) {
    $("#result").empty().append('Loading ...');
    var searchVal = $('#txtSearchVal').val();
    var startDate = $('#txtStartDate').val();
    var endDate = $('#txtEndDate').val();
    getContent(action, searchVal, startDate, endDate, 1, 20);
}
function getContent(action, searchVal, startDate, endDate, page, per_page){
    $("#result").empty().append('Loading ...');
    var posting = $.post(action, {
        searchVal: searchVal,
        startDate: startDate,
        endDate: endDate,
        page: page,
        per_page: per_page
    });
    posting.done(function(data) {
        $("#result").empty().append(data);
        setOnChangePerPage();
        setOnChangePage();
    });
    posting.fail(function(data){
        setTimeout(function() { getContent(action, searchVal, startDate, endDate, page, per_page); }, 5000);
    });
}
function setOnChangePerPage(){
    $('.ddlPerPage').change(function(){
        var per_page = $(this).val();
        var action = $(this).data('action');
        var searchVal = $(this).data('searchVal');
        var startDate = $(this).data('startDate');
        var endDate = $(this).data('endDate');
        var sort = $(this).data('sort');
        var order = $(this).data('order');
        getContent(action, searchVal, startDate, endDate, 1, per_page);
    });
}
function setOnChangePage(){
    $('.ddlPager').change(function(){
        var page = $(this).val();
        var action = $(this).data('action');
        var searchVal = $(this).data('searchVal');
        var startDate = $(this).data('startDate');
        var endDate = $(this).data('endDate');
        var perPage = $(this).data('per_page');
        getContent(action, searchVal, startDate, endDate, page, per_page);
    });
}
