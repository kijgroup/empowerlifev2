$(document).ready(function(){
  setupTextarea();
});
function setupTextarea(){
  $('.summernote').summernote({
    height: 400,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['fontname', ['fontname']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['height', ['height']],
      ['table', ['table']],
      ['insert', ['link', 'picture', 'video']],
      ['view', ['fullscreen', 'codeview', 'help']]
    ],
    minHeight: null,
    maxHeight: null,
    callbacks: {
      onImageUpload: function(files) {
        url = $(this).data('upload');
        sendFile(files[0], url, $(this));
      }
    }
  });
}
function sendFile(file, url, editor) {
  data = new FormData();
  data.append("file", file);
  $.ajax({
    data: data,
    type: "POST",
    url:url,
    cache: false,
    contentType: false,
    processData: false,
    success: function(url) {
      editor.summernote('insertImage', url);
    }
  });
}
