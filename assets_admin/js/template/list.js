$(document).ready(function() {
  $("#search-form").submit(function(event) {
    event.preventDefault();
    var action = $(this).attr("action");
    on_search_submit(action);
  });
  $("#search-form").submit();
});
function on_search_submit(action) {
  $("#result").empty().append('Loading ...');
  var search_str = $("#search-form").serialize()+'&page=1&per_page=20';
  get_content(action, search_str);
}
function get_content(action, str_search){
  $("#result").empty().append('Loading ...');
  var posting = $.post(action, str_search);
  posting.done(function(data) {
    $("#result").empty().append(data);
    set_on_change_per_page();
    set_on_change_page();
  });
  posting.fail(function(data){
    setTimeout(function() { get_content(action, str_search); }, 5000);
  });
}
function set_on_change_per_page(){
  $('.ddl_per_page').change(function(){
    var per_page = $(this).val();
    var action = $(this).data('action');
    var str_search = get_str_search_from_data($(this).data())+'page=1&per_page='+per_page;
    get_content(action, str_search);
  });
}
function set_on_change_page(){
  $('.ddl_pager').change(function(){
    var page = $(this).val();
    var action = $(this).data('action');
    var str_search = get_str_search_from_data($(this).data())+'page='+page;
    get_content(action, str_search);
  });
}

function get_str_search_from_data(data){
  var str_search = '';
  for(var i in data){
    if(i != 'action');
    str_search += i+'='+data[i]+'&';
  }
  return str_search;
}