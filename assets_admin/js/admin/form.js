$(document).ready(function() {
    validationForm();

    $("#admin-form").validate({
      highlight: function(label) {
        $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
      },
      success: function(label) {
        $(label).closest('.form-group').removeClass('has-error');
        label.remove();
      },
      errorPlacement: function( error, element ) {
        var placement = element.closest('.input-group');
        if (!placement.get(0)) {
          placement = element;
        }
        if (error.text() !== '') {
          placement.after(error);
        }
      }
    });
});
function validationForm(){
  $('#admin-form').validate({
    rules: {
      txtUsername: {
        required: true,
        minlength: 4
      },
      txtPassword: {
        required: true,
        minlength: 6
      },
      txtConfirmPassword: {
        required: true,
        minlength: 6,
        equalTo: "#txtPassword"
      },
    },
    onfocusout: injectTrim($.validator.defaults.onfocusout),
  });
}
function injectTrim(handler) {
  return function (element, event) {
    if (element.tagName === "TEXTAREA" || (element.tagName === "INPUT" && element.type !== "password")) {
      element.value = $.trim(element.value);
    }
    return handler.call(this, element, event);
  };
}
