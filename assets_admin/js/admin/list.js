$(document).ready(function() {
    $("#searchForm").submit(function(event) {
        event.preventDefault();
        var action = $(this).attr("action");
        onSearchSubmit(action);
    });
    $("#searchForm").submit();
});
function onSearchSubmit(action) {
    $("#result").empty().append('Loading ...');
    var searchVal = $('#txtSearchVal').val();
    var enableStatus = $('#ddlEnableStatus').val();
    getContent(action, searchVal, enableStatus, 1, 20);
}
function getContent(action, searchVal, enableStatus, page, perPage){
    $("#result").empty().append('Loading ...');
    var posting = $.post(action, {
        searchVal: searchVal,
        enableStatus: enableStatus,
        page: page,
        perPage: perPage
    });
    posting.done(function(data) {
        $("#result").empty().append(data);
        setOnChangePerPage();
        setOnChangePage();
    });
    posting.fail(function(data){
        setTimeout(function() { getContent(action, searchVal, enableStatus, page, perPage); }, 5000);
    });
}
function setOnChangePerPage(){
    $('.ddlPerPage').change(function(){
        var perPage = $(this).val();
        var action = $(this).data('action');
        var searchVal = $(this).data('searchval');
        var enableStatus = $(this).data('enablestatus');
        var sort = $(this).data('sort');
        var order = $(this).data('order');
        getContent(action, searchVal, enableStatus, 1, perPage);
    });
}
function setOnChangePage(){
    $('.ddlPager').change(function(){
        var page = $(this).val();
        var action = $(this).data('action');
        var searchVal = $(this).data('searchval');
        var enableStatus = $(this).data('enablestatus');
        var perPage = $(this).data('perpage');
        getContent(action, searchVal, enableStatus, page, perPage);
    });
}
