$(document).ready(function(){
  init_province();
  init_amphur();
  init_district();
  init_submit();
  setTimeout(function() { $('.select2-container').width('auto')});
});
function init_province(){
  $('.select-province').on('change', function(){
    var province_id = $(this).val();
    var action = $(this).data('action')+'/'+province_id;
    var target = '#'+$(this).data('target');
    get_address_list(action, province_id, target);
  });
}
function init_amphur(){
  $('.select-amphur').on('change', function(){
    var amphur_id = $(this).val();
    var action = $(this).data('action')+'/'+amphur_id;
    var target = '#'+$(this).data('target');
    get_address_list(action, amphur_id, target);
  });
}
function get_address_list(action, key_id, target){
  $(target).empty();
  var posting = $.post(action);
  posting.done(function(data) {
    data = '<option value="" disabled selected>โปรดระบุ</option>'+data;
    $(target).empty().append(data);
  });
  posting.fail(function(data){
    setTimeout(function() { get_address_list(action, key_id, target); }, 5000);
  });
}
function init_district(){
  $('.select-district').on('change', function(){
    var district_id = $(this).val();
    var action = $(this).data('action')+'/'+district_id;
    var target = '#'+$(this).data('target');
    get_postcode(action, district_id, target);
  });
}
function get_postcode(action, district_id, target){
  $(target).empty();
  var posting = $.post(action);
  posting.done(function(data) {
    $(target).empty().append(data);
  });
  posting.fail(function(data){
    setTimeout(function() { get_postcode(action, district_id, target); }, 5000);
  });
}
function init_submit(){
  $('.copy-address').on('click', function(){
    var source = $(this).data('source');
    var $province = $('#'+$(this).data('target_province'));
    var $amphur = $('#'+$(this).data('target_amphur'));
    var $district = $('#'+$(this).data('target_district'));
    var $postcode = $('#'+$(this).data('target_postcode'));
    var province_data = get_selected_text(source+'_province');
    var amphur_data = get_selected_text(source+'_amphur');
    var district_data = get_selected_text(source+'_district');
    var postcode_data = $('#'+source+'_postcode').html();
    $province.val(province_data);
    $amphur.val(amphur_data);
    $district.val(district_data);
    $postcode.val(postcode_data);
  });
}
function get_selected_text(elementId){
  var elt = document.getElementById(elementId);
  if (elt.selectedIndex == -1)
    return null;
  return elt.options[elt.selectedIndex].text;
}