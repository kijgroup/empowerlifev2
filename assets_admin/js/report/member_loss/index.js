$(document).ready(function() {
  setup_date_picker();
  $("#searchForm").submit(function(event) {
    event.preventDefault();
    var action = $(this).attr("action");
    onSearchSubmit(action);
  });
  //$("#searchForm").submit();
});
function setup_date_picker(){
  $('.form-control-date').datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    todayBtn: true
  });
}
function onSearchSubmit(action) {
  $("#result").empty().append('Loading ...');
  var search_val = $('#search_val').val();
  var date_after = $('#date_after').val();
  var member_type_code = $('#member_type_code').val();
  var enable_status = $('#enable_status').val();
  var sort = $('#sort').val();
  get_content(action, search_val, date_after, member_type_code, enable_status, sort, 1, 20);
}
function get_content(action, search_val, date_after, member_type_code, enable_status, sort, page, per_page){
  $("#result").empty().append('Loading ...');
  var posting = $.post(action, {
    search_val: search_val,
    date_after: date_after,
    member_type_code: member_type_code,
    enable_status: enable_status,
    sort: sort,
    page: page,
    per_page: per_page
  });
  posting.done(function(data) {
    $("#result").empty().append(data);
    setOnChangePerPage();
    setOnChangePage();
  });
  posting.fail(function(data){
    setTimeout(function() { get_content(action, search_val, date_after, member_type_code, enable_status, sort, page, per_page); }, 5000);
  });
}
function setOnChangePerPage(){
  $('.ddlPerPage').change(function(){
    var per_page = $(this).val();
    var action = $(this).data('action');
    var search_val = $(this).data('search_val');
    var date_after = $(this).data('date_after');
    var member_type_code = $(this).data('member_type_code');
    var enable_status = $(this).data('enable_status');
    var sort = $(this).data('sort');
    get_content(action, search_val, date_after, member_type_code, enable_status, sort, 1, per_page);
  });
}
function setOnChangePage(){
  $('.ddlPager').change(function(){
    var page = $(this).val();
    var action = $(this).data('action');
    var search_val = $(this).data('search_val');
    var date_after = $(this).data('date_after');
    var member_type_code = $(this).data('member_type_code');
    var enable_status = $(this).data('enable_status');
    var sort = $(this).data('sort');
    var per_page = $(this).data('perpage');
    get_content(action, search_val, date_after, member_type_code, enable_status, sort, page, per_page);
  });
}
