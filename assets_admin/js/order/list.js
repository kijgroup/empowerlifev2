$(document).ready(function() {
  setup_date_picker();
  $("#searchForm").submit(function(event) {
    event.preventDefault();
    var action = $(this).attr("action");
    onSearchSubmit(action);
  });
  $("#searchForm").submit();
  onActionSubmit();
});
function setup_date_picker(){
  $('.form-control-date').datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    todayBtn: true
  });
}
function onActionSubmit(){
  $('.btn-action').on('click', function(){
    var action = $(this).data('action');
    $('#action').val(action);
    if(['print_true','print_false'].indexOf(action) != -1){
      $('#action-form').attr('target', '_blank');
    }else{
      $('#action-form').attr('target', '_self');
    }
    $('#action-form').submit();
  });
}
function onSearchSubmit(action) {
  $("#result").empty().append('Loading ...');
  var search_val = $('#search_val').val();
  var payment_type = $('#payment_type').val();
  var order_channel_id = $('#order_channel_id').val();
  var send_time_status = $('#send_time_status').val();
  var create_by = $('#create_by').val();
  var start_date = $('#start_date').val();
  var end_date = $('#end_date').val();
  var order_status = $('#order_status').val();
  getContent(action, search_val, payment_type, order_channel_id, send_time_status, create_by, start_date, end_date, order_status, 1, 20);
}
function getContent(action, search_val, payment_type, order_channel_id, send_time_status, create_by, start_date, end_date, order_status, page, per_page){
  $("#result").empty().append('Loading ...');
  var posting = $.post(action, {
    search_val: search_val,
    payment_type: payment_type,
    order_channel_id: order_channel_id,
    send_time_status: send_time_status,
    create_by: create_by,
    start_date: start_date,
    end_date: end_date,
    order_status: order_status,
    page: page,
    per_page: per_page
  });
  posting.done(function(data) {
    $("#result").empty().append(data);
    setOnChangePerPage();
    setOnChangePage();
    setEventPdf();
    onActionSubmit();
  });
  posting.fail(function(data){
    setTimeout(function() { getContent(action, payment_type, order_channel_id, send_time_status, create_by, start_date, end_date, order_status, page, per_page); }, 5000);
  });
}
function setOnChangePerPage(){
  $('.ddlPerPage').change(function(){
    var per_page = $(this).val();
    var action = $(this).data('action');
    var search_val = $(this).data('search_val');
    var payment_type = $(this).data('payment_type');
    var order_channel_id = $(this).data('order_channel_id');
    var send_time_status = $(this).data('send_time_status');
    var create_by = $(this).data('create_by');
    var start_date = $(this).data('start_date');
    var end_date = $(this).data('end_date');
    var order_status = $(this).data('order_status');
    var sort = $(this).data('sort');
    var order = $(this).data('order');
    getContent(action, search_val, payment_type, order_channel_id, send_time_status, create_by, start_date, end_date, order_status, 1, per_page);
  });
}
function setOnChangePage(){
  $('.ddlPager').change(function(){
    var page = $(this).val();
    var action = $(this).data('action');
    var search_val = $(this).data('search_val');
    var payment_type = $(this).data('payment_type');
    var order_channel_id = $(this).data('order_channel_id');
    var send_time_status = $(this).data('send_time_status');
    var create_by = $(this).data('create_by');
    var start_date = $(this).data('start_date');
    var end_date = $(this).data('end_date');
    var order_status = $(this).data('order_status');
    var per_page = $(this).data('per_page');
    getContent(action, search_val, payment_type, order_channel_id, send_time_status, create_by, start_date, end_date, order_status, page, per_page);
  });
}
function setEventPdf(){
  $('#btn-download').on('click', function(){
    $('#print-form').submit();
  });
}
function selectall(source){
  checkboxes = document.getElementsByName('order_id[]');
  for(var i=0, n=checkboxes.length;i<n;i++) {
    checkboxes[i].checked = source.checked;
  }
}
