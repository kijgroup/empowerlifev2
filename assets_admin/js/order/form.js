$(document).ready(function(){
  event_on_change_member();
  on_change_member();
  check_hideen_member();
  $('#payment_type').on('change', on_change_payment_type);
  on_change_payment_type();
  $('.discount_point').on('change paste keyup', on_change_discount_point);
  $('#btn-plus').on('click', appendRow);
  $('.btn-remove-product').on('click', removeRow);
  $('.qty').on('change paste keyup', on_change_qty);
  $('.price_per_item').on('change paste keyup', on_change_price_per_item);
  $('#discount_price, #shipping_price').on('change paste keyup', update_total_price);
  appendRow();
  $('.product_id').on('change', on_change_product);
  //event_on_change_discount_point();
  $("#order-form").validate({
    highlight: function(label) {
      $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(label) {
      $(label).closest('.form-group').removeClass('has-error');
      label.remove();
    },
    errorPlacement: function( error, element ) {
      var placement = element.closest('.input-group');
      if (!placement.get(0)) {
        placement = element;
      }
      if (error.text() !== '') {
        placement.after(error);
      }
    }
  });
});
function on_change_payment_type(){
  var payment_type = $('#payment_type').val();
  if(payment_type == 'post'){
    $('#postoffice-form').slideDown();
  }else{
    $('#postoffice-form').slideUp();
  }
}
function event_on_change_member(){
  $('#member_id').on('change', function(){
    var member = $(this).find(':selected');
    $('#shipping_name').val(member.data('member_firstname')+" "+member.data('member_lastname'));
    $('#shipping_address').val(member.data('member_address'));
    $('#shipping_district').val(member.data('member_district'));
    $('#shipping_amphur').val(member.data('member_amphur'));
    $('#shipping_province').val(member.data('member_province'));
    $('#shipping_post_code').val(member.data('member_postcode'));
    $('#shipping_mobile').val(member.data('member_mobile'));
    $('#tax_name').val(member.data('member_firstname')+" "+member.data('member_lastname'));
    $('#tax_address').val(member.data('member_address'));
    $('#tax_district').val(member.data('member_district'));
    $('#tax_amphur').val(member.data('member_amphur'));
    $('#tax_province').val(member.data('member_province'));
    $('#tax_post_code').val(member.data('member_postcode'));
    $('#tax_mobile').val(member.data('member_mobile'));
    on_change_member();
  });
}
/*
function event_on_change_discount_point(){
  $('#discount_point_id').on('change', function(){
    var discount_point = $(this).find(':selected');
    $('#discount_point_amount').val(discount_point.data('use_point'));
    $('#discount_by_point_amount').val(discount_point.data('discount_amount'));
    update_total_price();
  });
}
*/
function appendRow(){
  $('#order_item_list').append($('#table_order_item').html());
  $('.btn-remove-product').off('click').on('click', removeRow);
  $('.product_id').off('click').on('change', on_change_product);
  $('.qty').off('change paste keyup').on('change paste keyup', on_change_qty);
  $('.price_per_item').off('change paste keyup').on('change paste keyup', on_change_price_per_item);
  setTimeout(update_select2, 300)
};

function removeRow(){
  var clickElement = $(this);
  var removeIndex = $('.btn-remove-product').index(clickElement);
  $('.row_item').eq(removeIndex).remove();
}
function on_change_product(){
  var change_element = $(this);
  var change_index = $('.product_id').index(change_element);
  var price_per_item = $('.product_id').eq(change_index).find(':selected').data('price');
  $('.price_per_item').eq(change_index).val(price_per_item);
  update_price_row(change_index);
  //update total price
}
function on_change_qty(){
  var change_index = $('.qty').index($(this));
  update_price_row(change_index);
}
function on_change_price_per_item(){
  var change_index = $('.price_per_item').index($(this));
  update_price_row(change_index);
}
function update_price_row(row_inx){
  var qty = parseInt($('.qty').eq(row_inx).val());
  var price_per_item = parseFloat($('.price_per_item').eq(row_inx).val());
  var total_price = qty * price_per_item;
  $('.price_total').eq(row_inx).html(total_price);
  update_total_price();
}
function on_change_discount_point(){
  $total_point_use = 0;
  $total_discount_get = 0;
  $('.discount_point').each(function(){
    $discount_amount = $(this).val();
    $total_point_use += $(this).data('use_point') * $discount_amount;
    $total_discount_get += $(this).data('discount_amount') * $discount_amount;
  });
  $('#discount_point_amount').val(parseFloat($total_point_use));
  $('#discount_by_point_amount').val(parseFloat($total_discount_get));
  update_total_price();
}
function update_total_price(){
  var total_qty = 0;
  var total_item_price = 0;
  var discount_by_point_amount = parseFloat($('#discount_by_point_amount').val());
  var discount_price = parseFloat($('#discount_price').val());
  var shipping_price = parseFloat($('#shipping_price').val());
  var total_row = $('.qty').length - 1;
  for(row_inx = 0; row_inx < total_row; row_inx++){
    qty = parseInt($('.qty').eq(row_inx).val());
    price_per_item = parseFloat($('.price_per_item').eq(row_inx).val());
    total_price = qty * price_per_item;
    total_qty += qty;
    total_item_price += total_price;
  }
  var grand_total = total_item_price - discount_price - discount_by_point_amount + shipping_price;
  $('#total_item').val(total_qty);
  $('#item_price').val(total_item_price);
  $('#grand_total').val(grand_total);
  on_change_member();
}
function on_change_member(){
  var member = $('#member_id').find(':selected');
  var member_type = member.data('member_type');
  var member_point = member.data('member_point');
  var member_mobile = member.data('member_mobile');
  var $member_type = $('#member_type_'+member_type);
  var min_amount = $member_type.data('min_amount');
  var discount = $member_type.data('discount');
  var item_price = $('#item_price').val();
  var discount_by_point_amount = parseFloat($('#discount_by_point_amount').val());
  var discount_rate = 0;
  if(item_price >= min_amount){
    discount_rate = discount;
  }
  var discount_amount = (item_price - discount_by_point_amount) * discount_rate / 100;
  $('#calc_discount_amount').html(discount_amount);
  $('#member_type_detail').html('<label class="col-md-3 control-label">ประเภทสมาชิก</label><div class="col-md-6"><p class="form-control-static">'+member_type+'</p></div>');
  $('#member_point_detail').html('<label class="col-md-3 control-label">แต้มสะสม</label><div class="col-md-6"><p class="form-control-static">'+member_point+' แต้ม</p></div>');
  $('#member_mobile_detail').html('<label class="col-md-3 control-label">เบอร์มือถือ</label><div class="col-md-6"><p class="form-control-static">'+member_mobile+'</p></div>');
}
function update_select2(){
  $('#order_item_list .product_id').select2();
}
function check_hideen_member(){
  var order_id = $('#hid_order_id').val();
  if(order_id > 0){
    return;
  }
  var member_id = $('#hid_member_id').val();
  if(member_id == 0){
    return;
  }
  $('#member_id').val(member_id)
  .trigger('change');
}