$(document).ready(function(){
  $('#btn-plus').on('click', appendRow);
  $('.btn-remove-reward').on('click', removeRow);
  $('.qty').on('change paste keyup', on_change_qty);
  $('.reward_point').on('change paste keyup', on_change_reward_point);
  appendRow();
  $('.reward_id').on('change', on_change_reward);
  $("#transaction-form").validate({
    highlight: function(label) {
      $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(label) {
      $(label).closest('.form-group').removeClass('has-error');
      label.remove();
    },
    errorPlacement: function( error, element ) {
      var placement = element.closest('.input-group');
      if (!placement.get(0)) {
        placement = element;
      }
      if (error.text() !== '') {
        placement.after(error);
      }
    }
  });

});
function appendRow(){
  $('#reward_item_list').append($('#table_reward_item').html());
  $('.btn-remove-reward').off('click').on('click', removeRow);
  $('.reward_id').off('click').on('change', on_change_reward);
  $('.qty').off('change paste keyup').on('change paste keyup', on_change_qty);
  $('.reward_point').off('change paste keyup').on('change paste keyup', on_change_reward_point);
};

function removeRow(){
  var clickElement = $(this);
  var removeIndex = $('.btn-remove-reward').index(clickElement);
  $('.row_item').eq(removeIndex).remove();
}
function on_change_reward(){
  var change_element = $(this);
  var change_index = $('.reward_id').index(change_element);
  var reward_point = $('.reward_id').eq(change_index).find(':selected').data('point');
  $('.reward_point').eq(change_index).val(reward_point);
  update_point_row(change_index);
}
function on_change_qty(){
  var change_index = $('.qty').index($(this));
  update_point_row(change_index);
}
function on_change_reward_point(){
  var change_index = $('.reward_point').index($(this));
  update_point_row(change_index);
}
function update_point_row(row_inx){
  var qty = parseInt($('.qty').eq(row_inx).val());
  var reward_point = parseFloat($('.reward_point').eq(row_inx).val());
  var total_point = qty * reward_point;
  $('.total_reward_point').eq(row_inx).html(total_point);
  update_total_point();
}
function update_total_point(){
  /*
  var total_qty = 0;
  var total_item_point = 0;
  var discount_point = parseFloat($('#discount_point').val());
  var shipping_point = parseFloat($('#shipping_point').val());
  var total_row = $('.qty').length - 1;
  for(row_inx = 0; row_inx < total_row; row_inx++){
    qty = parseInt($('.qty').eq(row_inx).val());
    reward_point = parseFloat($('.reward_point').eq(row_inx).val());
    total_point = qty * reward_point;
    total_qty += qty;
    total_item_point += total_point;
  }
  var grand_total = total_item_point - discount_point + shipping_point;
  $('#total_item').val(total_qty);
  $('#item_point').val(total_item_point);
  $('#grand_total').val(grand_total);
  */
}
