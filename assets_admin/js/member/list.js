$(document).ready(function() {
  $("#searchForm").submit(function(event) {
    event.preventDefault();
    var action = $(this).attr("action");
    onSearchSubmit(action);
  });
  $("#searchForm").submit();
});
function onSearchSubmit(action) {
  $("#result").empty().append('Loading ...');
  var searchVal = $('#txtSearchVal').val();
  var member_type_code = $('#member_type_code').val();
  var enableStatus = $('#ddlEnableStatus').val();
  var sort = $('#ddlSort').val();
  getContent(action, searchVal, member_type_code, enableStatus, sort, 1, 20);
}
function getContent(action, searchVal, member_type_code, enableStatus, sort, page, perPage){
  $("#result").empty().append('Loading ...');
  var posting = $.post(action, {
    searchVal: searchVal,
    member_type_code: member_type_code,
    enableStatus: enableStatus,
    sort: sort,
    page: page,
    perPage: perPage
  });
  posting.done(function(data) {
    $("#result").empty().append(data);
    setOnChangePerPage();
    setOnChangePage();
  });
  posting.fail(function(data){
    setTimeout(function() { getContent(action, searchVal, member_type_code, enableStatus, sort, page, perPage); }, 5000);
  });
}
function setOnChangePerPage(){
  $('.ddlPerPage').change(function(){
    var perPage = $(this).val();
    var action = $(this).data('action');
    var searchVal = $(this).data('searchval');
    var member_type_code = $(this).data('member_type_code');
    var enableStatus = $(this).data('enablestatus');
    var sort = $(this).data('sort');
    getContent(action, searchVal, member_type_code, enableStatus, sort, 1, perPage);
  });
}
function setOnChangePage(){
  $('.ddlPager').change(function(){
    var page = $(this).val();
    var action = $(this).data('action');
    var searchVal = $(this).data('searchval');
    var member_type_code = $(this).data('member_type_code');
    var enableStatus = $(this).data('enablestatus');
    var sort = $(this).data('sort');
    var perPage = $(this).data('perpage');
    getContent(action, searchVal, member_type_code, enableStatus, sort, page, perPage);
  });
}
