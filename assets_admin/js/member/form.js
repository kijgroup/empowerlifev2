$(document).ready(function() {
  $('.form-control-date').datepicker({
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    todayBtn: true
  });
  validationForm();
  $("#member-form").validate({
    highlight: function(label) {
      $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(label) {
      $(label).closest('.form-group').removeClass('has-error');
      label.remove();
    },
    errorPlacement: function( error, element ) {
      var placement = element.closest('.input-group');
      if (!placement.get(0)) {
        placement = element;
      }
      if (error.text() !== '') {
        placement.after(error);
      }
    }
  });
  $('#has_user').on('change', on_change_user);
  on_change_user();
});
function validationForm(){
  $('#member-form').validate({
    rules: {
      txtLoginName: {
        required: true,
        minlength: 4
      },
      txtPassword: {
        required: true,
        minlength: 6
      },
      txtConfirmPassword: {
        required: true,
        minlength: 6,
        equalTo: "#txtPassword"
      },
      txtEmail: {
        email: true
      },
    },
    onfocusout: injectTrim($.validator.defaults.onfocusout),
  });
}
function injectTrim(handler) {
  return function (element, event) {
    if (element.tagName === "TEXTAREA" || (element.tagName === "INPUT" && element.type !== "password")) {
      element.value = $.trim(element.value);
    }
    return handler.call(this, element, event);
  };
}
function on_change_user(){
  var has_user = $('#has_user').val();
  if(has_user == 'true'){
    $('.user_form').show();
  }else{
    $('.user_form').hide();
  }
}
