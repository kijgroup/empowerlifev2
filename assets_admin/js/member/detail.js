$(document).ready(function(){
  init_input();
});
function init_input(){
  setTimeout(function() { $('.form_edit').hide(); }, 300);
  $('.btn-edit-field').on('click', function(event){
    event.preventDefault();
    var fieldname = $(this).data('fieldname');
    $('#detail_'+fieldname).hide();
    $('#form_'+fieldname).show();
  });
  $('.btn-cancel-form').on('click', function(event){
    event.preventDefault();
    var fieldname = $(this).data('fieldname');
    $('#detail_'+fieldname).show();
    $('#form_'+fieldname).hide();
  });
}
