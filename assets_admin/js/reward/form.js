$(document).ready(function(){
  setupTextarea();
  $("#reward-form").validate({
    highlight: function(label) {
      $(label).closest('.form-group').removeClass('has-success').addClass('has-error');
    },
    success: function(label) {
      $(label).closest('.form-group').removeClass('has-error');
      label.remove();
    },
    errorPlacement: function( error, element ) {
      var placement = element.closest('.input-group');
      if (!placement.get(0)) {
        placement = element;
      }
      if (error.text() !== '') {
        placement.after(error);
      }
    }
  });
});
function setupTextarea(){
  $('.summernote').summernote({
    height: 400,
    toolbar: [
      ['style', ['style']],
      ['font', ['bold', 'underline', 'clear']],
      ['fontname', ['fontname']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['height', ['height']],
      ['table', ['table']],
      ['insert', ['link', 'picture', 'video']],
      ['view', ['fullscreen', 'codeview', 'help']]
    ],
    minHeight: null,
    maxHeight: null,
    fontSizes: ['8', '9', '10', '11', '12','13', '14','15','16','17', '18','20','22', '24','30', '36', '48' , '64'],
    callbacks: {
      onImageUpload: function(files) {
        url = $(this).data('upload');
        sendFile(files[0], url, $(this));
      }
    }
  });
}
function sendFile(file, url, editor) {
  data = new FormData();
  data.append("file", file);
  $.ajax({
    data: data,
    type: "POST",
    url:url,
    cache: false,
    contentType: false,
    processData: false,
    success: function(url) {
      editor.summernote('insertImage', url);
    }
  });
}
