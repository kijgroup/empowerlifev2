$(document).ready(function(){
    addEventListener();
    setupTextarea();
    setCoverType();
});
function addEventListener(){
    $('#ddlCoverType').on('change', setCoverType);
    $('#txtCourseStartDate').datepicker({format: 'yyyy-mm-dd'});
    $('#txtCourseStartTime').timepicker({showMeridian: false});
    $('#txtCourseEndDate').datepicker({format: 'yyyy-mm-dd'});
    $('#txtCourseEndTime').timepicker({showMeridian: false});
}
function setCoverType(){
	var coverType = $('#ddlCoverType').val();
    if(coverType == 'image'){
        $('#coverImageBox').slideDown();
        $('#coverVideoBox').slideUp();
    }else{
        $('#coverImageBox').slideUp();
        $('#coverVideoBox').slideDown();
    }
}
function setupTextarea(){
    $('.summernote').summernote({
        height: 300,
        minHeight: null,
        maxHeight: null,
        focus: true ,
        callbacks: {
            onImageUpload: function(files) {
            	console.log('call on Image Load')
                url = $(this).data('upload');
                sendFile(files[0], url, $(this));
            }
        }
    });
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var preview = $(input).attr('data-target');
            $(preview).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function sendFile(file, url, editor) {
    data = new FormData();
    data.append("file", file);
    $.ajax({
        data: data,
        type: "POST",
        url:url,
        cache: false,
        contentType: false,
        processData: false,
        success: function(url) {
            console.log('upload url :'+url);
            editor.summernote('insertImage', url);
            console.log('upload complete');
        }
    });
}
